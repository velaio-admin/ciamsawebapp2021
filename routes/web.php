<?php

use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use App\User;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

/*Route::get('/rutas_jaos', function () {

    $user = User::find(Auth::id());
    $user->assignRole('Admin');

    $user = User::find(Auth::id());

    dd($user->getRoleNames(),$user->getAllPermissions());

});*/

/* Recover Password Via Email */
Route::name('recoverPassword')->get('/recover_password', 'RecoverController@recoverPassword');
Route::name('getPasswordEmail')->post('/getPasswordEmail', 'RecoverController@getPasswordEmail');
Route::name('recoveringAccess')->get('/recovering_access/{token}/{id}/execute', 'RecoverController@recoveringAccess');
Route::name('savePasswordEmail')->post('/savePasswordEmail', 'RecoverController@savePasswordEmail');

Route::get('/edit_user', 'HomeController@editUserShow')->name('editUser.get');
Route::post('/edit_user', 'HomeController@editUserActualizar')->name('editUser.post');
Route::get('/register', 'HomeController@register')->name('register');
Route::post('/register', 'HomeController@create')->name('register.create');

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/detail_ind/data/{date_init}/{date_end}/{cliente_id}', 'HomeController@detail_ind');

Route::get('/merge_pdf_view', 'HomeController@mergePdfView')->name('merge_pdf');
Route::post('/merge_pdf', 'HomeController@mergePdf')->name('merge_pdf_ctrl');

Route::name('changePassword')->get('/change_password/{id}/execute', 'HomeController@changePassword');
Route::name('updatePassword')->put('/updatePassword/{user}', 'HomeController@updatePassword');

Route::get('Servicios_logisticos/estados', 'Servicios_logisticos\ServiciosLogisticosController@gestionEstados');

Route::get('Servicios_logisticos/estadoCarga', 'Servicios_logisticos\ServiciosLogisticosController@estadoCarga');

Route::get('Servicios_logisticos/anuncioCarga', 'URL_USCIIWS61Servicios_logisticos\ServiciosLogisticosController@anuncioCarga');

Route::get('detalle-bl/get/{id_bl}/{cliente_id}', 'Servicios_logisticos\ServiciosLogisticosController@getDetalleBl')->middleware('validateClient');
Route::get('containers/get/{id_contenedor}/{cliente_id}', 'Servicios_logisticos\ServiciosLogisticosController@getDetalleContenedor')->middleware('validateClient');

Route::get('rastreo-de-carga/{estado}/{cliente_id}/{pagina}/{cant_pag}', 'Servicios_logisticos\ServiciosLogisticosController@rastreoList')->middleware('validateClient');
Route::post('rastreo-de-carga/buscador', 'Servicios_logisticos\ServiciosLogisticosController@rastreoListBuscador');

Route::get('reservar-turno/{cliente_id}/{pagina}/{cant_pag}', 'Servicios_logisticos\ServiciosLogisticosController@reservarTurnoVehiculo')->middleware('validateClient');
Route::post('reservar-turno/buscador', 'Servicios_logisticos\ServiciosLogisticosController@rastreoListBuscador');

Route::get('despacho-de-mercancia/{cant_reg}', 'Servicios_logisticos\ServiciosLogisticosController@despachoMercancia');
Route::post('despacho-de-mercancia', 'Servicios_logisticos\ServiciosLogisticosController@despachoMercanciaBuscador');

Route::get('devolucion-de-contenedor/{cant_reg}', 'Servicios_logisticos\ServiciosLogisticosController@fechaLimiteDevolucionContenedor');
Route::post('devolucion-de-contenedor', 'Servicios_logisticos\ServiciosLogisticosController@fechaLimiteDevolucionContenedorBuscador');

Route::get('existencia-de-mercancia/{cant_reg}', 'Servicios_logisticos\ServiciosLogisticosController@existenciaMercancia');
Route::post('existencia-de-mercancia', 'Servicios_logisticos\ServiciosLogisticosController@existenciaMercanciaBuscador');

Route::get('cargarDocumentos', 'Servicios_logisticos\ServiciosLogisticosController@uploadDocumentsView');
Route::post('subirDocumentos', 'Servicios_logisticos\ServiciosLogisticosController@uploadDocuments');

Route::get('comercializadora/listado/contenedores/{id_booking}', 'Comercializadora\ComercializadoraController@containersList');

// Agricola
Route::view('agricola/cupo-de-credito', 'agricola/cupoCredito/cupoCredito');
Route::get('agricola/rastreo-de-pedido', 'Agricola\AgricolaController@consultaPedido');
Route::post('agricola/rastreo-de-pedido/buscador', 'Agricola\AgricolaController@consultaPedidoBuscador');
Route::get('agricola/rastreo-de-pedido/{id_pedido}', 'Agricola\AgricolaController@detallePedido');
Route::view('agricola/facturas', 'agricola/facturas/facturas');
Route::get('agricola/solicitar-pedido', 'Agricola\AgricolaController@solicitarPedido');
Route::get('agricola/solicitar-pedido/{param}', 'Agricola\AgricolaController@tipoProductos');
Route::get('agricola/estados', 'Agricola\AgricolaController@gestionEstados');

Route::get('agricola/reservar-turno/{cliente_id}/{pagina}/{cant_pag}', 'Agricola\AgricolaController@reservarTurnoAgricola');

// Comecializadora
Route::get('comercializadora/proceso-logistico/{cant_reg}', 'Comercializadora\ComercializadoraController@procesoLogistico')->middleware('validateClient');
Route::post('comercializadora/proceso-logistico/buscador', 'Comercializadora\ComercializadoraController@procesoLogisticoBuscador');
Route::get('comercializadora/proceso-logistico/{type}/{numero}/{cliente_id}', 'Comercializadora\ComercializadoraController@procesoLogisticoDetalle')->middleware('validateClient');
Route::get('comercializadora/estados-de-contenedores/contenedores-quedados/{cant_reg}', 'Comercializadora\ComercializadoraController@contenedoresQuedados')->middleware('validateClient');
Route::post('comercializadora/estados-de-contenedores/contenedores-quedados/buscador', 'Comercializadora\ComercializadoraController@contenedoresQuedadosBuscador');
Route::get('comercializadora/estados-de-contenedores/contenedores-reprogramados/{cant_reg}', 'Comercializadora\ComercializadoraController@contenedoresReprogramados')->middleware('validateClient');
Route::post('comercializadora/estados-de-contenedores/contenedores-reprogramados/buscador', 'Comercializadora\ComercializadoraController@contenedoresReprogramadosBuscador');
Route::get('comercializadora/nominaciones/{cant_reg}', 'Comercializadora\ComercializadoraController@nominaciones')->middleware('validateClient');
Route::get('comercializadora/nominaciones/detalle/nominacion/{numero}/{cliente_id}', 'Comercializadora\ComercializadoraController@detalleNominacion')->middleware('validateClient');
Route::get('comercializadora/nominaciones/detalle-de-entrega/{cliente_id}/{id_entrega}', 'Comercializadora\ComercializadoraController@detalleEntrega')->middleware('validateClient');
Route::post('comercializadora/nominaciones/buscador', 'Comercializadora\ComercializadoraController@nominacionesBuscador');

Route::group(['middleware' => ['web','auth']], function (){

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('docs', function(){
                return View::make('docs.api.v1.index');
            });
    
 }
);

/*
* Agricola
*/
//Consultar cupo de credito
Route::get('cupo-credito/{estado}/{cliente_id}/{pagina}/{cant_pag}', 'Agricola\AgricolaController@cupoCreditoView')->middleware('validateClient');
Route::post('cupo-credito/buscador', 'Agricola\AgricolaController@cupoCreditoViewBuscador');
Route::get('descargar-factura/get/{numero_factura}', 'Agricola\AgricolaController@descargarFactura');

//Facturas
Route::get('facturas/{cliente_id}/{pagina}/{cant_pag}', 'Agricola\AgricolaController@historialFacturas')->middleware('validateClient');
Route::post('facturas/buscador', 'Agricola\AgricolaController@historialFacturasBuscador');

/*
* Role Management
*/
Route::get('role', 'RoleController@index');
Route::get('role/edit/{user_id}', 'RoleController@roleEdit');
Route::get('role/inactive/{user_id}', 'RoleController@userInactive');
Route::get('role/active/{user_id}', 'RoleController@userActive');
Route::get('role/delete/{user_id}', 'RoleController@userDelete');
Route::post('role/assign/permission', 'RoleController@assignPermission')->name('asignar_permisos');
Route::post('role/assign/role', 'RoleController@assignRole')->name('asignar_roles');
Route::post('role/info/user', 'RoleController@userEdit')->name('editar_informacion');

// Test
Route::get('test', 'Servicios_logisticos\ServiciosLogisticosController@test');

/*Route::get('/registerTest', function () {
    \DB::table('users')
        ->insert([
            'name' => 'admin',
            'email' => 'prueba@soho.com',
            'avatar' => 'img/avatars/ciamsa.png',
            'password' => Hash::make('ciamsa.user'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ]);
});*/

Route::get('/roles', function () {
    dd(Auth::user()->identificacion);
});

/*Route::get('/permisos', function () {

});*/
