<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1','middleware' => ['json.response']], function () {
	
	Route::post('/bl/cont/observaciones','Api\Servicios_logisticos\ServiciosLogisticosController@observations');
	Route::get('/products/list/{type}','Agricola\AgricolaController@tipoProductos');
	Route::post('/products/radicar/pedido','Agricola\AgricolaController@generarPedido');
	Route::post('/products/notificar/pedido','Agricola\AgricolaController@notificarPedido');

	//States Services Fk
	Route::get('Servicios_logisticos/getBlContenedor/{client_id}', 'Api\Servicios_logisticos\ServiciosLogisticosController@getBlContenedor');
	Route::get('Servicios_logisticos/getClientsData', 'Api\Servicios_logisticos\ServiciosLogisticosController@getClientsData');
	Route::post('Servicios_logisticos/getDetailCont', 'Api\Servicios_logisticos\ServiciosLogisticosController@getDetailCont');
	Route::post('Servicios_logisticos/getBlContStates', 'Api\Servicios_logisticos\ServiciosLogisticosController@getBlContStates');
	Route::post('Servicios_logisticos/updateBlCont', 'Api\Servicios_logisticos\ServiciosLogisticosController@updateBlCont');
	Route::post('Servicios_logisticos/getBlAvalibleTran', 'Api\Servicios_logisticos\ServiciosLogisticosController@getBlAvalibleTran');
	Route::post('Servicios_logisticos/getBlAvaliblePro', 'Api\Servicios_logisticos\ServiciosLogisticosController@getBlAvaliblePro');

	//Semaforos
	Route::get('Servicios_logisticos/get/config/{id_user}', 'Api\Servicios_logisticos\ServiciosLogisticosController@getConfig');
	Route::get('Servicios_logisticos/get/configSaved/{id_user}', 'Api\Servicios_logisticos\ServiciosLogisticosController@getConfigSaved');
	Route::post('Servicios_logisticos/save/update/config', 'Api\Servicios_logisticos\ServiciosLogisticosController@saveUpdateConfig');
	Route::get('Servicios_logisticos/delete/update/config/{key}/{id_user}', 'Api\Servicios_logisticos\ServiciosLogisticosController@deleteConfig');
	Route::post('Servicios_logisticos/generate/list/trafficLight', 'Api\Servicios_logisticos\ServiciosLogisticosController@semaforos');
	Route::post('Servicios_logisticos/generate/restore/trafficLight', 'Api\Servicios_logisticos\ServiciosLogisticosController@restore_trafficLight');
	Route::get('Servicios_logisticos/generate/list/get/drag/drop/{id_user}', 'Api\Servicios_logisticos\ServiciosLogisticosController@getColumns');
	Route::post('Servicios_logisticos/save/update/drag/drop', 'Api\Servicios_logisticos\ServiciosLogisticosController@updateColumns');
	
	//Indicadores
	Route::post('Servicios_logisticos/post/indicador/cumplimiento/historico', 'Api\Servicios_logisticos\ServiciosLogisticosController@indicadorCumplimientoHistorico');
	Route::post('Servicios_logisticos/post/indicador/cumplimiento/acumulado', 'Api\Servicios_logisticos\ServiciosLogisticosController@indicadorCumplimientoAcumuladoMensual');
	Route::post('Servicios_logisticos/post/indicador/principales/causales', 'Api\Servicios_logisticos\ServiciosLogisticosController@principalesCausalesIncumplimiento');
	Route::post('Servicios_logisticos/post/indicador/cumplimiento/detailsIndIncumplimiento', 'Api\Servicios_logisticos\ServiciosLogisticosController@detailsIndIncumplimiento');

	//Generate Short Url
	Route::get('Servicios_logisticos/generate/short/url/{url_base64}', 'Api\Servicios_logisticos\ServiciosLogisticosController@shortUrl');
	Route::get('Servicios_logisticos/get/test', 'Api\Servicios_logisticos\ServiciosLogisticosController@testIndicadores');

	//Download url
	Route::get('Servicios_logisticos/dowload/file/{url_base64}', 'Api\Servicios_logisticos\ServiciosLogisticosController@downloadFileCiamsa');

	//Getting base64 to pdf detalle bl url
	Route::post('Servicios_logisticos/detalle_bl/base64', 'Api\Servicios_logisticos\ServiciosLogisticosController@getBase64');

	//Get pdf link new end point
	Route::post('comercializadora/servicio/obtenerpdf','Api\Comercializadora\ComercializadoraController@obtenerPdfEntrega');
	Route::post('comercializadora/servicio/infolotes','Api\Comercializadora\ComercializadoraController@infolotesEntrega');

	//Anuncio De Carga
	Route::post('Servicios_logisticos/anuncio_carga/datos_form', 'Api\Servicios_logisticos\AnuncioCargaController@get_data_form');
	Route::post('Servicios_logisticos/anuncio_carga/save_form', 'Api\Servicios_logisticos\AnuncioCargaController@save_data_form');

	//Reservación de turno CIAMSA2
	Route::post('Servicios_logisticos/info_reserva/', 'Api\Servicios_logisticos\ServiciosLogisticosController@infoReserva');
	Route::post('Servicios_logisticos/nueva_reserva/', 'Api\Servicios_logisticos\ServiciosLogisticosController@nuevaReserva');
	//Consultar nombre del conductor
	Route::post('Servicios_logisticos/nom_conductor/', 'Api\Servicios_logisticos\ServiciosLogisticosController@nomConductor');
	
	//Reservación de turno Buga
	Route::post('agricola/info_reserva/', 'Api\Agricola\AgricolaController@infoReserva');
	Route::post('agricola/nueva_reserva/', 'Api\Agricola\AgricolaController@nuevaReserva');

	//Apis App Movil Autenticacion
	Route::group(['prefix' => 'auth'], function () {
	    Route::post('login', 'Api\Auth\AuthController@login');
	    Route::post('signup', 'Api\Auth\AuthController@signup');
	});
	
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'Api\Auth\AuthController@logout');
        Route::get('user', 'Api\Auth\AuthController@user');

        //Apis App Movil Agricola
        Route::get('agricola/states/facturas/', 'Api\Agricola\AgricolaController@getTypeInvoiceStates');
        Route::get('agricola/cupo-credito/', 'Api\Agricola\AgricolaController@cupoCredito');
		Route::get('agricola/descargar-factura/{numero_factura}', 'Api\Agricola\AgricolaController@descargarFactura');
		Route::post('agricola/historial/facturas', 'Api\Agricola\AgricolaController@historialFacturas');
		Route::post('agricola/consulta/pedido', 'Api\Agricola\AgricolaController@consultaPedido');
		Route::get('agricola/consulta/detalle-pedido/{id_pedido}', 'Api\Agricola\AgricolaController@detallePedido');
		Route::get('agricola/consulta/productos', 'Api\Agricola\AgricolaController@productos');
		Route::post('agricola/consulta/formasDePago', 'Api\Agricola\AgricolaController@formasDePago');
		Route::get('agricola/consulta/ciudades', 'Api\Agricola\AgricolaController@ciudades');
		Route::post('agricola/radicar/pedido', 'Api\Agricola\AgricolaController@radicarPedido');
		Route::post('agricola/notificar/pedido', 'Api\Agricola\AgricolaController@notificarPedido');


		//Apis App Movil Comercializadora
		Route::post('comercializadora/nominaciones','Api\Comercializadora\ComercializadoraController@nominaciones');
		Route::post('comercializadora/nominacion/detalle','Api\Comercializadora\ComercializadoraController@detalleNominacion');
		Route::post('comercializadora/entrega/detalle','Api\Comercializadora\ComercializadoraController@detalleEntrega');
		Route::post('comercializadora/proceso/logistico','Api\Comercializadora\ComercializadoraController@procesoLogistico');
		Route::post('comercializadora/proceso/logistico/detalle','Api\Comercializadora\ComercializadoraController@procesoLogisticoDetalle');
		Route::post('comercializadora/contenedores/quedados','Api\Comercializadora\ComercializadoraController@contenedoresQuedados');
		Route::post('comercializadora/contenedores/reprogramados','Api\Comercializadora\ComercializadoraController@contenedoresReprogramados');
		
		//Apis App Movil Servicios Logisticos
		Route::post('/serviciosLogisticos/getDetalleBl/','Api\Servicios_logisticos\ServiciosLogisticosController@getDetalleBl');
		Route::post('/serviciosLogisticos/getDetalleContenedor/','Api\Servicios_logisticos\ServiciosLogisticosController@getDetalleContenedor');
		Route::post('/serviciosLogisticos/rastreoList/','Api\Servicios_logisticos\ServiciosLogisticosController@rastreoList');
		Route::post('/serviciosLogisticos/despachoMercancia/','Api\Servicios_logisticos\ServiciosLogisticosController@despachoMercancia');
		Route::post('/serviciosLogisticos/fechaLimiteDevolucionContenedor/','Api\Servicios_logisticos\ServiciosLogisticosController@fechaLimiteDevolucionContenedor');
		Route::post('/serviciosLogisticos/existenciaMercancia/','Api\Servicios_logisticos\ServiciosLogisticosController@existenciaMercancia');
    });
});
