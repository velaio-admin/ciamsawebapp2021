<?php

//use Illuminate\Support\Facades\Auth;

return [

    'tables' => [
        'links' => [
                    'Bls' => env('APP_URL').'/containers/get/%var%/%user%',
                    'Bl' => env('APP_URL').'/detalle-bl/get/%var%/%user%',
                    'FactD' => env('APP_URL').'/descargar-factura/get/%var%',
                    'NomL' => env('APP_URL').'/comercializadora/nominaciones/detalle/nominacion/%var%/%user%',
                    'EntG' => env('APP_URL').'/comercializadora/nominaciones/detalle-de-entrega/%user%/%var%',
                    'ConsD' => env('APP_URL').'/agricola/rastreo-de-pedido/%var%',
                    ],
    ],

];