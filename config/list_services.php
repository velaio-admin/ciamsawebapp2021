<?php

return [

    'headers' => [
        'existencia_mercancia' => '{ 
                                        "title":"Detalle del despacho de la mercancia",
                                        "titlevalue":"",
                                        "columns":[
                                            {
                                                "order":0,
                                                "label":"ID Bl",
                                                "variable":"IdBl",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":1,
                                                "label":"DO",
                                                "variable":"Do",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":2,
                                                "label":"Fecha de ingreso a deposito",
                                                "variable":"FechaIngresoDeposito",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":3,
                                                "label":"Dias de permanencia M/cia",
                                                "variable":"DiasPermanencia",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":4,
                                                "label":"ID Ciamsa",
                                                "variable":"Lote",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":5,
                                                "label":"Producto",
                                                "variable":"Producto",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":6,
                                                "label":"Cantidad de contenedores recibidos",
                                                "variable":"CantidadContenedores",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":7,
                                                "label":"Cantidad de contenedores Full",
                                                "variable":"CantidadContenedoresFull",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":8,
                                                "label":"Unidad de medida de inventario",
                                                "variable":"UM",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":9,
                                                "label":"Cantidad Recibida",
                                                "variable":"Recibido",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":10,
                                                "label":"Cantidad despacho",
                                                "variable":"Despachado",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":11,
                                                "label":"Averias",
                                                "variable":"Averias",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":12,
                                                "label":"Saldo",
                                                "variable":"Saldo",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":13,
                                                "label":"Tipo operacion",
                                                "variable":"TipoOperacion",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":14,
                                                "label":"Observaciones",
                                                "variable":"Recibido",
                                                "font_size":"0.7rem",
                                                "function":""
                                            }
                                            
                                        ]
                                    }',
        'nominaciones' => '{ 
            "title":"Resumen de nominaciones",
            "titlevalue":"",
            "columns":[
                {
                    "order":0,
                    "label":"Radicado",
                    "variable":"Radicado"
                },
                {
                    "order":1,
                    "label":"Booking",
                    "variable":"Booking"
                },
                {
                    "order":2,
                    "label":"Cliente",
                    "variable":"NombreCliente"
                },
                {
                    "order":3,
                    "label":"Motonave",
                    "variable":"Motonave"
                },
                {
                    "order":4,
                    "label":"Fecha Cierre",
                    "variable":"Cut_off"
                },
                {
                    "order":5,
                    "label":"ETA",
                    "variable":"Eta"
                },
                {
                    "order":6,
                    "label":"Contrato",
                    "variable":"Contrato"
                },
                {
                    "order":7,
                    "label":"Tipo de venta",
                    "variable":"TipoDeVenta"
                }
            ]
        }',
        'detalle_nominacion_1' => '{ 
            "title":"",
            "titlevalue":"",
            "columns":[
                {
                    "order":0,
                    "label":"Radicado",
                    "variable":"Radicado"
                },
                {
                    "order":1,
                    "label":"Booking",
                    "variable":"Booking"
                },
                {
                    "order":2,
                    "label":"Motonave",
                    "variable":"Motonave"
                },
                {
                    "order":3,
                    "label":"Viaje",
                    "variable":"Viaje"
                },
                {
                    "order":4,
                    "label":"Puerto de origen",
                    "variable":"PuertoDeOrigen"
                },
                {
                    "order":5,
                    "label":"Puerto destino",
                    "variable":"PuertoDestino"
                },
                {
                    "order":6,
                    "label":"ETA",
                    "variable":"Eta"
                },
                {
                    "order":7,
                    "label":"Cierre",
                    "variable":"Cierre"
                },
                {
                    "order":8,
                    "label":"Linea Maritima",
                    "variable":"Linea"
                },
                {
                    "order":9,
                    "label":"Toneladas",
                    "variable":"Toneladas"
                },
                {
                    "order":10,
                    "label":"Tipo de venta",
                    "variable":"TipoDeVenta"
                },
                {
                    "order":11,
                    "label":"Contrato",
                    "variable":"Contrato"
                }
            ]
        }',
        'detalle_nominacion_2' => '{ 
            "title":"",
            "titlevalue":"",
            "columns":[
                {
                    "order":0,
                    "label":"Entrega",
                    "variable":"entrega"
                },
                {
                    "order":1,
                    "label":"Producto",
                    "variable":"producto"
                },
                {
                    "order":2,
                    "label":"Cantidad nominada",
                    "variable":"CantidadNominada"
                },
                {
                    "order":3,
                    "label":"Cantidad embalada",
                    "variable":"CantidadEmbalada"
                },
                {
                    "order":4,
                    "label":"Cantidad en inventario",
                    "variable":"CantidadEnInventario"
                },
                {
                    "order":5,
                    "label":"Saldo por embalar",
                    "variable":"SaldoPorEmbalar"
                }
            ]
        }', 
        'entrega' => '{ 
            "title":"",
            "titlevalue":"",
            "columns":[
                {
                    "order":0,
                    "label":"Contenedor",
                    "variable":"contenedor"
                },
                {
                    "order":1,
                    "label":"Sellos",
                    "variable":"sellos"
                },
                {
                    "order":2,
                    "label":"Tipo de contenedor",
                    "variable":"tipoDeContenedor"
                },
                {
                    "order":3,
                    "label":"Marca",
                    "variable":"marca"
                },
                {
                    "order":4,
                    "label":"ID Booking",
                    "variable":"idBooking"
                },
                {
                    "order":5,
                    "label":"Material (es)",
                    "variable":"productos"
                },
                {
                    "order":6,
                    "label":"Cantidad (es) Embalada",
                    "variable":"cantidades"
                },
                {
                    "order":7,
                    "label":"Empaque(s)",
                    "variable":"empaques"
                },
                {
                    "order":8,
                    "label":"Estado del contenedor",
                    "variable":"EstadoDelContenedor"
                }
            ]
        }',
        'consultaDespacho' => '{ 
            "title":"Carga",
            "titlevalue":"",
            "columns":[
                {
                    "order":0,
                    "label":"Factura",
                    "variable":"factura"
                },
                {
                    "order":1,
                    "label":"Pedido",
                    "variable":"pedido"
                },
                {
                    "order":2,
                    "label":"Fecha solicitud",
                    "variable":"fecha_creacion"
                },
                {
                    "order":3,
                    "label":"Cantidad",
                    "variable":"cantidad"
                },
                {
                    "order":4,
                    "label":"Entregado",
                    "variable":"entregado"
                },
                {
                    "order":5,
                    "label":"Saldo",
                    "variable":"saldo"
                },
                {
                    "order":6,
                    "label":"Estado",
                    "variable":"estado"
                },
                {
                    "order":7,
                    "label":"Producto",
                    "variable":"producto"
                },
                {
                    "order":8,
                    "label":"Unidad de Medida",
                    "variable":"unidad_medida"
                }
            ]
        }',         
        'service_despacho_mercancia_table_1' => '{ 
                                                    "title":"Despachos",
                                                    "titlevalue":"",
                                                    "columns":[
                                                    {
                                                        "order":0,
                                                        "label":"ID BL",
                                                        "variable":"IdBl",
                                                        "font_size":"",
                                                        "function":""
                                                    },
                                                    {
                                                        "order":1,
                                                        "label":"DO/Pedido",
                                                        "variable":"DO",
                                                        "font_size":"",
                                                        "function":""
                                                    },
                                                    {
                                                        "order":2,
                                                        "label":"Fecha de traslado",
                                                        "variable":"FechaTraslado",
                                                        "font_size":"",
                                                        "function":""
                                                    },
                                                    {
                                                        "order":3,
                                                        "label":"Producto",
                                                        "variable":"Producto",
                                                        "font_size":"",
                                                        "function":""
                                                    },
                                                    {
                                                        "order":4,
                                                        "label":"ID Ciamsa",
                                                        "variable":"Lote",
                                                        "font_size":"",
                                                        "function":""
                                                    },
                                                    {
                                                        "order":5,
                                                        "label":"Cantidad total recibida",
                                                        "variable":"CantRecibida",
                                                        "font_size":"",
                                                        "function":""
                                                    },
                                                    {
                                                        "order":6,
                                                        "label":"Cantidad total despachada",
                                                        "variable":"CantDespachada",
                                                        "font_size":"",
                                                        "function":""
                                                    },
                                                    {
                                                        "order":7,
                                                        "label":"Saldo",
                                                        "variable":"Desembalaje",
                                                        "font_size":"",
                                                        "function":""
                                                    },
                                                    {
                                                        "order":8,
                                                        "label":"Unidad de medida",
                                                        "variable":"UM",
                                                        "font_size":"",
                                                        "function":""
                                                    }
                                                    ]
                                                }',
        'service_despacho_mercancia_table_2' => '{ 
                                                    "title":"Detalle del despacho de la mercancia",
                                                    "titlevalue":"",
                                                    "columns":[
                                                        {
                                                            "order":0,
                                                            "label":"Fecha Retiro",
                                                            "variable":"FechaRetiro",
                                                            "font_size":"",
                                                            "function":""
                                                        },
                                                        {
                                                            "order":1,
                                                            "label":"Placa",
                                                            "variable":"Placa",
                                                            "font_size":"",
                                                            "function":""
                                                        },
                                                        {
                                                            "order":2,
                                                            "label":"Destino",
                                                            "variable":"Destino",
                                                            "font_size":"",
                                                            "function":""
                                                        },
                                                        {
                                                            "order":3,
                                                            "label":"Peso bruto",
                                                            "variable":"PesoBruto",
                                                            "font_size":"",
                                                            "function":""
                                                        },
                                                        {
                                                            "order":4,
                                                            "label":"Peso tara",
                                                            "variable":"PesoTara",
                                                            "font_size":"",
                                                            "function":""
                                                        },
                                                        {
                                                            "order":5,
                                                            "label":"Peso neto",
                                                            "variable":"PesoNeto",
                                                            "font_size":"",
                                                            "function":""
                                                        },
                                                        {
                                                            "order":6,
                                                            "label":"Cantidad total despachada",
                                                            "variable":"Cantidad",
                                                            "font_size":"",
                                                            "function":""
                                                        },
                                                        {
                                                            "order":7,
                                                            "label":"Saldo Ciamsa",
                                                            "variable":"SaldoBL",
                                                            "font_size":"",
                                                            "function":""
                                                        }
                                                    ]
                                                }',
        'service_detalle_bl_table_1' => '{ 
                                            "title":"Estado del BL",
                                            "titlevalue":"Contenedores trasladados",
                                            "columns":[
                                            {
                                                "order":0,
                                                "label":"ID BL",
                                                "variable":"IdBl",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":1,
                                                "label":"DO",
                                                "variable":"Do",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":2,
                                                "label":"Último estado del BL",
                                                "variable":"UltimoEstadoBl",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":3,
                                                "label":"Cantidad de Contenedores",
                                                "variable":"CantidadContenedores",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":4,
                                                "label":"ETA",
                                                "variable":"Eta",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":5,
                                                "label":"Fecha de Ingreso",
                                                "variable":"FechaIngreso",
                                                "font_size":"",
                                                "function":""
                                            },                                            
                                            {
                                                "order":6,
                                                "label":"Naviera",
                                                "variable":"Naviera",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":7,
                                                "label":"Producto",
                                                "variable":"Producto",
                                                "font_size":"",
                                                "function":""
                                            }
                                            ]
                                        }',
        'service_detalle_bl_table_2' => '{ 
                                            "title":"Contenedores",
                                            "titlevalue":"",
                                            "columns":[
                                            {
                                                "order":0,
                                                "label":"ID contenedor",
                                                "variable":"IdContenedor",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":1,
                                                "label":"Fecha traslado",
                                                "variable":"FechaTraslado",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":2,
                                                "label":"Fecha desembalaje",
                                                "variable":"FechaDesembalaje",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":3,
                                                "label":"Fecha devolución vacío",
                                                "variable":"FechaDevolucionVacio",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":4,
                                                "label":"Fecha Límite devolución",
                                                "variable":"FechaLimiteDevolucion",
                                                "font_size":"",
                                                "function":""
                                            }
                                            ]
                                        }',
        'service_detalle_bl_table_3' => '{ 
                                            "title":"Actividad reciente del BL",
                                            "titlevalue":"",
                                            "columns":[
                                            {
                                                "order":0,
                                                "label":"Fecha último movimiento",
                                                "variable":"FechaUltimoMovimiento",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":1,
                                                "label":"Estado contenedor",
                                                "variable":"EstadoContenedor",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":2,
                                                "label":"Observaciones",
                                                "variable":"Observaciones",
                                                "font_size":"",
                                                "function":""
                                            }
                                            ]
                                        }',
        'service_detalle_cont_table_1' => '{ 
                                            "title":"Estado del Contenedor",
                                            "titlevalue":"Contenedores trasladados",
                                            "columns":[
                                            {
                                                "order":0,
                                                "label":"ID BL",
                                                "variable":"IdBl",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":1,
                                                "label":"Último estado del Contenedor",
                                                "variable":"UltimEstadoContenedor",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":2,
                                                "label":"DO",
                                                "variable":"Do",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":3,
                                                "label":"Fecha de Ingreso",
                                                "variable":"FechaIngreso",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":4,
                                                "label":"Motonave",
                                                "variable":"Motonave",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":5,
                                                "label":"Producto",
                                                "variable":"Producto",
                                                "font_size":"",
                                                "function":""
                                            }
                                            ]
                                        }',
        'service_detalle_cont_table_2' => '{ 
                                            "title":"Contenedores",
                                            "titlevalue":"",
                                            "columns":[
                                            {
                                                "order":0,
                                                "label":"ID contenedor",
                                                "variable":"IdContenedor",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":1,
                                                "label":"Fecha traslado",
                                                "variable":"FechaTraslado",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":2,
                                                "label":"Fecha desembalaje",
                                                "variable":"FechaDesembalaje",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":3,
                                                "label":"Fecha devolución vacío",
                                                "variable":"FechaDevolucionVacio",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":4,
                                                "label":"Fecha Límite devolución",
                                                "variable":"FechaLimiteDevolucion",
                                                "font_size":"",
                                                "function":""
                                            }
                                            ]
                                        }',
        'service_detalle_cont_table_3' => '{ 
                                            "title":"Actividad reciente del BL",
                                            "titlevalue":"",
                                            "columns":[
                                            {
                                                "order":0,
                                                "label":"Fecha último movimiento",
                                                "variable":"FechaUltimoMovimiento",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":1,
                                                "label":"Estado contenedor",
                                                "variable":"EstadoContenedor",
                                                "font_size":"",
                                                "function":""
                                            },
                                            {
                                                "order":2,
                                                "label":"Observaciones",
                                                "variable":"Observaciones",
                                                "font_size":"",
                                                "function":""
                                            }
                                            ]
                                        }',
        'service_fecha_limite_devolucion_contenedor' => '{ 
                                                            "title":"Detalle",
                                                            "titlevalue":"",
                                                            "columns":[
                                                            {
                                                                "order":0,
                                                                "label":"ID BL",
                                                                "variable":"Bl",
                                                                "font_size":"",
                                                                "function":""
                                                            },
                                                            {
                                                                "order":1,
                                                                "label":"DO/Pedido",
                                                                "variable":"DO",
                                                                "font_size":"",
                                                                "function":""
                                                            },
                                                            {
                                                                "order":2,
                                                                "label":"ID del contenedor",
                                                                "variable":"idContenedor",
                                                                "font_size":"",
                                                                "function":""
                                                            },
                                                            {
                                                                "order":3,
                                                                "label":"Fecha limite de devolucion",
                                                                "variable":"FechaLimiteDevolucion",
                                                                "font_size":"",
                                                                "function":""
                                                            },
                                                            {
                                                                "order":4,
                                                                "label":"Fecha de devolucion",
                                                                "variable":"FechaDevolucion",
                                                                "font_size":"",
                                                                "function":""
                                                            }
                                                            ]
                                                            }',
        'services_rastreo_list' => '{ 
                                        "title":"Carga",
                                        "titlevalue":"",
                                        "columns":[
                                        {
                                          "order":0,
                                          "label":"BL",
                                          "variable":"id",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":1,
                                          "label":"DO/Pedido",
                                          "variable":"Do",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":2,
                                          "label":"Estado",
                                          "variable":"Estado",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":3,
                                          "label":"Fecha de arribo o ETA",
                                          "variable":"Eta",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":4,
                                          "label":"Motonave",
                                          "variable":"Motonave",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":5,
                                          "label":"Producto",
                                          "variable":"Producto",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":6,
                                          "label":"Tipo de operacion",
                                          "variable":"TipoOperacion",
                                          "font_size":"",
                                          "function":""
                                        }
                                        ]
                                        }',
        'services_facturas_cupo_credito' => '{ 
                                        "title":"Facturas por pagar",
                                        "titlevalue":"",
                                        "columns":[
                                        {
                                          "order":0,
                                          "label":"Fecha de pago",
                                          "variable":"fechaDePago",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":1,
                                          "label":"Factura #",
                                          "variable":"factura",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":2,
                                          "label":"Días vencidos",
                                          "variable":"diasVencidos",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":3,
                                          "label":"Estado",
                                          "variable":"estado",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":4,
                                          "label":"Valor a pagar",
                                          "variable":"valorAPagar",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":5,
                                          "label":"Factura",
                                          "variable":"facturaElectronica",
                                          "font_size":"",
                                          "function":""
                                        }
                                        ]
                                        }',
        'services_historial_facturas' => '{ 
                                        "title":"Historico de facturas",
                                        "titlevalue":"",
                                        "columns":[
                                        {
                                          "order":0,
                                          "label":"Fecha de creación",
                                          "variable":"fechaDeCreacion",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":1,
                                          "label":"Factura #",
                                          "variable":"factura",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":2,
                                          "label":"Valor de pagar",
                                          "variable":"valorAPagar",
                                          "font_size":"",
                                          "function":""
                                        },
                                        {
                                          "order":3,
                                          "label":"Factura PDF",
                                          "variable":"facturaElectronica",
                                          "font_size":"",
                                          "function":""
                                        }
                                        ]
                                        }',
    ],

    'services' => [
        'existencia_mercancia' => '{
                                      "meta": {
                                        "totalPages":13,
                                        "currentPage":1,
                                        "code": 1,
                                        "message": " "
                                      },
                                      "data": {
                                        "type": "existenciaMercancia",
                                        "attributes": [
                                          {
                                            "0": "IdBl",
                                            "1": "Do",
                                            "2": "FechaIngresoDeposito",
                                            "3": "DiasPermanencia",
                                            "4": "Lote",
                                            "5": "Producto",
                                            "6": "CantidadContenedores",
                                            "7": "UM",
                                            "8": "Recibido",
                                            "9": "Despachado",
                                            "10": "Averias",
                                            "11": "Saldo",
                                            "12": "Observaciones",
                                            "data": [
                                              {
                                                "0": 565656,
                                                "1": 1212,
                                                "2": "10/10/2018 08:20",
                                                "3": 20,
                                                "4": 5656,
                                                "5": "Arroz",
                                                "6": 25,
                                                "7": "UN",
                                                "8": 2050,
                                                "9": 1055,
                                                "10": 10,
                                                "11": 1000,
                                                "12": "En bodega esperando"
                                              },
                                              {
                                                "0": 565669,
                                                "1": 2030,
                                                "2": "10/10/2018 08:20",
                                                "3": 20,
                                                "4": 5656,
                                                "5": "Arroz",
                                                "6": 25,
                                                "7": "UN",
                                                "8": 2050,
                                                "9": 1050,
                                                "10": 0,
                                                "11": 1000,
                                                "12": "En bodega esperando ensaque"
                                              },
                                              {
                                                "0": 565652465434556,
                                                "1": 1214534,
                                                "2": "10/10/2018 08:20",
                                                "3": 20,
                                                "4": 5656,
                                                "5": "Arroz",
                                                "6": 25,
                                                "7": "UN",
                                                "8": 2050,
                                                "9": 1055,
                                                "10": 10,
                                                "11": 1000,
                                                "12": "En bodega esperando"
                                              }
                                            ]
                                          }
                                        ]
                                      }
                                    }',
        'service_despacho_mercancia' => '{
                                          "meta": {
                                            "totalPages": 13,
                                            "currentPage": 1,
                                            "code": 1,
                                            "message": " "
                                          },
                                          "data": {
                                            "type": "despachoMercancia",
                                            "attributes": [
                                              {
                                                "0": "IdBl",
                                                "1": "DO",
                                                "2": "FechaTraslado",
                                                "3": "Producto",
                                                "4": "Lote",
                                                "5": "CantidadBL",
                                                "6": "Desembalaje",
                                                "data": [
                                                  {
                                                    "0": "SUDUB8603A2P2L6K",
                                                    "1": 2413,
                                                    "2": "03/12/2016",
                                                    "3": "ARVEJA VERDE SACOSX50KG LA PERLA",
                                                    "4": 8021003,
                                                    "5": 6760,
                                                    "6": 6762
                                                  }
                                                ]
                                              },
                                              {
                                                "0": "FechaRetiro",
                                                "1": "Placa",
                                                "2": "Destino",
                                                "3": "PesoBruto",
                                                "4": "PesoTara",
                                                "5": "PesoNeto",
                                                "6": "Cantidad",
                                                "7": "SaldoBL",
                                                "8": "SaldoKgBL",
                                                "data": [
                                                  {
                                                    "0": "10/10/2018 20:20",
                                                    "1": "GUM109",
                                                    "2": "Medellin",
                                                    "3": 35.5,
                                                    "4": 33.5,
                                                    "5": 35,
                                                    "6": 700,
                                                    "7": 537,
                                                    "8": 269
                                                  },
                                                  {
                                                    "0": "10/10/2018 20:20",
                                                    "1": "GUM110",
                                                    "2": "Bogota",
                                                    "3": 34.5,
                                                    "4": 32.5,
                                                    "5": 35,
                                                    "6": 600,
                                                    "7": 537,
                                                    "8": 269
                                                  },
                                                  {
                                                    "0": "10/10/2018 20:20",
                                                    "1": "GUM109",
                                                    "2": "Medellin",
                                                    "3": 35.5,
                                                    "4": 33.5,
                                                    "5": 35,
                                                    "6": 700,
                                                    "7": 537,
                                                    "8": 269
                                                  },
                                                  {
                                                    "0": "10/10/2018 20:20",
                                                    "1": "GUM110",
                                                    "2": "Bogota",
                                                    "3": 34.5,
                                                    "4": 32.5,
                                                    "5": 35,
                                                    "6": 600,
                                                    "7": 537,
                                                    "8": 269
                                                  },
                                                  {
                                                    "0": "10/10/2018 20:20",
                                                    "1": "GUM109",
                                                    "2": "Medellin",
                                                    "3": 35.5,
                                                    "4": 33.5,
                                                    "5": 35,
                                                    "6": 700,
                                                    "7": 537,
                                                    "8": 269
                                                  },
                                                  {
                                                    "0": "10/10/2018 20:20",
                                                    "1": "GUM110",
                                                    "2": "Bogota",
                                                    "3": 34.5,
                                                    "4": 32.5,
                                                    "5": 35,
                                                    "6": 600,
                                                    "7": 537,
                                                    "8": 269
                                                  },
                                                  {
                                                    "0": "10/10/2018 20:20",
                                                    "1": "GUM109",
                                                    "2": "Medellin",
                                                    "3": 35.5,
                                                    "4": 33.5,
                                                    "5": 35,
                                                    "6": 700,
                                                    "7": 537,
                                                    "8": 269
                                                  },
                                                  {
                                                    "0": "10/10/2018 20:20",
                                                    "1": "GUM110",
                                                    "2": "Bogota",
                                                    "3": 34.5,
                                                    "4": 32.5,
                                                    "5": 35,
                                                    "6": 600,
                                                    "7": 537,
                                                    "8": 269
                                                  },
                                                  {
                                                    "0": "10/10/2018 20:20",
                                                    "1": "GUM109",
                                                    "2": "Medellin",
                                                    "3": 35.5,
                                                    "4": 33.5,
                                                    "5": 35,
                                                    "6": 700,
                                                    "7": 537,
                                                    "8": 269
                                                  },
                                                  {
                                                    "0": "10/10/2018 20:20",
                                                    "1": "GUM110",
                                                    "2": "Bogota",
                                                    "3": 34.5,
                                                    "4": 32.5,
                                                    "5": 35,
                                                    "6": 600,
                                                    "7": 537,
                                                    "8": 269
                                                  }
                                                ]
                                              }
                                            ]
                                          }
                                        }',
        'service_detalle_bl' => '{  
                                   "meta":{  
                                      "totalPages":13,
                                      "code":1,
                                      "message":" "
                                   },
                                   "data":  
                                      {  
                                         "type":"container",
                                         "attributes":[  
                                            {  
                                               "0":"IdBl",
                                               "1":"UltimEstadoContenedor",
                                               "2":"Do",
                                               "3":"FechaIngreso",
                                               "4":"Naviera",
                                               "5":"Producto",
                                               "data":[  
                                                  {  
                                                     "0":"SUDUB8603A2P2L6K",
                                                     "1":"Entrega del contenedor",
                                                     "2":22015,
                                                     "3":"10/10/2018 20:20",
                                                     "4":"HAMBURG SUD",
                                                     "5":"Arroz"
                                                  }
                                               ]
                                            },
                                            {  
                                               "0":"IdContenedor",
                                               "1":"FechaTraslado",
                                               "2":"FechaDesembalaje",
                                               "3":"FechaDevolucionVacio",
                                               "4":"FechaLimiteDevolucion",
                                               "data":[  
                                                  {  
                                                     "0":"SUDUB8603A2P2L6K",
                                                     "1":"10/10/2018 20:20",
                                                     "2":"10/10/2018 23:20",
                                                     "3":"11/10/2018 20:20",
                                                     "4":"13/10/2018 20:20"
                                                  }
                                               ]
                                            }
                                         ],
                                         "relationships":{  
                                            "trackingContainer":{  
                                               "0":"FechaUltimoMovimiento",
                                               "1":"EstadoContenedor",
                                               "2":"Observaciones",
                                               "data":[  
                                                  {  
                                                     "0":"10/10/2018 20:20",
                                                     "1":"Pendiente retiro contenedor por parte de CIAMSA",
                                                     "2":""
                                                  },
                                                  {  
                                                     "0":"11/10/2018 20:20",
                                                     "1":"Entrega del contenedor vacío",
                                                     "2":"Se devuelve el contenedor con un rasguño"
                                                  }
                                               ]
                                            }
                                         }
                                      }
                                }',
        'service_fecha_limite_devolucion_contenedor' => '{
                                                          "meta": {
                                                            "totalPages": 13,
                                                            "currentPage": 1,
                                                            "code": 1,
                                                            "message": " "
                                                          },
                                                          "data": {
                                                            "type": "devolucionContenedor",
                                                            "attributes": [
                                                              {
                                                                "0": "Bl",
                                                                "1": "DO",
                                                                "2": "idContenedor",
                                                                "3": "FechaLimiteDevolucion",
                                                                "4": "FechaDevolucion",
                                                                "data": [
                                                                  {
                                                                    "0": "HLCUBC1180526660",
                                                                    "1": 2355,
                                                                    "2": "HLCUHOU171130145",
                                                                    "3": "10/10/2018",
                                                                    "4": "10/10/2018"
                                                                  },
                                                                  {
                                                                    "0": "HLCUBC1180528860",
                                                                    "1": 2351,
                                                                    "2": "HLCUHOU171130100",
                                                                    "3": "10/07/2018",
                                                                    "4": "10/07/2018"
                                                                  },
                                                                  {
                                                                    "0": "HLCUBC1180526660",
                                                                    "1": 2355,
                                                                    "2": "HLCUHOU171130145",
                                                                    "3": "10/10/2018",
                                                                    "4": "10/10/2018"
                                                                  },
                                                                  {
                                                                    "0": "HLCUBC1180528860",
                                                                    "1": 2351,
                                                                    "2": "HLCUHOU171130100",
                                                                    "3": "10/07/2018",
                                                                    "4": "10/07/2018"
                                                                  },
                                                                  {
                                                                    "0": "HLCUBC1180526660",
                                                                    "1": 2355,
                                                                    "2": "HLCUHOU171130145",
                                                                    "3": "10/10/2018",
                                                                    "4": "10/10/2018"
                                                                  },
                                                                  {
                                                                    "0": "HLCUBC1180528860",
                                                                    "1": 2351,
                                                                    "2": "HLCUHOU171130100",
                                                                    "3": "10/07/2018",
                                                                    "4": "10/07/2018"
                                                                  }
                                                                ]
                                                              }
                                                            ]
                                                          }
                                                        }',
        'services_rastreo_list' => '{  
                                       "meta":{  
                                          "totalPages":13,
                                          "currentPage":1,
                                          "code":1,
                                          "message":" "
                                       },
                                       "data":
                                          {  
                                             "type":"Bls",
                                             "attributes":[  
                                                {  
                                                   "0":"id",
                                                   "1":"Do",
                                                   "2":"Estado",
                                                   "3":"Eta",
                                                   "4":"Motonave",
                                                   "data":[  
                                                      {  
                                                         "0":"MSCUZY968977",
                                                         "1":29010,
                                                         "2":"BL en Tránsito",
                                                         "3":"10/10/2018",
                                                         "4":"MAPOCHO"
                                                      },
                                                      {  
                                                         "0":"MSCUZY968978",
                                                         "1":29011,
                                                         "2":"BL en Tránsito",
                                                         "3":"10/10/2018",
                                                         "4":"MINERVA"
                                                      },
                                                      {  
                                                         "0":"MSCUZY968978",
                                                         "1":29012,
                                                         "2":"BL en Tránsito",
                                                         "3":"10/10/2018",
                                                         "4":"JPO LIBRA"
                                                      },
                                                      {  
                                                         "0":"MSCUZY968977",
                                                         "1":29010,
                                                         "2":"BL en Tránsito",
                                                         "3":"10/10/2018",
                                                         "4":"MAPOCHO"
                                                      }
                                                   ]
                                                }
                                             ]
                                          }
                                    }',
    ],

    'trafficlight' => [
        'estandar' => '{
    "0": {
        "name": "Mes",
        "variable": "Mes",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "31",
        "tooltip_msj": "Mes de la columna <Fecha Descargue Motonave>"
    },
    "1": {
        "name": "Nombre Cliente",
        "variable": "Nombre_Cliente",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "9",
        "tooltip_msj": ""
    },
    "2": {
        "name": "DO",
        "variable": "DO",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "7",
        "tooltip_msj": ""
    },
    "3": {
        "name": "Numero BL",
        "variable": "Numero_BL",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "0",
        "tooltip_msj": ""
    },
    "4": {
        "name": "Contenedor",
        "variable": "Contenedor",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "8",
        "tooltip_msj": ""
    },
    "5": {
        "name": "Tipo de contenedor",
        "variable": "Tipo_de_contenedor",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "23",
        "tooltip_msj": ""
    },
    "6": {
        "name": "Tipo de operación",
        "variable": "Tipo_Operacion",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "25",
        "tooltip_msj": ""
    },
    "7": {
        "name": "Estado",
        "variable": "Estado",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "1",
        "tooltip_msj": ""
    },
    "8": {
        "name": "Observaciones",
        "variable": "Observaciones",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "2",
        "tooltip_msj": ""
    },
    "9": {
        "name": "Linea naviera",
        "variable": "Linea_naviera",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "10",
        "tooltip_msj": ""
    },
    "10": {
        "name": "Motonave",
        "variable": "Motonave",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "11",
        "tooltip_msj": ""
    },
    "11": {
        "name": "Numero Viaje",
        "variable": "Numero_Viaje",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "12",
        "tooltip_msj": ""
    },
    "12": {
        "name": "Fecha Descargue Motonave",
        "variable": "Fecha_Descargue_Motonave",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "13",
        "tooltip_msj": ""
    },
    "13": {
        "name": "ETA",
        "variable": "ETA",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "14",
        "tooltip_msj": ""
    },
    "14": {
        "name": "Puerto de Descargue",
        "variable": "Puerto_de_Descargue",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "15",
        "tooltip_msj": ""
    },
    "15": {
        "name": "Fecha Firma Planilla de envio",
        "variable": "Fecha_Firma_Planilla_de_envio",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "16",
        "tooltip_msj": ""
    },
    "16": {
        "name": "Fecha Recibo Doc De Traslado",
        "variable": "Fecha_Recibo_Doc_De_Traslado",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "17",
        "tooltip_msj": ""
    },
    "17": {
        "name": "Hora Recibo Doc",
        "variable": "Hora_Recibo_Doc",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "18",
        "tooltip_msj": ""
    },
    "18": {
        "name": "Fecha optima traslado",
        "variable": "Fecha_Optima_Traslado",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "4",
        "tooltip_msj": ""
    },
    "19": {
        "name": "Fecha Traslado a Deposito",
        "variable": "Fecha_Traslado_a_Deposito",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "19",
        "tooltip_msj": ""
    },
    "20": {
        "name": "Fecha Ingreso a Muisca",
        "variable": "Fecha_Ingreso_a_Muisca",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "20",
        "tooltip_msj": ""
    },
    "21": {
        "name": "Fecha desembalaje",
        "variable": "Fecha_Desembalaje",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "5",
        "tooltip_msj": ""
    },
    "22": {
        "name": "Dias libres Contenedor con Naviera",
        "variable": "Dias_Libres",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "3",
        "tooltip_msj": ""
    },
    "23": {
        "name": "Fecha limite devolucion",
        "variable": "Fecha_Limite_Devolucion",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "6",
        "tooltip_msj": ""
    },
    "24": {
        "name": "Fecha Devolucion Cont",
        "variable": "Fecha_Devolución_Cont",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "21",
        "tooltip_msj": ""
    },
    "25": {
        "name": "Tiempo Global",
        "variable": "Tiempo_Global",
        "state": true,
        "color": "#FFF",
        "calculate": true,
        "config_color": [],
        "string_validate": false,
        "order": "24",
        "tooltip_msj": "<Fecha Descargue Motonave> MENOS <Fecha Traslado a Deposito>"
    },
    "26": {
        "name": "Tiempo Deposito",
        "variable": "Tiempo_Deposito",
        "state": true,
        "color": "#FFF",
        "calculate": true,
        "config_color": [{
            "operator": "<",
            "value": "3",
            "color": "#008000"
        }, {
            "operator": ">",
            "value": "3",
            "color": "#FFFF00"
        }, {
            "operator": ">",
            "value": "4",
            "color": "#FF0000"
        }],
        "string_validate": false,
        "order": "25",
        "tooltip_msj": "<Fecha Recibo Doc De Traslado> MENOS <Fecha Traslado a Deposito>"
    },
    "27": {
        "name": "Causal Incumplimiento",
        "variable": "Causal_Incumplimiento",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "22",
        "tooltip_msj": ""
    },
    "28": {
        "name": "Rango Global",
        "variable": "Rango_Global",
        "state": true,
        "color": "#FFF",
        "calculate": true,
        "config_color": [],
        "string_validate": true,
        "order": "26",
        "tooltip_msj": "Rango de días de la columna <Tiempo Global>"
    },
    "29": {
        "name": "Rango Deposito",
        "variable": "Rango_Deposito",
        "state": true,
        "color": "#FFF",
        "calculate": true,
        "config_color": [],
        "string_validate": true,
        "order": "27",
        "tooltip_msj": "Rango de días de la columna <Tiempo Deposito>"
    },
    "30": {
        "name": "Firma Planilla",
        "variable": "Firma_Planilla",
        "state": true,
        "color": "#FFF",
        "calculate": true,
        "config_color": [],
        "string_validate": false,
        "order": "28",
        "tooltip_msj": "<ETA> MENOS <Fecha Firma Planilla de envio>"
    },
    "31": {
        "name": "Tiempo entrega de planilla",
        "variable": "Tiempo_entrega_de_planilla",
        "state": true,
        "color": "#FFF",
        "calculate": true,
        "config_color": [{
            "operator": "<=",
            "value": "1",
            "color": "#008000"
        }, {
            "operator": ">",
            "value": "3",
            "color": "#FFFF00"
        }, {
            "operator": ">",
            "value": "4",
            "color": "#FF0000"
        }],
        "string_validate": false,
        "order": "34",
        "tooltip_msj": "<Fecha Descargue Motonave> MENOS <Fecha Recibo Doc De Traslado>"
    },
    "32": {
        "name": "Días de traslado a deposito",
        "variable": "Dias_trasl_depo",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "24",
        "tooltip_msj": ""
    },
    "33": {
        "name": "Dias MUISCA",
        "variable": "Dias_MUISCA",
        "state": true,
        "color": "#FFF",
        "calculate": true,
        "config_color": [],
        "string_validate": false,
        "order": "36",
        "tooltip_msj": "<Fecha Traslado a Deposito> MENOS <Fecha Ingreso a Muisca>"
    },
    "34": {
        "name": "Dias en puerto",
        "variable": "Dias_en_puerto",
        "state": true,
        "color": "#FFF",
        "calculate": true,
        "config_color": [],
        "string_validate": false,
        "order": "37",
        "tooltip_msj": "<Fecha Descargue Motonave> MENOS <Fecha Traslado a Deposito>"
    },
    "35": {
        "name": "Cumplimiento Compromiso Traslado Estado",
        "variable": "Cump_Comp_Tras_Est",
        "state": true,
        "color": "#FFF",
        "calculate": false,
        "config_color": [],
        "string_validate": false,
        "order": "39",
        "tooltip_msj": "<Fecha Traslado Deposito> MENOS <Fecha Maxima Traslado>"
    }
    
}'
    ],

];