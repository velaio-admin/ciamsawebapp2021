var colors = [
	{
		hex: '#FFFF00',
	  	name: 'Amarillo'
	},
	{
		hex: '#008000',
	  	name: 'Verde'
	},
	{
		hex: '#FF0000',
	  	name: 'Rojo'
	}
];

var rank_options_global = [
	{
	  	name: 'Verificar'
	},
	{
	  	name: 'Entre 0-3 días'
	},
	{
	  	name: 'Entre 4-5 días'
	},
	{
	  	name: '6 días'
	},
	{
	  	name: 'Entre 7-8 días'
	},
	{
	  	name: 'Más de 8 días'
	}
];

var rank_options_deposito = [
	{
	  	name: 'Verificar'
	},
	{
	  	name: 'Entre 0-2 días'
	},
	{
	  	name: '3 días'
	},
	{
	  	name: '4 días'
	},
	{
	  	name: 'Más de 4 días'
	}
];

var rank_options_docs = [
	{
	  	name: 'A TIEMPO'
	},
	{
	  	name: 'PROXIMO'
	},
	{
	  	name: 'URGENTE'
	}
];

const app = new Vue({
		  el: '#semaforo',
		  components: {
          	'popper': VuePopper
          },
		  data: {
		  	colors: colors,
		  	rank_options_global: rank_options_global,
		  	rank_options_deposito: rank_options_deposito,
		  	rank_options_docs: rank_options_docs,
		  	options_data_validate: true,
		  	selects_especials: false,
		  	array_dinamic_sel_especials: [],
		  	active_col_pick: false,
		  	active_col_pick2: false,
		  	active_col_pick3: false,
			selectedColor: '#008000',
			selectedColorName: 'Verde',
			selectedColor2: '#FFFF00',
			selectedColorName2: 'Amarillo',
			selectedColor3: '#FF0000',
			selectedColorName3: 'Rojo',
		    items: [],
		    items_calc: [],
		    history: [],
		    item_filter: [],
		    sem_conf_data: [],
		    sem_conf_data_to_edit: [],
		    action_config: false,
		    sem_conf_data_result: [],
		    result_table: [],
		    result_table_vals: [],
		    evt_data: '',  
		    var_load: '', 
		    section_1: false, 
		    section_2: false, 
		    add_sem: false, 
		    section_table: true, 
		    go_sem_selected: [], 
		    item_var: '', 
		    item_var_edit: '', 
		    name_var_edit: '', 
		    name_field_edit: '', 
		    condition: '', 
		    condition_2: '', 
		    condition_3: '', 
		    number_semaforo: '',
		    number_semaforo_2: '',
		    number_semaforo_3: '',
		    id_user: '',
		    identification: '',
		    variable: '',
		    btn_export_data: false,
		    cant_res_table: 0,
		    fechainicial: '',
		    fechafinal: '',
		    state_search: true,
		    definition: '',
		    help_text: 'Número de días',
		    exp_excel_btn: true,
			NUM_RESULTS: 50, // Numero de resultados por página
            pag: 1 // Página inicial
		  },
		  mounted () {

		  	this.id_user = document.getElementById("id_user").value;
		  	this.identification = document.getElementById("identification").value;

		  	/* Get Drag And Drop Configurations Columns*/
		  	this.getConfigCols(this.id_user);
		  	/* Get Form Configurations Settings*/
		  	this.getConfigSetting(this.id_user);
		  	/* Get Form Configurations Tables*/
		  	this.getConfigTables(this.id_user, this.identification, this.fechainicial, this.fechafinal);
		  	
		  },
		  computed: {
			selector: function() {
				if(!this.selectedColor) {
						return 'Seleccione color';
				}
				else {
					return '<span style="background: ' + this.selectedColor + '"></span> ' + this.selectedColorName;
				}
			},
			selector2: function() {
				if(!this.selectedColor2) {
						return 'Seleccione color';
				}
				else {
					return '<span style="background: ' + this.selectedColor2 + '"></span> ' + this.selectedColorName2;
				}
			},
			selector3: function() {
				if(!this.selectedColor3) {
						return 'Seleccione color';
				}
				else {
					return '<span style="background: ' + this.selectedColor3 + '"></span> ' + this.selectedColorName3;
				}
			}
		  },
		  watch:{
		  	'selectedColor'  : function () {

	  			if (this.selectedColor === this.selectedColor2 || this.selectedColor === this.selectedColor3) {
	  				alert('Debe seleccionar un color diferente a los ya elegidos');
	  				this.selectedColor = '';
	  				return false;
	  			}

	  			if (this.item_var === '') {
		    		alert('Error!! Seleccione columna');
		    		this.selectedColor = '';
		    		return false;
		    	}else if (this.condition === '') {
		    		alert('Error!! Seleccione regla');
		    		this.selectedColor = '';
		    		return false;
		    	}else if (this.number_semaforo === '') {
		    		alert('Error!! Ingrese días');	
		    		this.selectedColor = '';		    
		    		return false;
		    	}

		  	},
		  	'selectedColor2'  : function () {

		  		if (this.selectedColor2 === this.selectedColor || this.selectedColor2 === this.selectedColor3) {
	  				alert('Debe seleccionar un color diferente a los ya elegidos');
	  				this.selectedColor2 = ''
	  				return false;
	  			}	
		  		
		  	},
		  	'selectedColor3'  : function () {

		  		if (this.selectedColor3 === this.selectedColor || this.selectedColor3 === this.selectedColor2) {
	  				alert('Debe seleccionar un color diferente a los ya elegidos');
	  				this.selectedColor3 = '';
	  				return false;
	  			}
		  		
		  	}
		  },
		  methods: {
		    afterAdd(evt) {

		      console.log(evt);
		      this.evt_data = evt;

		      const element = evt.moved.element
		      const oldIndex = evt.moved.oldIndex
		      const newIndex = evt.moved.newIndex
		      this.history.push(`${element.name} is moved from position ${oldIndex} to ${newIndex}`);

		      console.log(this.history);

		    },
		    check_event(){

		    	console.log('res ', this.evt_data);
		    	if (this.evt_data !== '') {
		    		this.afterAdd(this.evt_data);
		    	}
		    	
		    },
		    states_confs(){

        		try {
	        		axios.get('/api/v1/Servicios_logisticos/get/configSaved/'+this.id_user, {
	            })
	            .then(response => {
	            	console.log('configSaved',response);
	                //if (response.data.success === true) {
	                this.sem_conf_data_result = response.data.data;
	                /*}else{
	                    alert(response.data.message);
	                }*/
	            })
		        }catch(error) {
		          alert(error+ '- Fallo en traer el listado de configuraciones.');
		        }

		    },
		    getConfigCols(id_user){		    	
			    try {
	        		axios.get('/api/v1/Servicios_logisticos/generate/list/get/drag/drop/'+id_user, {
	            })
	            .then(response => {
	                if (response.data.success === true) {
	                	this.items = response.data.data;
	                	console.log('response 1 ',this.items);
	                }else{
	                    alert(response.data.message);
	                }
	            })
		        }catch(error) {
		          alert(error+ '- Fallo en traer el listado de columnas.');
		        }
		    },
		    getConfigSetting(id_user){		    	
			    try {
	        		axios.get('/api/v1/Servicios_logisticos/get/config/'+id_user, {
	            })
	            .then(response => {
	            	console.log('res conf ',response);
	                if (response.data.success === true) {
	                	this.sem_conf_data = response.data.data;
	                	console.log('response 2 ',this.sem_conf_data);
	                }else{
	                    alert(response.data.message);
	                }
	            })
		        }catch(error) {
		          alert(error+ '- Fallo en traer el listado de configuraciones.');
		        }
		        /*setTimeout(()=>{
				   this.states_confs()
				},2000);*/		        

		    },
		    getConfigTables(id_user, identification, fechainicial, fechafinal){		    			    	
			    try {
			    	this.var_load = 'loading';
	        		axios.post('/api/v1/Servicios_logisticos/generate/list/trafficLight', {
	        			id_user: id_user,
	        			identification: identification,
	        			fechainicial: fechainicial,
	        			fechafinal: fechafinal
	            })
	            .then(response => {
	            	console.log('res conf table', response);
	                if (response.data.success === true) {
	                	this.result_table = response.data.data;
	                	console.log('this.result_table ', response.data.data);
	                	//Vuelvo objeto a array
	                	this.result_table = Object.values(this.result_table);
	                	//Decido un alcance para los resultados a mostrar
	                	var tab_fin_res = this.result_table.length - 1;
	                	this.result_table = this.result_table.slice(0, tab_fin_res);

	                	this.result_table_vals = response.data.data.data;
	                	console.log('this.result_table_vals ',this.result_table_vals);
	                	this.cant_res_table = this.result_table_vals.length;
	                	this.btn_export_data = true;
	                	setTimeout(()=>{
						   this.var_load = '';
						},3000);
	                }else{

	                    alert(response.data.message);
	                    this.var_load = '';
	                    this.btn_export_data = false;
	                }
	            })
		        }catch(error) {
		          alert(error+ '- Fallo en traer el listado de configuraciones.');
		          this.var_load = '';
		          this.btn_export_data = false;
		        }
		        setTimeout(()=>{
				   this.var_load = '';
				},10000);	        

		    },
		    go_sect_2(){
		    	this.section_1     = false;
		    	this.section_2 	   = true;
		    	this.section_table = false;
		    	this.action_config = false;
		    	this.exp_excel_btn = false;

		    	this.states_confs();
		    	//this.getConfigSetting(this.id_user);
		    },
		    save_drag_drop(){

		    	this.item_filter = this.items.filter(state => state.state === true );

		    	if (this.item_filter.length == 0) {
		    		alert("Error!! Debe seleccionar al menos una columna a mostrar.");
		    		return false;
		    	}

		    	this.items_calc = JSON.stringify(Object.assign({}, this.items)); 

		    	try {	        		
        		axios.post('/api/v1/Servicios_logisticos/save/update/drag/drop', {
		            colums: this.items_calc,
		            id_user: this.id_user
		        })
	            .then(response => {
	            	console.log('response 2 ',response);
	                if (response.data.success === true) {

	                	alert(response.data.message);

	                	this.section_1 = false;
		    			this.section_table = true;
		    			this.exp_excel_btn = true;
		    			this.getConfigTables(this.id_user, this.identification, this.fechainicial, this.fechafinal);

	                }else{
	                    alert(response.data.message);
	                }
	            })
		        }catch(error) {
		          alert(error+ '- Fallo en traer el listado de clientes.');
		        }	    	

		    },
		    cancel_save_drag_drop(){
				this.section_1 = false;
		    	this.section_2 = false;
		    	this.section_table = true;	  
		    	this.exp_excel_btn = true;
		    	this.getConfigTables(this.id_user, this.identification, this.fechainicial, this.fechafinal);  	
		    },
		    restore_drag_drop(){
				/*this.section_1 = false;
		    	this.section_2 = false;
		    	this.section_table = true;	  
		    	this.exp_excel_btn = true;
		    	this.getConfigTables(this.id_user, this.identification, this.fechainicial, this.fechafinal);*/
		    	try {
			    	this.var_load = 'loading';
	        		axios.post('/api/v1/Servicios_logisticos/generate/restore/trafficLight', {
	        			id_user: this.id_user
	            })
	            .then(response => {
	            	console.log('restore conf table', response);
	                if (response.data.success === true) {
	                	
	                	alert(response.data.message);
	                	location.reload();
	                	
	                }else{

	                    alert(response.data.message);
	                    this.var_load = '';

	                }
	            })
		        }catch(error) {
		          alert(error+ '- Fallo en traer el listado de configuraciones.');
		          this.var_load = '';
		          this.btn_export_data = false;
		        }
		        setTimeout(()=>{
				   this.var_load = '';
				},10000);	  	
		    },
		    go_conf_sem(){
		    	this.section_1 = true;
		    	this.section_2 = false;
		    	this.section_table = false;
		    	this.exp_excel_btn = false;
		    },
		    save_sem_btn(){

		    	if(this.action_config === false ){

			    	if (this.item_var === '') {
			    		alert('Error!! Seleccione columna');
			    		return false;
			    	}
			    	
			    	this.variable = document.getElementById("item_var"+this.item_var).getAttribute("data-rs");
			    	
			    	if (this.condition === '') {
			    		alert('Error!! Seleccione regla');
			    		return false;
			    	}
			    	if (this.number_semaforo === '') {
			    		alert('Error!! Ingrese parámetro');
			    		return false;
			    	}
			    	if (this.selectedColor === '') {
			    		alert('Error!! Seleccione color de la primera fila');
			    		return false;
			    	}
			    	if (this.selectedColor2 === '') {
			    		alert('Error!! Seleccione color de la segunda fila');
			    		return false;
			    	}
			    	if (this.selectedColor3 === '') {
			    		alert('Error!! Seleccione color de la tercera fila');
			    		return false;
			    	}

			    	if (this.condition_2 !== '' || this.condition_3 !== '') {

			    		if (this.selects_especials === false) {

				    		if (this.condition === this.condition_2 && this.number_semaforo === this.number_semaforo_2) {
					    		alert('Error!! No puede seleccionar la misma regla en las configuraciones!!');
					    		return false;
					    	}
					    	if (this.condition === this.condition_3 && this.number_semaforo === this.number_semaforo_3) {
					    		alert('Error!! No puede seleccionar la misma regla en las configuraciones!!');
					    		return false;
					    	}
					    	if (this.condition_2 === this.condition_3 && this.number_semaforo_2 === this.number_semaforo_3) {
					    		alert('Error!! No puede seleccionar la misma regla en las configuraciones!!');
					    		return false;
					    	}

			    		}				    	

			    	}

	                this.go_sem_selected =  [
		                {
					      "key": this.item_var,
		                  "variable": this.variable,
		                  "operator": this.condition,
		                  "value": this.number_semaforo,
		                  "color": this.selectedColor
					    },
					    {
					      "key": this.item_var,
		                  "variable": this.variable,
		                  "operator": this.condition_2,
		                  "value": this.number_semaforo_2,
		                  "color": this.selectedColor2
					    },
					    {
					      "key": this.item_var,
		                  "variable": this.variable,
		                  "operator": this.condition_3,
		                  "value": this.number_semaforo_3,
		                  "color": this.selectedColor3
					    }
				    ];	 

	                var save_config = JSON.stringify(this.go_sem_selected);

	                console.log('save_config ', save_config);
	                
	                try {	        		
	        		axios.post('/api/v1/Servicios_logisticos/save/update/config', {
			            configuration: save_config,
			            id_user: this.id_user
			        })
		            .then(response => {
		            	console.log('response 3 ',response);
		                if (response.data.success === true) {

		                	alert(response.data.message);
		                	this.states_confs();
		                	this.getConfigSetting(this.id_user);

		                	this.condition = '';
		                	this.number_semaforo = '';
		                	this.condition_2 = '';
		                	this.number_semaforo_2 = '';
		                	this.condition_3 = '';
		                	this.number_semaforo_3 = '';
		                	this.add_sem = false;

		                }else{
		                    alert(response.data.message);
		                }
		            })
			        }catch(error) {
			          alert(error+ '- Fallo en traer el listado de clientes.');
			        }

		        }else{

		        	var cond_edit0 = document.getElementById("cond_edit0").value;
		        	var cond_edit1 = document.getElementById("cond_edit1").value;
		        	var cond_edit2 = document.getElementById("cond_edit2").value;

		        	var dias_edit0 = document.getElementById("dias_edit0").value;
		        	var dias_edit1 = document.getElementById("dias_edit1").value;
		        	var dias_edit2 = document.getElementById("dias_edit2").value;

		        	var color_get_edit0 = document.getElementById("color_get_edit0").value;
		        	var color_get_edit1 = document.getElementById("color_get_edit1").value;
		        	var color_get_edit2 = document.getElementById("color_get_edit2").value;

		        	//if (this.selects_especials === false) {

			        	if (cond_edit0 === '') {
				    		alert('Error!! Seleccione regla');
				    		return false;
				    	}
				    	if (dias_edit0 === '') {
				    		alert('Error!! Ingrese parámetro');
				    		return false;
				    	}
				    	if (dias_edit0 < 1) {
				    		alert('El valor del campo debe ser mayor a cero');
				    		document.getElementById("dias_edit0").value = 1;
				    		return false;
				    	}			    	

			    	//}

			    	if (color_get_edit0 === '') {
			    		alert('Error!! Seleccione color');
			    		return false;
			    	}

			    	if (color_get_edit0 === color_get_edit1 || color_get_edit0 === color_get_edit2 || color_get_edit1 === color_get_edit2) {
		  				alert('Debe seleccionar un color diferente a los ya elegidos');
		  				return false;
		  			}

				    this.go_sem_selected =  [
		                {
					      "key": this.item_var_edit,
		                  "variable": this.name_var_edit,
		                  "operator": cond_edit0,
		                  "value": dias_edit0,
		                  "color": color_get_edit0
					    },
					    {
					      "key": this.item_var_edit,
		                  "variable": this.name_var_edit,
		                  "operator": cond_edit1,
		                  "value": dias_edit1,
		                  "color": color_get_edit1
					    },
					    {
					      "key": this.item_var_edit,
		                  "variable": this.name_var_edit,
		                  "operator": cond_edit2,
		                  "value": dias_edit2,
		                  "color": color_get_edit2
					    }
				    ];	

				    var save_config = JSON.stringify(this.go_sem_selected);

				    console.log('save_config edit ', save_config);

	                try {	        		
	        		axios.post('/api/v1/Servicios_logisticos/save/update/config', {
			            configuration: save_config,
			            id_user: this.id_user
			        })
		            .then(response => {
		            	console.log('response 3 ',response);
		                if (response.data.success === true) {
		                	alert(response.data.message);
		                	this.states_confs();
		                	this.getConfigSetting(this.id_user);
		                	this.add_sem = false;
		                }else{
		                    alert(response.data.message);
		                }
		            })
			        }catch(error) {
			          alert(error+ '- Fallo en traer el listado de clientes.');
			        }	 

		        } 			

		    },
		    save_conf_finish(event){
		    	event.preventDefault();
		    	this.section_1	 	= false;
		    	this.section_2 		= false;
		    	this.section_table 	= true;
		    	this.exp_excel_btn = true;
		    	this.getConfigTables(this.id_user, this.identification, this.fechainicial, this.fechafinal);
		    },
		    edit_especific_rule(event, name_field, item_var, name_var, config_info, string_validate){
		    	this.add_sem = true;
		    	this.action_config = true;

		    	this.name_field_edit = name_field;
		    	this.item_var_edit = item_var;
		    	this.name_var_edit = name_var;

		    	this.sem_conf_data_to_edit = config_info;

		    	if (string_validate === true) {
		    		this.options_data_validate = false;
		    		this.selects_especials = true;

					if (name_var === 'Rango_Global') {
						this.array_dinamic_sel_especials = this.rank_options_global;
					}else if(name_var === 'Rango_Deposito'){
						this.array_dinamic_sel_especials = this.rank_options_deposito;
					}else if(name_var === 'Fecha_entrega_de_Docs_a_la_agencia'){
						this.array_dinamic_sel_especials = this.rank_options_docs;
					}

		    	}else{
		    		this.options_data_validate = true;
		    		this.selects_especials = false;
		    	}		    	

		    	console.log('this.string_validate edit ', string_validate);
		    	console.log('this.sem_conf_data edit ', this.sem_conf_data_to_edit);
		    	console.log('data config_info ', this.item_var_edit, this.name_var_edit, this.name_field_edit);

		    	//this.sem_conf_data_to_edit = this.sem_conf_data.filter(sem_conf_data => sem_conf_data.variable == name_var);
		    },
		    del_especific_rule(event, item_var){
		    	
		    	event.preventDefault();	

		    	console.log('item to del ', item_var);  	

		    	var del = confirm("Desea eliminar esta configuración?");

		    	if (del === true) {
		    		this.var_load = 'loading';
		    		try {
	        			axios.get('/api/v1/Servicios_logisticos/delete/update/config/'+item_var+'/'+this.id_user, {
			            })
			            .then(response => {
			            	console.log('Response del ',response);
			                if (response.data.success === true) {
			                	alert(response.data.message);
			                	this.states_confs();
			                	this.getConfigSetting(this.id_user);
			                	this.var_load = '';
			                }else{
			                    alert(response.data.message);
			                    this.var_load = '';
			                }
			            })
			        }catch(error) {
			          alert(error+ '- Fallo en eliminación de esta configuración.');
			          this.var_load = '';
			        }
		    	}
		    	
		    },
		    returnName(value){
		    	return value.data;
		    },
		    /*returnColor(value){
		    },*/
		    setColor: function(color, colorName, row) {
		    	if (row == 1) {
		    		this.selectedColor = color;
					this.selectedColorName = colorName;
					this.active_col_pick = false;
		    	}else if(row == 2){
		    		this.selectedColor2 = color;
					this.selectedColorName2 = colorName;
					this.active_col_pick2 = false;
		    	}else if(row == 3){
		    		this.selectedColor3 = color;
					this.selectedColorName3 = colorName;
					this.active_col_pick3 = false;
		    	}				
			},
			toggleDropdown: function(row) {
				if(row == 1){
					this.active_col_pick = !this.active_col_pick;
				}else if(row == 2){
					this.active_col_pick2 = !this.active_col_pick2;
				}else if(row == 3){
					this.active_col_pick3 = !this.active_col_pick3;
				}				
			},
			inputSemRule: function() {
				if (this.number_semaforo < 1) {
					alert('El valor del campo debe ser mayor a cero');
					this.number_semaforo = 1;
				}
			},
			validate_options: function() {
				var opt = document.getElementById("item_var"+this.item_var).getAttribute("data-validate");
				var opt_variable = document.getElementById("item_var"+this.item_var).getAttribute("data-rs");
				this.definition = document.getElementById("item_var"+this.item_var).getAttribute("data-definition");

				if(opt === 'true'){
					this.options_data_validate = false;
					this.selects_especials = true;
					if (opt_variable === 'Rango_Global') {
						this.array_dinamic_sel_especials = this.rank_options_global;
						this.help_text = 'Seleccione Rango Global';
					}else if(opt_variable === 'Rango_Deposito'){
						this.array_dinamic_sel_especials = this.rank_options_deposito;
						this.help_text = 'Seleccione Rango Deposito';
					}else if(opt_variable === 'Fecha_entrega_de_Docs_a_la_agencia'){
						this.array_dinamic_sel_especials = this.rank_options_docs;
						this.help_text = 'Seleccione Valores';
					}
				}else{
					this.options_data_validate = true;
					this.selects_especials = false;
					this.help_text = 'Número de días';
				}
			},
			search: function(event) {
				event.preventDefault();

				if (this.fechainicial !== '') {
					this.fechainicial = moment(this.fechainicial).format('DD-MM-YYYY');	
				}else{
					alert('Ingrese fecha inicial!!');
					return false;
				}

				if (this.fechafinal !== '') {
					this.fechafinal = moment(this.fechafinal).format('DD-MM-YYYY');
				}else{
					this.fechafinal = moment().format('DD-MM-YYYY');
				}

				console.log(this.fechainicial, this.fechafinal);
				this.getConfigTables(this.id_user, this.identification, this.fechainicial, this.fechafinal);

				var element = document.getElementById("buscador-cargas");
  				element.classList.remove("active");

				this.state_search = false;

				this.section_1	 	= false;
		    	this.section_2 		= false;
		    	this.section_table 	= true;
			},
			tableToExcel: function(table, name, filename) {
				//alert(12);
				let uri = 'data:application/vnd.ms-excel;base64,',

		        /*template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><title></title><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>',*/

		        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" ' +
                                        'xmlns="http://www.w3.org/TR/REC-html40"><head>' +
                                        '<!--[if gte mso 9]>' +

                                        '<xsl:template name="styles">' +
                                        '<style>' +

                                        'td {' +
                                        'padding-top:1px;' +
                                        'padding-right:1px;' +
                                        'padding-left:1px;' +
                                        'mso-ignore:padding;' +
                                        'color:windowtext;' +
                                        'vertical-align:bottom;' +
                                        'text-align:left;' +
                                        'white-space:wrap;' +
                                        '}' +

                                        '.header {' +
                                        'mso-style-parent:style0;' +
                                        'font-weight:700;' +
                                        '}' +

                                        '.number {mso-number-format:"0";}' +
                                        '</style>' +

                                        '<xml>' +
                                        '<x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>' +
                                        '<x:Name>{worksheet}</x:Name>' +
                                        '<x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions>' +
                                        '</x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook>' +
                                        '</xml>' +
                                        '<![endif]-->' +
                                        '</head>' +
                                        '<body>' +
                                        '<table>{table}</table>' +
                                        '</body>' +
                                        '</html>',


		        /*base64 = function(s) { return window.btoa(decodeURIComponent(encodeURIComponent(s))) },  */
		        base64 = function(s = "äöüÄÖÜçéèñ") { 
	              return window.btoa(unescape(encodeURIComponent(s))) 
	            }, 

		        format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; })}
		        
		        if (!table.nodeType) table = document.getElementById(table)
		        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}

		        var link = document.createElement('a');
		        link.download = filename;
		        link.href = uri + base64(format(template, ctx));
		        link.click();
			}
		  }
		});