appEstados = new Vue({
    el: '#reservation',
    data :{
        var_load: '',
        cliente: '',
        pedido: '',
        idPlaca: '',
        idCcond: '',
        idTelcond: '',
        idDate: '',
        idHora: '',
        nombre_cliente: '',
        nombre_vendedor: '',
        nombre_conductor: '',
        productos: [],
        prods_filter: [],
        prods_result: [],
        btn_reserva: false
    },
    mounted () {
        
    },
    methods:{
        consult_info: function(){
            
            this.cliente = document.getElementById("cliente_id").value;
            this.pedido = document.getElementById("idPed").value;
            //this.pedido = '7079664';

            this.var_load = 'loading'; 
            this.btn_reserva = false;
            
            axios.post('/api/v1/agricola/info_reserva', {
                cliente: this.cliente,
                pedido:  this.pedido
            })
            .then(response => { 

                if (response.data.success === true) {

                    this.nombre_cliente = response.data.data.pedido.nombre_cliente;
                    this.nombre_vendedor = response.data.data.pedido.nombre_vendedor;
                    this.productos = response.data.data.pedido.productos;

                    this.btn_reserva = true;

                    console.log('pro ',this.productos);
                    
                    var array_globals = [];

                    this.productos.forEach(function(element) {
                       array_globals.push(
                                            element.state = false,
                                            element.cantidad = 1
                                          );
                    });

                    console.log('pro res ',this.productos);
                    
                }else{
                    alert(response.data.data[0].message);
                    this.productos = [];
                    this.nombre_cliente = '';
                    this.nombre_vendedor = '';

                    this.btn_reserva = false;
                }

                this.var_load = '';  

            })
            .catch(error => {
                console.log(error);
                this.var_load = ''; 
                alert(error + '- Fallo en servicio!!');
            });

        },
        consult_conductor: function(){ 

            var idCcond = document.getElementById("idCcond").value;

            this.var_load = 'loading'; 
            
            axios.post('/api/v1/Servicios_logisticos/nom_conductor', {
                idconductor: idCcond
            })
            .then(response => { 

                console.log('response cond ', response);

                if (response.data.success === true) {
                        
                    this.nombre_conductor = response.data.data.conductor.nombre_conductor;   
                    
                }else{
                    
                    alert(response.data.data[0].message);
                    this.nombre_conductor = response.data.data[0].message;
                    
                }

                this.var_load = '';  

            })
            .catch(error => {
                console.log(error);
                this.var_load = ''; 
                alert(error + '- Fallo en servicio!!');
            });

        },
        generar_reserva: function(){            

            this.pedido = document.getElementById("idPed").value;
            this.cliente = document.getElementById("cliente_id").value;

            this.idPlaca = document.getElementById("idPlaca").value;
            this.idCcond = document.getElementById("idCcond").value;
            this.idTelcond = document.getElementById("idTelcond").value;
            this.idDate = document.getElementById("idDate").value;
            this.idHora = document.getElementById("idHora").value;

            if (this.pedido === '' || this.cliente === '' || this.idPlaca === '' || this.idCcond === '' || this.idTelcond === '' || this.idDate === '' || this.idHora === '') {
                alert('Por favor complete todos los campos!!');
                return false;
            }

            this.prods_filter = this.productos.filter(state => state.state === true );
            var arr_products = this.prods_filter.length;            
            if (arr_products == 0) {
                alert('¡¡Error!! Debe seleccionar al menos 1 producto.');
                return false;
            }

            var prods_amounts = [];
            prods_amounts = this.prods_filter.filter(cantidad => cantidad.cantidad == 0);
            //console.log('prods_amounts ',prods_amounts.length);
            if (prods_amounts.length > 0) {
                alert('¡¡Error!! No esta permitido enviar productos con cantidad 0');
                return false;
            }

            var conf = confirm("¿Desea realizar la reserva?");

            if (conf) {
                        
                this.prods_result = this.prods_filter.map(pro => ({
                    idproducto: pro.codigo,
                    bl: pro.um,
                    cantidad: pro.cantidad
                }));

                this.prods_result = JSON.stringify(this.prods_result);

                //console.log('reserva cantidad ', this.prods_filter);
                console.log('reserva action ', this.prods_result);

                this.var_load = 'loading'; 

                axios.post('/api/v1/agricola/nueva_reserva', {
                    prods_arr: this.prods_result,
                    cliente: this.cliente,
                    pedido: this.pedido,
                    idPlaca: this.idPlaca,
                    idCcond: this.idCcond,
                    idTelcond: this.idTelcond,
                    idDate: this.idDate,
                    idHora: this.idHora
                })
                .then(response => { 

                    console.log('respuesta ',response);
                    if (response.data.success === true) {
                        alert(response.data.message);
                        location.reload();
                        this.var_load = ''; 
                    }else{
                        alert(response.data.message);
                        this.var_load = ''; 
                    }

                    this.var_load = ''; 
                    
                })
                .catch(error => {
                    console.log(error);
                    //this.loader = false;
                    alert(error + '- Fallo en el servicio!!');
                    this.var_load = ''; 
                });

            }

        }

    },

});