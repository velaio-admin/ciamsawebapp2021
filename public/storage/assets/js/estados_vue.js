Vue.component('v-select', VueSelect.VueSelect)

appEstados = new Vue({
    components: {
        Multiselect: window.VueMultiselect.default
    },
    el: '#estado_app',
    data :{
        value: [],
        bl_item: '',
        id_state: '',
        id_state_old: '',
        id_state_cont: '',
        id_client: '',
        states_det: [],
        states: [],
        states_cont: [],
        list_edit_cont: [],
        array_globals: null,
        loader: false,
        result_request: false,
        individual: true,
        global: false,
        show_state_bl: false,
        clients: [],
        CONTENEDOR_ARR: '',
        consult: 1, //1 = Update Individual, 2 = Update Global.
        cont_form: [],
        observacion: '',
        observacion_global: '',
        response_comp: '',
        response_comp_2: '',
        bl_data_change: ''
    },
    mounted () {
        //Servicio que trae los clientes.
        try {
            this.loader = true;
            axios.get('/api/v1/Servicios_logisticos/getClientsData', {
            })
            .then(response => {
                if (response.data.success === true) {
                    this.clients = response.data.data[0];
                    //Convierto Objeto A Array
                    this.clients = Object.entries(this.clients).map(([id, name]) => ({id, name}));
                    console.log('Clients ',this.clients);
                    this.loader = false;
                }else{
                    //alert('El sistema no encotró clientes disponibles. fkcli');
                }

            })
        }catch(error) {
          alert(error+ '- Fallo en traer el listado de clientes.');
          this.loader = false;
        }
        //Servicio que trae los estados del BL.
        axios.post('/api/v1/Servicios_logisticos/getBlContStates', {
            tipo: 'BL'
        })
        .then(response => { 
            console.log('Estados Del Bl Jaos ', response);
            if (response.data.success === true) {
                this.states = response.data.data[0];
                console.log('BL states ',this.states);
            }else{
                alert('El sistema no encotró estados disponibles.');
            }

        })
        .catch(error => {
            console.log(error);
            this.loader = false;
            alert(error + '- Fallo en servicio de estados BL.');
        });
        //Servicio que trae los estados de los contenedores.
        axios.post('/api/v1/Servicios_logisticos/getBlContStates', {
            tipo: 'CONTENEDOR'
        })
        .then(response => { 
            if (response.data.success === true) {
                this.states_cont = response.data.data[0];
                console.log('Cont states ',this.states_cont);
            }else{
                alert('El sistema no encotró estados de contenedores disponibles.');
            }

        })
        .catch(error => {
            console.log(error);
            this.loader = false;
            alert(error + '- Fallo en servicio de estados Contenedores.');
        });

    },
    watch:{
       'bl_item'  : function (val, oldval) {
          //console.log('Select Change ', val.BL);
          this.list_edit_cont = [];

          this.bl_data_change = val.BL;

          this.service_bl_consult();

        },
        'cont_form'  : function (val, oldval) {
            this.list_edit_cont = val;
            console.log('CONTENEDOR change2 ', this.list_edit_cont);

            this.id_state_cont = this.CONTENEDOR_ARR.filter(CONTENEDOR_ARR => CONTENEDOR_ARR.CONTENEDOR == this.list_edit_cont[0].CONTENEDOR);

            console.log('SOCIOLOGIA ', this.list_edit_cont[0].CONTENEDOR);

        }
    },
    methods:{
        search_bl: function(){
            this.bl_item = '';
            this.result_request = false;
            this.loader = true;

            //console.log('this.bl_data_change ',this.id_client.id);

            if(this.id_client === '' || this.id_client === null){
                alert('Seleccione cliente para realizar la búsqueda!!');
                this.loader = false;
                return false;
            }else{               

                //Servicio que trae los BL.
                axios.get('/api/v1/Servicios_logisticos/getBlContenedor/'+this.id_client.id, {
                })
                .then(response => {
                console.log('ESTADOS RESPONSE ',response); 
                    if (response.data.success === true) {
                        this.states_det = response.data.data[0];
                        console.log('States Info BL ',this.states_det);
                        this.loader = false;
                        this.result_request = true;
                    }else{
                        alert('El sistema no encotró BL disponibles.');
                    }
                })
                .catch(error => {
                    console.log(error);
                    this.loader = false;
                    //alert(error+ '- Fallo en el servicio de búsqueda de los BL.');
                });

            }

        },
        getBlAvalible: function(id_client){

            this.loader = true;

            axios.post('/api/v1/Servicios_logisticos/getBlAvalibleTran', {
                id_client: id_client
            })
            .then(response => { 

                if (response.data.success === true) {

                   this.response_comp = response.data.data.data[0].attributes[0].data;
                   var array_globals = [];

                    this.response_comp.forEach(function(element) {
                       array_globals.push(element[0]);
                    });

                    this.array_globals = array_globals;

                    axios.post('/api/v1/Servicios_logisticos/getBlAvaliblePro', {
                        id_client: id_client
                    })
                    .then(response =>{

                        if (response.data.success === true) {

                            this.response_comp_2 = response.data.data.data[0].attributes[0].data;

                            console.log('play 2 ', this.response_comp_2);

                            var array_globals2 = [];

                            this.response_comp_2.forEach(function(element) {
                               array_globals2.push(element[0]);
                            });

                            this.array_globals2 = array_globals2;
                            //Usado para unir los resultados de los Bl en Transito y En Proceso.
                            Array.prototype.push.apply(this.array_globals, this.array_globals2);
                            console.log('ARRAY BL AVALIBLE ',this.array_globals);

                            this.loader = false;
                            this.result_request = true;

                        }else{
                            this.loader = false;
                            this.result_request = false;
                            alert('No se hallaron BL en transito disponibles!!');
                        }

                    })

                }         

            })
            .catch(error => {
                this.loader = false;
                console.log(error);
                alert(error + '- Fallo en servicio de BL disponibles.');
            });

        },
        service_bl_consult: function(){

            axios.post('/api/v1/Servicios_logisticos/getDetailCont', {
                id_bl: this.bl_data_change
            })
            .then(response => { 
                if (response.data.success === true) {
                    this.CONTENEDOR_ARR = response.data.data[0];
                    console.log('CONTENEDOR ',this.CONTENEDOR_ARR);

                    this.id_state = this.states_det.filter(states_det => states_det.BL == this.bl_data_change);
                    this.id_state = this.id_state[0];

                    this.id_state_old = this.id_state.ID_ESTADO;
                    this.show_state_bl = true;

                }else{
                    alert('No se encotraron contenedores asociados a ese BL.');
                    this.show_state_bl = false;
                }
            })
            .catch(error => {
                console.log(error);
                this.loader = false;
                //alert(error+ '- Fallo en el servicio de búsqueda de BL.');
            });

        },
        update_bl_est: function(){

            console.log(this.bl_item.BL);

            if (this.id_state_old === this.id_state.ID_ESTADO) {
                alert('Debe seleccionar un estado diferente al actual!!');
                return false;
            }

            if (this.bl_item == '') {
                alert('Seleccione BL!!');
                return false;
            }else if(this.id_state === '' || this.id_state === undefined){
                alert('Seleccione estado BL!!');
                return false;
            }

            this.loader = true;

            axios.post('/api/v1/Servicios_logisticos/updateBlCont', {
                consult: this.consult,
                id: this.bl_item.BL,
                tipo: 'BL',
                status: this.id_state.ID_ESTADO,
                observacion: this.observacion
            })
            .then(response => { 
                console.log('response ',response);
                if (response.data.success === true) {
                    this.loader = false;
                    alert('BL Actualizado Exitosamente!!');
                }

            })
            .catch(error => {
                this.result_request = true;
                this.loader = false;
                console.log(error);
                alert(error);
            });

        },
        update_bl_cont_ind: function(key, container){
           
            var val_cont = document.getElementById(key+"val_cont").value;
            var val_obser = document.getElementById(key+"val_obser").value;

            if (val_cont === this.list_edit_cont[0].ID_ESTADO) {
                alert('Debe seleccionar un estado diferente al actual!!');
                return false;
            }

            if (val_cont == '') {
                alert('Debe seleccionar el estado para el contenedor ' + container);
                return false;
            }

            this.loader = true;

            axios.post('/api/v1/Servicios_logisticos/updateBlCont', {
                consult: this.consult,
                id: container,
                tipo: 'CONTENEDOR',
                status: val_cont,
                observacion: val_obser
            })
            .then(response => { 

                if (response.data.success === true) {

                    this.loader = false;

                    this.list_edit_cont.splice(this.list_edit_cont.findIndex(e => e.CONTENEDOR === container),1);
                    console.log('this.list_edit_cont ',this.list_edit_cont);

                    this.service_bl_consult();

                    alert('Contenedor Actualizado Exitosamente!!');

                }

            })
            .catch(error => {
                this.result_request = true;
                this.loader = false;
                console.log(error);
                alert(error);
            });
            
        },
        update_bl_cont: function(){

            if (this.consult == 1) {

                /*if (this.bl_item == '') {
                    alert('Seleccione BL!!');
                    return false;
                }else if(this.id_state === '' || this.id_state === undefined){
                    alert('Seleccione estado BL!!');
                    return false;
                }else if(this.cont_form ==''){
                    alert('Seleccione contenedor!!');
                    return false;
                }else if(this.id_state_cont === '' || this.id_state_cont === undefined){
                    alert('Seleccione estado Contenedor!!');
                    return false;
                }else if(this.observacion == ''){
                    alert('Agregue observación!!');
                    return false;
                }

                this.result_request = false;
                this.loader = true;

                axios.post('/api/v1/Servicios_logisticos/updateBlCont', {
                    consult: this.consult,
                    id: this.bl_item,
                    tipo: 'BL',
                    status: this.id_state.ID_ESTADO,
                    observacion: this.observacion
                })
                .then(response => { 
                    console.log('response ',response);
                    if (response.data.success === true) {

                        axios.post('/api/v1/Servicios_logisticos/updateBlCont', {
                            consult: this.consult,
                            id: this.cont_form.CONTENEDOR,
                            tipo: 'CONTENEDOR',
                            status: this.id_state_cont.ID_ESTADO,
                            observacion: this.observacion
                        })
                        .then(response => { 

                            if (response.data.success === true){

                                this.result_request = true;
                                this.loader = false;
                                alert('Registro Actualizado Exitosamente!!');

                            }else{
                                this.result_request = true;
                                this.loader = false;
                                alert('Error para actualizar el Contenedor');
                            }

                        })

                    }else{
                        this.result_request = true;
                        this.loader = false;
                        alert('No se pudo actulizar el BL!! Por lo tanto no puede continuar actualizando.');
                    }
                })
                .catch(error => {
                    this.result_request = true;
                    this.loader = false;
                    console.log(error);
                    alert(error);
                });*/

            }else if (this.consult == 2) {

                if(this.id_state_cont === '' || this.id_state_cont === undefined){
                    alert('Seleccione estado Contenedor!!');
                    return false;
                }

                //console.log('this.CONTENEDOR ', this.CONTENEDOR);
                var array_globals = [];

                this.CONTENEDOR_ARR.forEach(function(element) {
                   array_globals.push(element.CONTENEDOR);
                });

                console.log('array_ send cont', array_globals);

                var commas_ids = array_globals.join(",");
                console.log('commas_ids ', commas_ids);

                axios.post('/api/v1/Servicios_logisticos/updateBlCont', {
                    consult: this.consult,
                    id: commas_ids,
                    tipo: 'BL',
                    status: this.id_state.ID_ESTADO,
                    observacion: this.observacion_globalacion
                })
                .then(response => { 
                    if (response.data.success === true) {

                        axios.post('/api/v1/Servicios_logisticos/updateBlCont', {
                            consult: this.consult,
                            id: commas_ids,
                            tipo: 'CONTENEDOR',
                            status: this.id_state_cont.ID_ESTADO,
                            observacion: this.observacion
                        })
                        .then(response => { 

                            if (response.data.success === true){

                                this.result_request = true;
                                this.loader = false;
                                alert('Registro Actualizado Exitosamente!!');

                            }else{
                                this.result_request = true;
                                this.loader = false;
                                alert('Error para actualizar el Contenedor');
                            }

                        })
                    }else{
                        this.result_request = true;
                        this.loader = false;
                        alert('No se pudo actulizar el BL!! Por lo tanto no puede continuar actualizando.');
                    }

                })
                .catch(error => {
                    this.result_request = true;
                    this.loader = false;
                    console.log(error);
                    alert(error);
                });

            }

        },
        changeOption: function(){
            if (this.consult == 1) {
                this.individual = true;
                this.global = false;
            }else if(this.consult == 2) {
                this.individual = false;
                this.global = true;
            }
        }

    },

});