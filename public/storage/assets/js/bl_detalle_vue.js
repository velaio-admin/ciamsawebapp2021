var app = new Vue({
    el: '#rastreoContainers',
    data: {
      var_load: '', 
      base64: '' 
    },
    mounted () {

    },
    methods: {
      dataConsult: function (event, id_doc_var) {

        event.preventDefault();

        try {
          this.var_load = 'loading'; 

          axios.post('/api/v1/Servicios_logisticos/detalle_bl/base64', {
              id_doc: id_doc_var
          })
          .then(response => {
            console.log('resp data request', response);
            if (response.data.success === true) {
              this.var_load = ''; 

              this.base64 = response.data.data;
              //console.log('base64 ',this.base64);
              const linkSource = `data:application/pdf;base64,${this.base64}`;
              const downloadLink = document.createElement("a");
              const fileName = id_doc_var+'_documento.pdf';
              downloadLink.href = linkSource;
              downloadLink.download = fileName;
              downloadLink.click();

            }else{
              this.var_load = ''; 
              alert(response.data.data.message);
            }            
          })
        }catch(error) {
          this.var_load = '';  
          alert(error+ '- Fallo en consultar servicio.');
        }

      }

    }
    
  })