Vue.component('v-select', VueSelect.VueSelect)

var app = new Vue({
    el: '#carga_app',
    vuetify: new Vuetify(),
    data: {
      id_user: '',
      valid: true,
      var_load: '',

      num_bl: '',
      numBlRules: [
        v => !!v || 'Número BL es requirido',
        v => v.length >= 5 || 'Debe ser mínimo 5 caracteres',
      ],
      motonave_form: '',      
      motonave_list: [],
      nit_cliente: '', 
      nitRules: [
        v => !!v || 'Nit cliente es requirido',
        v => v.length >= 8 || 'Debe ser mínimo 8 caracteres',
      ],     
      viaje: '',
      viajeRules: [
        v => !!v || 'Viaje es requirido',
      ],      
      cliente_form: '',
      cliente_list: [],
      et_mn: '',
      et_mnRules: [
        v => !!v || 'ETA MN es requirido',
      ],      
      importador: '',  
      importadorRules: [
        v => !!v || 'El importador es requirido',
      ],    
      fec_des_mn: '',
      tip_car_form: '',
      tip_car_list: [],
      muelle_form: '',
      muelle_list: [],
      contenedor: '',
      contenedorRules: [
        v => !!v || 'El contenedor es requirido',
      ], 
      pro_muis_form: '',
      pro_muis_list: [],
      lin_nav_form: '',
      lin_nav_list: [],
      fec_ing_muis: '',        
      num_importacion: '',
      num_importacionRules: [
        v => !!v || 'Número importación es requirido',
      ],         
      cant_contenedores: '',
      cant_contenedoresRules: [
        v => !!v || 'Cantidad contenedores es requirido',
      ],
      img_data: []   
    },
    components: {
        vuejsDatepicker
    },
    mounted () {

      this.id_user = document.getElementById("id_user").value;
      
      try {
          this.var_load = 'loading';                   
          axios.post('/api/v1/Servicios_logisticos/anuncio_carga/datos_form', {
            
          })
          .then(response => {
            console.log('resp data get', response);

            if(response.data.success === true){

              this.motonave_list = response.data.data[0].data.attributes[0].motonave[0];
              this.motonave_list = Object.entries(this.motonave_list).map(([id, value]) => ({id, value}));

              this.cliente_list = response.data.data[0].data.attributes[0].clientes[0];
              this.cliente_list = Object.entries(this.cliente_list).map(([id, value]) => ({id, value}));

              this.tip_car_list = response.data.data[0].data.attributes[0].tipos_carga[0];
              this.tip_car_list = Object.entries(this.tip_car_list).map(([id, value]) => ({id, value}));

              this.muelle_list = response.data.data[0].data.attributes[0].muelles[0];
              this.muelle_list = Object.entries(this.muelle_list).map(([id, value]) => ({id, value}));

              this.pro_muis_list = response.data.data[0].data.attributes[0].productos_muisca[0];
              this.pro_muis_list = Object.entries(this.pro_muis_list).map(([id, value]) => ({id, value}));

              this.lin_nav_list = response.data.data[0].data.attributes[0].lineas_navieras[0];
              this.lin_nav_list = Object.entries(this.lin_nav_list).map(([id, value]) => ({id, value}));

              this.var_load = ''; 
            }

          })
        }catch(error) {
          alert(error+ '- Fallo en eliminar registro.');
          this.var_load = ''; 
        }

        setTimeout(()=>{
           this.var_load = ''; 
        },15000);

    },
    methods: {
      dataConsult: function (event) {

        event.preventDefault();

        console.log('IMG ',this.img_data);

        if(this.motonave_form === '' || this.motonave_form === null){
          alert('Error!! Seleccione motonave');
          return false;
        }else if(this.cliente_form == '' || this.cliente_form === null){
          alert('Error!! Seleccione cliente');
          return false;
        }else if(this.tip_car_form == '' || this.tip_car_form === null){
          alert('Error!! Seleccione tippo de carga');
          return false;
        }else if(this.muelle_form == '' || this.muelle_form === null){
          alert('Error!! Seleccione muelle de descargue');
          return false;
        }else if(this.pro_muis_form == '' || this.pro_muis_form === null){
          alert('Error!! Seleccione producto muisca');
          return false;
        }else if(this.lin_nav_form == '' || this.lin_nav_form === null){
          alert('Error!! Seleccione linea naviera');
          return false;
        }

        var fec_des_mn = moment(this.fec_des_mn).format('DD-MM-YYYY');
        var fec_ing_muis = moment(this.fec_ing_muis).format('DD-MM-YYYY');

        try {
          this.var_load = 'loading'; 

          var formData = new FormData();

          formData.append('numeroBL',this.num_bl);
          formData.append('identificacionCliente',this.id_user);
          formData.append('nombreCliente',this.cliente_form.value.trim());
          formData.append('importador',this.importador);
          formData.append('tipoCarga',this.tip_car_form.value.trim());
          formData.append('muelleDescargue',this.muelle_form.value.trim());
          formData.append('numeroContenedor',this.contenedor);
          formData.append('lineaMaritima',this.lin_nav_form.value.trim());
          formData.append('numeroImportancion',this.num_importacion);
          formData.append('motonave',this.motonave_form.value.trim());
          formData.append('viaje',this.viaje);
          formData.append('eta',this.et_mn);
          formData.append('fechaDescargue', fec_des_mn);
          formData.append('productoMuisca',this.pro_muis_form.value.trim());
          formData.append('cantContenedores',this.cant_contenedores);
          formData.append('fechaIngresoMuisca', fec_ing_muis);
          //formData.append("image", this.img_data);

          for( var i = 0; i < this.img_data.length; i++ ){
            let file = this.img_data[i];
            console.log('files[' + i + ']', file);
            formData.append('files[' + i + ']', file);
          }

          axios.post('/api/v1/Servicios_logisticos/anuncio_carga/save_form', formData, {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
          })
          .then(response => {
            console.log('resp data request', response);
            if (response.data.data[0].code === '1') {
              this.var_load = ''; 
              alert('Registro almacenado');
              setTimeout(function(){ 
                location.reload();
              }, 1500);
            }else{
              this.var_load = ''; 
              alert(response.data.data[0].message);
            }            
          })
        }catch(error) {
          this.var_load = '';  
          alert(error+ '- Fallo en eliminar registro.');
        }

      }

    }
    
  })