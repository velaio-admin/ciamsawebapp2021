$(document).ready(function() {
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });

  $("select#select_documentos").on("change", function() {
    var select = $("#select_documentos option:selected").text();
    $("#info_tipo_documento").html(select);

    if ($(this).val() == "03") {
      $("#label_peso").show();
      $("#peso").show();
    } else {
      $("#label_peso").hide();
      $("#peso").hide();
    }

    if ($(this).val() == '04') {
      $( "#label_f_limit" ).show();
      $( "#f_limit" ).show();
    }else{
      $( "#label_f_limit" ).hide( );
      $( "#f_limit" ).hide();
    }
  });

  // Mostrar u ocultar el buscador de carga de rastreo
  $(document).on("click", ".header-rastreoList .open-search span", function() {
    $("#buscador-cargas").addClass("active");
  });

    if ($(this).val() == '03') {
      $( "#label_peso" ).show();
      $( "#peso" ).show();
    }else{
      $( "#label_peso" ).hide( );
      $( "#peso" ).hide();
    }

    if ($(this).val() == '04') {
      $( "#label_f_limit" ).show();
      $( "#f_limit" ).show();
    }else{
      $( "#label_f_limit" ).hide( );
      $( "#f_limit" ).hide();
    }
  });
  $(document).on("click", "#buscador-cargas .cerrar-formulario", function() {
    $("#buscador-cargas").removeClass("active");
  });

  // Mostrar u ocultar formulario de observaciones
  $(document).on("click", ".header-rastreoContainers .open-observ span", function() {
    $("#observaciones").addClass("active");
    $("#observaciones textarea").focus();
  });

  $(document).on("click", "#observaciones .cancelform", function() {
    $("#observaciones").removeClass("active");
  });

  function relaodSelectProduct(){
    $(document).ready(function(){
      var value =  $('select#typeProduct').val();
      $.ajax({
        url: "/api/v1/products/list/"+value,
        success: function(data){
          data = data.data;
          var option = '<option value="none" data-type="none" data-und="" data-price-ton="0" data-price-saco="0" selected>Seleccione un producto</option>';
          $.each(data, function(i, item){
            option += '<option value="'+item.atributos.cod_material+'-'+i+'"  data-und="'+item.atributos.und+'" data-price-ton="'+item.atributos.precio_ton+'" data-price-saco="'+item.atributos.precio_saco+'" data-type="'+item.atributos.tipo+'">'+item.nombre+'</option>';
          });
          $('select.product').html(option);
        }
      });
      //console.log(value);
    });  
  }
  // Invoke
  relaodSelectProduct();

  // Cambiar tipo de productos
  $("select#typeProduct").change(function(){
    var newVal = $(this).val();    
    swal("Esta seguro que quiere cambiar el tipo? perdera todo lo que ha hecho hasta el momento.", {
        buttons: {
          cancel: "Cancelar",
          value: "Aceptar"
        },
      })
      .then((value) => {
        //console.log(value);
        if(value == "value"){
          $('.body .v-row-first').not(':first').remove();
          $('.body .v-row-first').css('background-color','transparent');
          $('.body .v-row-first .quantity').val(1);
          $('.body .v-row-first .remove').addClass('disable');
          $('.body .v-row-first .presentation .value').html('');
          $('.body .v-row-first .price-und .value, .body .v-row-first .price-total .value, .body .row-footer .subtotal .value, .body .row-footer .total .value').html('0');
          
          $.ajax({
            url: "/api/v1/products/list/"+newVal,
            success: function(data){
              data = data.data;
              var option = '<option value="none" data-type="none" data-und="" data-price-ton="0" data-price-saco="0" selected>Seleccione un producto</option>';
              $.each(data, function(i, item){
                option += '<option value="'+item.atributos.cod_material+'-'+i+'"  data-und="'+item.atributos.und+'" data-price-ton="'+item.atributos.precio_ton+'" data-price-saco="'+item.atributos.precio_saco+'" data-type="'+item.atributos.tipo+'">'+item.nombre+'</option>';
              });
              $('select.product').html(option);
            }
          });  
          $.data(this, 'val', newVal);
          $('#message').removeClass('message error');
          $('#message').html('');
        }else{
          $(this).val($.data(this, 'val')); //set back
          return;
        }
    });
  });

  //Agregar nuevo producto
  $(document).on("click", ".row-footer .add", function() {

    var num = $(".body .v-row-first").length; // how many "duplicatable" input fields we currently have
    var newNum = new Number(num + 1); // the numeric ID of the new input field being added
    // create the new element via clone(), and manipulate it's ID using newNum value
    var newElem = $("#input" + num).clone().attr("id", "input" + newNum);
    // manipulate the name/id values of the input inside the new element    
    newElem.children(":last").attr("id", "name" + newNum).attr("name", "name" + newNum);
    var flag = 0;
    $(".body .v-row-first").each(function(i){
      var product = $(this).find('.product').val();
      if(product == "none"){
        $(this).css('background-color','#fffdec');
        flag++;
      }
    });
    if(flag > 0){
      swal("Ups!","Debes elegir un producto!","error");
    }else{
      $("#input" + num).after(newElem);
      $(".body .v-row-first:last").children(".left").find('.quantity').val(1);
      $(".body .v-row-first").children(":last").find('.price-und .value, .price-total .value').html('0');
      $(".body .v-row-first:last").children(".left").find('.presentation .value').html('');
      $(".body .v-row-first").css('background-color','transparent');
      $('.body .v-row-first .remove').removeClass('disable');
      var allValuesSelects = [];
      $('.body .v-row-first').each(function(i){
        var value = $(this).find('option:selected').val();
        if(value !== "none"){
          allValuesSelects[i] = value;
        }
      });
      $('.body .v-row-first .product option').each(function(i){
        var value = $(this).val();
        var n = allValuesSelects.includes(value);
        if(n){
          $(this).attr('disabled','disabled');
        }        
      });
      $('.body .v-row-first .product option:selected').each(function(i){
        $(this).removeAttr('disabled');
      });
    }
  });

  //Cambiar producto 
  $(document).on("change","select.product",function(){
    var value =  $('option:selected', this).attr('value');
    var dataUnd =  $('option:selected', this).attr('data-und');
    var dataPrice =  $('option:selected', this).attr('data-price-saco');
    var parent = $(this).parent().parent().parent();
    var cantidad = parent.find('.quantity').val();
    var cantidadTotal = parseFloat(cantidad)*parseFloat(dataPrice);
    parent.find('.presentation .value').html(dataUnd);
    parent.find('.price-und .value').html(numberWithCommas(dataPrice));
    parent.find('.price-total .value').html(numberWithCommas(cantidadTotal));
    if(value !== "none"){
      parent.css('background-color','transparent');
    }
    // sub total y gran total
    var sumaUnd = 0;
    var sumaTotal = 0;
    $('.v-row.v-row-first').each(function(i){
      var priceUnd = $(this).find('.price-und .value').text();
      var priceTotal = $(this).find('.price-total .value').text();
      priceUnd = priceUnd.replace(/,/g, "");
      priceTotal = priceTotal.replace(/,/g, "");
      sumaUnd+=parseFloat(priceUnd);
      sumaTotal+=parseFloat(priceTotal);
    });
    $('.totals .subtotal .value').html(numberWithCommas(sumaUnd));
    $('.totals .total .value').html(numberWithCommas(sumaTotal));

    var allValuesSelects = [];
    $('.body .v-row-first').each(function(i){
      var value = $(this).find('option:selected').val();
      if(value !== "none"){
        allValuesSelects[i] = value;
      }
    });
    $('.body .v-row-first .product option').each(function(i){
      var value = $(this).val();
      var n = allValuesSelects.includes(value);
      if(n){
        $(this).attr('disabled','disabled');
      }else{
        $(this).removeAttr('disabled');
      }     
    });
    $('.body .v-row-first .product option:selected').each(function(i){
      $(this).removeAttr('disabled');
    });
  });

  //Aumentar cantidad de producto
  $(document).on("keyup mouseup", ".quantity", function() {  
    var parent = $(this).parent().parent().parent();
    var valorUnd = parent.find('.price-und .value').text();
    valorUnd = valorUnd.replace(/,/g, "");
    var valorTotal = parseFloat(valorUnd)*parseFloat($(this).val());
    parent.find('.price-total .value').html(numberWithCommas(valorTotal));
    //console.log(numberWithCommas(valorTotal));

    // sub total y gran total
    var suma=0;
    $('.v-row.v-row-first').each(function(i){
      var priceUnd = $(this).find('.price-total .value').text();
      priceUnd = priceUnd.replace(/,/g, "");
      suma+=parseFloat(priceUnd);
    });
    $('.totals .total .value').html(numberWithCommas(suma));
  });

  //Eliminar producto
  $(document).on('click','.body .v-row-first .remove',function(event){
    var count = $('.v-row.v-row-first').length;
    //console.log(count);
    var parent = $(this).parent().parent();
    if(count > 1){
    swal("Esta seguro que quiere eliminar este producto?", {
        buttons: {
          cancel: "Cancelar",
          value: "Aceptar"
        },
        icon:'warning',
      })
      .then((value) => {
        //console.log(value);
        if(value == "value"){
          var i =1;
          var priceUnd = parent.find('.price-und .value').text();
          var priceTotal = parent.find('.price-total .value').text();
          priceUnd = priceUnd.replace(/,/g, "");
          priceTotal = priceTotal.replace(/,/g, "");
          

          var valorSubTotal = $('.totals .subtotal .value').text();
          valorSubTotal = valorSubTotal.replace(/,/g, "");
          var valorTotal = $('.totals .total .value').text();
          valorTotal = valorTotal.replace(/,/g, "");

          var restaSubTotal = valorSubTotal-priceUnd;
          var restaTotal = valorTotal-priceTotal;

          $('.totals .subtotal .value').html(numberWithCommas(restaSubTotal));
          $('.totals .total .value').html(numberWithCommas(restaTotal));
          var activeOption = parent.find('select.product option:selected').val();
          $('.body .v-row-first .product option').each(function(i){
            var value = $(this).val();
            if(value == activeOption && activeOption !== 'none'){
              $(this).removeAttr('disabled');
            }
          });
          parent.remove();
          var countNew = $('.v-row.v-row-first').length;
          if(countNew == 1) $('.body .v-row-first .remove').addClass('disable');
          //console.log(count);
        }
        //console.log(priceTotal);
        $('.v-row.v-row-first').each(function(){
          $(this).attr('id','input'+i);
          i++;
        });
        $('.body .v-row-first:last .product').removeAttr('disabled');        
      });       
    } else{
      $('.body .v-row-first .remove').addClass('disable');
    }  
  });

  //Envio de formulario
  $( "#solicitarPedido" ).submit(function( event ) {
    var flag = 0;
    $(".body .v-row-first").each(function(i){
      var product = $(this).find('.product').val();
      //console.log(i);
      if(product == "none"){
        $(this).css('background-color','#fffdec');
        flag++;
      } 
    });    
    if(flag > 0){
      swal("Ups!","Debes elegir un producto!","error");
      return false;
    }
    
    var subtotal = $('.body .row-footer .subtotal .value').text();
    var total = $('.body .row-footer .total .value').html();
    var ciudad = "BUGA"; //Quemado
    var Condicionpago = $("#wayPay option:selected").val();
    var user = $("#id_info_u").val();

    var data =   {"subtotal":subtotal,"descuentos":0,"total":total,"Ciudad":ciudad,"condicionpago":Condicionpago,"id_info_u":user,"productos":[]};
    
    $(".body .v-row-first select.product").each(function(i){
      var product = $(this).val();
      if(product !== "none"){
      var cod_material = $('option:selected', this).attr('value');
      var tipo = $('option:selected', this).attr('data-type');
      var parent = $(this).parent().parent().parent();
      var cantidad = parent.find('.quantity').val(); 
      var cod_material_real = cod_material.split("-");
      console.log(cod_material_real);
        data.productos.push({
          "cod_material":cod_material_real[0],
          "cantidad":cantidad,
          "tipo":tipo        
        });
      } 
    });
    
    var json = JSON.stringify(data); 
    $.ajax({
      type: "POST",
      url: '/api/v1/products/radicar/pedido',
      data: {"Productos" : json},
      cache: false,
      success: function(msg) {
        //console.log(msg.message);
        if(msg.code == 4){
          $('#message').html('<span>'+msg.message+'</span>');
          $('#message').addClass('message success');
          relaodSelectProduct();
          $('.body .v-row-first').not(':first').remove();
          $('.body .v-row-first').css('background-color','transparent');
          $('.body .v-row-first .quantity').val(1);
          $('.body .v-row-first .remove').addClass('disable');
          $('.body .v-row-first .presentation .value').html('');
          $('.body .v-row-first .price-und .value, .body .v-row-first .price-total .value, .body .row-footer .subtotal .value, .body .row-footer .total .value').html('0');
          var body = $("html, body");
          body.stop().animate({scrollTop:0}, 500);

        } 
        if(msg.code == 5){
          $('#message').html('<span>'+msg.message+'</span>');
          $('#message').addClass('message error');
          var body = $("html, body");
          body.stop().animate({scrollTop:0}, 500);
        }
      },
      error: function(msg) {       
        if(msg.code == 6){
          alert('Error');
        }
      }
    });

    return false;
  });
 /* $(window).on("beforeunload", function() { 
    return confirm("Do you really want to close?"); 
  })
  
  $(document).on('click','label',function(){
    var $confirm = confirm("Do you really want to close?");
    console.log($confirm);
  });*/

  //Ocultar o mostar tablas en Procesos logico
  $(document).on("click", "#comercializadora-procesoLogistico .table-responsive .dropdown", function() {
    $(this).parent().toggleClass("closed");
  });
  //poner activo menu
  var path = location.pathname;
  //console.log(path);
  $("#menu .dropdown_menu a").each(function(i) {
    var $href = $(this).attr("data-href");
    var $path = path.split("/");
    var pathParent = "/" + $path[1] + "/" + $path[2];
    //console.log(pathParent);
    if ($href === path || $href === pathParent) {
      $("body").removeClass("blue green");
      $(this).addClass("active");
      $(this).parents("#menu .dropdown_menu").addClass("active");
      var color = $(this).parents("#menu .dropdown_menu").attr("data-color");
      $("body").addClass(color);
    }
  });
function selectFile() {
  $("#documento").trigger("click");
}

function labelDoc() {
  var doc = $("input[type=file]");
  var iSize = doc[0].files[0].size / 1024;
  if (iSize / 1024 > 1) {
    if (iSize / 1024 / 1024 > 1) {
      iSize = Math.round((iSize / 1024 / 1024) * 100) / 100;
      size = iSize + "Gb";
      if (iSize > 0.01) {
        $("#size").val("true");
      }
    } else {
      iSize = Math.round((iSize / 1024) * 100) / 100;
      size = iSize + "Mb";
      if (iSize > 10) {
        $("#size").val("true");
      }
    }
  } else {
    iSize = Math.round(iSize * 100) / 100;
    size = iSize + "kb";
    if (iSize > 10000) {
      $("#size").val("true");
    }
  }
  //console.log(doc);
  $("#nombre_documento").html(doc.val() + " " + size);
}

function submitForm() {
  var val_select = $("#select_documentos option:selected").val();
  
  if (val_select == "03" && $("#peso").val() == "") {
    alert("Para este tipo de documento, debe ingresar el Peso en kilogramos");
    return false;
  } else if (val_select == "04" && $("#f_limit").val() == "") {
    alert("Para este tipo de documento, debe ingresar la fecha limite de devolucion de contenedores.");
    return false;
  } else if ($("#id_bl").val() == "") {
    alert("Para enviar la solicitud es necesario el ID del BL, por favor ingreselo e intente nuevamente.");
    return false;
  } else if ($("#documento").val().length <= 0) {
    alert("Para enviar la solicitud es necesario adjuntar el documento que se va a registrar.");
    return false;
  //} else if ($("#maxSize").val() == "true") {
  //  alert("Para enviar la solicitud es necesario adjuntar un documento menor a 10 MB.");
  //  return false;
  //} else {
  } else {
    $("#envio_documentos").submit();
  }
}

$(document).ready(function() {
  $("#flUpload").change(function() {});
});
// agregando coma a numeros 
function numberWithCommas(number) {
  var parts = number.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

function getAlert(){
  alert('Hola mundo!');
}