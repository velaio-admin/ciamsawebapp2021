Vue.use(HighchartsVue.default)
Vue.component('v-select', VueSelect.VueSelect);
 
var app = new Vue({
    el: "#indicators",
    data() {
        return {
        	result: false,
            loader_1: false,
            loader_2: false,
            loader_3: false,
            graph_element_1: false,
            graph_element_2: false,
            graph_element_3: false,
            data_range_1: 1,
            form_consult_1: 2,
            fec_ini_1: '',
            fec_fin_1: '',
            form_consult_2: 1,
            month_frm: '',
            data_range_3: 1,
            form_consult_3: 2,
            fec_ini_3: '',
            fec_fin_3: '',
            title1: 'Cumplimiento histórico',
            title2: 'Cumplimiento acumulado mensual',
            title3: 'Principales causales de incumplimiento',
            subtitle1: '',
            subtitle2: '',
            subtitle3: '',
            series_data1: [],
            series_data1_table: [],
            series_data1_vals_new: [],
            series_data2: [],
            series_data3: [],
            array_options: [],
            id_user: '',
            current_year: '',
            filter_cump1: false,
            filter_cump2: false,
            res_months: [],
            res_details: [],
            result_details: true

        }
    },
    components: {
        vuejsDatepicker
    },
    mounted (){
        var mes_test = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
        console.log('this.mes_test ',mes_test);
        //Métodos que inician al cargar la página.
        this.id_user = document.getElementById("id_user").value;

        //alert(this.id_user);

        //console.log('fgh ', rank_options_deposito);
        this.filter_cump1 = true;

        var name_month = moment.months()[moment().month()];
        var number_month = moment().month(name_month).format("M"); 

        this.current_year = moment().year();

        if (number_month < 10) {
            number_month = '0'+number_month;
        }

        console.log(number_month);

        this.month_frm = number_month;

        //this.testIndicadores();
        this.indicatorsGraph1(this.id_user, this.form_consult_1, this.data_range_1, '', '');
        //this.indicatorsGraph2(this.id_user, this.form_consult_2, number_month);
        this.indicatorsGraph3(this.id_user, this.form_consult_3, this.data_range_3, '', '');

    },
    computed: {
        //Funciones para graficar los datos. 
        chartOptionsTest() {
            return {
                chart: {
                    type: 'line'
                },
                title: {
                    text: this.title1
                },
                subtitle: {
                    text: this.subtitle1
                },
                xAxis: {
                    categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
                    //categories: this.res_months
                },
                yAxis: {
                    title: {
                        text: 'Porcentaje (%)'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: '',
                    data: this.series_data1_vals_new
                    //data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                }]
            }
        },         
        chartOptions() {
            return {
                    chart: {  type: 'column'},
                    title: {  text: this.title1 },
                    subtitle: { text: this.subtitle1 },
                    series: this.series_data1,
                    plotOptions: {
                        series: {
                            shadow:false,
                            borderWidth:0,
                            dataLabels:{
                                enabled:true,
                                formatter:function() {
                                    var pcnt = (this.y / 100) * 100;
                                    return Highcharts.numberFormat(pcnt, 1) + '%';
                                }
                            }
                        }
                    },
                     xAxis:{
                        lineColor:'#999',
                        lineWidth:1,
                        tickColor:'#666',
                        tickLength:3,
                        title:{
                            text:'Meses'
                        }
                    },
                    yAxis:{
                        lineColor:'#999',
                        lineWidth:1,
                        tickColor:'#666',
                        tickWidth:1,
                        tickLength:3,
                        gridLineColor:'#ddd',
                        title:{
                            text:'Porcentaje <br>Cumplimiento',
                            rotation:0,
                            margin:50,
                        }
                    },  
                    /*tooltip: {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                        shared: true
                    },*/
              }
        },
        chartOptions2() {
            return {
                    chart: {  type: 'column'},
                    title: {  text: this.title2 },
                    subtitle: {
                        //text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
                        text: this.subtitle2
                    },
                    xAxis: {
                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Porcentaje de contenedores trasladados'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Datos vigentes: <b>{point.y:.1f} resultados</b>'
                    },
                    series: [{
                        name: 'resultados',
                        data: this.series_data2,
                        //Data Label
                        dataLabels: {
                            enabled: true,
                            //rotation: -90,
                            color: '#575757',
                            align: 'right',
                            format: '{point.y:.2f}'+'%', // two decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }],

            }
        },
        chartOptions3() {
            return {
                    chart: {  type: 'pie'},
                    title: {  text: this.title3 },
                    subtitle: { text: this.subtitle3 },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}: {point.y:.1f}%'
                            }
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                    },
                    series: [
                        {
                            name: "Browsers",
                            colorByPoint: true,
                            data: this.series_data3
                        }
                    ]
              }
        },
    },
    methods:{
        testIndicadores(event) {

            try {                   
                axios.get('/api/v1/Servicios_logisticos/get/test', {
           
                })
                .then(response => {

                    console.log('response fk data ',response);
                    if(response.data.success === true){
                    }else{
                        alert('No se encontraron datos');
                    }
                    
                })
            }catch(error) {
              alert(error+ '- Fallo en traer el listado de datos.');
            }
            
        },
        consultData(event, typeGraph) {

            event.preventDefault();

            if (typeGraph == 1) {

                var f1 = '';
                var f2 = '';
                
                if (this.form_consult_1 === '') {
                    alert('Error!! Seleccione Tipo De Consulta.');
                    return false;
                }
                if (this.data_range_1 === '') {
                    alert('Error!! Seleccione Rango De Tiempo.');
                    return false;
                }                
                if (this.fec_ini_1 !== '') {
                    f1 = moment(this.fec_ini_1).format('DD-MM-YYYY');
                }
                if (this.fec_fin_1 !== '') {
                    f2 = moment(this.fec_fin_1).format('DD-MM-YYYY');
                }

                this.indicatorsGraph1(this.id_user, this.form_consult_1, this.data_range_1, f1, f2);

            }else if(typeGraph == 2){
                
                if (this.form_consult_2 === '') {
                    alert('Error!! Seleccione Tipo De Consulta.');
                    return false;
                }
                if (this.month_frm === '') {
                    alert('Error!! Seleccione Mes.');
                    return false;
                }

                this.indicatorsGraph2(this.id_user, this.form_consult_2, this.month_frm);

            }else if(typeGraph == 3){

                var f1 = '';
                var f2 = '';
                
                if (this.form_consult_3 === '') {
                    alert('Error!! Seleccione Tipo De Consulta.');
                    return false;
                }
                if (this.data_range_3 === '') {
                    alert('Error!! Seleccione Rango De Tiempo.');
                    return false;
                }
                if (this.fec_ini_3 !== '') {
                    f1 = moment(this.fec_ini_3).format('DD-MM-YYYY');
                }
                if (this.fec_fin_3 !== '') {
                    f2 = moment(this.fec_fin_3).format('DD-MM-YYYY');
                }

                this.indicatorsGraph3(this.id_user, this.form_consult_3, this.data_range_3, f1, f2); 

            }                       

            /*setTimeout(function(){ 
                window.scrollTo(0,document.querySelector("#ref").scrollHeight); 
            }, 500);*/

        },
        indicatorsGraph1(id_user, tipo_consulta, rango, fecha_inicial, fecha_final) {
            try {
                this.loader_1 = true;
                this.graph_element_1 = false;                   
                axios.post('/api/v1/Servicios_logisticos/post/indicador/cumplimiento/historico', {
                    id_user: id_user,
                    tipo_consulta: tipo_consulta,
                    rango: rango,
                    fecha_inicial: fecha_inicial,
                    fecha_final: fecha_final
                })
                .then(response => {
                    console.log('response graph 1 ',response);
                    if (response.data.success === true) {

                        this.subtitle1 = 'Meta 95%';

                        this.series_data1 = response.data.data;
                        this.series_data1_table = response.data.data;

                        this.series_data1_table = this.series_data1_table.filter(fulfillment => fulfillment.fulfillment > 0);

                        //Condición para arreglar cantidad de contenedores
                        /*var length_res_g1 = this.series_data1_table.length;
                        var suma_cont = 0;
                        if(length_res_g1 == 1){
                            this.series_data1.forEach(function(series_data1){
                                suma_cont += series_data1.cont_total;
                            });
                            this.series_data1_table[0].cont_total = suma_cont;
                        }*/
                        //alert(suma_cont);

                        var cump_data_val = [];

                        this.series_data1.forEach(function(element) {
                            cump_data_val.push(
                                element.fulfillment
                            );
                        });

                        var month_data = [];

                        //this.series_data1.forEach(function(element) {
                        //Old Graph
                        this.series_data1_table.forEach(function(element) {
                            month_data.push({
                                'name': element.month,
                                'data': [element.fulfillment]
                            });
                        });
                        this.series_data1 = month_data;
                        //End Old Graph

                        /*var res_months = [];
                        this.series_data1_table.forEach(function(element) {
                            res_months.push(
                                element.month
                            );
                        });*/

                        this.series_data1_vals_new = cump_data_val;
                        console.log('series_data1_vals_new', this.series_data1_vals_new);

                        //this.res_months = res_months;

                        this.loader_1 = false;
                        this.graph_element_1 = true;

                    }else{
                        alert(response.data.message);
                    }
                })
            }catch(error) {
                this.loader_1 = false;
                this.graph_element_1 = true;
                alert(error+ '- Fallo en traer el listado de datos.');
            }
            setTimeout(()=>{
               this.loader_1 = false;
            },10000);   
        },
        indicatorsGraph2(id_user, tipo_consulta, mes) {
            try {
                this.loader_2 = true;
                this.graph_element_2 = false;                     
                axios.post('/api/v1/Servicios_logisticos/post/indicador/cumplimiento/acumulado', {
                    id_user: id_user,
                    tipo_consulta: tipo_consulta,
                    mes: mes
                })
                .then(response => {
                    console.log('response graph 2 ',response);
                    if (response.data.success === true) {
                        
                        this.subtitle2 = '% de participación';
                        this.series_data2 = response.data.data;

                        this.series_data2 = Object.entries(this.series_data2).map(([id, value]) => ({id, value}));

                        var result_data = [];

                        this.series_data2.forEach(function(element) {
                            result_data.push(
                                [element.value.label, element.value.fulfillment]
                            );
                        });

                        this.series_data2 = result_data;

                        this.loader_2 = false;
                        this.graph_element_2 = true;  

                    }else{
                        //this.loader_2 = false;
                        //this.graph_element_2 = true; 
                        alert(response.data.message);
                    }
                })
            }catch(error) {
                this.loader_2 = false;
                this.graph_element_2 = true; 
                alert(error+ '- Fallo en traer el listado de datos.');
            }
            setTimeout(()=>{
               this.loader_2 = false;
            },10000); 
        },
        indicatorsGraph3(id_user, tipo_consulta, rango, fecha_inicial, fecha_final) {
            try {
                this.loader_3 = true;
                this.graph_element_3 = false;                    
                axios.post('/api/v1/Servicios_logisticos/post/indicador/principales/causales', {
                    id_user: id_user,
                    tipo_consulta: tipo_consulta,
                    rango: rango,
                    fecha_inicial: fecha_inicial,
                    fecha_final: fecha_final
                })
                .then(response => {
                    console.log('response graph 3 ',response);
                    if (response.data.success === true) {
                        
                        //this.subtitle3 = 'Alimentos Finca';
                        this.series_data3 = response.data.data;

                        var result_data = [];

                        this.series_data3.forEach(function(element) {
                            result_data.push({
                                'name': element.label,
                                'y': element.percentage
                            });
                        });

                        this.series_data3 = result_data;

                        this.loader_3 = false;
                        this.graph_element_3 = true;  

                    }else{
                        alert(response.data.message);
                    }
                })
            }catch(error) {
                this.loader_3 = false;
                this.graph_element_3 = true;  
                alert(error+ '- Fallo en traer el listado de datos.');
            }
            setTimeout(()=>{
               this.loader_3 = false;
            },10000);  
        },
        validate_options: function() {

            if (this.form_consult_3 == 1) {
                this.filter_cump1 = true;
                this.filter_cump2 = false;
            }else{
                this.filter_cump1 = false;
                this.filter_cump2 = true;
            }

        },
        open_detail: function(event) {
            
            event.preventDefault();

            if (this.fec_ini_1 == '' || this.fec_fin_1 == '') {
                alert('Por favor seleccione la fecha inicial y fecha final para visualizar la consulta.');
            }else{
                var f1 = moment(this.fec_ini_1).format('DD-MM-YYYY');
                var f2 = moment(this.fec_fin_1).format('DD-MM-YYYY');
                window.location.href = '/detail_ind/data/'+f1+'/'+f2+'/'+this.id_user;
            }

        },
        open_detail_new: function(event) {
            
            event.preventDefault();

            if (this.id_user == '') {
                alert('No se reconoce el identificador del usuario!!');
            }else{

                var f1 = '';
                var f2 = '';

                if (this.fec_ini_1 !== '') {
                    f1 = moment(this.fec_ini_1).format('DD-MM-YYYY');
                }
                if (this.fec_fin_1 !== '') {
                    f2 = moment(this.fec_fin_1).format('DD-MM-YYYY');
                }

                try {                  
                    axios.post('/api/v1/Servicios_logisticos/post/indicador/cumplimiento/detailsIndIncumplimiento', {
                        date1: f1,
                        date2: f2,
                        id_user: this.id_user
                    })
                    .then(response => {
                        console.log('response detail ',response);
                        if (response.data.success === true) {
                            
                            this.res_details = response.data.data[0];
                            this.result_details = true;

                        }else{
                            this.result_details = false;
                        }
                    })
                }catch(error) {
                    alert(error+ '- Fallo en traer el listado de datos.');
                }

            }

        }

    },
});