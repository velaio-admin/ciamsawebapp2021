<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailToken extends Model
{
    public $table = "mail_tokens";
    protected $fillable = ['user_id', 'token', 'state'];
}
