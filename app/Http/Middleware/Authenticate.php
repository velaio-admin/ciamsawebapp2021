<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if ($request->expectsJson()) {

            return response()->json([
                "success" => false,
                "action" => 'Operación de login',
                "message" => 'La sesión ha caducado o no recibió el token de sesión o el token de sesión no es el correcto, inicie sesión nuevamente o revise el envío del token de autenticación.',
                "code" => 401,
                "data" => null
            ], 200);

        }else{
            return route('login');
        }
    }
}
