<?php

namespace App\Http\Middleware;

use Closure;

class CheckClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        abort_if(isset($request->cliente_id) && $request->cliente_id != \Auth::user()->identificacion, 403);

        abort_if(\Auth::user()->state != 1,403);

        return $next($request);
    }
}
