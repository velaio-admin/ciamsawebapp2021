<?php

namespace App\Http\Controllers\Api\Agricola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class AgricolaController extends Controller
{

	/**
    * Este servicio permite consultar los posibles estados de una factura.
    *
    * @return Single Response
    */
    public function getTypeInvoiceStates(){

        /* Validacion del Request */
        /*$array_vars = array('id_bl');

    	$validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
    	if (!$validation_request_var['bool']) {
    		return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
    	}*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
            'TOKEN' => env('TOKEN_SAP_AGRICOLA'),
        );

        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS27'), 'POST', null, $params);
        
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

     /**
    * Este servicio conocer el cupo de credito de un cliente en particular.
    * 
    *
    * @return Single Response
    */
    public function cupoCredito(Request $request){

        /* Validacion del Request */
        /*$array_vars = array('id_bl');

    	$validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
    	if (!$validation_request_var['bool']) {
    		return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
    	}*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
            'TOKEN' => env('TOKEN_SAP_AGRICOLA'),
            'ClientId' => $this->clienteid($request->header('idusuario')),
        );

        $response_tmp = $this->generalWebServiceSapWs(env('URL_USCIIWS30'), 'POST', null, $params);
        
        /* Fin consumo de servicio web */

        //Procesamiento adicional
        if (isset($response_tmp['meta']) && $response_tmp['meta']['code'] == 1) {
            $data = $response_tmp['data']['attributes'][0]['data'];

            //$cupo_credito_val = (int) str_replace(' ', '', $data[0][1]);
            $cupo_credito_val = str_replace(' ', '', $data[0][1]);


            if (!empty($cupo_credito_val)) {

                $cupo_credito_val_tmp = (int)str_replace('.', '', $data[0][1]);

                $per_credito_val = 100;

                //$cupo_utilizado_val = (int) str_replace(' ', '', $data[1][1]);
                $cupo_utilizado_val = str_replace(' ', '', $data[1][1]);
                $cupo_utilizado_val_tmp = (int)str_replace('.', '', $cupo_utilizado_val);
                $per_utilizado_val = ($cupo_utilizado_val_tmp / $cupo_credito_val_tmp) * 100;

                //$cupo_disponible_val = (int) str_replace(' ', '', $data[2][1]);
                $cupo_disponible_val = str_replace(' ', '', $data[2][1]);
                $cupo_disponible_val_tmp = (int)str_replace('.', '', $cupo_disponible_val);

                if(strpos($cupo_disponible_val,"-") !== false){
                    $cupo_disponible_val = 0;
                    $cupo_disponible_val_tmp = 0;
                }

                $per_disponible_val = (($cupo_disponible_val_tmp / $cupo_credito_val_tmp) * 100);
            } else {
                $cupo_credito_val = 0;
                $per_credito_val = 100;

                $cupo_utilizado_val = 0;
                $per_utilizado_val = 0;

                $cupo_disponible_val = 0;
                $per_disponible_val = 0;
            }

            //dd($cupo_credito_val,$cupo_utilizado_val,$cupo_disponible_val);

            $response = array(
                'credito_aprobado' => $cupo_credito_val,
                'per_credito_aprobado' => round($per_credito_val, 2),
                'credito_utilizado' => $cupo_utilizado_val,
                'per_credito_utilizado' => round($per_utilizado_val, 2),
                'credito_disponible' => $cupo_disponible_val,
                'per_credito_disponible' => round($per_disponible_val, 2),
            );

        } else {
            //dd('aqui no');
            $response = null;
        }


        /* Respuesta de servicio web */
        if (!empty($response)) {
            return $this->jsonResponse(true, 'Consumo de servicios web SAP validacion de response', 'Se ha generado información para mostrar.',0000,$response,200);
        }else{
            return $this->jsonResponse(false, 'Consumo de servicios web SAP validacion de response','No se generó información desde el servicio web para su busqueda.',0000,null,200);
        }
        /* Fin respuesta de servicio web */
    }

    public function infoReserva(Request $request){

        $cliente = $request->get('cliente'); 
        $pedido  = $request->get('pedido'); 

        $url = 'http://ciadev.ciamsa.com:8000/sap/bc/zenturnamiento/zbuga/zcons_pedido?';

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url, [
            'headers' => [
                          'token' => 'ABC123', 
                          'clienteid' => $cliente,
                          'pedido' => $pedido
                         ],
        ]);

        $result_tmp = $response->getBody()->getContents();
        //dd($result_tmp);
        $result = json_decode($result_tmp, true);

        if (isset($result['pedido'])) {
            return $this->jsonResponse(true, 'Consumo de servicios web SAP validacion de response', 'Se ha generado información para mostrar.',1,$result,200);
        }else{
            return $this->jsonResponse(false, 'Consumo de servicios web SAP validacion de response', 'Se ha generado información para mostrar.',0,$result,200);
        }        

    }

    public function nuevaReserva(Request $request){

        $cliente = $request->get('cliente'); 
        $pedido  = $request->get('pedido'); 
        
        $idPlaca  = $request->get('idPlaca'); 
        $idCcond  = $request->get('idCcond'); 
        $idTelcond  = $request->get('idTelcond'); 

        $idDate  = $request->get('idDate'); 
        $newDate = date("d-m-Y", strtotime($idDate));   

        $idHora  = $request->get('idHora').':00';

        $prods_arr  = $request->get('prods_arr'); 
        $prods_arr = json_decode($prods_arr, true);

        $headers = [
                      'token' => 'ABC123', 
                      'placa' => $idPlaca,
                      'conductor' => $idCcond,
                      'pedido' => $pedido,
                      'clienteid' => $cliente,
                      'fecha_est' => $newDate,
                      'hora_est' => $idHora,
                      'tel_conductor' => $idTelcond 
                    ];

        $url = 'http://ciadev.ciamsa.com:8000/sap/bc/zenturnamiento/zbuga/zregist_turno';

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, [
            'headers' => $headers,
            'json' => $prods_arr,
        ]);

        $result_tmp = $response->getBody()->getContents();
        $result_tmp = json_decode($result_tmp, true);
        $res_message = $result_tmp[0]['message'];

        if ($res_message !== '') {
            return $this->jsonResponse(true, 'Consumo de servicios web SAP validacion de response', $res_message, 1, $prods_arr, 200);
        }else{
            return $this->jsonResponse(false, 'Consumo de servicios web SAP validacion de response', 'No se pudo generar la reserva en el sistema.', 0, null, 200);
        }        

    }

     /**
    * Este servicio permite obtener la descarga de una factura.
    * 
    * @param integer $numero_factura Nombre: Numero de factura.
    *
    * @return Single Response
    */
    public function descargarFactura($numero_factura){

        /* Validacion del Request */
        /*$array_vars = array('numero_factura');

    	$validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
    	if (!$validation_request_var['bool']) {
    		return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
    	}*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA'),
            'NumeroFactura' => $numero_factura,
        );

        $response_tmp = $this->generalWebServiceSapWs(env('URL_USCIIWS29'), 'POST', null, $params);
        
        if (isset($response_tmp['data']['attributes'][0]['data'][0][0]) && $response_tmp['data']['attributes'][0]['data'][0][0] != '') {
            $pdf = $response_tmp['data']['attributes'][0]['data'][0][0];

            // Real date format (xxx-xx-xx)
            $toDay = date('Y-m-d');

            // we give the file a random name
            $name = 'invoice_'.$toDay.'_'.$numero_factura.'.pdf';

            // a route is created, (it must already be created in its repository(pdf)).
            $rute = 'tmp/'.$name;

            // decode base64
            $pdf_b64 = base64_decode($pdf);

            // you record the file in existing folder
            if (file_put_contents($rute, $pdf_b64)) {
                //just to force download by the browser
                $headers = [
                    'Content-Type' => 'application/pdf',
                ];

                //print base64 decoded
                $response = response()->file($rute, $headers);

                $response = env('APP_URL').'/'.$rute;
            }
            
        } else {
            $response = null;
        }
        
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        if (!empty($response)) {
            return $this->jsonResponse(true, 'Consumo de servicios web SAP validacion de response', 'Se ha generado información para mostrar.',0000,$response,200);
        }else{
            return $this->jsonResponse(false, 'Consumo de servicios web SAP validacion de response','No se generó información desde el servicio web para su busqueda.',0000,null,200);
        }
        /* Fin respuesta de servicio web */
    }

    /**
    * Este servicio permite obtener la informacion del historial de facturas.
    * 
    * @param string $numero (Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.
    * @param string $fechainicial (Opcional) Nombre: Fecha inicial. Es la fecha de inicio para consultar la información en un rango determinado de tiempo.
    * @param string $fechafinal (Opcional) Nombre: Fecha final. Es la fecha de finalización para consultar la información en un rango determinado de tiempo.
    * @param integer $pagina (Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.
    * @param integer $cant_pag (Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.
    *
    * @return Single Response
    */
    public function historialFacturas(Request $request){
        
        /* Validacion del Request */
        /*$array_vars = array('estado');

        $validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
        if (!$validation_request_var['bool']) {
            return $this->jsonResponse(false, 'Operacion de validacion de request',$validation_request_var['msj'],0000,null,200);
        }*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
      	$params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA'),
            'ClientId' => $this->clienteid($request->header('idusuario')),
            'Numerofactura' => (!empty($request->get('numero')))? $request->get('numero'): null,
            'Fechainicial' => (!empty($request->get('fechainicial')))?  GeneralController::cleanDatesWs($request->get('fechainicial')): Carbon::now()->subMonths(12)->format('d/m/Y'),
            'Fechafinal' => (!empty($request->get('fechafinal')))? GeneralController::cleanDatesWs($request->get('fechafinal')): Carbon::now()->format('d/m/Y'),
            'PaginaActual' => (!empty($request->get('pagina')))? $request->get('pagina') : 1,
            'NumeroRegistrosPorPagina' => (!empty($request->get('cant_pag')))? $request->get('cant_pag') : 50,
        );

        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS31'),"POST",null,$params);
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */ 
    }

    /**
    * Este servicio permite obtener la información de detalle del BL.
    * 
    * @param integer $pagina (Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.
    * @param integer $cant_pag (Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.
    *
    * @return Single Response
    */
    public function consultaPedido(Request $request){

        /* Validacion del Request */
        /*$array_vars = array('id_bl');

    	$validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
    	if (!$validation_request_var['bool']) {
    		return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
    	}*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
       	$params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA'),
            'ClientId' => $this->clienteid($request->header('idusuario')),
            'PaginaActual' => (!empty($request->get('pagina')))? $request->get('pagina') : 1,
            'NumeroRegistrosPorPagina' => (!empty($request->get('cant_pag')))? $request->get('cant_pag') : 50,
        );
        
        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS32'),"POST",null,$params);
        
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

    /**
    * Este servicio permite obtener la información de detalle del BL.
    * 
    * @param integer $id_pedido (Opcional) Nombre: Id Pedido. Es el numero del pedido a consultar.
    *
    * @return Single Response
    */
    public function detallePedido($id_pedido){

        /* Validacion del Request */
        /* $array_vars = array('id_pedido');

    	$validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
    	if (!$validation_request_var['bool']) {
    		return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
    	}*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA'),
            'idpedido' => $id_pedido,
        );
        
        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS33'),"POST",null,$params);
        
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

    /**
    * Este servicio permite obtener la información de todos los productos disponibles para la solicitud de pedido.
    *
    * @return Single Response
    */
    public function productos(){

        /* Validacion del Request */
        /*$array_vars = array('id_pedido');

        $validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
        if (!$validation_request_var['bool']) {
            return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
        }*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA')
        );
        
        $response = $this->generalWebServiceSap(env('URL_USCIIWS40'),"POST",null,$params);
        
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

    /**
    * Este servicio permite obtener la información de todos las formas de pago disponibles.
    *
    * @return Single Response
    */
    public function formasDePago(Request $request){

        /* Validacion del Request */
        /*$array_vars = array('id_pedido');

        $validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
        if (!$validation_request_var['bool']) {
            return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
        }*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA'),
            'Clienteid' => $this->clienteid($request->header('idusuario')),
        );
        
        //$response = $this->generalWebServiceSap(env('URL_USCIIWS38'),"POST",null,$params);
        /* Fin consumo de servicio web */
$formaDePago_tmp = '{
    "meta": {
        "totalPages": 1,
        "currentPage": 1,
        "code": 1,
        "message": " "
    },
    "data": {
        "type": "formasPago",
        "attributes": [{
            "0": "nombreFormaPago",
            "data": [{
                "Z004": "30 DIAS",
                "Z006": "60 DIAS"
            }]
        }]
    }
}';
$response = json_decode($formaDePago_tmp);
        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

    /**
    * Este servicio permite obtener la información de todas las ciudades disponibles.
    *
    * @return Single Response
    */
    public function ciudades(){

        /* Validacion del Request */
        /*$array_vars = array('id_pedido');

        $validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
        if (!$validation_request_var['bool']) {
            return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
        }*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA')
        );
        
        $response = $this->generalWebServiceSap(env('URL_USCIIWS37'),"POST",null,$params);
        
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

    /**
    * Este servicio permite radicar una solicitud de pedido.
    * 
    * @param string $productos Nombre: Array de productos como STRING. Es el array de productos como STRING que contienen los codigos de material, cantidades y tipo de pedido que se va a solicitar.
    *
    * @return Single Response
    */
    public function radicarPedido(Request $request){

        if (!empty($request->get('productos'))) {

            //$json = json_decode($request->get('productos'));

            $params = array(
                'Token' => env('TOKEN_SAP_AGRICOLA'),
                'ClientId' => $this->clienteid($request->header('idusuario')),
                'productos' => $request->get('productos')
            );

            $response = $this->generalWebServiceSapWs(env('URL_USCIIWS41'), 'POST', null, $params);
            
            if (isset($response[0]['code']) && $response[0]['code'] == 1) {
                return $this->jsonResponse(true, 'Operación de radicacion de pedido',  $response[0]['message'], 4, null, 200);
            } else {
                return $this->jsonResponse(false, 'Operación de radicacion de pedido', $response[0]['message'], 5, null, 200);
            }
        } else {
            return $this->jsonResponse(false, 'Operación de radicacion de pedido', 'No se ha recibido la informacion de productos por radicar.', 6, null, 200);
        }
    }

    /**
    * Este servicio permite notificar una solicitud de pedido no efectuada.
    *
    * @return Single Response
    */
    public function notificarPedido(Request $request){

        $user = $this->clienteid($request->header('idusuario'));  

        if (!empty($user)) {
            if (!empty($request->get('productos'))) {

                $json = json_decode($request->get('productos'));

                if (sizeof($json->productos) > 0) {

                   $params = array(
                        'Token' => env('TOKEN_SAP_AGRICOLA'),
                        'ClientId' => $user,
                        'productos' => $request->get('productos')
                    );

                    $response = $this->generalWebServiceSapWs(env('URL_USCIIWS46'), 'POST', null, $params);
                    
                    if (isset($response[0]['code']) && $response[0]['code'] == 1) {
                        return $this->jsonResponse(true, 'Operación de radicacion de pedido',  $response[0]['message'], 4, null, 200);
                    } else {
                        return $this->jsonResponse(false, 'Operación de radicacion de pedido', $response[0]['message'], 5, null, 200);
                    } 

                }else{
                   return $this->jsonResponse(false, 'Operación de radicacion de pedido', 'No se recibió ningún producto', 7, null, 200); 
                }
            }else{
                return $this->jsonResponse(false, 'Operación de radicacion de pedido', 'No se recibió un array de productos', 6, null, 200);
            }
        } else {
            return $this->jsonResponse(false, 'Operación de radicacion de pedido', 'No se ha recibido la informacion de productos por radicar.', 8, null, 200);
        }

    }

}