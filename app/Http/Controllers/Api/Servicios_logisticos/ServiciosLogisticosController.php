<?php

namespace App\Http\Controllers\Api\Servicios_logisticos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Controllers\GeneralController;
use Illuminate\Support\Facades\Auth;

class ServiciosLogisticosController extends Controller
{   
    /**
    * Este servicio permite obtener la información de detalle del BL.
    * 
    * @param string $id_bl Nombre: Id del BL. Es el numero de BL.
    *
    * @return Single Response
    */
    public function getDetalleBl(Request $request){

        /* Validacion del Request */
        $array_vars = array('id_bl');

        $validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
        if (!$validation_request_var['bool']) {
            return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
        }
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
                        'token' => env('TOKEN_SAP'),
                        'id_bl' => $request->get('id_bl'),
                        'clienteid' => $this->clienteid($request->header('idusuario'))
                        );
        
        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS21'),"POST",null,$params);
        
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

    /**
    * Este servicio permite obtener la información de detalle del Contenedor.
    * 
    * @param string $id_contenedor Nombre: Id del Contenedor. Es el numero de Contenedor.
    *
    * @return Single Response
    */
    public function getDetalleContenedor(Request $request){

        /* Validacion del Request */
        $array_vars = array('id_contenedor');

        $validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
        if (!$validation_request_var['bool']) {
            return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
        }
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
                        'token' => env('TOKEN_SAP'),
                        'id_cont' => $request->get('id_contenedor'),
                        'clienteid' => $this->clienteid($request->header('idusuario'))
                        );

        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS22'),"POST",null,$params);
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */ 
    }

    /**
    * Este servicio permite obtener la información del rastreo de bls segun la identificación del usuario y tambien realizar una busqueda especifica por medio de los parametros opcionales.
    * 
    * @param string $estado Nombre: Estado. Es el filtro de estado con el que se buscará la información, los posibles estados son TRANSITO (transito), PROCESO (proceso) y PROC_FINAL (proceso finalizado).
    * @param string $numero (Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.
    * @param string $type (Opcional) Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son bls(son los BL) y dos(son los DO).
    * @param string $fechainicial (Opcional) Nombre: Fecha inicial. Es la fecha de inicio para consultar la información en un rango determinado de tiempo.
    * @param string $fechafinal (Opcional) Nombre: Fecha final. Es la fecha de finalización para consultar la información en un rango determinado de tiempo.
    * @param integer $pagina (Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.
    * @param integer $cant_pag (Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.
    *
    * @return Single Response
    */
    public function rastreoList(Request $request){
        
        /* Validacion del Request */
        $array_vars = array('estado');

        $validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
        if (!$validation_request_var['bool']) {
            return $this->jsonResponse(false, 'Operacion de validacion de request',$validation_request_var['msj'],0000,null,200);
        }
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
                        'token' => env('TOKEN_SAP'),
                        'estado' => $request->get('estado'),
                        'clienteid' => $this->clienteid($request->header('idusuario')),
                        'bls' => ($request->get('type') == 'bls' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'dos' => ($request->get('type') == 'dos' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'fechainicial' => (!empty($request->get('fechainicial')))?  GeneralController::cleanDatesWs($request->get('fechainicial')): null,
                        'fechafinal' => (!empty($request->get('fechafinal')))? GeneralController::cleanDatesWs($request->get('fechafinal')): null,
                        'paginaactual' => (!empty($request->get('pagina')))? $request->get('pagina') : 1 ,
                        'numeroregistrosporpagina' => (!empty($request->get('cant_pag')))? $request->get('cant_pag') : 50
                        );

        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS20'),"POST",null,$params);
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */ 
    }

    /**
    * Este servicio permite obtener la información de despachos de mercancia de BLs o DOs segun la identificación del usuario y tambien realizar una busqueda especifica por medio de los parametros opcionales, este servicio por defecto trae la información de los ultimos 90 dias.
    * 
    * @param string $numero (Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.
    * @param string $type (Opcional) Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son bls(son los BL) y dos(son los DO).
    * @param string $fechainicial (Opcional) Nombre: Fecha inicial. Es la fecha de inicio para consultar la información en un rango determinado de tiempo.
    * @param string $fechafinal (Opcional) Nombre: Fecha final. Es la fecha de finalización para consultar la información en un rango determinado de tiempo.
    * @param integer $pagina (Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.
    * @param integer $cant_pag (Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.
    *
    * @return Single Response
    */
    public function despachoMercancia(Request $request){

        /* Validacion del Request (Esta comentado porque no hay valores requeridos, solo opcionales)*/
        /*$array_vars = array();

        $validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
        if (!$validation_request_var['bool']) {
            return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
        }*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
                        'token' => env('TOKEN_SAP'),
                        'bls' => ($request->get('type') == 'bls' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'dos' => ($request->get('type') == 'dos' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'clienteid' => $this->clienteid($request->header('idusuario')),
                        'fechainicial' => (!empty($request->get('fechainicial')))? GeneralController::cleanDatesWs($request->get('fechainicial')): Carbon::now()->subMonths(3)->format('d/m/Y'),
                        'fechafinal' => (!empty($request->get('fechafinal')))? GeneralController::cleanDatesWs($request->get('fechafinal')): Carbon::now()->format('d/m/Y'),
                        'paginaactual' => (!empty($request->get('pagina')))? $request->get('pagina') : 1 ,
                        'numeroregistrosporpagina' => (!empty($request->get('cant_pag')))? $request->get('cant_pag') : 50
                        );

        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS24'),"POST",null,$params);
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */ 
    }

    /**
    * Este servicio permite obtener la información de las fechas limites de devolucion de contenedores de BLs o DOs segun la identificación del usuario y tambien realizar una busqueda especifica por medio de los parametros opcionales, este servicio por defecto trae la información de los ultimos 90 dias.
    * 
    * @param string $numero (Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.
    * @param string $type (Opcional) Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son bls(son los BL) y dos(son los DO).
    * @param string $fechainicial (Opcional) Nombre: Fecha inicial. Es la fecha de inicio para consultar la información en un rango determinado de tiempo.
    * @param string $fechafinal (Opcional) Nombre: Fecha final. Es la fecha de finalización para consultar la información en un rango determinado de tiempo.
    * @param integer $pagina (Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.
    * @param integer $cant_pag (Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.
    *
    * @return Single Response
    */
    public function fechaLimiteDevolucionContenedor(Request $request){

        /* Validacion del Request (Esta comentado porque no hay valores requeridos, solo opcionales)*/
        /*$array_vars = array();

        $validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
        if (!$validation_request_var['bool']) {
            return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
        }*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
                        'token' => env('TOKEN_SAP'),
                        'bls' => ($request->get('type') == 'bls' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'dos' => ($request->get('type') == 'dos' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'fechainicial' => (!empty($request->get('fechainicial')))? GeneralController::cleanDatesWs($request->get('fechainicial')): Carbon::now()->subMonths(3)->format('d/m/Y'),
                        'fechafinal' => (!empty($request->get('fechafinal')))? GeneralController::cleanDatesWs($request->get('fechafinal')): Carbon::now()->format('d/m/Y'),
                        'clienteid' => $this->clienteid($request->header('idusuario')),
                        'paginaactual' => (!empty($request->get('pagina')))? $request->get('pagina') : 1 ,
                        'numeroregistrosporpagina' => (!empty($request->get('cant_pag')))? $request->get('cant_pag') : 50
                        );

        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS26'),"POST",null,$params);
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */ 
         
    }

    /**
    * Este servicio permite obtener la información de existencia de mercancia de BLs o DOs segun la identificación del usuario y tambien realizar una busqueda especifica por medio de los parametros opcionales, este servicio por defecto trae la información de los ultimos 90 dias.
    * 
    * @param string $numero (Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.
    * @param string $fechainicial (Opcional) Nombre: Fecha inicial. Es la fecha de inicio para consultar la información en un rango determinado de tiempo.
    * @param string $fechafinal (Opcional) Nombre: Fecha final. Es la fecha de finalización para consultar la información en un rango determinado de tiempo.
    * @param integer $pagina (Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.
    * @param integer $cant_pag (Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.
    *
    * @return Single Response
    */
    public function existenciaMercancia(Request $request){

        /* Validacion del Request (Esta comentado porque no hay valores requeridos, solo opcionales)*/
        /*$array_vars = array();

        $validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
        if (!$validation_request_var['bool']) {
            return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
        }*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
                        'token' => env('TOKEN_SAP'),
                        'fechainicial' => (!empty($request->get('fechainicial')))? GeneralController::cleanDatesWs($request->get('fechainicial')): Carbon::now()->subMonths(3)->format('d/m/Y'),
                        'fechafinal' => (!empty($request->get('fechafinal')))? GeneralController::cleanDatesWs($request->get('fechafinal')): Carbon::now()->format('d/m/Y'),
                        'bls' => ($request->get('type') == 'bls' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'dos' => ($request->get('type') == 'dos' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'clienteid' => $this->clienteid($request->header('idusuario')),
                        'paginaactual' => (!empty($request->get('pagina')))? $request->get('pagina') : 1 ,
                        'numeroregistrosporpagina' => (!empty($request->get('cant_pag')))? $request->get('cant_pag') : 50
                        );

        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS23'),"POST",null,$params);
        /* Fin consumo de servicio web */
        
        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */ 
    }

    /**
    * Este servicio permite ingresar las observaciones a un BL o un Contenedor.
    * 
    * @param string $id_cliente Nombre: Id del cliente. Es el numero de cedula o nit del consultante.
    * @param string $observacion Nombre: Observaciónes. Son las observaciones que se van a incluir en el BL o Contenedor
    * @param string $tipo Nombre: Tipo. Es el tipo de objeto al cual le adicionaremos la observacion, sea un BL o Contenedor, las opciones son bl y cont.
    * @param string $id Nombre: Id. Es el id del BL o el Contenedor.
    *
    * @return Single Response
    */
    public function observations(Request $request){

        if (!empty($request->get('observacion')) && !empty($request->get('id')) && !empty($request->get('tipo')) && !empty($request->get('id_cliente'))) {

            $params = array(
                        'token' => env('TOKEN_SAP'),
                        'id_cliente' => $request->get('id_cliente'),
                        'observacion' => $request->get('observacion'),
                        'id_bl' => ($request->get('tipo') == 'bl') ? $request->get('id') : null,
                        'id_cont' => ($request->get('tipo') == 'cont') ? $request->get('id') : null
                        );

            $response = $this->generalWebServiceSap(env('URL_USCIIWS49'),"POST",null,$params);

            if($response['meta']['code'] == "1"){
                return $this->jsonResponse(true,'Operación de observaciones','Se ha guardado correctamente la observación.',3,null,200);
            }else{
                return $this->jsonResponse(false,'Operación de observaciones',$response['meta']['message'],2,null,200);
            }
        }else{
            return $this->jsonResponse(false,'Operación de observaciones','No se recibieron los parametros necesarios para la operación.',1,null,200);
        }
    }

    //Nuevo servicio para obtener informaciòn de los BL
    public function getBlContenedor($client_id){

        $url = env('URL_USCIIWS57')."token=".env('TOKEN_SAP')."&clienteid=".$client_id;

                $client = new \GuzzleHttp\Client();
                $result_tmp = $client->get($url);
                $result_tmp2 = $result_tmp->getBody()->getContents();
                $result = json_decode($result_tmp2);

        if (!empty($result)) {
            $array = [];
            $array['meta'] = [];
            $array[0] = $result;
        }

        //dd($array);

        return $this->responseGeneralWs($array);

    }

    public function getClientsData(){

        $params = array(
                        'token' => env('TOKEN_SAP')
                        );

        $response = $this->generalWebServiceSap(env('URL_USCIIWS61'),"POST",null,$params);

        if (!empty($response)) {
            $array = [];
            $array['meta'] = [];
            $array[0] = $response[0];
        }

        return $this->responseGeneralWs($array);

    }

    public function getDetailCont(Request $request){

        if (!empty($request->get('id_bl'))){

            $params = array(
                'id_bl' => $request->get('id_bl')
            );
            
            $response = $this->generalWebServiceSap(env('URL_USCIIWS58'),"POST",null,$params);
            
            /* Fin consumo de servicio web */

            if (!empty($response)) {
                $array = [];
                $array['meta'] = [];
                $array[0] = $response;
            }

            return $this->responseGeneralWs($array);

        }else{
            return $this->jsonResponse(false,'Operación de consulta','No se recibieron los parametros necesarios para la operación.',1,null,200);
        }        

    }

    public function getBlContStates(Request $request){

        $params = array(
            'tipo' => $request->get('tipo')
        );

        $response = $this->generalWebServiceSap(env('URL_USCIIWS59'),"POST",null,$params);

        if (!empty($response)) {
            $array = [];
            $array['meta'] = [];
            $array[0] = $response;
        }

        return $this->responseGeneralWs($array);

    }

    public function updateBlCont(Request $request){

        if ($request->get('consult') !== '') {

            if ($request->get('tipo') == 'BL') {
                $params = array(
                    'token' => env('TOKEN_SAP'),
                    'id' => $request->get('id'),
                    'tipo' => $request->get('tipo'),
                    'status' => $request->get('status'),
                    'observacion' => $request->get('observacion')
                );

                $response = $this->generalWebServiceSap(env('URL_USCIIWS60'),"POST",null,$params);
                return $this->responseGeneralWs($response);
            }

            if ($request->get('tipo') == 'CONTENEDOR') {
                $params = array(
                    'token' => env('TOKEN_SAP'),
                    'id' => $request->get('id'),
                    'tipo' => $request->get('tipo'),
                    'status' => $request->get('status'),
                    'observacion' => $request->get('observacion')
                );

                $response = $this->generalWebServiceSap(env('URL_USCIIWS60'),"POST",null,$params);
                return $this->responseGeneralWs($response);
            }

        }else {

            return $this->jsonResponse(false,'Operación de consulta','No se recibieron los parametros necesarios para la operación.',1,null,200);

        }

    }

    public function getBlAvalibleTran(Request $request){

        $params = array(
            'token' => env('TOKEN_SAP'),
            'estado' => 'TRANSITO',
            'clienteid' => $request->get('id_client')
        );

        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS20'),"POST",null,$params);

        return $this->responseGeneralWs($response);

    }
    public function getBlAvaliblePro(Request $request){

        $params = array(
            'token' => env('TOKEN_SAP'),
            'estado' => 'PROCESO',
            'clienteid' => $request->get('id_client')
        );

        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS20'),"POST",null,$params);

        return $this->responseGeneralWs($response);

    }

    /**
    * Este servicio permite generar la informacion especifica de Ciamsa para sus clientes de forma semaforizada.
    * 
    * @param integer $id_user Nombre: Id del cliente. Es id de base de datos del usuario.
    * @param string $fecha_inicial Nombre: Fecha inicial. Fecha inicial desde donde se quiere obtener la informacion
    * @param string $fecha_final Nombre: Fecha final. Fecha final desde donde se quiere obtener la informacion
    *
    * @return Single Response
    */
    public function semaforos(Request $request){

        if (!empty($request->get('id_user')) && !empty($request->get('identification'))) {

            /*$data_maestra = '{
                "meta": {
                    "totalPages": 1,
                    "currentPage": 1,
                    "code": 1,
                    "message": " "
                },
                "data": {
                    "type": "detallePedido",
                    "attributes": [{
                        "0": "Numero_BL",
                        "1": "Contenedor",
                        "2": "Nombre_Cliente",
                        "3": "Linea_naviera",
                        "4": "Motonave",
                        "5": "Numero_Viaje",
                        "6": "Fecha_Descargue_Motonave",
                        "7": "ETA",
                        "8": "Puerto_de_Descargue",
                        "9": "Fecha_Firma_Planilla_de_envio",
                        "10": "Fecha_Recibo_Doc_De_Traslado",
                        "11": "Hora_Recibo_Doc",
                        "12": "Fecha_Traslado_a_Deposito",
                        "13": "Fecha_Ingreso_a_Muisca",
                        "14": "Fecha_Devolución_Cont",
                        "15": "Causal_Incumplimiento",
                        "16": "Tipo_de_contenedor",
                        "data": [
                            {
                                "0": "NGBS18172431",
                                "1": "MSKU7227603",
                                "2": "CENCOSUD COLOMBIA S.A.",
                                "3": "HAMBURGSUD",
                                "4": "CCNI ARAUCO",
                                "5": "838E",
                                "6": "16/10/2018",
                                "7": "16/10/2018",
                                "8": "TCBUEN",
                                "9": "26/10/2018",
                                "10": "2/11/2018",
                                "11": "16:00:00",
                                "12": "3/11/2018",
                                "13": "3/11/2018",
                                "14": "17/11/2018",
                                "15": "Motonave atrasada",
                                "16": "20"
                            },
                            {
                                "0": "777NGBS18",
                                "1": "7777227603",
                                "2": "EMPRESA COLOMBIA S.A.",
                                "3": "HAMBURGSUD",
                                "4": "CCNI ARAUCO",
                                "5": "838E",
                                "6": "16/10/2018",
                                "7": "16/10/2018",
                                "8": "TCBUEN",
                                "9": "26/10/2018",
                                "10": "2/11/2018",
                                "11": "16:00:00",
                                "12": "3/11/2018",
                                "13": "3/11/2018",
                                "14": "17/11/2018",
                                "15": "Motonave atrasada",
                                "16": "21"
                            },
                            {
                                "0": "125DSSDSD8",
                                "1": "SS1S1S1S",
                                "2": "DELTEC S.A",
                                "3": "DELTEC 123",
                                "4": "CCNI ARAUCO",
                                "5": "838E",
                                "6": "16/10/2018",
                                "7": "16/10/2018",
                                "8": "TCBUEN",
                                "9": "26/10/2018",
                                "10": "2/11/2018",
                                "11": "16:00:00",
                                "12": "3/11/2018",
                                "13": "3/11/2018",
                                "14": "17/11/2018",
                                "15": "Motonave DEC",
                                "16": "22"
                            }
                        ]
                    }]
                }
            }';

            $data_maestra_new = (array)json_decode($data_maestra,true);*/

            $clienteid = $request->get('identification');

            $cur_day = '01';
            $month = date('m');
            $year = date('Y');

            if(empty($request->get('fechainicial'))){
              $fecha_inicial = $cur_day.'-'.$month.'-'.$year;  
            }else{
               $fecha_inicial = $request->get('fechainicial'); 
            }

            if(empty($request->get('fechafinal'))){
              $f = carbon::now();
              $fecha_final = $f->format('d-m-Y');
            }else{
               $fecha_final = $request->get('fechafinal'); 
            }

            $url = env('URL_USCIIWS62')."token=".env('TOKEN_SAP')."&clienteid=".$clienteid."&fechainicial=".$fecha_inicial."&fechafinal=".$fecha_final;

            $client = new \GuzzleHttp\Client();
            $result_tmp = $client->get($url);  
            $result_tmp2 = $result_tmp->getBody()->getContents();

            //return $result_tmp2;
            
            $result = json_decode($result_tmp2,true);
            $data_maestra_new = $result;

            //return $data_maestra_new;
        
            if(isset($data_maestra_new['meta'])){

                $data_maestra_new['data']['attributes'][0][] = 'Tiempo_Global';
                $data_maestra_new['data']['attributes'][0][] = 'Tiempo_Deposito';
                $data_maestra_new['data']['attributes'][0][] = 'Rango_Global';
                $data_maestra_new['data']['attributes'][0][] = 'Rango_Deposito';
                $data_maestra_new['data']['attributes'][0][] = 'Firma_Planilla';
                $data_maestra_new['data']['attributes'][0][] = 'Finalizacion_Entrega';
                $data_maestra_new['data']['attributes'][0][] = 'Traslado';
                $data_maestra_new['data']['attributes'][0][] = 'Mes';
                $data_maestra_new['data']['attributes'][0][] = 'Fecha_entrega_de_Docs_a_la_agencia';
                $data_maestra_new['data']['attributes'][0][] = 'Tiempo_firma_de_planilla';
                $data_maestra_new['data']['attributes'][0][] = 'Tiempo_entrega_de_planilla';
                //$data_maestra_new['data']['attributes'][0][] = 'Dias_de_traslado_al_Depósito';
                $data_maestra_new['data']['attributes'][0][] = 'Dias_MUISCA';
                $data_maestra_new['data']['attributes'][0][] = 'Dias_en_puerto';
                $data_maestra_new['data']['attributes'][0][] = 'Cump_Comp_Tras';
                $data_maestra_new['data']['attributes'][0][] = 'Cump_Comp_Tras_Est';
                //$data_maestra_new['data']['attributes'][0][] = 'test_fk';

                foreach ($data_maestra_new['data']['attributes'][0]['data'] as $key => $value) {

                    //Calculo Tiempo_global
                    //$calc_17 = $this->calculateDate($value[6],$value[12],"Sin Fecha Descar.");
                    $calc_17 = $this->calculateDate($value[13],$value[19],"Sin Fecha Descar.");

                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['17'] = $calc_17;
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['26'] = $calc_17;

                    //Tiempo_deposito
                    //$calc_18 = $this->calculateDate($value[10],$value[12],"Sin Fecha Rec. Doc");
                    $calc_18 = $this->calculateDate($value[17],$value[19],"Sin Fecha Rec. Doc");

                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['18']  = $calc_18;
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['27']  = $calc_18;

                    //Rango_global
                    $Rango_global = "";
                    if ($calc_17 == "" || $calc_17 == "No hay datos") {
                        $Rango_global = "";
                    }elseif ($calc_17 < 0) {
                        $Rango_global = "Verificar";
                    }elseif ($calc_17 >= 0 && $calc_17 <= 3) {
                        $Rango_global = "Entre 0-3 dias";
                    }elseif ($calc_17 > 3 && $calc_17 < 6) {
                        $Rango_global = "Entre 4-5 dias";
                    }elseif ($calc_17 > 5 && $calc_17 <= 6) {
                        $Rango_global = "6 dias";
                    }elseif ($calc_17 >= 7 && $calc_17 <= 8) {
                        $Rango_global = "Entre 7-8 dias";
                    }else{
                        $Rango_global = "Más de 8 dias";
                    }

                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['19']  = $Rango_global;
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['28']  = $Rango_global;

                    //Rango_deposito
                    $Rango_deposito = "";
                    if ($calc_18 == "" || $calc_18 == "No hay datos") {
                        $Rango_deposito = "";
                    }elseif ($calc_18 < 0) {
                        $Rango_deposito = "Verificar";
                    }elseif ($calc_18 >= 0 && $calc_18 <= 2) {
                        $Rango_deposito = "Entre 0-2 dias";
                    }elseif ($calc_18 > 2 && $calc_18 < 4) {
                        $Rango_deposito = "3 dias";
                    }elseif ($calc_18 >= 4 && $calc_18 < 5) {
                        $Rango_deposito = "4 dias";
                    }else{
                        $Rango_deposito = "Más de 4 dias";
                    }

                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['20']  = $Rango_deposito;
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['29']  = $Rango_deposito;

                    //Firma Planilla
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['21']  = $this->calculateDate($value[7],$value[9],"Sin Fecha Firm. Pla");

                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['28']  = $this->calculateDate($value[7],$value[9],"Sin Fecha Firm. Pla");
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['30']  = $this->calculateDate($value[14],$value[16],"Sin Fecha Firm. Pla");

                    //Finalizacion/Entrega
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['22']  = $this->calculateDate($value[10],$value[6],"");

                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['29']  = $this->calculateDate($value[10],$value[6],"");
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['31']  = $this->calculateDate($value[17],$value[13],"");

                    //Traslado - Traslado1
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['23']  = $this->calculateDate($value[10],$value[12],"");
                    
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['30']  = $this->calculateDate($value[10],$value[12],"");
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['32']  = $this->calculateDate($value[17],$value[19],"");

                    //Mes
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['24']  = $this->stringMonth(Carbon::parse(str_replace("/", "-", $value[6]))->format('F'));
                    
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['31']  = $this->stringMonth(Carbon::parse(str_replace("/", "-", $value[6]))->format('F'));
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['33']  = $this->stringMonth(Carbon::parse(str_replace("/", "-", $value[13]))->format('F'));

                    //Fecha_entrega_de_Docs_a_la_agencia
                    //$calc_25 = $this->calculateDate($value[7],Carbon::now()->format('d/m/Y'),"Sin Fecha ETA");
                    $calc_25 = $this->calculateDate($value[14],Carbon::now()->format('d/m/Y'),"Sin Fecha ETA");

                    if ($calc_25 >= 7) {
                        $Fecha_entrega_de_Docs_a_la_agencia = "A TIEMPO";
                    }elseif ($calc_25 == 6) {
                        $Fecha_entrega_de_Docs_a_la_agencia = "PROXIMO";
                    }else{
                        $Fecha_entrega_de_Docs_a_la_agencia = "URGENTE";
                    }

                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['25'] = $Fecha_entrega_de_Docs_a_la_agencia;
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['34'] = $Fecha_entrega_de_Docs_a_la_agencia;

                    //Tiempo_firma_de_planilla
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['26']  = $this->calculateDate($value[9],$value[6],"Sin Fecha Firm. Pla");
                    
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['33']  = $this->calculateDate($value[9],$value[6],"Sin Fecha Firm. Pla");
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['35']  = $this->calculateDate($value[16],$value[13],"Sin Fecha Firm. Pla");

                    //Tiempo_entrega_de_planilla
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['27']  = $this->calculateDate($value[10],$value[6],"Sin Fecha Recibo. Doc");
                    
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['34']  = $this->calculateDate($value[10],$value[6],"Sin Fecha Recibo. Doc");
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['36']  = $this->calculateDate($value[17],$value[13],"Sin Fecha Recibo. Doc");

                    //Dias_de_traslado_al_Depósito - Traslado2
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['28']  = $this->calculateDate($value[12],$value[10],"");
                    /*$data_maestra_new['data']['attributes'][0]['data'][$key]['37']  = $this->calculateDate($value[19],$value[17],"");*/

                    //Dias_MUISCA
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['29']  = $this->calculateDate($value[13],$value[12],"");
                    
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['36']  = $this->calculateDate($value[13],$value[12],"");
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['37']  = $this->calculateDate($value[20],$value[19],"");

                    //Dias_en_puerto
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['30']  = $this->calculateDate($value[12],$value[6],"");
                    
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['37']  = $this->calculateDate($value[12],$value[6],"");
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['38']  = $this->calculateDate($value[19],$value[13],"");

                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['31']  = 1749;

                    //Cumplimiento Compromiso Traslado
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['39']  = $this->calculateDate($value[19],$value[4],"");

                    //Cumplimiento Compromiso Traslado Estado
                    $values_cump_comp=$this->calculateDate($value[19],$value[4],"");
                    $desc_cump_comp='';

                    if ($values_cump_comp >= 0) {
                        $desc_cump_comp='CUMPLIDO';
                    }else{
                        $desc_cump_comp='NO CUMPLIDO';
                    }

                    $data_maestra_new['data']['attributes'][0]['data'][$key]['40']  = $desc_cump_comp;

                }

                if (!empty($data_maestra_new)) {
                    
                    $order_data_tmp = $this->queryListColumns($request->get('id_user'));

                    foreach ($order_data_tmp as $key_order => $value_order) {
                        if (!$value_order['state']) {
                            unset($order_data_tmp[$key_order]);
                        }
                    }

                    $order_data = array_values($order_data_tmp);//Reordenamiento de variables

                    if (!empty($order_data)) {
                        
                        $keys = [];

                        foreach ($order_data as $key_key => $value_key) {

                            $valid = array_search($value_key['variable'], $data_maestra_new['data']['attributes'][0]);

                            if($valid !== false || $valid === 0){
                                $keys[] = $valid;
                            }

                        }

                        if (!empty($keys)) {

                            $data_order_data = [];

                            foreach ($data_maestra_new['data']['attributes'][0]['data'] as $key_data => $value_data) {

                                $data_order_data_values = [];

                                foreach ($keys as $key => $value) {

                                    //$valid_data = array_search($value_data, $value);

                                    //if($valid_data !== false || $valid_data === 0){
                                        $data_order_data_values[]['data'] = $data_maestra_new['data']['attributes'][0]['data'][$key_data][$value];
                                    //}
                                }

                                if (sizeof($data_order_data_values) > 0) {
                                    $data_order_data[] = array_values($data_order_data_values);
                                }
                            }

                            if (sizeof($data_order_data) > 0) {

                                //Operaciones
                                foreach ($data_order_data as $key_1 => $value_1) {
                                    foreach ($value_1 as $key_2 => $value_2) {
                                        $data_order_data[$key_1][$key_2]['config'] = $order_data[$key_2];

                                        $color= "";

                                        if (sizeof($order_data[$key_2]['config_color']) > 0) {

                                            $data_order_data[$key_1][$key_2]['color'] = $this->operatorValue($order_data[$key_2]['config_color'], $data_order_data[$key_1][$key_2]['data']);

                                        }else{
                                            $data_order_data[$key_1][$key_2]['color'] = $color;
                                        }

                                    }
                                }

                                if (sizeof($data_order_data) > 0) {
                                    $order_data['data'] = $data_order_data;

                                    return $this->jsonResponse(true,'Operacion de generacion de información semaforos','Se ha generado exitosamente la información',1,$order_data,200);

                                }else{
                                    //Ocurrio un error en la generacion de información
                                    return $this->jsonResponse(false,'Operacion de generacion de información semaforos','Ocurrio un error en la generacion de información',2,null,200);
                                }
                            }else{
                                //No se encontro informacion para generar
                                return $this->jsonResponse(false,'Operacion de generacion de información semaforos','No se encontro informacion para generar',3,null,200);
                            }
                        }else{
                            //No se obtuvieron las llaves de datos para imprimirlos
                            return $this->jsonResponse(false,'Operacion de generacion de información semaforos','No se obtuvieron las llaves de datos para imprimirlos',4,null,200);
                        }
                    }else{
                        //No existe una configuracion de columnas de información para mostrar.
                        return $this->jsonResponse(false,'Operacion de generacion de información semaforos','No existe una configuracion de columnas de información para mostrar',5,null,200);
                    }
                }else{
                    //Ocurrio un error durante los calculos de la informacion, contacte a su departamento de tecnologia.
                    return $this->jsonResponse(false,'Operacion de generacion de información semaforos','Ocurrio un error durante los calculos de la informacion, contacte a su departamento de tecnologia',6,null,200);
                }
            }else{
                //No se recibió informacion desde SAP.
                return $this->jsonResponse(false,'Operacion de generacion de información semaforos','No se recibió informacion desde SAP',7,null,200);
            }
        }else{
            return $this->jsonResponse(false,'Operacion de generacion de información semaforos','No se recibió la identificación del usuario',8,null,200);
        }
    }

    public function restore_trafficLight(Request $request){

        if (!empty($request->get('id_user'))) {

            $update_columns = \DB::table('users')
                            ->where('id', $request->get('id_user'))
                            ->update(['traffic_light' => null]);

            if ($update_columns) {
                return $this->jsonResponse(true,'Operacion de actualización de lista semaforos','Columnas restablecidas exitosamente.',1,$update_columns,200);
            }else{
                return $this->jsonResponse(false,'Operacion de actualización de lista semaforos','No se realizaron cambios nuevos.',2,null,200);
            }
            
        }else{
            return $this->jsonResponse(false,'Operacion de generacion de información semaforos','No se recibió la identificación del usuario',8,null,200);
        }

    }

    public function stringMonth($value){

        $return = "";

        if ($value == "January") {
            $return = "Enero";
        }elseif ($value == "February") {
            $return = "Febrero";
        }elseif ($value == "March") {
            $return = "Marzo";
        }elseif ($value == "April") {
            $return = "Abril";
        }elseif ($value == "May") {
            $return = "Mayo";
        }elseif ($value == "June") {
            $return = "Junio";
        }elseif ($value == "July") {
            $return = "Julio";
        }elseif ($value == "August") {
            $return = "Agosto";
        }elseif ($value == "September") {
            $return = "Septiembre";
        }elseif ($value == "October") {
            $return = "Octubre";
        }elseif ($value == "November") {
            $return = "Noviembre";
        }elseif ($value == "December") {
            $return = "Diciembre";
        }

        return $return;

    }

    public function calculateDate($value_1, $value_2, $text){

        $response = "";

        if ($value_1 == "" && $value_2 == "" || $value_1 == "00/00/0000" && $value_2 == "00/00/0000") {
            $response = "No hay datos";
        }elseif ($value_1 != "" && $value_2 == "" || $value_1 != "00/00/0000" && $value_2 == "00/00/0000") {
            //fecha actual menos value6
            
            $fecha_inicial_tmp = str_replace("/", "-", $value_1);
            $fecha_inicial = Carbon::parse($fecha_inicial_tmp);

            $fecha_final = Carbon::now();

            $response = $fecha_final->diffInDays($fecha_inicial);

        }elseif ($value_1 == "" && $value_2 != "" || $value_1 == "00/00/0000" && $value_2 != "00/00/0000") {
            $response = "No hay datos";
        }else{
            $fecha_inicial_tmp = str_replace("/", "-", $value_1);
            $fecha_inicial = Carbon::parse($fecha_inicial_tmp);

            $fecha_final_tmp = str_replace("/", "-", $value_2);
            $fecha_final = Carbon::parse($fecha_final_tmp);

            $response = $fecha_final->diffInDays($fecha_inicial);
        }

        return $response;
    }

    public function operatorValue($operations, $value_data){

        $color = "";
        if($value_data != "No hay datos"){
            foreach ($operations as $key_config => $value_config) {

                if($value_config['operator'] === "<"){
                    if ($value_data < $value_config['value']) {
                        $color = $value_config['color'];
                    }

                }elseif($value_config['operator'] === "<="){
                    if ($value_data <= $value_config['value']) {
                        $color = $value_config['color'];
                    }

                }elseif($value_config['operator'] === "!="){
                    if ($value_data != $value_config['value']) {
                        $color = $value_config['color'];
                    }

                }elseif($value_config['operator'] === "=="){
                    if ($value_data == $value_config['value']) {
                        $color = $value_config['color'];
                    }

                }elseif($value_config['operator'] === ">="){
                    if ($value_data >= $value_config['value']) {
                        $color = $value_config['color'];
                    }

                }elseif($value_config['operator'] === ">"){
                    if ($value_data > $value_config['value']) {
                        $color = $value_config['color'];
                    }

                }                  
            
            }
        }

        return $color;
    }

    /**
    * Este servicio permite obtener las columnas del listado de informacion.
    * 
    * @param integer $id_user Nombre: Id del cliente. Es id de base de datos del usuario.
    *
    * @return Single Response
    */
    public function getColumns($id_user){

        $order_data = $this->queryListColumns($id_user);

        if (sizeof($order_data) > 0) {
            return $this->jsonResponse(true,'Operacion de generacion de lista semaforos','Generación exitosa',1,$order_data,200);
        }else{
            return $this->jsonResponse(false,'Operacion de generacion de lista semaforos','No se pudo encontrar información',2,null,200);
        }
    }

    /**
    * Este servicio permite actualizar la informacion de las columnas y su visibilidad en el informe de semaforos.
    * 
    * @param integer $id_user Nombre: Id del cliente. Es id de base de datos del usuario.
    * @param string $colums Nombre: Columnas. Json en formato string con la informacion actualizada
    *
    * @return Single Response
    */
    public function updateColumns(Request $request){

        if (!empty($request->get('colums')) && !empty($request->get('id_user'))) {
            $update_columns = \DB::table('users')
                            ->where('id', $request->get('id_user'))
                            ->update(['traffic_light' => $request->get('colums')]);

            if ($update_columns) {
                return $this->jsonResponse(true,'Operacion de actualización de lista semaforos','Actualización exitosa',1,$update_columns,200);
            }else{
                return $this->jsonResponse(false,'Operacion de actualización de lista semaforos','No se realizaron cambios nuevos.',2,null,200);
            }

        }else{
            return $this->jsonResponse(false,'Operacion de actualización de lista semaforos','No se pudo encontrar actualizar la información',3,null,200);
        }
    }

    //CRUD Configuracion semaforos

    /**
    * Este servicio permite obtener la informacion de configuraciones actuales en formato JSON.
    * 
    * @param integer $id_user Nombre: Id del cliente. Es id de base de datos del usuario.
    *
    * @return Single Response
    */
    public function getConfig($id_user){

        $order_data = [];

        $order_data = $this->queryListColumns($id_user);

        foreach ($order_data as $key => $value) {
            if ($value['calculate'] == false || sizeof($value['config_color']) > 0) {
                unset($order_data[$key]);
            }
        }

        if (sizeof($order_data) > 0) {
            return $this->jsonResponse(true,'Operacion de CRUD configuración semaforos','Listado exitosa',1,$order_data,200);
        }else{
            return $this->jsonResponse(false,'Operacion de CRUD configuración semaforos','No se pudo encontrar la información',2,null,200);
        }
    }

    /**
    * Este servicio permite obtener la informacion de configuraciones actuales de semáforos guardados por usuario en formato JSON.
    * 
    * @param integer $id_user Nombre: Id del cliente. Es id de base de datos del usuario.
    *
    * @return Single Response
    */
    public function getConfigSaved($id_user){

        $order_data = [];

        $order_data = $this->queryListColumns($id_user);

        foreach ($order_data as $key => $value) {
            if ($value['calculate'] == false || sizeof($value['config_color']) == 0) {
                unset($order_data[$key]);
            }
        }

        if (sizeof($order_data) > 0) {
            return $this->jsonResponse(true,'Operacion de CRUD configuración semaforos guardados','Listado exitosa',1,$order_data,200);
        }else{
            return $this->jsonResponse(false,'Operacion de CRUD configuración semaforos guardados','No se pudo encontrar la información',2,null,200);
        }
    }

    /**
    * Este servicio permite guardar y/o actualizar la informacion de las configuraciones de cada columna. Esta operacion se hace columna por columna.
    * 
    * @param integer $id_user Nombre: Id del cliente. Es id de base de datos del usuario.
    * @param string $configuration Nombre: Configuracion. Json en formato string con la informacion actualizada o para insertar de configuracion
    *
    * @return Single Response
    */
    public function saveUpdateConfig(Request $request){

        /* Modelo de datos que se recibe por configuracion de COLUMNA
        [{
          "key":"22",
          "variable": "Finalizacion_Entrega",
          "operator":"<=",
          "value":"12",
          "color":"#DAFF59"
        },{
           "key":"22",
          "variable": "Finalizacion_Entrega",
          "operator":"==",
          "value":"18",
          "color":"#4DFFF3"
        }]deleteConfig
        */

        if (!empty($request->get('configuration')) && sizeof(json_decode($request->get('configuration'))) > 0 && !empty($request->get('id_user'))) {

            //return $request->get('configuration');
            //return false;
            
            $order_data = $this->queryListColumns($request->get('id_user'));

            if (sizeof($order_data) > 0) {

                foreach (json_decode($request->get('configuration')) as $key => $value) {

                    if ($key == 0) {
                        $new_array = [];
                        $order_data[$value->key]['config_color'] = [];
                    }

                    $new_array[$key]['operator'] = $value->operator;
                    $new_array[$key]['value'] = $value->value;
                    $new_array[$key]['color'] = $value->color;

                }

                $order_data[$value->key]['config_color'] = (array)$new_array;

                if (sizeof($new_array) > 0) {

                    try {
                        
                        $action_db = \DB::table('users')
                                    ->where('id', $request->get('id_user'))
                                    ->update(['traffic_light' => null]);

                        $action_db = \DB::table('users')
                                    ->where('id', $request->get('id_user'))
                                    ->update(['traffic_light' => json_encode($order_data)]);

                        return $this->jsonResponse(true,'Operacion de CRUD configuración semaforos','Actualizacion satisfactoria.',1,$new_array,200); 

                    } catch (Exception $e) {
                        
                        return $this->jsonResponse(false,'Operacion de CRUD configuración semaforos','Ocurrió un problema durante la actualización de la información',2,$e,200); 

                    }
                }else{
                    return $this->jsonResponse(false,'Operacion de CRUD configuración semaforos','No se procesaron nuevas configuraciones, intentelo nuevamente',3,null,200); 
                }
            }else{
                return $this->jsonResponse(false,'Operacion de CRUD configuración semaforos','No se obtuvo la configuracion estandar o del usuario para procesar',4,null,200);
            }

        }else{
            return $this->jsonResponse(false,'Operacion de CRUD configuración semaforos','No se recibió información',5,null,200);
        }
    }

    /**
    * Este servicio permite eliminar una configuracion de una columna.
    * 
    * @param integer $id_user Nombre: Id del cliente. Es id de base de datos del usuario.
    * @param integer $key Nombre: Llave. Llave o key indicadora del array donde se encuentra la columna.
    *
    * @return Single Response
    */
    public function deleteConfig($key,$id_user){

        $order_data = $this->queryListColumns($id_user);

        //return $order_data[$key]['config_color']; 

        if (sizeof($order_data) > 0) {
            
            $order_data[$key]['config_color'] = [];

            if (sizeof($order_data[$key]['config_color']) <= 0) {

                try {

                    $action_db = \DB::table('users')
                                    ->where('id', $id_user)
                                    ->update(['traffic_light' => null]);

                    $action_db = \DB::table('users')
                                ->where('id', $id_user)
                                ->update(['traffic_light' => json_encode($order_data)]);

                    return $this->jsonResponse(true,'Operacion de CRUD configuración semaforos','Borrado y Actualizacion satisfactoria.',1,$key,200);

                } catch (Exception $e) {
                    return $this->jsonResponse(false,'Operacion de CRUD configuración semaforos','Ocurrió un problema durante la actualización de la información',2,$e,200); 
                }
            }else{
                return $this->jsonResponse(false,'Operacion de CRUD configuración semaforos','No se pudo ejecutar el borrado de la configuracion',3,null,200); 
            }
        }else{
            //No se obtuvieron configuraciones
            return $this->jsonResponse(false,'Operacion de CRUD configuración semaforos','No se obtuvieron configuraciones',4,null,200); 
        }
    }

    public function queryListColumns($id_user){

        $query = \DB::table('users')->where('id',$id_user)->select('traffic_light')->first();

        if (!empty($query->traffic_light)) {
            $order_data = json_decode($query->traffic_light, true);
        }else{
            //File trafficlight
            $order_data = json_decode(config('list_services.trafficlight.estandar'), true);
        }

        return $order_data;
    }

    public function testIndicadores(Request $request){

        $clienteid = '17150081';
        $fechainicial = '01-01-2019';
        $fechafinal = '';

        $url = env('URL_USCIIWS62')."token=".env('TOKEN_SAP')."&clienteid=".$clienteid."&fechainicial=".$fechainicial."&fechafinal=".$fechafinal;

        $client = new \GuzzleHttp\Client();
        $result_tmp = $client->get($url);  
        $result_tmp2 = $result_tmp->getBody()->getContents();
        
        $result = json_decode($result_tmp2,true);

        return $result;

        if (!empty($result)) {
            $array = [];
            $array['meta'] = [];
            $array[0] = $result;
        }

        return $this->responseGeneralWs($array);

    }

    /**
    * Este servicio permite generar la informacion especifica de Ciamsa para sus clientes de forma de indicadorCumplimientoHistorico.
    * 
    * @param integer $id_user Nombre: Id del cliente. Es id de base de datos del usuario.
    * @param integer $tipo_consulta Nombre: Tipo de consulta. Es id del tipo de consulta, sen envia uno (1) para "Consulta Global" y dos (2) para "Consulta Deposito".
    * @param integer $rango Nombre: Rango de dias. Si el tipo de consulta es "Consulta Global", se envia el numero uno (1) para "Entre 0-3 dias", dos (2) para "Entre 4-5 dias", tres (3) para "Entre 6 dias", cuatro (4) para "Entre 7-8 dias, cinco (5) para "Mas de 8 dias". Si el tipo de consulta es "Consulta Deposito", se envia el numero uno (1) para "Entre 0-2 dias", dos (2) para "Entre 3 dias", tres (3) para "Entre 4 dias", cuatro (4) para "Mas de 4 dias.
    * @param string $fecha_inicial Nombre: Fecha inicial. Fecha inicial desde donde se quiere obtener la informacion
    * @param string $fecha_final Nombre: Fecha final. Fecha final desde donde se quiere obtener la informacion
    *
    * @return Single Response
    */
    public function indicadorCumplimientoHistorico(Request $request){

        if (!empty($request->get('id_user')) && !empty($request->get('tipo_consulta')) && !empty($request->get('rango'))) {

            $clienteid = $request->get('id_user');

            $cur_day = '01';
            $month = '01';
            $year = date('Y');

            if(empty($request->get('fecha_inicial'))){
              $fecha_inicial = $cur_day.'-'.$month.'-'.$year;  
            }else{
               $fecha_inicial = $request->get('fecha_inicial'); 
            }

            if(empty($request->get('fecha_final'))){
              $f = carbon::now();
              $fecha_final = $f->format('d-m-Y');
            }else{
               $fecha_final = $request->get('fecha_final'); 
            }

            $url = env('URL_USCIIWS62')."token=".env('TOKEN_SAP')."&clienteid=".$clienteid."&fechainicial=".$fecha_inicial."&fechafinal=".$fecha_final;

            //return $url;

            $client = new \GuzzleHttp\Client();
            $result_tmp = $client->get($url);  
            $result_tmp2 = $result_tmp->getBody()->getContents();
            
            $result = json_decode($result_tmp2,true);
            $data_maestra_new = $result;

            //dd($data_maestra_new); die();

            //$data_maestra_new = (array)json_decode($result,true);
            //return $data_maestra_new;
        
            if(isset($data_maestra_new['meta'])){

                $data_maestra_new['data']['attributes'][0][] = 'Tiempo_Global';
                $data_maestra_new['data']['attributes'][0][] = 'Tiempo_Deposito';
                $data_maestra_new['data']['attributes'][0][] = 'Rango_Global';
                $data_maestra_new['data']['attributes'][0][] = 'Rango_Deposito';
                $data_maestra_new['data']['attributes'][0][] = 'Mes';

                //return $data_maestra_new;
                //dd($data_maestra_new);

                /*dd($data_maestra_new['data']['attributes'][0]['data']);
                die();*/

                foreach ($data_maestra_new['data']['attributes'][0]['data'] as $key => $value) {

                    //Cantidad por mes fk
                //var_dump($data_maestra_new['data']['attributes'][0]['data'][$key]['19']);

                    //Calculo Tiempo_global
                    //$calc_17 = $this->calculateDate($value[6],$value[12],"Sin Fecha Descar.");
                    $calc_17 = $this->calculateDate($value[13],$value[19],"Sin Fecha Descar.");
                    //dd($calc_17);

                    /*var_dump($value[17]);
                    echo "rrrrrrrr";
                    var_dump($value[19]);*/

                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['17'] = $calc_17;

                    //Tiempo_deposito
                    //$calc_18 = $this->calculateDate($value[10],$value[12],"Sin Fecha Rec. Doc");
                    $calc_18 = $this->calculateDate($value[17],$value[19],"Sin Fecha Rec. Doc");

                    //Flag fk last
                    //var_dump($calc_18);

                    //var_dump($calc_18);
                    //var_dump($calc_18); die();

                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['18']  = $calc_18;

                    //Rango_global
                    $Rango_global = "";
                    if ($calc_17 === "" || $calc_17 === "No hay datos") {
                        $Rango_global = "";
                    }elseif ($calc_17 < 0) {
                        $Rango_global = "Verificar";
                    }elseif ($calc_17 >= 0 && $calc_17 <= 3) {
                        $Rango_global = 1; //Entre 0-3 dias
                    }elseif ($calc_17 > 3 && $calc_17 < 6) {
                        $Rango_global = 2; //Entre 4-5 dias
                    }elseif ($calc_17 > 5 && $calc_17 <= 6) {
                        $Rango_global = 3; //Entre 6 dias
                    }elseif ($calc_17 >= 7 && $calc_17 <= 8) {
                        $Rango_global = 4; //Entre 7-8 dias
                    }else{
                        $Rango_global = 5; //Mas de 8 dias
                    }

                    $data_maestra_new['data']['attributes'][0]['data'][$key]['26']  = $Rango_global;

                    //Rango_deposito
                    /*$Rango_deposito = "";
                    if ($calc_18 === "" || $calc_18 === "No hay datos") {
                        $Rango_deposito = "";
                    }elseif ($calc_18 < 0) {
                        $Rango_deposito = "Verificar";
                    }elseif ($calc_18 >= 0 && $calc_18 <= 2) {
                        $Rango_deposito = 1; //Entre 0-2 dias                        
                    }elseif ($calc_18 > 2 && $calc_18 < 4) {
                        $Rango_deposito = 2; //Entre 3 dias
                    }elseif ($calc_18 >= 4 && $calc_18 < 5) {
                        $Rango_deposito = 3; //Entre 4 dias
                    }else{
                        $Rango_deposito = 4; //Mas de 4 dias
                    }*/

                    $Rango_deposito = "";
                    if ($calc_18 === "" || $calc_18 === "No hay datos") {
                        $Rango_deposito = "";
                    }elseif ($calc_18 < 0) {
                        $Rango_deposito = "Verificar";
                    }elseif ($calc_18 >= 0 && $calc_18 <= 3) {
                        $Rango_deposito = 1; //Entre 0-3 dias
                    }elseif ($calc_18 > 3 && $calc_18 < 6) {
                        $Rango_deposito = 2; //Entre 4-5 dias
                    }elseif ($calc_18 > 5 && $calc_18 <= 6) {
                        $Rango_deposito = 3; //Entre 6 dias
                    }elseif ($calc_18 >= 7 && $calc_18 <= 8) {
                        $Rango_deposito = 4; //Entre 7-8 dias
                    }else{
                        $Rango_deposito = 5; //Mas de 8 dias
                    }

                    //var_dump($Rango_deposito);
                    //Flag 1 fk

                    $data_maestra_new['data']['attributes'][0]['data'][$key]['27']  = $Rango_deposito;

                    //dd($data_maestra_new); die();

                    //Mes
                    //$data_maestra_new['data']['attributes'][0]['data'][$key]['21']  = Carbon::parse(str_replace("/", "-", $value[6]))->format('F');

                    //Month Name
                    $data_maestra_new['data']['attributes'][0]['data'][$key]['19']  = Carbon::parse(str_replace("/", "-", $value[19]))->format('F');

                    //var_dump($data_maestra_new['data']['attributes'][0]['data'][$key]['19']);
                    

                }

                if (!empty($data_maestra_new)) {

                    $data_final = [];
                    
                    $months = array('January',
                                    'February',
                                    'March',
                                    'April',
                                    'May',
                                    'June',
                                    'July',
                                    'August',
                                    'September',
                                    'October',
                                    'November',
                                    'December');

                    $months_spanish = array('January' => 'Enero',
                                    'February' => 'Febrero',
                                    'March' => 'Marzo',
                                    'April' => 'Abril',
                                    'May' => 'Mayo',
                                    'June' => 'Junio',
                                    'July' => 'Julio',
                                    'August' => 'Agosto',
                                    'September' => 'Septiembre',
                                    'October' => 'Octubre',
                                    'November' => 'Noviembre',
                                    'December' => 'Diciembre');

                    $count_months = array('January' => 0,
                                    'February' => 0,
                                    'March' => 0,
                                    'April' => 0,
                                    'May' => 0,
                                    'June' => 0,
                                    'July' => 0,
                                    'August' => 0,
                                    'September' => 0,
                                    'October' => 0,
                                    'November' => 0,
                                    'December' => 0);

                    $count_ranges = array('January' => 0,
                                    'February' => 0,
                                    'March' => 0,
                                    'April' => 0,
                                    'May' => 0,
                                    'June' => 0,
                                    'July' => 0,
                                    'August' => 0,
                                    'September' => 0,
                                    'October' => 0,
                                    'November' => 0,
                                    'December' => 0);

                    $tot_cont_total = 0;
                    //Apariciones totales por mes
                    foreach ($data_maestra_new['data']['attributes'][0]['data'] as $key_count_m => $value_count_m) {

                        //var_dump($value_count_m['21']);
                        //var_dump($value_count_m['19']);
                        
                       if (array_search($value_count_m['19'], $months) !== false) {
                            $tot_cont_total ++;
                            $count_months[$value_count_m['19']]++;
                        } 
                        //var_dump($tot_cont_total);
                    }

                    //var_dump($tot_cont_total);
                    //dd($data_maestra_new['data']['attributes'][0]['data']);

                    //conteo segun el rango
                    if ($request->get('tipo_consulta') == 1) {//Consulta global
                        foreach ($data_maestra_new['data']['attributes'][0]['data'] as $key_count_c_g => $value_count_c_g) 
                        {

                           if (array_search($value_count_c_g['19'], $months) !== false && $value_count_c_g['26'] == $request->get('rango')) {
                                $count_ranges[$value_count_c_g['19']] = $count_ranges[$value_count_c_g['19']] + 1;
                            } 
                        }

                        //dd("fk1");

                    }elseif($request->get('tipo_consulta') == 2){ //Consulta deposito
                        foreach ($data_maestra_new['data']['attributes'][0]['data'] as $key_count_c_d => $value_count_c_d) {

                            //var_dump($value_count_c_d['27']);

                           if (array_search($value_count_c_d['19'], $months) !== false && $value_count_c_d['27'] == $request->get('rango')) {

                            //if (array_search($value_count_c_d['19'], $months) !== false && $value_count_c_d['27'] == $request->get('rango')) {
                                //var_dump($value_count_c_d['19']);

                                $count_ranges[$value_count_c_d['19']] = $count_ranges[$value_count_c_d['19']] + 1;

                            } 
                        }

                        //dd("fk2");
                    }

                    $array_porcent =  [];

                    //Generacion de data para imprimir
                    foreach ($months as $key_m => $value_m) {
                        $data_final[$key_m]['month'] = $months_spanish[$value_m];
                        $data_final[$key_m]['cont_rango'] = $count_ranges[$value_m];
                        $data_final[$key_m]['cont_total'] = $count_months[$value_m];
                        //$data_final[$key_m]['fulfillment'] = ($count_months[$value_m] != 0) ? /*round($count_ranges[$value_m] / $count_months[$value_m], 1)*100 : 0 ;*/

                        if ($count_months[$value_m] != 0) {
                            $res_fulfillment = ($count_ranges[$value_m] / $count_months[$value_m])*100;
                            $res_fulfillment = round($res_fulfillment, 2);
                        }else{
                           $res_fulfillment = 0; 
                        }

                        $data_final[$key_m]['fulfillment'] = $res_fulfillment;
                        $data_final[$key_m]['goal'] = 95.0;

                    } 

                    //return $data_final;

                    if (sizeof($data_final) > 0) {
                        return $this->jsonResponse(true,'Operacion de generacion de indicador de cumplimiento historico','Informacion generada con exito',1,$data_final,200);
                    }else{
                        //Ocurrio un error en el calculo de la informacion
                        return $this->jsonResponse(false,'Operacion de generacion de indicador de cumplimiento historico','Ocurrio un error en el calculo de la informacion',2,null,200);
                    }
                }else{
                    //Ocurrio un error durante los calculos de la informacion, contacte a su departamento de tecnologia.
                    return $this->jsonResponse(false,'Operacion de generacion de indicador de cumplimiento historico','Ocurrio un error durante los calculos de la informacion, contacte a su departamento de tecnologia',3,null,200);
                }
            }else{
                //No se recibió informacion desde SAP.
                return $this->jsonResponse(false,'Operacion de generacion de indicador de cumplimiento historico','No se recibió informacion desde SAP',4,null,200);
            }
        }else{
            return $this->jsonResponse(false,'Operacion de generacion de indicador de cumplimiento historico','No se recibió la identificación del usuario',5,null,200);
        }
    }

    /**
    * Este servicio permite generar la informacion especifica de Ciamsa para sus clientes de forma de Indicador de Cumplimiento Acumulado Mensual
    * 
    * @param integer $id_user Nombre: Id del cliente. Es id de base de datos del usuario.
    * @param integer $tipo_consulta Nombre: Tipo de consulta. Es id del tipo de consulta, sen envia uno (1) para "Consulta Global" y dos (2) para "Consulta Deposito".
    * @param integer $mes Nombre: Mes. Se debe enviar January' para 'Enero','February' para 'Febrero','March' para 'Marzo','April' para 'Abril','May' para 'Mayo','June' para 'Junio','July' para 'Julio','August' para 'Agosto','September' para 'Septiembre','October' para 'Octubre','November' para 'Noviembre','December' para 'Diciembre.
    *
    * @return Single Response
    */
    public function indicadorCumplimientoAcumuladoMensual(Request $request){

        if (!empty($request->get('id_user')) && !empty($request->get('tipo_consulta')) && !empty($request->get('mes')))
        { 

            $clienteid = $request->get('id_user');

            $month = date($request->get('mes'));
            $year = date('Y');

            //Calculo el último día dependiendo del mes y el año
            $last_day_month = date("d", mktime(0,0,0, $month+1, 0, $year));

            $cur_day = '01';
            $init_date = $cur_day.'-'.$month.'-'.$year;
            $end_date = $last_day_month.'-'.$month.'-'.$year;

            $url = env('URL_USCIIWS62')."token=".env('TOKEN_SAP')."&clienteid=".$clienteid."&fechainicial=".$init_date."&fechafinal=".$end_date;

            $string_month = '';
            if ($month === '01') {
                $string_month = 'January';
            }else if($month === '02'){
                $string_month = 'February';
            }else if($month === '03'){
                $string_month = 'March';
            }else if($month === '04'){
                $string_month = 'April';
            }else if($month === '05'){
                $string_month = 'May';
            }else if($month === '06'){
                $string_month = 'June';
            }else if($month === '07'){
                $string_month = 'July';
            }else if($month === '08'){
                $string_month = 'August';
            }else if($month === '09'){
                $string_month = 'September';
            }else if($month === '10'){
                $string_month = 'October';
            }else if($month === '11'){
                $string_month = 'November';
            }else if($month === '12'){
                $string_month = 'December';
            }

            $client = new \GuzzleHttp\Client();
            $result_tmp = $client->get($url);  
            $result_tmp2 = $result_tmp->getBody()->getContents();
            
            $result = json_decode($result_tmp2,true);
            $data_maestra_new = $result;

            //$data_maestra_new = (array)json_decode($data_maestra,true);
        
            if(isset($data_maestra_new["meta"])){

                $process = false;

                $data_maestra_new['data']['attributes'][0][] = 'Tiempo_Global';
                $data_maestra_new['data']['attributes'][0][] = 'Tiempo_Deposito';
                $data_maestra_new['data']['attributes'][0][] = 'Rango_Global';
                $data_maestra_new['data']['attributes'][0][] = 'Rango_Deposito';
                $data_maestra_new['data']['attributes'][0][] = 'Mes';

                //dd($data_maestra_new);

                foreach ($data_maestra_new['data']['attributes'][0]['data'] as $key => $value) {

                    //if (Carbon::parse(str_replace("/", "-", $value[6]))->format('F') == $string_month){
                    if (Carbon::parse(str_replace("/", "-", $value[13]))->format('F') == $string_month){

                        $process = true;

                        //Calculo Tiempo_global
                        //$calc_17 = $this->calculateDate($value[6],$value[12],"Sin Fecha Descar.");
                        $calc_17 = $this->calculateDate($value[13],$value[19],"Sin Fecha Descar.");
                        $data_maestra_new['data']['attributes'][0]['data'][$key]['17'] = $calc_17;

                        //Tiempo_deposito
                        //$calc_18 = $this->calculateDate($value[10],$value[12],"Sin Fecha Rec. Doc");
                        $calc_18 = $this->calculateDate($value[17],$value[19],"Sin Fecha Rec. Doc");
                        $data_maestra_new['data']['attributes'][0]['data'][$key]['18']  = $calc_18;

                        //Rango_global
                        $Rango_global = "";
                        if ($calc_17 == "" || $calc_17 == "No hay datos") {
                            $Rango_global = "";
                        }elseif ($calc_17 < 0) {
                            $Rango_global = "Verificar";
                        }elseif ($calc_17 >= 0 && $calc_17 <= 3) {
                            $Rango_global = 1; //Entre 0-3 dias
                        }elseif ($calc_17 > 3 && $calc_17 < 6) {
                            $Rango_global = 2; //Entre 4-5 dias
                        }elseif ($calc_17 > 5 && $calc_17 <= 6) {
                            $Rango_global = 3; //Entre 6 dias
                        }elseif ($calc_17 >= 7 && $calc_17 <= 8) {
                            $Rango_global = 4; //Entre 7-8 dias
                        }else{
                            $Rango_global = 5; //Mas de 8 dias
                        }

                        $data_maestra_new['data']['attributes'][0]['data'][$key]['19']  = $Rango_global;

                        //Rango_deposito
                        $Rango_deposito = "";
                        if ($calc_18 == "" || $calc_18 == "No hay datos") {
                            $Rango_deposito = "";
                        }elseif ($calc_18 < 0) {
                            $Rango_deposito = "Verificar";
                        }elseif ($calc_18 >= 0 && $calc_18 <= 2) {
                            $Rango_deposito = 1; //Entre 0-2 dias
                        }elseif ($calc_18 > 2 && $calc_18 < 4) {
                            $Rango_deposito = 2; //Entre 3 dias
                        }elseif ($calc_18 >= 4 && $calc_18 < 5) {
                            $Rango_deposito = 3; //Entre 4 dias
                        }else{
                            $Rango_deposito = 4; //Mas de 4 dias
                        }

                        $data_maestra_new['data']['attributes'][0]['data'][$key]['20']  = $Rango_deposito;

                        //Mes
                        //$data_maestra_new['data']['attributes'][0]['data'][$key]['21']  = Carbon::parse(str_replace("/", "-", $value[6]))->format('F');
                        $data_maestra_new['data']['attributes'][0]['data'][$key]['21']  = Carbon::parse(str_replace("/", "-", $value[13]))->format('F');

                    }else{
                        unset($data_maestra_new['data']['attributes'][0]['data'][$key]);
                    }
                }
                
                if (!empty($data_maestra_new) && $process == true) {

                    $data_final = [];
                    $total = 0;
                    
                    if ($request->get('tipo_consulta') == 1) {//Consulta global

                        $count_ranges = array(1 => 0,
                                    2 => 0,
                                    3 => 0,
                                    4 => 0,
                                    5 => 0);

                        $ranges_name = array(1 => 'Entre 0-3 dias',
                                    2 => 'Entre 4-5 dias',
                                    3 => 'Entre 6 dias',
                                    4 => 'Entre 7-8 dias',
                                    5 => 'Mas de 8 dias');


                        foreach ($data_maestra_new['data']['attributes'][0]['data'] as $key_r => $value_r) {
                            foreach ($count_ranges as $key_count_r => $value_count_r) {
                                if ($value_r['19'] == $key_count_r) {
                                    $count_ranges[$key_count_r] = $count_ranges[$key_count_r] +1;
                                } 
                            }
                        }

                    }elseif ($request->get('tipo_consulta') == 2) {//Consulta deposito

                        $count_ranges = array(1 => 0,
                                    2 => 0,
                                    3 => 0,
                                    4 => 0);

                        $ranges_name = array(1 => 'Entre 0-2 dias',
                                    2 => 'Entre 3 dias',
                                    3 => 'Entre 4 dias',
                                    4 => 'Mas de 4 dias');

                        foreach ($data_maestra_new['data']['attributes'][0]['data'] as $key_r => $value_r) {
                            foreach ($count_ranges as $key_count_r => $value_count_r) {
                                if ($value_r['20'] == $key_count_r) {
                                    $count_ranges[$key_count_r] = $count_ranges[$key_count_r] +1;
                                } 
                            }
                        }
                    }

                    //Conteo de totales
                    foreach ($count_ranges as $key_c_r => $value_c_r) {
                        $total = $total + $value_c_r;
                    }

                    //Generacion de data para imprimir
                    foreach ($count_ranges as $key_m => $value_m) {
                        $data_final[$key_m]['label'] = $ranges_name[$key_m];
                        $data_final[$key_m]['value'] = $value_m;
                        $data_final[$key_m]['index'] = $key_m;
                        $data_final[$key_m]['fulfillment'] = ($total != 0) ? round($value_m / $total, 1)*100 : 0 ;
                    }                    

                    if (sizeof($data_final) > 0) {
                        return $this->jsonResponse(true,'Operacion de generacion de Indicador de Cumplimiento Acumulado Mensual','Informacion generada con exito',1,$data_final,200);
                    }else{
                        //Ocurrio un error en el calculo de la informacion
                        return $this->jsonResponse(false,'Operacion de generacion de Indicador de Cumplimiento Acumulado Mensual','Ocurrio un error en el calculo de la informacion',2,null,200);
                    }
                }else{
                    //Ocurrio un error durante los calculos de la informacion, contacte a su departamento de tecnologia.
                    return $this->jsonResponse(false,'Operacion de generacion de Indicador de Cumplimiento Acumulado Mensual','No se encontro información para generar.',3,null,200);
                }
            }else{
                //No se recibió informacion desde SAP.
                return $this->jsonResponse(false,'Operacion de generacion de Indicador de Cumplimiento Acumulado Mensual','No se recibió informacion desde SAP',4,null,200);
            }
        }else{
            return $this->jsonResponse(false,'Operacion de generacion de Indicador de Cumplimiento Acumulado Mensual','No se recibió la identificación del usuario',5,null,200);
        }
    }

    /**
    * Este servicio permite generar la informacion especifica de Ciamsa para sus clientes de forma de Principales Causales de Incumplimiento
    * 
    * @param integer $id_user Nombre: Id del cliente. Es id de base de datos del usuario.
    * @param integer $tipo_consulta Nombre: Tipo de consulta. Es id del tipo de consulta, sen envia uno (1) para "Consulta Global" y dos (2) para "Consulta Deposito".
    * @param integer $rango Nombre: Rango de dias. Si el tipo de consulta es "Consulta Global", se envia el numero uno (1) para "Entre 0-3 dias", dos (2) para "Entre 4-5 dias", tres (3) para "Entre 6 dias", cuatro (4) para "Entre 7-8 dias, cinco (5) para "Mas de 8 dias". Si el tipo de consulta es "Consulta Deposito", se envia el numero uno (1) para "Entre 0-2 dias", dos (2) para "Entre 3 dias", tres (3) para "Entre 4 dias", cuatro (4) para "Mas de 4 dias.
    * @param string $fecha_inicial Nombre: Fecha inicial. Fecha inicial desde donde se quiere obtener la informacion
    * @param string $fecha_final Nombre: Fecha final. Fecha final desde donde se quiere obtener la informacion
    *
    * @return Single Response
    */
    public function principalesCausalesIncumplimiento(Request $request){

        if (!empty($request->get('id_user')) && !empty($request->get('tipo_consulta')) && !empty($request->get('rango'))) {

            $clienteid = $request->get('id_user');
            
            $cur_day = '01';
            $month = '01';
            $year = date('Y');

            if(empty($request->get('fecha_inicial'))){
              $fecha_inicial = $cur_day.'-'.$month.'-'.$year;  
            }else{
               $fecha_inicial = $request->get('fecha_inicial'); 
            }

            if(empty($request->get('fecha_final'))){
              $f = carbon::now();
              $fecha_final = $f->format('d-m-Y');
            }else{
                $fecha_final = $request->get('fecha_final'); 
            }

            $url = env('URL_USCIIWS62')."token=".env('TOKEN_SAP')."&clienteid=".$clienteid."&fechainicial=".$fecha_inicial."&fechafinal=".$fecha_final;

            $client = new \GuzzleHttp\Client();
            $result_tmp = $client->get($url);  
            $result_tmp2 = $result_tmp->getBody()->getContents();            
            
            $result = json_decode($result_tmp2,true);
            $data_maestra_new = $result;

            //return $data_maestra_new;
        
            if(isset($data_maestra_new['meta'])){

                $data_maestra_new['data']['attributes'][0][] = 'Tiempo_Global';
                $data_maestra_new['data']['attributes'][0][] = 'Tiempo_Deposito';
                $data_maestra_new['data']['attributes'][0][] = 'Rango_Global';
                $data_maestra_new['data']['attributes'][0][] = 'Rango_Deposito';
                $data_maestra_new['data']['attributes'][0][] = 'Mes';

                $causales = [];

                //dd($data_maestra_new);

                foreach ($data_maestra_new['data']['attributes'][0]['data'] as $key => $value) {

                    //dd($data_maestra_new['data']['attributes'][0]['data'][$key]['22']);

                    if ($data_maestra_new['data']['attributes'][0]['data'][$key]['22'] !== '') {

                        //Calculo Tiempo_global
                        //$calc_17 = $this->calculateDate($value[6],$value[12],"Sin Fecha Descar.");
                        $calc_17 = $this->calculateDate($value[13],$value[19],"Sin Fecha Descar.");
                        $data_maestra_new['data']['attributes'][0]['data'][$key]['17'] = $calc_17;

                        //Tiempo_deposito
                        //$calc_18 = $this->calculateDate($value[10],$value[12],"Sin Fecha Rec. Doc");
                        $calc_18 = $this->calculateDate($value[17],$value[19],"Sin Fecha Rec. Doc");
                        $data_maestra_new['data']['attributes'][0]['data'][$key]['18']  = $calc_18;

                        //Rango_global
                        $Rango_global = "";
                        if ($calc_17 == "" || $calc_17 == "No hay datos") {
                            $Rango_global = "";
                        }elseif ($calc_17 < 0) {
                            $Rango_global = "Verificar";
                        }elseif ($calc_17 >= 0 && $calc_17 <= 3) {
                            $Rango_global = 1; //Entre 0-3 dias
                        }elseif ($calc_17 > 3 && $calc_17 < 6) {
                            $Rango_global = 2; //Entre 4-5 dias
                        }elseif ($calc_17 > 5 && $calc_17 <= 6) {
                            $Rango_global = 3; //Entre 6 dias
                        }elseif ($calc_17 >= 7 && $calc_17 <= 8) {
                            $Rango_global = 4; //Entre 7-8 dias
                        }else{
                            $Rango_global = 5; //Mas de 8 dias
                        }

                        $data_maestra_new['data']['attributes'][0]['data'][$key]['19']  = $Rango_global;

                        //Rango_deposito
                        $Rango_deposito = "";
                        if ($calc_18 == "" || $calc_18 == "No hay datos") {
                            $Rango_deposito = "";
                        }elseif ($calc_18 < 0) {
                            $Rango_deposito = "Verificar";
                        }elseif ($calc_18 >= 0 && $calc_18 <= 2) {
                            $Rango_deposito = 1; //Entre 0-2 dias
                        }elseif ($calc_18 > 2 && $calc_18 < 4) {
                            $Rango_deposito = 2; //Entre 3 dias
                        }elseif ($calc_18 >= 4 && $calc_18 < 5) {
                            $Rango_deposito = 3; //Entre 4 dias
                        }else{
                            $Rango_deposito = 4; //Mas de 4 dias
                        }

                        $data_maestra_new['data']['attributes'][0]['data'][$key]['20']  = $Rango_deposito;

                        //Mes
                        //$data_maestra_new['data']['attributes'][0]['data'][$key]['21']  = Carbon::parse(str_replace("/", "-", $value[6]))->format('F');
                        $data_maestra_new['data']['attributes'][0]['data'][$key]['21']  = Carbon::parse(str_replace("/", "-", $value[13]))->format('F');

                        //Inclusion de causales
                        if ($request->get('tipo_consulta') == 1) {
                            if ($request->get('rango') == $Rango_global) {
                                //$causales[] = $data_maestra_new['data']['attributes'][0]['data'][$key]['15'];
                                $causales[] = $data_maestra_new['data']['attributes'][0]['data'][$key]['22'];
                            }
                        }elseif($request->get('tipo_consulta') == 2){
                            if ($request->get('rango') == $Rango_deposito) {
                                //$causales[] = $data_maestra_new['data']['attributes'][0]['data'][$key]['15'];
                                $causales[] = $data_maestra_new['data']['attributes'][0]['data'][$key]['22'];
                            }
                        }

                    }                    

                }

                if (!empty($data_maestra_new)) {

                    //return $data_maestra_new['data']['attributes'][0]['data'][1]['22'];
                    
                    $data_final = [];

                    $causales_unique = array_unique($causales);
                    $causales_values = array_values($causales_unique);
                    $causales_count = count($causales_values);

                    //Saco la cantidad de las causales.
                    $cant_causales = count($causales);

                    //return $cant_causales;                    

                    foreach ($causales_values as $key_c => $value_c) {

                        $data_final[$key_c]['count'] = 0;
                        $data_final[$key_c]['label'] = $value_c;
                        $data_final[$key_c]['percentage'] = 0;

                        foreach ($causales as $key_q_c => $value_q_c) {
                            if ($value_c == $value_q_c) {

                                $data_final[$key_c]['count'] = $data_final[$key_c]['count'] + 1;

                                $data_final[$key_c]['percentage'] = ($data_final[$key_c]['count'] / $cant_causales)*100;
                                //return ($data_final[$key_c]['count'] / $cant_causales)*100;

                            }
                        }

                    }                 

                    if (sizeof($data_final) > 0) {
                        return $this->jsonResponse(true,'Operacion de generacion de indicador Principales Causales de Incumplimiento','Informacion generada con exito',1,$data_final,200);
                    }else{
                        //Ocurrio un error en el calculo de la informacion
                        return $this->jsonResponse(false,'Operacion de generacion de indicador Principales Causales de Incumplimiento','No se encontraron datos para este indicador',2,null,200);
                    }
                }else{
                    //Ocurrio un error durante los calculos de la informacion, contacte a su departamento de tecnologia.
                    return $this->jsonResponse(false,'Operacion de generacion de indicador Principales Causales de Incumplimiento','Ocurrio un error durante los calculos de la informacion, contacte a su departamento de tecnologia',3,null,200);
                }
            }else{
                //No se recibió informacion desde SAP.
                return $this->jsonResponse(false,'Operacion de generacion de indicador Principales Causales de Incumplimiento','No se recibió informacion desde SAP',4,null,200);
            }
        }else{
            return $this->jsonResponse(false,'Operacion de generacion de indicador Principales Causales de Incumplimiento','No se recibió la identificación del usuario',5,null,200);
        }
    }

    public function shortUrl($url_base64){

        if(!empty($url_base64)){

            $id = \DB::table('url_token')->insertGetId(
                ['url' => $url_base64]
            );

            $id_base64 = base64_encode($id);

            return env('APP_URL')."/api/v1/Servicios_logisticos/dowload/file/".$id_base64;

        }else{
            return "error";
        }


        
    }

    public function downloadFileCiamsa($url_base64){

        if (!empty($url_base64)) {
            
            $id = base64_decode($url_base64);

            $url_file = \DB::table('url_token')->where('id', $id)->first();

            if (!empty($url_file)) {      

                //Consumo de servicio web de descarga de url
                $url = env('URL_USCIIWS65')."token=".env('TOKEN_SAP')."&url=".base64_decode($url_file->url);
                $client = new \GuzzleHttp\Client();
                $result_tmp = $client->get($url);
                $result = $result_tmp->getBody()->getContents();
                $url_down = base64_decode($result);

                $file = 'file.pdf';
                file_put_contents($file, $url_down);

                if (file_exists($file)) {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="'.basename($file).'"');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));
                    readfile($file);
                    unlink($file);
                    exit;
                }

            }else{
                return "El archivo que intenta descargar no se encuentra en nuestro sistema.";
            }

        }else{
            return "Ha ocurrido un error en la descarga, contactese con su administrador de sistema CIAMSA.";
        }
    }

    public function getBase64(Request $request){

        if (!empty($request->get('id_doc'))) {
 
            $id_doc = $request->get('id_doc');  

            $params = array(
                            'token' => env('TOKEN_SAP'),
                            'id_documento' => $id_doc
                            );

            $url = env('URL_USCIIWSGETBASE64');

            $client = new \GuzzleHttp\Client();

            $response = $client->request('POST',$url,['timeout' => 4000,
                'form_params' => $params
            ]);

            $result_tmp = $response->getBody()->getContents();

            //Convierto la respuesta del base64 a String
            $b64 = strval($result_tmp);

            if (base64_decode($b64, true)) {
                // is valid
                return $this->jsonResponse(true, 'Consumo de servicios web SAP para obtener el base64 del pdf.', 'La consulta ha generado información.',1,$b64,200);
            } else {
                // not valid
                return $this->jsonResponse(false, 'Consumo de servicios web SAP para obtener el base64 del pdf.', 'El formato del base64 presenta errores.',0,$b64,200);
            }

        }else{

            return $this->jsonResponse(false, 'Consumo de servicios web SAP para obtener el base64 del pdf.', 'No se obtuvo el identificador del documento.',0,null,200);

        }

    }

    public function detailsIndIncumplimiento(Request $request){

        if (!empty($request->get('id_user'))) {

            $cliente_id = $request->get('id_user');

            $cur_day = '01';
            $month = '01';
            $year = date('Y');

            if(empty($request->get('date1'))){
              $date_init = $cur_day.'-'.$month.'-'.$year;  
            }else{
               $date_init = $request->get('date1'); 
            }

            if(empty($request->get('date2'))){
              $f = carbon::now();
              $date_end = $f->format('d-m-Y');
            }else{
               $date_end = $request->get('date2'); 
            }

            $url = env('URL_USCIIWS62')."token=".env('TOKEN_SAP')."&clienteid=".$cliente_id."&fechainicial=".$date_init."&fechafinal=".$date_end;

            //return $url;

            $client = new \GuzzleHttp\Client();
            $result_tmp = $client->get($url);  
            $result_tmp2 = $result_tmp->getBody()->getContents();
            
            $result = json_decode($result_tmp2,true);
            $data_maestra = $result;

            if (isset($data_maestra[0]['code'])) {
                $table1 = [];
                $state_consult = false;
            }else{
                $table1 = $data_maestra['data']['attributes'];
                $state_consult = true;
            }

            return $this->jsonResponse($state_consult,'Operación de detalles','Información generada satisfactoriamente.',1,$table1,200);

        }else{
            return $this->jsonResponse(false,'Operación de detalles','No se recibieron los parametros necesarios para la operación.',0,null,200);
        }

    }

    public function infoReserva(Request $request){

        $cliente = $request->get('cliente'); 
        //$pedido  = $request->get('pedido');

        $url = 'http://ciadev.ciamsa.com:8000/sap/bc/zenturnamiento/zcia2/zcons_bls?';

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url, [
            'headers' => [
                          'token' => 'ABC123', 
                          'clienteid' => $cliente
                         ],
        ]);

        $result_tmp = $response->getBody()->getContents();
        //dd($result_tmp);
        $result = json_decode($result_tmp, true);

        $arr_bls = $result['cliente']['bls'];

        if (sizeof($arr_bls) > 0) {
            return $this->jsonResponse(true, 'Consumo de servicios web SAP validacion de response', 'Se ha generado información para mostrar.',1,$result,200);
        }else{
            return $this->jsonResponse(false, 'Consumo de servicios web SAP validacion de response', 'Se ha generado información para mostrar.',0,$result,200);
        }        

    }

    public function nomConductor(Request $request){

        $idconductor = $request->get('idconductor'); 

        $url = 'http://ciadev.ciamsa.com:8000/sap/bc/zenturnamiento/zcons_cond';

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url, [
            'headers' => [
                          'token' => 'ABC123', 
                          'idconductor' => $idconductor
                         ],
        ]);

        $result_tmp = $response->getBody()->getContents();
        $result = json_decode($result_tmp, true);

        //return $result[0]['message'];

        if (isset($result[0]['message'])) {
            return $this->jsonResponse(false, 'Consumo de servicios web SAP validacion de response', 'Se ha generado información para mostrar.',0,$result,200);            
        }else{
            return $this->jsonResponse(true, 'Consumo de servicios web SAP validacion de response', 'Se ha generado información para mostrar.',1,$result,200);
        }        

    }

    public function nuevaReserva(Request $request){

        $cliente = $request->get('cliente'); 
        
        $idPlaca  = $request->get('idPlaca'); 
        $idCcond  = $request->get('idCcond'); 
        $idTelcond  = $request->get('idTelcond'); 

        $idDate  = $request->get('idDate'); 
        $newDate = date("d-m-Y", strtotime($idDate));   

        $idHora  = $request->get('idHora').':00';

        $prods_arr  = $request->get('prods_arr'); 
        $prods_arr = json_decode($prods_arr, true);

        $headers = [
                      'token' => 'ABC123', 
                      'placa' => $idPlaca,
                      'conductor' => $idCcond,
                      'clienteid' => $cliente,
                      'fecha_est' => $newDate,
                      'hora_est' => $idHora
                    ];

        $url = 'http://ciadev.ciamsa.com:8000/sap/bc/zenturnamiento/zcia2/zregist_turno?';

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, [
            'headers' => $headers,
            'json' => $prods_arr,
        ]);

        $result_tmp = $response->getBody()->getContents();
        $result_tmp = json_decode($result_tmp, true);

        $res_message = $result_tmp[0]['message'];

        if ($res_message !== '') {
            return $this->jsonResponse(true, 'Consumo de servicios web SAP validacion de response', $res_message, 1, $prods_arr, 200);
        }else{
            return $this->jsonResponse(false, 'Consumo de servicios web SAP validacion de response', 'No se pudo generar la reserva en el sistema.', 0, null, 200);
        }        

    }    

}

