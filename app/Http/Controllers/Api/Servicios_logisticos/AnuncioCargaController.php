<?php

namespace App\Http\Controllers\Api\Servicios_logisticos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Controllers\GeneralController;
use Illuminate\Support\Facades\Auth;

class AnuncioCargaController extends Controller
{

    public function get_data_form(Request $request){

		$url = env('URL_USCIIWS63')."token=".env('TOKEN_SAP');

        $client = new \GuzzleHttp\Client();
        $result_tmp = $client->get($url);  
        $result_tmp2 = $result_tmp->getBody()->getContents();
        
        $result = json_decode($result_tmp2,true);
        
        if (!empty($result)) {
            $array = [];
            $array['meta'] = [];
            $array[0] = $result;
        }

        return $this->responseGeneralWs($array);

    }

    public function save_data_form(Request $request){

        $name_img = '';
        $data_base_64 = '';
        $control = 0;

        if($request->hasfile('files')){

            foreach ($request->file('files') as $file) {
                if($control > 0){
                    $data_base_64 .= ",";
                }
                //$name_img = sha1(Carbon::now()).".".$file->getClientOriginalExtension();
                $name_img = sha1(Carbon::now()).".pdf";
                \Storage::disk('tmp')->put($name_img,  \File::get($file));
                $data_base_64 .= base64_encode(file_get_contents(public_path().'/tmp/'.$name_img));
                $control = $control + 1;
            }

        }
        
        $params = array(
            'token' => env('TOKEN_SAP'), //varchar
            'numeroBL' => $request->get('numeroBL'), //varchar
            'identificacionCliente' => $request->get('identificacionCliente'),
            'nombreCliente' => $request->get('nombreCliente'),
            'importador' => $request->get('importador'),
            'tipoCarga' => $request->get('tipoCarga'),
            'muelleDescargue' => $request->get('muelleDescargue'),
            'numeroContenedor' => $request->get('numeroContenedor'),
            'lineaMaritima' => $request->get('lineaMaritima'),
            'numeroImportancion' => $request->get('numeroImportancion'),
            'motonave' => $request->get('motonave'),
            'viaje' => $request->get('viaje'),
            'eta' => $request->get('eta'),
            'fechaIngresoMuisca' => $request->get('fechaIngresoMuisca'),
            'fechaDescargue' => $request->get('fechaDescargue'),
            'productoMuisca' => $request->get('productoMuisca'),
            'cantContenedores' => $request->get('cantContenedores'),
            'arrayDocumentos' => $data_base_64
        );

        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS64'),"POST",null,$params);        
        /* Fin consumo de servicio web */

        if (!empty($response)) {
            $array = [];
            $array['meta'] = [];
            $array[0] = $response[0];
        }

        return $this->responseGeneralWs($array);


        /* Respuesta de servicio web */
        //return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */      
        
    }


}
