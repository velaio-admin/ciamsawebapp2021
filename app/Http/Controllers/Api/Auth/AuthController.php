<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $request->validate([
            'name'     => 'required|string',
            'email'    => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
        ]);
        $user = new User([
            'name'     => $request->name,
            'email'    => $request->email,
            'password' => bcrypt($request->password),
        ]);
        $user->save();
        return response()->json([
            'message' => 'Successfully created user!'], 201);
    }

    /**
    * Este servicio permite hacer loguin en la aplicacion de ciamsa.
    * 
    * @param string $email Nombre: Email. Es el correo electronico del usuario previamente registrado.
    * @param string $password Nombre: Contraseña. Es el numero del BL a consultar.
    * @param string $remember_me (Opcional) Nombre: Recuerdame. Es un campo booleano que alarga la fecha de expiracion del token, los valores posibles son 1(true) o 0(false.)
    *
    * @return Single Response
    */
    public function login(Request $request)
    {
        $request->validate([
            'email'       => 'required|string|email',
            'password'    => 'required|string',
            'remember_me' => 'boolean',
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return $this->jsonResponse(false,'Operación de login','La sesión a caducado, por favor inicie sesión nuevamente.',401,null,200);
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();

        if ($user->state != 1) {
            return $this->jsonResponse(false,'Operación de login','El usuario se encuentra en estado Inactivo.',401,null,200);
        }

        $roles = \DB::table('model_has_roles')
                ->leftJoin('roles','roles.id','=','model_has_roles.role_id')
                ->where('model_has_roles.model_id',Auth::id())
                ->select('name')
                ->get();

        $permissions = \DB::table('model_has_permissions')
                ->leftJoin('permissions','permissions.id','=','model_has_permissions.permission_id')
                ->where('model_has_permissions.model_id',Auth::id())
                ->select('name')
                ->get();

        $data = array(
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse(
                $tokenResult->token->expires_at)
                    ->toDateTimeString(),
            'roles' => $roles,
            'permissions' => $permissions,
            'user' => $user
        );

        return $this->jsonResponse(true,'Operación de login','Se ha generado el login correctamente.',0002,$data,200);
    }

    /**
    * Este servicio permite hacer logout en la aplicacion de ciamsa, solo se le envia el token en el header, con esta informacion automaticamente ejecuta el logout del usuario.
    *
    * @return Single Response
    */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return $this->jsonResponse(true,'Operación de logout','Se ha generado el logout correctamente.',0003,null,200);
    }

    /**
    * Este servicio permite traer la informacion del usuario en la aplicacion de ciamsa, solo se le envia el token en el header, con esta informacion automaticamente ejecuta este servicio del usuario.
    *
    * @return Single Response
    */
    public function user(Request $request)
    {
    		
	    	$info_user = (!empty($request->user())) ? $request->user(): null; 
	        
	        if (!empty($info_user)) {
	        	return $this->jsonResponse(true,'Operación de generación de información de usuario','Se ha generado la información correctamente.',0004,$info_user,200);
	        }else{
	        	return $this->jsonResponse(false,'Operación de generación de información de usuario','No se ha podido obtener la información.',0005,null,200);
	        }
	    
    }
}
