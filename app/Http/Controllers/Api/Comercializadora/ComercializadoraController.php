<?php

namespace App\Http\Controllers\Api\Comercializadora;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Controllers\GeneralController;
use Illuminate\Support\Facades\Auth;

class ComercializadoraController extends Controller
{	
	/**
    * Este servicio permite obtener la información de las nominaciones .
    * 
    * @param string $fechainicial (Opcional) Nombre: Fecha inicial. Es la fecha de inicio para consultar la información en un rango determinado de tiempo.
    * @param string $fechafinal (Opcional) Nombre: Fecha final. Es la fecha de finalización para consultar la información en un rango determinado de tiempo.
    * @param string $type (Opcional) Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son booking(son los booking) y radicado(son los radicado).
    * @param string $numero (Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.
    * @param integer $pagina (Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.
    * @param integer $cant_pag (Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.
    *
    * @return Single Response
    */
    public function nominaciones(Request $request){

    	/* Validacion del Request */
        /*$array_vars = array('id_bl');

    	$validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
    	if (!$validation_request_var['bool']) {
    		return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
    	}*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
                    'token' => env('TOKEN_SAP_COMERCIALIZADORA'),
					'clienteid' => $this->clienteid($request->header('idusuario')),
					'fechainicial' => (!empty($request->get('fechainicial')))?  GeneralController::cleanDatesWs($request->get('fechainicial')): null,
                    'fechafinal' => (!empty($request->get('fechafinal')))? GeneralController::cleanDatesWs($request->get('fechafinal')): null,
					'bookings' => ($request->get('type') == 'booking') ? $request->get('numero') : null,
					'radicados' => ($request->get('type') == 'radicado') ? $request->get('numero') : null,
					'paginaactual' => (!empty($request->get('pagina')))? $request->get('pagina') : 1 ,
					'registrosporpag' => (!empty($request->get('cant_pag')))? $request->get('cant_pag') : 50
                    );
        
        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS50'),"POST",null,$params);
        
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

    /**
    * Este servicio permite obtener la información del detalle de la nominacion .
    * 
    * @param string $type Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son booking(son los booking) y radicado(son los radicado).
    * @param string $numero Nombre: Numero de la nominacion. Es el numero de la nominacion a consultar.
    *
    * @return Single Response
    */
    public function detalleNominacion(Request $request){

    	/* Validacion del Request */
        $array_vars = array('type','numero');

    	$validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
    	if (!$validation_request_var['bool']) {
    		return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
    	}
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
                  'token' => env('TOKEN_SAP_COMERCIALIZADORA'),
                  'clienteid' => $this->clienteid($request->header('idusuario')),
                  'booking' => ($request->get('type') == 'booking') ? $request->get('numero') : null,
                  'radicado' => ($request->get('type') == 'radicado') ? $request->get('numero') : null
                );
        
        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS51'),"POST",null,$params);
        
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

    /**
    * Este servicio permite obtener la información del detalle de la entrega .
    * 
    * @param string $numero Nombre: Numero de la nominacion. Es el numero de la nominacion a consultar.
    *
    * @return Single Response
    */
    public function detalleEntrega(Request $request){

    	/* Validacion del Request */
        $array_vars = array('numero');

    	$validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
    	if (!$validation_request_var['bool']) {
    		return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
    	}
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
                  'token' => env('TOKEN_SAP_COMERCIALIZADORA'),
                  'clienteid' => $this->clienteid($request->header('idusuario')),
                  'id_entrega' => (!empty($request->get('numero'))) ? $request->get('numero') : null
                );
        
        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS52'),"POST",null,$params);
        
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

    /**
    * Este servicio permite obtener la información del detalle de la nominacion .
    * 
    * @param string $type (Opcional) Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son booking(son los booking) y entrega(son los entrega).
    * @param string $numero (Opcional) Nombre: Numero de la nominacion. Es el numero de la nominacion a consultar.
    *
    * @return Single Response
    */
    public function procesoLogistico(Request $request){

    	/* Validacion del Request */
        /*$array_vars = array('type','numero');

    	$validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
    	if (!$validation_request_var['bool']) {
    		return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
    	}*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
                'token' => env('TOKEN_SAP_COMERCIALIZADORA'),
                'clienteid' => $this->clienteid($request->header('idusuario')),
                'idbooking' => (null !== $request->get('type') && $request->get('type') == 'booking') ? $request->get('numero') : null,
                'identrega' => (null !== $request->get('type') && $request->get('type') == 'entrega') ? $request->get('numero') : null
              );
        
        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS53'),"POST",null,$params);
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

    /**
    * Este servicio permite obtener la información del detalle del proceso logistico .
    * 
    * @param string $type Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son booking(son los booking) y entrega(son los entrega).
    * @param string $numero Nombre: Numero de la nominacion. Es el numero de la nominacion a consultar.
    *
    * @return Single Response
    */
    public function procesoLogisticoDetalle(Request $request){

    	/* Validacion del Request */
        $array_vars = array('type','numero');

    	$validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
    	if (!$validation_request_var['bool']) {
    		return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
    	}
        /* Fin validacion del Request */

        /* Consumo de servicio web */
        $params = array(
                  'token' => env('TOKEN_SAP_COMERCIALIZADORA'),
                  'clienteid' => $this->clienteid($request->header('idusuario')),
                  'booking' => ($request->get('type') == 'booking') ? $request->get('numero') : null,
                  'IdEntrega' => ($request->get('type') == 'entrega') ? $request->get('numero') : null
                );
        
        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS54'),"POST",null,$params);
        
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

    /**
    * Este servicio permite obtener la información de los contenedores quedados .
    * 
    * @param string $numero (Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.
    * @param integer $pagina (Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.
    * @param integer $cant_pag (Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.
    *
    * @return Single Response
    */
    public function contenedoresQuedados(Request $request){

    	/* Validacion del Request */
        /*$array_vars = array('type','numero');

    	$validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
    	if (!$validation_request_var['bool']) {
    		return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
    	}*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
       	$params = array(
                  'token' => env('TOKEN_SAP_COMERCIALIZADORA'),
                  'clienteid' => $this->clienteid($request->header('idusuario')),
                  'idbooking' => (!empty($request->get('numero'))) ? $request->get('numero') : null,
                  'status' => 'QUED',
                  'paginaactual' => (!empty($request->get('pagina')))? $request->get('pagina') : 1 ,
				  'registrosporpag' => (!empty($request->get('cant_pag')))? $request->get('cant_pag') : 50
                );
        
        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS55yUSCIIWS56'),"POST",null,$params);
        
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

    /**
    * Este servicio permite obtener la información de los contenedores quedados .
    * 
    * @param string $numero (Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.
    * @param integer $pagina (Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.
    * @param integer $cant_pag (Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.
    *
    * @return Single Response
    */
    public function contenedoresReprogramados(Request $request){

    	/* Validacion del Request */
        /*$array_vars = array('type','numero');

    	$validation_request_var = $this->ValidatorJsonVars($request, $array_vars);
    	if (!$validation_request_var['bool']) {
    		return $this->jsonResponse(false,'Operacion de validacion de request', $validation_request_var['msj'],0000,null,204);
    	}*/
        /* Fin validacion del Request */

        /* Consumo de servicio web */
       	$params = array(
                  'token' => env('TOKEN_SAP_COMERCIALIZADORA'),
                  'clienteid' => $this->clienteid($request->header('idusuario')),
                  'idbooking' => (!empty($request->get('numero'))) ? $request->get('numero') : null,
                  'status' => 'RPRG',
                  'paginaactual' => (!empty($request->get('pagina')))? $request->get('pagina') : 1 ,
				  'registrosporpag' => (!empty($request->get('cant_pag')))? $request->get('cant_pag') : 50
                );
        
        $response = $this->generalWebServiceSapWs(env('URL_USCIIWS55yUSCIIWS56'),"POST",null,$params);
        
        /* Fin consumo de servicio web */

        /* Respuesta de servicio web */
        return $this->responseGeneralWs($response);
        /* Fin respuesta de servicio web */
    }

    public function obtenerPdfEntrega(Request $request){

        if ($request->get('data_detail') !== null and $request->get('data_date') !== null) {

            $data_detail = $request->get('data_detail'); 
            $data_date   = $request->get('data_date'); 

          $url = 'https://multisync.nubenegocios.com/api/ObtenerDatos';
          $APIKeyID = '1495B6F6-1AB7-40E6-9A5A-F3125C9C2416';

          //Remuevo el - del contenedor
          $data_detail_res = preg_replace('([^A-Za-z0-9 ])', '', $data_detail);
          //return $data_detail_res;

          /*$v1 = 'GATU1319882';
          $v2 = '20201018';*/

          $client = new \GuzzleHttp\Client();
          $req = $client->get($url, [
            'json' => [
              'empresa'     => 'CIAMSA',
              'proceso'     => 'CIAMSA_PDF',
              'dato_1'      => $data_detail_res,
              'dato_2'      => $data_date
            ],
            'headers' => [
              'Content-Type'  => 'application/json',
              'APIKeyID'      => $APIKeyID
          ]]);

          $response = $req->getBody()->getContents();
          //return $response;

          if ($response === '' || empty($response) || $response == '""') {
              return $this->jsonResponse(false, 'Consumo de servicios web para obtener la ruta del pdf.', 'No se encontró la ruta del archivo pdf.',0,null,200);
          }else{
            return $this->jsonResponse(true, 'Consumo de servicios web para obtener la ruta del pdf.', 'Ruta generada satisfactoriamente.',1,$response,200);
          }
            
        }else{

            return $this->jsonResponse(false, 'Consumo de servicios web para obtener la ruta del pdf.', 'No se encontraron las variables para ejecutar la consulta.',0,null,200);

        }

    }

    public function infolotesEntrega(Request $request){

        if ($request->get('clienteid') !== null || $request->get('id_entrega') !== null) {

            $clienteid  = $request->get('clienteid');
            $id_entrega = $request->get('id_entrega');
            $tkn = env('TOKEN_SAP');

            /*$clienteid  = '444000022';
            $id_entrega = '9060448';*/

            $url = env('URL_USCIIWS70');

            $url_values = $url.'token='.$tkn.'&clienteid='.$clienteid.'&id_entrega='.$id_entrega;

            $client = new \GuzzleHttp\Client();
              $req = $client->get($url_values, [
                'json' => null,
                'headers' => [
                  'Content-Type'  => 'application/json'
              ]]);

              $response_tmp = $req->getBody()->getContents();
              $response = json_decode($response_tmp,true);

              if (!isset($response['meta'])) {
                $service = [];
              }else{
                $service = $response['data']['attributes'][0]['data'];
              }

              return $this->jsonResponse(true, 'Consumo de servicios web para obtener la ruta del detalle de la entrega en nominaciones.', 'Servicio consultado exitosamente.',1,$service,200);

        }else{

            return $this->jsonResponse(false, 'Consumo de servicios web para obtener la ruta del detalle de la entrega en nominaciones.', 'No se encontraron las suficientes variables para ejecutar la consulta.',0,null,200);

        }

    }
}
