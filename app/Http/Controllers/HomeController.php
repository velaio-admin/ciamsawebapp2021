<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
/*use Illuminate\Foundation\Auth\RegistersUsers;*/
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function detail_ind($date_init, $date_end, $cliente_id)
    {
        //return $cliente_id;

        $url = env('URL_USCIIWS62')."token=".env('TOKEN_SAP')."&clienteid=".$cliente_id."&fechainicial=".$date_init."&fechafinal=".$date_end;

        //return $url;

        $client = new \GuzzleHttp\Client();
        $result_tmp = $client->get($url);  
        $result_tmp2 = $result_tmp->getBody()->getContents();
        
        $result = json_decode($result_tmp2,true);
        $data_maestra = $result;
        //dd($data_maestra);

        if (isset($data_maestra[0]['code'])) {
            $table1 = [];
        }else{
            $table1 = $data_maestra['data']['attributes'];
        }

        return view('detail')->with('table1',$table1);

    }

    public function register()
    {
        return view('auth.register')->with('error',null)->with('success',null);
    }

    public function create(Request $data)
    {
        $data->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'identificacion' => 'required',
        ]);

        try {
            
            User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'avatar' => 'img/avatars/ciamsa.png',
                'password' => Hash::make($data['password']),
                'identificacion' => $data['identificacion'],
                'tipo_cliente' => $data['tipo_cliente'],
            ]);

            //Crear en SAP
            /*$url = env('URL_ACTUALIZAR_USUARIO');
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST',$url,[
                'form_params' => [
                    'token' => env('TOKEN_SAP'),
                    'email_notif' => $data['email'],
                    'id_cliente' => $data['identificacion'],
                    'tipo_ctl' => $data['tipo_cliente'],
                    'nombre' => $data['name']
                ]
            ]);

            $result = $response->getBody()->getContents();

            $response = json_decode($result, true);
            //Fin Crear en SAP*/

            return view('auth.register')->with('error',null)->with('success','Registro satisfactorio.');

        } catch (Exception $e) {
            
            return view('auth.register')->with('error',$e)->with('success',null);

        }
    }

    public function editUserShow(Request $request){

        $user_info = $this->userInfo(Auth::id());

        return view('auth.edit_user')->with('user',$user_info);
    }

    public function editUserActualizar(Request $request){

        try {

            $img_bool = false;

            //Actualizar en SAP
           /* $url = env('URL_ACTUALIZAR_USUARIO');
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST',$url,[
                'form_params' => [
                    'token' => env('TOKEN_SAP'),
                    'email_notif' => $request->get('email'),
                    'id_cliente' => $request->get('identificacion'),
                    'tipo_ctl' => Auth::user()->tipo_cliente,
                    'nombre' => $request->get('name')
                ]
            ]);

            $result = $response->getBody()->getContents();

            $response = json_decode($result, true);*/
            //Fin actualizar en SAP

            /*Tranport image*/
            if ($request->has('avatar')) {

                $file = $request->file('avatar');

                //obtenemos el nombre del archivo
                $name_img = sha1(Carbon::now()).".".$file->getClientOriginalExtension();

                //indicamos que queremos guardar un nuevo archivo en el disco local

                if(\Storage::disk('local')->put($name_img,  \File::get($file))){

                    $source = "/".$name_img;
                    $destination = "/img/avatars/".$name_img;

                    if(\Storage::move($source, $destination)){
                        $img_bool = true;
                    }
                }
            }

            if ($img_bool) {
                $update_user = \DB::table('users')
                    ->where('id',Auth::id())
                    ->update([
                        'name' => $request->get('name'),
                        'identificacion' => $request->get('identificacion'),
                        'avatar' => $destination,
                        'email' => $request->get('email')
                    ]);
            }else{

                $update_user = \DB::table('users')
                        ->where('id',Auth::id())
                        ->update([
                            'name' => $request->get('name'),
                            'identificacion' => $request->get('identificacion'),
                            'email' => $request->get('email')
                        ]);
            }

        } catch (Exception $e) {
            //null;
        }

        $user_info = $this->userInfo(Auth::id());

        return view('auth.edit_user')->with('user',$user_info);
    }

    public function mergePdfView(){

        return view('auth.merge_pdf');
        
    }

    public function mergePdf(Request $request){

        $name = "merge_".Carbon::now()->format('Y-m-d')."-".Carbon::now()->format('H:i:s').".pdf";

        $pdf = new \PDFMerger;

        $files = $request->file('list_pdfs');

        if($request->hasFile('list_pdfs')){
            foreach ($files as $file) {
                
                $pdf->addPDF($file, 'all');
            }

            $pdf->merge('download', $name);

        }else{
            return back()->withInput();
        }

    }

    public function changePassword($id){

        return view('auth.changePassword', ['id' => $id]);
        
    }

    public function updatePassword(Request $request, $id){

        $this->validate($request, [
            'password' => 'required|string|min:6',
            'password_confirm' => 'required|string|min:6',
        ]);

        $password         = $request['password'];
        $password_confirm = $request['password_confirm'];

        if ($password !== $password_confirm) {

           return back()->with('error', 'Error!! Las contraseñas no coinciden.'); 

        }else{
            
            $id_user = $id;

            $user_info = User::find($id_user);

            $user_info->password = Hash::make($password);

            if ($user_info->save()) {
            
                return back()->with('success', 'La contraseña ha sido cambiada exitosamente!!'); 

            }else{

               return back()->with('error', 'Hubo un error de almacenamiento'); 

            }

        }

    }    

}
