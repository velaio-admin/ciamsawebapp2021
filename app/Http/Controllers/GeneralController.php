<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GeneralController extends Controller
{
//controlador para funciones generales
/** ====================================================================

* @refDocumetarion: http://docs.phpdoc.org/references/phpdoc/index.html and Javadocs
* @author:cjimenez@velaio.com
* @description:procesa una respuesta json
* @deprecated:false
* @param:response json decode object
* @return: string
* @see:none
* @example:false
* @internal:true
* @inheritdoc:false
* @api:false

*/
public static function processResponse($response, $condition)
{
  //dd($condition);
  //Método para pintar listado de contenedores fk
  $tipo = $response["data"]['type'];
  //dd($tipo); exit();
  $response_process=array();
  switch ($tipo) {
    case 'Bl':
       $header = [];
       for ($i=1; $i <= 3; $i++) { 
         /*$url = env('APP_URL').'/services/headers/serviceDetalleBl/serviceDetalleBl-table-'.$i;
         $result = file_get_contents($url);*/
         $name = "list_services.headers.service_detalle_bl_table_".$i;
         $result = config($name);
         $response_order = json_decode($result, true);
         $header[$i] = $response_order;
       }
       //print_r($header);die;
       //$pagination = $response
       $response["data"]["attributes"]= GeneralController::reOrderArray($response["data"]["attributes"],array($header[1],$header[2]));
       $response["data"]["relationships"]= GeneralController::reOrderArray($response["data"]["relationships"],array($header[3]));
       $salt = 'Bl*';
       //var_dump($response["data"]["attributes"]);die();

       foreach ($response['data']['attributes'][1]['data'] as $key => $value) {
            foreach ($value as $key_ => $value_) {
                if ($key_ == 0) {
                    $response['data']['attributes'][1]['data'][$key][$key_] = $salt.$value_;
                }
            }
        }

       $first_table = GeneralController::convertArrayToTable($response["data"]["attributes"],false,null,$salt);

       $second_table = GeneralController::convertArrayToTable($response["data"]["relationships"],false);
       $response_process[]=$first_table;
       $response_process[]=$second_table;
    break;
    case 'container':
       $header = [];
       for ($i=1; $i <= 3; $i++) { 
         /*$url = env('APP_URL').'/services/headers/serviceDetalleBl/serviceDetalleBl-table-'.$i;
         $result = file_get_contents($url);*/
         $name = "list_services.headers.service_detalle_cont_table_".$i;
         $result = config($name);
         $response_order = json_decode($result, true);
         $header[$i] = $response_order;
       }
       //print_r($header);die;
       //$pagination = $response
       $response["data"]["attributes"]= GeneralController::reOrderArray($response["data"]["attributes"],array($header[1],$header[2]));
       $response["data"]["relationships"]= GeneralController::reOrderArray($response["data"]["relationships"],array($header[3]));
       //var_dump($response["data"]["attributes"]);die();
       $first_table = GeneralController::convertArrayToTable($response["data"]["attributes"],false);
       $second_table = GeneralController::convertArrayToTable($response["data"]["relationships"],false);
       $response_process[]=$first_table;
       $response_process[]=$second_table;
    break;
    case 'Bls':
       $header = [];
       /*$url = env('APP_URL').'/services/headers/serviceRastreoList/serviceRastreoList';
       $result = file_get_contents($url);*/
       //dd($condition);

       //Condition ETA field FK
       if ($condition == 2) {
        $result = config("list_services_cond.headers.services_rastreo_list_cond");
        //dd(8);
       }else{        
        $result = config("list_services.headers.services_rastreo_list");
        //dd(9);
       }

       //$result = config("list_services.headers.services_rastreo_list");

       $response_order = json_decode($result, true);
       $header[] = $response_order;
       
       $salt = 'Bls*';
       //fk

       //dd($header[0]);

       //dd($response["data"]["attributes"][0][0]);
       
       /*Not Necesary*/
       $response["data"]["attributes"]= GeneralController::reOrderArray($response["data"]["attributes"],array($header[0]));
        
        foreach ($response['data']['attributes'][0]['data'] as $key => $value) {
            foreach ($value as $key_ => $value_) {
              if ($key_ == 0) {
                  $response['data']['attributes'][0]['data'][$key][$key_] = $salt.$value_;
              }
            }
        }

        //Data To Table FK
        //dd($response["meta"]);
        //dd($salt);

       $first_table = GeneralController::convertArrayToTable($response["data"]["attributes"],true,$response['meta'],$salt);

       //dd($first_table);

       $response_process[]=$first_table;
    break;
    case 'Facturas':
       $header = [];
       /*$url = env('APP_URL').'/services/headers/serviceRastreoList/serviceRastreoList';
       $result = file_get_contents($url);*/
       $result = config("list_services.headers.services_facturas_cupo_credito");
       $response_order = json_decode($result, true);
       $header[] = $response_order;
       $salt = 'FactD*';
       $response["data"]["attributes"]= GeneralController::reOrderArray($response["data"]["attributes"],array($header[0]));
        
          foreach ($response['data']['attributes'][0]['data'] as $key => $value) {
            $value[] = $value[1];
              foreach ($value as $key_ => $value_) {
                  if ($key_ == 5 && $value_ == "X") {
                      $response['data']['attributes'][0]['data'][$key][$key_] = $salt.$value[1];
                  }
              }
          }

       $first_table = GeneralController::convertArrayToTable($response["data"]["attributes"],true,$response['meta'],$salt);
       $response_process[]=$first_table;
    break;
    case 'HistFacturas':
       $header = [];
       /*$url = env('APP_URL').'/services/headers/serviceRastreoList/serviceRastreoList';
       $result = file_get_contents($url);*/
       $result = config("list_services.headers.services_historial_facturas");
       $response_order = json_decode($result, true);
       $header[] = $response_order;
       $salt = 'FactD*';
       $response["data"]["attributes"]= GeneralController::reOrderArray($response["data"]["attributes"],array($header[0]));
        
          foreach ($response['data']['attributes'][0]['data'] as $key => $value) {
            $value[] = $value[1];
              foreach ($value as $key_ => $value_) {
                  if ($key_ == 3 && $value_ == "X") {
                      $response['data']['attributes'][0]['data'][$key][$key_] = $salt.$value[1];
                  }
              }
          }

       $first_table = GeneralController::convertArrayToTable($response["data"]["attributes"],true,$response['meta'],$salt);
       $response_process[]=$first_table;
    break;
    case 'despachoMercancia':
       $header = [];
       $salt = 'DespM*';
       for ($i=1; $i <=2; $i++) { 
         /*$url = env('APP_URL').'/services/headers/serviceDespachoMercancia/serviceDespachoMercancia-table-'.$i;
         $result = file_get_contents($url);*/
         $name = "list_services.headers.service_despacho_mercancia_table_".$i;
         $result = config($name);
         $response_order = json_decode($result, true);
         $header[$i] = $response_order;
       }

       $response["data"]["attributes"]= GeneralController::reOrderArray($response["data"]["attributes"],array($header[1],$header[2]));

       $first_table = GeneralController::convertArrayToTable($response["data"]["attributes"],false,$response['meta'],$salt);
       $response_process[]=$first_table;
    break;
    case 'devolucionContenedor':
       $header = [];
       /*$url = env('APP_URL').'/services/headers/serviceFechaLimiteDevolucionContenedor/serviceFechaLimiteDevolucionContenedor';
       $result = file_get_contents($url);*/
       $result = config("list_services.headers.service_fecha_limite_devolucion_contenedor");
       $response_order = json_decode($result, true);
       $header[] = $response_order;

       $response["data"]["attributes"]= GeneralController::reOrderArray($response["data"]["attributes"],array($header[0]));

       $first_table = GeneralController::convertArrayToTable($response["data"]["attributes"],false);
       $response_process[]=$first_table;
    break;
    case 'nominaciones':
       $header = [];
       /*$url = env('APP_URL').'/services/headers/serviceFechaLimiteDevolucionContenedor/serviceFechaLimiteDevolucionContenedor';
       $result = file_get_contents($url);*/
       $result = config("list_services.headers.nominaciones");
       $response_order = json_decode($result, true);
       //dd($response_order);
       $salt = 'NomL*';
       $header[] = $response_order;
       $response["data"]["attributes"]= GeneralController::reOrderArray($response["data"]["attributes"],array($header[0]));

       foreach ($response['data']['attributes'][0]['data'] as $key => $value) {
              foreach ($value as $key_ => $value_) {
                  //if ($key_ === 0 || $key_ === 1) {
                  if ($key_ === 1) {
                      $response['data']['attributes'][0]['data'][$key][$key_] = $salt.$value_;
                  }
              }
          }

       $first_table = GeneralController::convertArrayToTable($response["data"]["attributes"],true,$response['meta'],$salt);
       $response_process[]=$first_table;
    break;
    case 'detalleNominacion':
       $header = [];
       for ($i=1; $i <=2; $i++) { 
         /*$url = env('APP_URL').'/services/headers/serviceDespachoMercancia/serviceDespachoMercancia-table-'.$i;
         $result = file_get_contents($url);*/
         $name = "list_services.headers.detalle_nominacion_".$i;
         $result = config($name);
         $response_order = json_decode($result, true);
         $header[$i] = $response_order;
       }
       $salt = 'EntG*';

       $response["data"]["attributes"]= GeneralController::reOrderArray($response["data"]["attributes"],array($header[1],$header[2]));
       
       foreach ($response['data']['attributes'][1]['data'] as $key => $value) {
              foreach ($value as $key_ => $value_) {
                  if ($key_ === 0) {
                      $response['data']['attributes'][1]['data'][$key][$key_] = $salt.$value_;
                  }
              }
          }

       $first_table = GeneralController::convertArrayToTable($response["data"]["attributes"],false,null,$salt);
       $response_process[]=$first_table;
    break;
    case 'consultaDespacho':
       $header = [];
       /*$url = env('APP_URL').'/services/headers/serviceFechaLimiteDevolucionContenedor/serviceFechaLimiteDevolucionContenedor';
       $result = file_get_contents($url);*/
       $result = config("list_services.headers.consultaDespacho");
       $response_order = json_decode($result, true);
       //dd($response_order);
       $salt = 'ConsD*';
       $header[] = $response_order;
       $response["data"]["attributes"]= GeneralController::reOrderArray($response["data"]["attributes"],array($header[0]));

        foreach ($response['data']['attributes'][0]['data'] as $key => $value) {
            foreach ($value as $key_ => $value_) {
                if ($key_ === 1) {
                    $response['data']['attributes'][0]['data'][$key][$key_] = $salt.$value_;
                }
            }
        }
        //dd($response);

       $first_table = GeneralController::convertArrayToTable($response["data"]["attributes"],true,$response['meta'],$salt);
       $response_process[]=$first_table;
    break;
    case 'DetalleEntrega':
       $header = [];
       /*$url = env('APP_URL').'/services/headers/existenciaMercancia/existenciaMercancia';
       $result = file_get_contents($url);*/
       $result = config("list_services.headers.entrega");
       $response_order = json_decode($result, true);
       $header[] = $response_order;
       $response["data"]["attributes"]= GeneralController::reOrderArray($response["data"]["attributes"],array($header[0]));
       

       $first_table = GeneralController::convertArrayToTable($response["data"]["attributes"],false);
       $response_process[]=$first_table;
    break;
    case 'existenciaMercancia':
       $header = [];
       /*$url = env('APP_URL').'/services/headers/existenciaMercancia/existenciaMercancia';
       $result = file_get_contents($url);*/
       $result = config("list_services.headers.existencia_mercancia");
       $response_order = json_decode($result, true);
       $header[] = $response_order;

       $response["data"]["attributes"]= GeneralController::reOrderArray($response["data"]["attributes"],array($header[0]));

       $first_table = GeneralController::convertArrayToDivs($response["data"]["attributes"]);
       $response_process[]=$first_table;
    break;

    default:
                # code...
    break;
  }
  return $response_process;
}



/** ====================================================================

* @refDocumetarion: http://docs.phpdoc.org/references/phpdoc/index.html and Javadocs
* @author:cjimenez@velaio.com
* @description:convierte arreglos en tablas html
* @deprecated:false
* @param:arreglo
* @return:string
* @see:none
* @example:false
* @internal:true
* @inheritdoc:false
* @api:false
*/
public static function convertArrayToTable($arreglo, $pagination = 1, $cantidad_pag = 50, $salt = null)
{
  //dd($arreglo[0]);
  //dd($pagination);
  if (!empty($salt)) {
    $content=view('general.convertarraytotable', ['arreglo' =>$arreglo, 'pagination' => $pagination, 'cantidad_pag' => $cantidad_pag,'salt' => $salt]); 
  }else{
    $content=view('general.convertarraytotable', ['arreglo' =>$arreglo, 'pagination' => $pagination, 'cantidad_pag' => $cantidad_pag]); 
  }
 
 $contents = $content->render();
 return $contents;
}


/** ====================================================================

* @refDocumetarion: http://docs.phpdoc.org/references/phpdoc/index.html and Javadocs
* @author:cjimenez@velaio.com
* @description:convierte arreglos en divs html
* @deprecated:false
* @param:arreglo
* @return:string
* @see:none
* @example:false
* @internal:true
* @inheritdoc:false
* @api:false
*/

public static function convertArrayToDivs($arreglo, $pagination = false)
{
 $content=view('general.convertarraytodivs', ['arreglo' =>$arreglo, 'pagination' => $pagination]); 
 $contents = $content->render();
 return $contents;
}

/** ====================================================================

* @refDocumetarion: http://docs.phpdoc.org/references/phpdoc/index.html and Javadocs
* @author:cjimenez@velaio.com
* @description:convierte un arreglo en tabla con un orden definido
* @deprecated:false
* @param:arreglo , orden
* @return:arreglo_nuevo_orden
* @see:none
* @example:false 
* @internal:true
* @inheritdoc:false
* @api:false

*/
public static function reOrderArray($arreglo,$orden){

  $arreglo_nuevo_orden=array();
  $contador = 0;
  foreach($arreglo as $key=>$row){
    if(isset($orden[$contador]) && is_array($orden[$contador])){
      $title=$orden[$contador]["title"];
      $titlevalue=$orden[$contador]["titlevalue"];
      $orden[$contador]=$orden[$contador]["columns"];
    }else{
      $title="";
      $titlevalue="";
    }
    $flagtitulo=false;
    $flagtitulodelta=0;
    foreach($row as $delta=>$value){
      if(!is_array($value)){
        if($titlevalue!=$value){
          //var_dump($orden);die();
          if(!isset($orden[$contador])){
            $order_response=false;
          }else{
            $order_response=GeneralController::search_order_by_value($value,$orden[$contador]);

          }
          //var_dump($order_response,$value,$orden[$key]);die();
          if($order_response!=false){
            $nuevo_orden=$order_response[0];   
            $arreglo_nuevo_orden[$contador][$nuevo_orden]=$order_response[1];
          }else{
            $arreglo_nuevo_orden[$contador][$delta]=$value;
          }
        }
        else{
         $flagtitulo=true;
         $arreglo_nuevo_orden[$contador]['titlevalue']=array($row['data'][0][$delta]);
         $flagtitulodelta=$delta;
         $arreglo_nuevo_orden[$contador]['title']=array($title);
       }
     }else{
      $arreglo_nuevo_orden[$contador]['data']=$value;
    }
    if(!$flagtitulo){
      if(!empty($title)){
        $arreglo_nuevo_orden[$contador]['titlevalue']=array($titlevalue);
        $arreglo_nuevo_orden[$contador]['title']=array($title);
      }
    }
  }
  if($flagtitulo){
    unset($arreglo_nuevo_orden[$contador]['data'][0][$flagtitulodelta]);
  }
  ksort( $arreglo_nuevo_orden[$contador]);
  $contador++;
}
return $arreglo_nuevo_orden;
}

public static function search_order_by_value($value,$order){

  if(is_array($order)){
    foreach($order as $delta=>$item){
                //var_dump($value,$item["variable"]);
      if($value==$item["variable"]){
        return array($item["order"],$item["label"]);
      }
    }
  }
        //die('test');
  return false;
}


/** ====================================================================

* @refDocumetarion: http://docs.phpdoc.org/references/phpdoc/index.html and Javadocs
* @author:cjimenez@velaio.com
* @description:valida si la respuesta del servicio es valida o ocurrio un error
* @deprecated:false
* @param:status
* @return:boolean
* @see:false
* @example:false
* @internal:true
* @inheritdoc:false
* @api:false

*/

public static function validateJsonResponse($response)
{
  //dd($response);
  $code = $response["meta"]["code"];
  $message = $response["meta"]["code"];
  if ($code != "1") {
    return false;
  }
  return true;
}

public static function cleanDates($date_source){

  $date = null;

  $date_tmp = explode("-",$date_source);
  $date = $date_tmp[2]."/".$date_tmp[1]."/".$date_tmp[0];

  return $date;

}

public static function cleanDatesWs($date_source){

  $date = null;

  $date_tmp = explode("-",$date_source);
  $date = $date_tmp[0]."/".$date_tmp[1]."/".$date_tmp[2];

  return $date;

}

}
