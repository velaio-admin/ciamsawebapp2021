<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    public function index(){
    	$user_info = $this->userInfo();
    	return view('Auth\role')->with('users',$user_info);
    }

    public function roleEdit($user_id){
    	$user_info = $this->userInfo($user_id);

    	$roles = \DB::table('roles')
    			->get();

    	$rel_roles = $this->relRoles($roles,$user_id);

    	$permisos = \DB::table('permissions')
    			->get();

    	$rel_permisos = $this->relPermisos($permisos,$user_id);

    	return view('Auth\role_edit')
    			->with('user',$user_info)
    			->with('roles',$roles)
    			->with('permisos',$permisos)
    			->with('rel_roles',$rel_roles)
    			->with('rel_permisos',$rel_permisos);
    }

    public function userInactive($user_id){

        $inactive = \DB::table('users')
                    ->where('id',$user_id)
                    ->update([
                        'state' => 0
                        ]);

        $user_info = $this->userInfo();
        return view('Auth\role')->with('users',$user_info);

    }

    public function userActive($user_id){

        $active = \DB::table('users')
                    ->where('id',$user_id)
                    ->update([
                        'state' => 1
                        ]);

        $user_info = $this->userInfo();
        return view('Auth\role')->with('users',$user_info);

    }

    public function userDelete($user_id){

        $delete = \DB::table('users')
                    ->where('id',$user_id)
                    ->delete();

        $user_info = $this->userInfo();
        return view('Auth\role')->with('users',$user_info);

    }

    public function userEdit(Request $request){

       $edit = \DB::table('users')
                ->where('id',$request->get('user_id'))
                ->update([
                    'name' => $request->get('name'),
                    'identificacion' => $request->get('identificacion')
                    ]);


        //Redireccionar a pagina
        $user_info = $this->userInfo($request->get('user_id'));

        $roles = \DB::table('roles')
                ->get();

        $rel_roles = $this->relRoles($roles,$request->get('user_id'));

        $permisos = \DB::table('permissions')
                ->get();

        $rel_permisos = $this->relPermisos($permisos,$request->get('user_id'));

        return view('Auth\role_edit')
                ->with('user',$user_info)
                ->with('roles',$roles)
                ->with('permisos',$permisos)
                ->with('rel_roles',$rel_roles)
                ->with('rel_permisos',$rel_permisos);

    }

    public function relRoles($roles,$user_id){

    	$array_rel = [];

    	$rel = \DB::table('model_has_roles')
    			->where('model_id',$user_id)
    			->select('role_id')
    			->get();

    	foreach ($rel as $key_rel => $value_rel) {
    		$array_rel[] = $value_rel->role_id; 
    	}

    	return $array_rel;

    }

    public function relPermisos($permisos,$user_id){

    	$array_rel = [];

    	$rel = \DB::table('model_has_permissions')
    			->where('model_id',$user_id)
    			->select('permission_id')
    			->get();

    	foreach ($rel as $key_rel => $value_rel) {
    		$array_rel[] = $value_rel->permission_id; 
    	}

    	return $array_rel;

    }

    public function assignRole(Request $request){
    	//dd($request->all());
    	$user = User::find($request->get('user_id'));
    	$first_time = true;


    	foreach ($request->all() as $key => $value) {
    		if (strpos($key, 'rol_') !== false) {

    			if ($first_time) {
    				\DB::table('model_has_roles')->where('model_id',$user->id)->delete();
    				$first_time = false;
    			}

    			$user->assignRole($value);
    		}
    	}

    	//Redireccionar a pagina
    	$user_info = $this->userInfo($request->get('user_id'));

    	$roles = \DB::table('roles')
    			->get();

    	$rel_roles = $this->relRoles($roles,$request->get('user_id'));

    	$permisos = \DB::table('permissions')
    			->get();

    	$rel_permisos = $this->relPermisos($permisos,$request->get('user_id'));

    	return view('Auth\role_edit')
    			->with('user',$user_info)
    			->with('roles',$roles)
    			->with('permisos',$permisos)
    			->with('rel_roles',$rel_roles)
    			->with('rel_permisos',$rel_permisos);

    }

    public function assignPermission(Request $request){
    	//dd($request->all());
    	$user = User::find($request->get('user_id'));
    	$first_time = true;


    	foreach ($request->all() as $key => $value) {
    		if (strpos($key, 'permiso_') !== false) {

    			if ($first_time) {
    				\DB::table('model_has_permissions')->where('model_id',$user->id)->delete();
    				$first_time = false;
    			}

    			$user->givePermissionTo($value);
    		}
    	}

    	//Redireccionar a pagina
    	$user_info = $this->userInfo($request->get('user_id'));

    	$roles = \DB::table('roles')
    			->get();

    	$rel_roles = $this->relRoles($roles,$request->get('user_id'));

    	$permisos = \DB::table('permissions')
    			->get();

    	$rel_permisos = $this->relPermisos($permisos,$request->get('user_id'));

    	return view('Auth\role_edit')
    			->with('user',$user_info)
    			->with('roles',$roles)
    			->with('permisos',$permisos)
    			->with('rel_roles',$rel_roles)
    			->with('rel_permisos',$rel_permisos);
    }
}
