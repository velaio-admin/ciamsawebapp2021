<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function userInfo($id = null){

    	if (!empty($id)) {
    		$user_info = \DB::table('users')
                    ->where('id',$id)
                    ->first();
    	}else{
    		$user_info = \DB::table('users')
                    ->get();
    	}
        

        return $user_info;
    }

    public function jsonResponse($success, $action, $message,$code_trans,$data,$code){

        return response()->json([
            "success" => $success,
            "action" => $action,
            "message" => $message,
            "code" => $code_trans,
            "data" => $data
        ], $code);
    }

    public function generalWebServiceSap($url,$method,$params_get = null,$params_post = null){
        
        if ($method == "POST") {
            
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST',$url,['timeout' => 4000,
                'form_params' => $params_post
            ]);

        }elseif($method == "GET"){

            if (!empty($params_get)) {
                $url = $url.$params_get;
            }

            $client = new \GuzzleHttp\Client();
            $response = $client->get($url);

        }

        if ($response->getStatusCode() != 500) {

            $result_tmp = $response->getBody()->getContents();
            //dd($result_tmp);
            $result_tmp2 = str_replace("},]", "}]", $result_tmp);
            $result_tmp3 = str_replace("[{{", "[{", $result_tmp2);
            $result_tmp4 = str_replace("[{{", "[{", $result_tmp3);
            $result = str_replace("[}]", "[{}]", $result_tmp4);

            if ($url == env('URL_USCIIWS53') || $url == env('URL_USCIIWS21')) {
                $result_tmp1 = str_replace("'", " und", $result);
                $result_tmp2 = str_replace("•", "", $result_tmp1);
                $result_tmp3 = trim(preg_replace("!\s+!", " ", $result_tmp2));
                $respuesta = json_decode($result_tmp3,true);
            }elseif($url == env('URL_USCIIWS33')){
                $result_tmp1 = str_replace('""', '","', $result);
                $respuesta = json_decode($result_tmp1,true);
            }else{
                $respuesta = json_decode($result,true);
            }

            return $respuesta;

        }else{

            return view('errors.500')->with('msj','Lo sentimos, en este momento el sistema la comunicación de SAP con Ciamsa Digital no está disponible, por favor intente más tarde o contacte a su administrador de sistema.');

        }
    }

    public function generalWebServiceSapWs($url,$method,$params_get = null,$params_post = null){

        //Array de tipos de json de SAP reestructurables
        $array_json_reestructurables = array(env('URL_USCIIWS20'));

        if ($method == "POST") {
            
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST',$url,['timeout' => 4000,
                'form_params' => $params_post
            ]);

        }elseif($method == "GET"){

            if (!empty($params_get)) {
                $url = $url.$params_get;
            }

            $client = new \GuzzleHttp\Client();
            $response = $client->get($url);

        }

        if ($response->getStatusCode() != 500) {

            $result_tmp = $response->getBody()->getContents();
            
            $result_tmp2 = str_replace("},]", "}]", $result_tmp);
            $result_tmp3 = str_replace("[{{", "[{", $result_tmp2);
            $result = str_replace('""1":', '","1":', $result_tmp3);

            if ($url == env('URL_USCIIWS53')) {
                $result_tmp1 = str_replace("'", " und", $result);
                $result_tmp2 = str_replace("•", "", $result_tmp1);
                $result_tmp3 = trim(preg_replace("!\s+!", " ", $result_tmp2));
                $respuesta = json_decode($result_tmp3,true);
            }else{
                $respuesta = json_decode($result,true);
            }

            //Validar el Json
            if(!$this->validateJsonResponseWs($respuesta)){
                return $this->jsonResponse(false, 'El Json recibido desde SAP no tiene una estructura correcta', 'contacte a su administrador de sistema para remediar esta situación.',0000,null,200);
            }

            //Validacion y mensaje generico de información no encontrada
            /*if (!isset($respuesta['meta'])) {
                return null; 
            }*/

            //Reestructurar Json especificos
            if (array_search($url, $array_json_reestructurables) != false) {
                $respuesta_tmp = array(
                    'meta' => $respuesta['meta'],
                    'data' => $respuesta['data'][0],
                );

                $respuesta = $respuesta_tmp;
            }

            return $respuesta;

        }else{

            $this->jsonResponse(false, 'Consumo de servicios web SAP', 'Lo sentimos, en este momento el sistema la comunicación de SAP con Ciamsa Digital no está disponible, por favor intente más tarde o contacte a su administrador de sistema.',0000,null,200);

        }
    }
    public function validateJsonResponseWs($response)
    {
        /*dd($response);
        $code = $response["meta"]["code"];
        $message = $response["meta"]["code"];*/
        if (isset($response["meta"]["code"]) && $response["meta"]["code"] != "1") {
        return false;
        }
        return true;
    }

    public function ValidatorJsonVars($request_ws, $array_vars = []){

        //Valida si las variables necesarias para el controlador llegan y son diferentes a vacio, si esto ocurre se envia un FALSE, de lo contrario un TRUE

        $valid = [];

        if (sizeof($array_vars) > 0 ) {

            foreach ($array_vars as $key => $value) {
                if (null !== $request_ws->get($value) && empty($request_ws->get($value))) {
                    $valid[] = false;
                }else{
                    $valid[] = true;
                }
            }

            if (array_search(false, $valid) != false) {
                return array('bool' => false, 'msj' => 'Hacen falta variables necesarias para el consumo del servicio web, revise e intente nuevamente.');
            }else{
                return array('bool' => true, 'msj' => null);
            }

        }else{

            return array('bool' => false, 'msj' => 'No se recibió el nombre de las variables para verificar la operación, contacte a su administrador de sistema para que verifique y corrija el problema.');

        }

    }

    public function responseGeneralWs($response){

        if (isset($response[0]['meta']) || isset($response['meta'])) {
            return $this->jsonResponse(true, 'Consumo de servicios web SAP validacion de response', 'Se ha generado información para mostrar.',0000,$response,200);
        }else{
            return $this->jsonResponse(false, 'Consumo de servicios web SAP validacion de response','No se generó información desde el servicio web para su busqueda.',0000,null,200);
        }

    }

    public function clienteid($id_cliente){
        if (empty($id_cliente)) {
            return $this->jsonResponse(false, 'Validacion de información de usuario', 'No se pudo obtener información del usuario, verifique si el id del mismo fue enviado',0000,null,200);
        }else{

            $Cliente = \DB::table('users')
                        ->where('id',$id_cliente)
                        ->first();

            if (!empty($Cliente)) {
                return $Cliente->identificacion;
            }else{
                return $this->jsonResponse(false, 'Validacion de información de usuario', 'No se pudo obtener información del usuario, el id de usuario no existe o no esta registrado',0000,null,200);
            }

        }
    }

    public function paginate($items,$perPage)
    {
        $pageStart = \Request::get('page', 1);
        // Start displaying items from this number;
        $offSet = ($pageStart * $perPage) - $perPage; 

        // Get only the items you need using array_slice

        //$itemsForCurrentPage = array_slice($items, $offSet, $perPage, true);
        $itemsForCurrentPage = $items;

        //return new LengthAwarePaginator($itemsForCurrentPage, count($items), $perPage,Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
        return new LengthAwarePaginator($itemsForCurrentPage, 25, $perPage, Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
    }

}
