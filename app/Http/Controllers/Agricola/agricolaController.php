<?php

namespace App\Http\Controllers\Agricola;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class AgricolaController extends Controller
{
    /*
     *
     * Consultar cupo de credito
     *
     */
    public function getTypeInvoiceStates()
    {
        $params = array(
            'TOKEN' => env('TOKEN_SAP_AGRICOLA'),
        );

        $response = $this->generalWebServiceSap(env('URL_USCIIWS27'), 'POST', null, $params);

        if (isset($response['meta']) && $response['meta']['code'] == 1) {
            $return = $response['data']['attributes'][0]['data'][0];
        } else {
            $return = null;
        }

        return $return;
    }

    public function cupoCredito($cliente_id)
    {
        $params = array(
            'TOKEN' => env('TOKEN_SAP_AGRICOLA'),
            'ClientId' => $cliente_id,
        );

        $response = $this->generalWebServiceSap(env('URL_USCIIWS30'), 'POST', null, $params);

        if (isset($response['meta']) && $response['meta']['code'] == 1) {
            $data = $response['data']['attributes'][0]['data'];

            //$cupo_credito_val = (int) str_replace(' ', '', $data[0][1]);
            $cupo_credito_val = str_replace(' ', '', $data[0][1]);


            if (!empty($cupo_credito_val)) {

                $cupo_credito_val_tmp = (int)str_replace('.', '', $data[0][1]);

                $per_credito_val = 100;

                //$cupo_utilizado_val = (int) str_replace(' ', '', $data[1][1]);
                $cupo_utilizado_val = str_replace(' ', '', $data[1][1]);
                $cupo_utilizado_val_tmp = (int)str_replace('.', '', $cupo_utilizado_val);
                $per_utilizado_val = ($cupo_utilizado_val_tmp / $cupo_credito_val_tmp) * 100;

                //$cupo_disponible_val = (int) str_replace(' ', '', $data[2][1]);
                $cupo_disponible_val = str_replace(' ', '', $data[2][1]);
                $cupo_disponible_val_tmp = (int)str_replace('.', '', $cupo_disponible_val);

                if(strpos($cupo_disponible_val,"-") !== false){
                    $cupo_disponible_val = 0;
                    $cupo_disponible_val_tmp = 0;
                }

                $per_disponible_val = (($cupo_disponible_val_tmp / $cupo_credito_val_tmp) * 100);
            } else {
                $cupo_credito_val = 0;
                $per_credito_val = 100;

                $cupo_utilizado_val = 0;
                $per_utilizado_val = 0;

                $cupo_disponible_val = 0;
                $per_disponible_val = 0;
            }

            //dd($cupo_credito_val,$cupo_utilizado_val,$cupo_disponible_val);

            $return = array(
                'credito_aprobado' => $cupo_credito_val,
                'per_credito_aprobado' => round($per_credito_val, 2),
                'credito_utilizado' => $cupo_utilizado_val,
                'per_credito_utilizado' => round($per_utilizado_val, 2),
                'credito_disponible' => $cupo_disponible_val,
                'per_credito_disponible' => round($per_disponible_val, 2),
            );
        } else {
            $return = null;
        }

        return $return;
    }

    public function descargarFactura($numero_factura)
    {
        $params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA'),
            'NumeroFactura' => $numero_factura,
        );

        $response = $this->generalWebServiceSap(env('URL_USCIIWS29'), 'POST', null, $params);

        if (isset($response['data']['attributes'][0]['data'][0][0]) && $response['data']['attributes'][0]['data'][0][0] != '') {
            $pdf = $response['data']['attributes'][0]['data'][0][0];

            // Real date format (xxx-xx-xx)
            $toDay = date('Y-m-d');

            // we give the file a random name
            $name = 'invoice_'.$toDay.'_'.$numero_factura.'.pdf';

            // a route is created, (it must already be created in its repository(pdf)).
            $rute = 'tmp/'.$name;

            // decode base64
            $pdf_b64 = base64_decode($pdf);

            // you record the file in existing folder
            if (file_put_contents($rute, $pdf_b64)) {

                //just to force download by the browser
                $headers = [
                    'Content-Type' => 'application/pdf',
                ];

                //print base64 decoded
                return response()->file($rute, $headers);
            }
        } else {
            $return = null;
        }

        return $return;
    }

    public function cupoCreditoView($estado, $cliente_id, $pagina, $cant_pag)
    {
        $estados_factura = $this->getTypeInvoiceStates();
        $cupo_credito = $this->cupoCredito($cliente_id);
        // $descargar_factura = $this->descargarFactura('0000122720');

        $params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA'),
            'Estado' => (!empty($estado) && $estado != 'all') ? $estado : null,
            'ClientId' => $cliente_id,
            'Numerofactura' => null,
            'Fechainicial' => Carbon::now()->subMonths(12)->format('d/m/Y'),
            'Fechafinal' => Carbon::now()->format('d/m/Y'),
            'PaginaActual' => (!empty($pagina)) ? $pagina : 1,
            'NumeroRegistrosPorPagina' => (!empty($cant_pag)) ? $cant_pag : 50,
        );

        $response_tmp = $this->generalWebServiceSap(env('URL_USCIIWS28'), 'POST', null, $params);
//dd($cupo_credito,$response_tmp);
        if (!isset($response_tmp['meta']) || $response_tmp['meta']['code'] == 0) {
            return view('agricola.cupoCredito.cupoCredito', ['estados_factura' => $estados_factura, 'cupo_credito' => $cupo_credito, 'table1' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>"]);
        }

        $response = array(
            'meta' => $response_tmp['meta'],
            'data' => $response_tmp['data'],
        );

        //print_r($response);die;
        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            return 'error';
        }
//dd($response['data']['attributes'][0]['data']);
        //arreglarle los menos
        foreach ($response['data']['attributes'][0]['data'] as $key => $value) {
            if ($value[2] != null && strpos($value[2], '-') !== false) {
                $explode = explode("-", $value[2]);
                $response['data']['attributes'][0]['data'][$key][2] = '-'.$explode[0];
            }
            //$response['data']['attributes'][0]['data'][$key][4] = '$ '.number_format($value[4], 0, '.', ',');
            $response['data']['attributes'][0]['data'][$key][4] = '$ '.$value[4];
        }

        $html_proceso = GeneralController::processResponse($response, $condition=1);
        //dd($estados_factura,$cupo_credito,$response_tmp,$response,$html_proceso);
        return view('agricola.cupoCredito.cupoCredito', ['estados_factura' => $estados_factura, 'cupo_credito' => $cupo_credito, 'table1' => $html_proceso[0]]);
    }

    public function cupoCreditoViewBuscador(Request $request)
    {
        $estados_factura = $this->getTypeInvoiceStates();
        $cupo_credito = $this->cupoCredito($request->get('cliente_id'));
        // $descargar_factura = $this->descargarFactura('0000122720');

        $params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA'),
            'Estado' => (!empty($request->get('status')) && $request->get('status') != 'all') ? $request->get('status') : null,
            'ClientId' => $request->get('cliente_id'),
            'Numerofactura' => (!empty($request->get('numero'))) ? $request->get('numero') : null,
            'Fechainicial' => (!empty($request->get('fechainicial'))) ? GeneralController::cleanDates($request->get('fechainicial')) : null,
            'Fechafinal' => (!empty($request->get('fechafinal'))) ? GeneralController::cleanDates($request->get('fechafinal')) : null,
            'PaginaActual' => (!empty($pagina)) ? $pagina : 1,
            'NumeroRegistrosPorPagina' => (!empty($cant_pag)) ? $cant_pag : 50,
        );

        $response_tmp = $this->generalWebServiceSap(env('URL_USCIIWS28'), 'POST', null, $params);

        if (!isset($response_tmp['meta']) || $response_tmp['meta']['code'] == 0) {
            return view('agricola.cupoCredito.cupoCredito', ['estados_factura' => $estados_factura, 'cupo_credito' => $cupo_credito, 'table1' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>"]);
        }

        $response = array(
            'meta' => $response_tmp['meta'],
            'data' => $response_tmp['data'],
        );

        //print_r($response);die;
        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            return 'error';
        }

        //arreglarle los menos
        foreach ($response['data']['attributes'][0]['data'] as $key => $value) {
            if ($value[2] != null && strpos($value[2], '-') !== false) {
                $explode = explode("-", $value[2]);
                $response['data']['attributes'][0]['data'][$key][2] = '-'.$explode[0];
            }
            //$response['data']['attributes'][0]['data'][$key][4] = '$ '.number_format($value[4], 0, '.', ',');
            $response['data']['attributes'][0]['data'][$key][4] = '$ '.$value[4];
        }

        $html_proceso = GeneralController::processResponse($response, $condition=1);
        //dd($html_proceso);
        return view('agricola.cupoCredito.cupoCredito', ['estados_factura' => $estados_factura, 'cupo_credito' => $cupo_credito, 'table1' => $html_proceso[0]]);
    }

    public function historialFacturas($cliente_id, $pagina, $cant_pag)
    {
        $params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA'),
            'ClientId' => $cliente_id,
            'Numerofactura' => null,
            'Fechainicial' => Carbon::now()->subMonths(12)->format('d/m/Y'),
            'Fechafinal' => Carbon::now()->format('d/m/Y'),
            'PaginaActual' => (!empty($pagina)) ? $pagina : 1,
            'NumeroRegistrosPorPagina' => (!empty($cant_pag)) ? $cant_pag : 50,
        );

        //dd($params); die();

        $response_tmp = $this->generalWebServiceSap(env('URL_USCIIWS31'), 'POST', null, $params);

        //dd($response_tmp); die();

        if (!isset($response_tmp['meta']) || $response_tmp['meta']['code'] == 0) {
            return view('agricola.facturas.facturas', ['table1' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>"]);
        }


        $response = array(
            'meta' => $response_tmp['meta'],
            'data' => $response_tmp['data'],
        );

        //print_r($response);die;
        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            return 'error';
        }

        //arreglarle los menos
        foreach ($response['data']['attributes'][0]['data'] as $key => $value) {
            $response['data']['attributes'][0]['data'][$key][2] = '$ '.number_format($value[2], 0, '.', ',');
        }

        $html_proceso = GeneralController::processResponse($response, $condition=1);
        //dd($estados_factura,$cupo_credito,$response_tmp,$response,$html_proceso);
        return view('agricola.facturas.facturas', ['table1' => $html_proceso[0]]);
    }

    public function historialFacturasBuscador(Request $request)
    {
        // $descargar_factura = $this->descargarFactura('0000122720');

        $params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA'),
            'ClientId' => $cliente_id,
            'Numerofactura' => (!empty($request->get('numero'))) ? $request->get('numero') : null,
            'Fechainicial' => (!empty($request->get('fechainicial'))) ? GeneralController::cleanDates($request->get('fechainicial')) : null,
            'Fechafinal' => (!empty($request->get('fechafinal'))) ? GeneralController::cleanDates($request->get('fechafinal')) : null,
            'PaginaActual' => (!empty($pagina)) ? $pagina : 1,
            'NumeroRegistrosPorPagina' => (!empty($cant_pag)) ? $cant_pag : 50,
        );

        $response_tmp = $this->generalWebServiceSap(env('URL_USCIIWS31'), 'POST', null, $params);

        if (!isset($response_tmp['meta']) || $response_tmp['meta']['code'] == 0) {
            return view('agricola.facturas.facturas', ['table1' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>"]);
        }

        $response = array(
            'meta' => $response_tmp['meta'],
            'data' => $response_tmp['data'],
        );

        //print_r($response);die;
        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            return 'error';
        }

        //arreglarle los menos
        foreach ($response['data']['attributes'][0]['data'] as $key => $value) {
            $response['data']['attributes'][0]['data'][$key][2] = '$ '.number_format($value[2], 0, '.', ',');
        }

        $html_proceso = GeneralController::processResponse($response, $condition=1);
        //dd($html_proceso);
        return view('agricola.facturas.facturas', ['table1' => $html_proceso[0]]);
    }

    public function consultaPedido()
    {
        $cliente_id = Auth::user()->identificacion;

        $params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA'),
            'ClientId' => $cliente_id,
            'fechainicial' => Carbon::now()->subMonths(2)->format('d/m/Y'),
            'fechafinal' => Carbon::now()->format('d/m/Y'),
            'NumeroPedido' => null,
            'Estado' => null,
            'PaginaActual' => 1,
            'NumeroRegistrosPorPagina' => 50,
        );
        /*$response = '{"meta":{"totalPages":1,"currentPage":1,"code":1,"message":" "},"data":{"type":"consultaDespacho","attributes":[{"0":"factura","1":"pedido","2":"cantidad","3":"entregado","4":"saldo","5":"estado","data":[{"0":"10","1":"20","3":"30","4":"10","5":"20","6":"Pedido radicado"},{"0":"450","1":"490","3":"60","4":"10","5":"50","6":"Camión despachado"}]}]}}';

        $response = json_decode($response, true);*/

        $response = $this->generalWebServiceSap(env('URL_USCIIWS32'),"POST",null,$params);

        if (!isset($response['meta']) || $response['meta']['code'] == 0) {
            return view('agricola.rastreoPedido.consultaDespacho', ['table1' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>"]);
        }
        
        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            return 'error';
        }

        //arreglarle los menos
        $new_array = [];
        foreach ($response['data']['attributes'][0]['data'] as $key => $value) {

            if(strpos($value[3],"-") !== false){

                $clean = str_replace('-', '', $value[3]);
                $response['data']['attributes'][0]['data'][$key][3] = number_format($clean, 0, ',', '.');

            }else{
                $response['data']['attributes'][0]['data'][$key][3] = number_format($value[3], 0, ',', '.');
            }

            if(strpos($value[4],"-") !== false){

                $clean = str_replace('-', '', $value[4]);
                $response['data']['attributes'][0]['data'][$key][4] = number_format($clean, 0, ',', '.');

            }else{
                $response['data']['attributes'][0]['data'][$key][4] = number_format($value[4], 0, ',', '.');
            }

            if(strpos($value[5],"-") !== false){

                $clean = str_replace('-', '', $value[5]);
                $response['data']['attributes'][0]['data'][$key][5] = number_format($clean, 0, ',', '.');

            }else{
                $response['data']['attributes'][0]['data'][$key][5] = number_format($value[5], 0, ',', '.');
            }

        
            if ($value[8] === "T" || $value[8] === "S50")  {
                $new_array[$key]=$response['data']['attributes'][0]['data'][$key];
            }

        }

        $response['data']['attributes'][0]['data'] = $new_array;

        
        $html_proceso = GeneralController::processResponse($response, $condition=1);

        return view('agricola.rastreoPedido.consultaDespacho', ['table1' => $html_proceso[0]]);
        //return view('agricola.consultaDespacho.consultaDespacho', ['data' => $response]);
    }

    public function consultaPedidoBuscador(Request $request)
    {
        $cliente_id = Auth::user()->identificacion;

        $params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA'),
            'ClientId' => $cliente_id,
            'fechainicial' => (!empty($request->get('fechainicial'))) ? GeneralController::cleanDates($request->get('fechainicial')) : null,
            'fechafinal' => (!empty($request->get('fechafinal'))) ? GeneralController::cleanDates($request->get('fechafinal')) : null,
            'NumeroPedido' => (!empty($request->get('numero'))) ? $request->get('numero') : null,
            'Estado' => null,
            'PaginaActual' => 1,
            'NumeroRegistrosPorPagina' => 50,
        );
        /*$response = '{"meta":{"totalPages":1,"currentPage":1,"code":1,"message":" "},"data":{"type":"consultaDespacho","attributes":[{"0":"factura","1":"pedido","2":"cantidad","3":"entregado","4":"saldo","5":"estado","data":[{"0":"10","1":"20","3":"30","4":"10","5":"20","6":"Pedido radicado"},{"0":"450","1":"490","3":"60","4":"10","5":"50","6":"Camión despachado"}]}]}}';

        $response = json_decode($response, true);*/

        $response = $this->generalWebServiceSap(env('URL_USCIIWS32'),"POST",null,$params);

        if (!isset($response['meta']) || $response['meta']['code'] == 0) {
            return view('agricola.rastreoPedido.consultaDespacho', ['table1' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>"]);
        }

        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            // return error
        }

        //arreglarle los menos
        foreach ($response['data']['attributes'][0]['data'] as $key => $value) {

            if(strpos($value[3],"-") !== false){

                $clean = str_replace('-', '', $value[3]);
                $response['data']['attributes'][0]['data'][$key][3] = number_format($clean, 0, ',', '.');

            }else{
                $response['data']['attributes'][0]['data'][$key][3] = number_format($value[3], 0, ',', '.');
            }

            if(strpos($value[4],"-") !== false){

                $clean = str_replace('-', '', $value[4]);
                $response['data']['attributes'][0]['data'][$key][4] = number_format($clean, 0, ',', '.');

            }else{
                $response['data']['attributes'][0]['data'][$key][4] = number_format($value[4], 0, ',', '.');
            }

            if(strpos($value[5],"-") !== false){

                $clean = str_replace('-', '', $value[5]);
                $response['data']['attributes'][0]['data'][$key][5] = number_format($clean, 0, ',', '.');

            }else{
                $response['data']['attributes'][0]['data'][$key][5] = number_format($value[5], 0, ',', '.');
            }

        }
        
        $html_proceso = GeneralController::processResponse($response, $condition=1);

        return view('agricola.rastreoPedido.consultaDespacho', ['table1' => $html_proceso[0]]);
        //return view('agricola.consultaDespacho.consultaDespacho', ['data' => $response]);
    }

    public function detallePedido($id_pedido)
    {

        $params = array(
            'Token' => env('TOKEN_SAP'),
            'idpedido' => $id_pedido,
        );
        /*$response = '{"meta":{"totalPages":1,"currentPage":1,"code":1,"message":" "},"data":{"type":"detallePedido","attributes":[{"0":"factura","1":"pedido","2":"cantidad","3":"entregado","4":"saldo","5":"estado","data":[{"0":"10","1":"20","3":"30","4":"10","5":"20","6":"Pedido radicado"}]}],"relationships":{"tracking":{"0":"FechaUltimoMovimiento","1":"EstadoDespacho","data":[{"0":"10/10/2018 20:20","1":"Camion despachado"},{"0":"11/10/2018 20:20","1":"Camion ingreso"}]}}}}';

        $response = json_decode($response, true);*/

        $response = $this->generalWebServiceSap(env('URL_USCIIWS33'),"POST",null,$params);

        if (!isset($response['meta']) || $response['meta']['code'] == 0) {
            return view('agricola.rastreoPedido.rastreoPedido', ['data' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>"]);
        }
        
        //dd($response);
        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            // return error
        }
        // $response = $this->generalWebServiceSap(env('URL_USCIIWS51'),"POST",null,$params);

        //arreglarle los menos
        foreach ($response['data']['attributes'][0]['data'] as $key => $value) {

            if(strpos($value[3],"-") !== false){

                $clean = str_replace('-', '', $value[3]);
                $response['data']['attributes'][0]['data'][$key][3] = number_format($clean, 0, ',', '.');

            }else{
                $response['data']['attributes'][0]['data'][$key][3] = number_format($value[3], 0, ',', '.');
            }

            if(strpos($value[4],"-") !== false){

                $clean = str_replace('-', '', $value[4]);
                $response['data']['attributes'][0]['data'][$key][4] = number_format($clean, 0, ',', '.');

            }else{
                $response['data']['attributes'][0]['data'][$key][4] = number_format($value[4], 0, ',', '.');
            }

            if(strpos($value[5],"-") !== false){

                $clean = str_replace('-', '', $value[5]);
                $response['data']['attributes'][0]['data'][$key][5] = number_format($clean, 0, ',', '.');

            }else{
                $response['data']['attributes'][0]['data'][$key][5] = number_format($value[5], 0, ',', '.');
            }

        }

        $html_proceso = GeneralController::processResponse($response, $condition=1);

        return view('agricola.rastreoPedido.rastreoPedido', ['data' => $response]);
    }

    public function solicitarPedido()
    {

        $msjBool = false;
        $msj = '';

        $cliente_id = Auth::user()->identificacion;

        $params = array(
            'Token' => env('TOKEN_SAP_AGRICOLA')
        );

        $ciudades = $this->generalWebServiceSap(env('URL_USCIIWS37'),"POST",null,$params); 
        //dd($ciudades);//Servicio con problemas

        $params_formp = array(
            'Token' => env('TOKEN_SAP_AGRICOLA'),
            'Clienteid' => $cliente_id
        );

        $formaDePago = $this->generalWebServiceSap(env('URL_USCIIWS38'),"POST",null,$params_formp); //Servicio con problemas
        //dd($formaDePago);

        $producto = $this->generalWebServiceSap(env('URL_USCIIWS40'),"POST",null,$params); //Falta subir la lista de precios y productos
        //dd($producto);

        if (empty($ciudades['data']['attributes'][0]['data'][0]) || sizeof($ciudades['data']['attributes'][0]['data'][0]) < 0) {
            $msjBool = true;
            $msj = 'No se tienen ciudades reportadas en SAP para solicitar pedidos, por favor comuniquese con su administrador de sistema, solicite que sean diligenciadas y vuelva a intentar nuevamente.';
        }

        if (empty($formaDePago['data']['attributes'][0]['data'][0]) || sizeof($formaDePago['data']['attributes'][0]['data'][0]) < 0) {
            $msjBool = true;
            $msj = 'El cliente no tiene relacionadas formas de pago en SAP, por favor comuniquese con su administrador de sistema, solicite que sean diligenciadas y vuelva a intentar nuevamente.';
        }

        if (empty($producto['data'][0]['attributes'][0]['data']) || sizeof($producto['data'][0]['attributes'][0]['data']) < 0) {
            $msjBool = true;
            $msj = 'No hemos recibido un listado de productos desde SAP, por favor comuniquese con su administrador de sistema, solicite que sean diligenciados y vuelva a intentar nuevamente.';
        }

        return view('agricola.solicitarPedido.solicitarPedido', [
            'ciudades' => $ciudades,
            'formaDePago' => $formaDePago,
            'producto' => $producto,
            'msjBool' => $msjBool,
            'msj' => $msj,
        ]);
    }

    public function tipoProductos($type)
    {
        $list = [];

        $params = array(
                    'token' => env('TOKEN_SAP_AGRICOLA')
                );

        $data = $this->generalWebServiceSap(env('URL_USCIIWS40'),"POST",null,$params);

        if (isset($data['meta'])) {
            $list_ws = $data['data'][0]['attributes'][0]['data'];

            foreach ($list_ws as $key => $value) {
                if ($value[0]['atributos'][0]['tipo'] == $type) {
                    $list[$key]['nombre'] = $value[0]['nombre'];
                    $list[$key]['atributos'] = $value[0]['atributos'][0];
                }
            }
        }

        if (sizeof($list) > 0) {
            return $this->jsonResponse(true, 'Operación de lista de productos', 'Se ha generado satisfactoriamente la lista de productos.', 4, $list, 200);
        } else {
            return $this->jsonResponse(false, 'Operación de lista de productos', 'No se ha generado la lista de productos.', 5, null, 200);
        }
    }

    public function generarPedido(Request $request)
    {
        if (!empty($request->get('Productos'))) {

            $json = json_decode($request->get('Productos'));

            $params = array(
                'Token' => env('TOKEN_SAP_AGRICOLA'),
                'ClientId' => $json->id_info_u,
                'productos' => $request->get('Productos'),
            );

            $response = $this->generalWebServiceSap(env('URL_USCIIWS41'), 'POST', null, $params);

            if (isset($response[0]['code']) && $response[0]['code'] == 1) {
                return $this->jsonResponse(true, 'Operación de radicacion de pedido',  $response[0]['message'], 4, null, 200);
            } else {
                return $this->jsonResponse(false, 'Operación de radicacion de pedido', $response[0]['message'], 5, null, 200);
            }
        } else {
            return $this->jsonResponse(false, 'Operación de radicacion de pedido', 'No se ha recibido la informacion de productos por radicar.', 6, null, 200);
        }
    }

    public function notificarPedido(Request $request){

        if (!empty($request->get('id_info_u'))) {

            if (!empty($request->get('Productos'))) {

                $json = $request->get('Productos');

                if (sizeof($json['productos']) > 0) {

                    $params = array(
                        'Token' => env('TOKEN_SAP_AGRICOLA'),
                        'ClientId' => $request->get('id_info_u'),
                        'productos' => $request->get('Productos')
                    );

                    $response = $this->generalWebServiceSap(env('URL_USCIIWS46'), 'POST', null, $params);
                    
                    if (isset($response[0]['code']) && $response[0]['code'] == 1) {
                        return $this->jsonResponse(true, 'Operación de radicacion de pedido',  $response[0]['message'], 4, null, 200);
                    } else {
                        return $this->jsonResponse(false, 'Operación de radicacion de pedido', $response[0]['message'], 5, null, 200);
                    }
                }else{
                   return $this->jsonResponse(false, 'Operación de radicacion de pedido', 'No se recibió ningún producto', 7, null, 200); 
                }
            }else{
                return $this->jsonResponse(false, 'Operación de radicacion de pedido', 'No se recibió un array de productos', 6, null, 200);
            }
        } else {
            return $this->jsonResponse(false, 'Operación de radicacion de pedido', 'No se ha recibido la informacion de productos por radicar.', 7, null, 200);
        }

    }

    public function reservarTurnoAgricola($cliente_id,$pagina,$cant_pag)
    { 

        return view('agricola.reservarTurnoAgricola.reservarTurnoAgricola', ['cliente_id' => $cliente_id]); 

    }
    
}
