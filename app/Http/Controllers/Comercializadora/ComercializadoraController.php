<?php

namespace App\Http\Controllers\Comercializadora;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ComercializadoraController extends Controller
{
   	public function nominaciones($cant_reg){

      $clienteid = Auth::user()->identificacion;

        if (isset($_GET['page'])) {
          $currentPage = $_GET['page'];
        }else{
          $currentPage = 1;
        }

        $url = env('URL_USCIIWS50')."token=".env('TOKEN_SAP')."&clienteid=".$clienteid."&registrosporpag=".$cant_reg."&paginaactual=".$currentPage;

        \Log::info('service nominaciones '.env('URL_USCIIWS50')); 

        $client = new \GuzzleHttp\Client();
        $result_tmp = $client->get($url);                
        $response = $result_tmp->getBody()->getContents();
        $response_tmp = json_decode($response, true);  

        //return $response_tmp;

        if (!isset($response_tmp['meta'])) {
            return view('comercializadora.nominaciones.nominaciones', ['table1' => [], 'cant_reg' => $cant_reg]); 
        }

        $response = array(
            'meta' => $response_tmp['meta'],
            'data' => $response_tmp['data'][0],
        );
       
        //$html_proceso = GeneralController::processResponse($response, $condition=1);

        $arr_data = $response['data']['attributes'][0]['data'];

        //Paginator fkm
        //$arr_data = $this->paginate($arr_data, 10);
        //return view('comercializadora.nominaciones.nominaciones', ['table1' =>$html_proceso[0]]); 
        return view('comercializadora.nominaciones.nominaciones', ['table1' =>$arr_data, 'cant_reg' => $cant_reg]); 

   	}

   	public function nominacionesBuscador(Request $request){

      $PaginaActual = 1;
      $NumeroRegistrosPorPagina = 50;
      
      if (empty($request->get('cliente_id'))) {

        return view('comercializadora.procesoLogistico.procesoLogistico', ['data' => "<br><p class='text-center'>No se recibió el NIT del cliente para consultar.</p>"]); 
      }

      $params = array(
                    'token' => env('TOKEN_SAP'),
        						'clienteid' => $request->get('cliente_id'),
        						'fechainicial' => (!empty($request->get('fechainicial'))) ? GeneralController::cleanDates($request->get('fechainicial')) : null,
        						'fechafinal' => (!empty($request->get('fechafinal'))) ? GeneralController::cleanDates($request->get('fechafinal')) : null,
        						'bookings' => ($request->get('type') == 'booking') ? $request->get('numero') : null,
        						'radicados' => ($request->get('type') == 'radicado') ? $request->get('numero') : null,
                    'entregas' => ($request->get('type') == 'entrega') ? $request->get('numero') : null
        						/*'paginaactual' => (!empty($PaginaActual))? $PaginaActual : 1 ,
        						'registrosporpag' => (!empty($NumeroRegistrosPorPagina))? $NumeroRegistrosPorPagina : 50*/
                    );

     //return $params;

      $response_tmp = $this->generalWebServiceSap(env('URL_USCIIWS50'),"POST",null,$params);

      if (!isset($response_tmp['meta'])) {
          return view('comercializadora.nominaciones.nominaciones', ['table1' => []]); 
      }

      $response = array(
          'meta' => $response_tmp['meta'],
          'data' => $response_tmp['data'][0],
      );

      $validation = GeneralController::validateJsonResponse($response_tmp);
      if (!$validation) {
          // return error
      }

      $arr_data = $response['data']['attributes'][0]['data'];

      //Paginator fkm
      //$arr_data = $this->paginate($arr_data, 10);

      return view('comercializadora.nominaciones.nominaciones', ['table1' =>$arr_data]);
      
      //$html_proceso = GeneralController::processResponse($response, $condition=1);
      //return view('comercializadora.nominaciones.nominaciones', ['table1' =>$html_proceso[0]]); 

   	}

  	public function detalleNominacion($numero,$cliente_id){

      $param = strpos($numero,'-');
      $booking = null;
      $radicado = null;

      if($param !== false){
        $radicado = $numero;
      }else{
        $booking = $numero;
      }

      $params = array(
                  'token' => env('TOKEN_SAP'),
                  'clienteid' => $cliente_id,
                  'booking' => $booking,
                  'radicado' => $radicado
                );
      
      $response = $this->generalWebServiceSap(env('URL_USCIIWS51'),"POST",null,$params);

      //return $response;

      if (!isset($response['meta'])) {
          $table = null;
          return view('comercializadora.nominaciones.detalleNominacion', ['table' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>", "cliente_id" => $cliente_id]); 
      }

      /*$response = array(
          'meta' => $response_tmp['meta'],
          'data' => $response_tmp['data'][0],
      );*/
 //dd($response);
      $validation = GeneralController::validateJsonResponse($response);
      if (!$validation) {
          // return error
      }

      //$html_proceso = GeneralController::processResponse($response);
      $units_temp=[];
      $sum = [];
      foreach ($response['data']['attributes'][1]['data'] as $key_1 => $value_1) {
        foreach ($value_1 as $key_2 => $value_2) {
          if ($key_2 == 7) {
            $units_temp[] = $value_2;
          }
        }
      }

      if (sizeof($units_temp) > 0) {
        $units = array_unique($units_temp);
        foreach ($units as $key_assig_units => $value_assig_units) {
          $sum[$value_assig_units]= array( '','Total '.$value_assig_units.'',0,0,0,0,'','');
        }
        $sum['total']= array( '','Total',0,0,0,0,'','');
      }

      //sumatoria de entregas
      //dd($response['data']['attributes'][1]['data'],$sum,$units);
      foreach ($response['data']['attributes'][1]['data'] as $key_1 => $value_1) {
        foreach ($value_1 as $key_2 => $value_2) {
          //dd($key_2,$value_2);
          if ($key_2 == 2 || $key_2 == 3 || $key_2 == 4 || $key_2 == 5) {
            $response['data']['attributes'][1]['data'][$key_1][$key_2] = number_format((float)$value_2, 0, ',', '.');
            $value_temp = (float)$value_2 + $sum[$value_1[7]][$key_2];
            $value_temp_2 = (float)$value_2 + $sum['total'][$key_2];//total
            $sum[$value_1[7]][$key_2] = number_format((float)$value_temp, 0, ',', '.');
            $sum['total'][$key_2] = number_format((float)$value_temp_2, 0, ',', '.');//total
          }
        }
      }

      foreach ($sum as $key_sum => $value_sum) {
        $response['data']['attributes'][1]['data'][] = $value_sum;
      }

      //dd($response['data']['attributes'][1]['data']);

      return view('comercializadora.nominaciones.detalleNominacion', ['table' => $response, "cliente_id" => $cliente_id]);
    }

    public function detalleEntrega($cliente_id,$id_entrega = null){

      $params = array(
                  'token' => env('TOKEN_SAP'),
                  'clienteid' => $cliente_id,
                  'id_entrega' => (!empty($id_entrega)) ? $id_entrega : null
                );

      $response = $this->generalWebServiceSap(env('URL_USCIIWS52'),"POST",null,$params);

        if (!isset($response['meta'])) {
            return view('comercializadora.nominaciones.detalleEntrega', ['id_entrega' => $id_entrega, 'table1' => []]); 
        }

        $arr_data = $response['data']['attributes'];

        //dd($arr_data); exit();

        /*$validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            // return error
        }

        $html_proceso = GeneralController::processResponse($response, $condition=1);*/

        //return view('comercializadora.nominaciones.detalleEntrega', ['table1' =>$html_proceso[0], 'id_entrega' => $id_entrega, 'table1' => $arr_data]);
        return view('comercializadora.nominaciones.detalleEntrega', ['id_entrega' => $id_entrega, 'table1' => $arr_data]);
   	} 

    public function procesoLogistico($cant_reg){

      $info_response_empty = false;

      if (isset($_GET['page'])) {
        $currentPage = $_GET['page'];
      }else{
        $currentPage = 1;
      } 

      $clienteid = Auth::user()->identificacion;

      $url = env('URL_USCIIWS53')."token=".env('TOKEN_SAP')."&clienteid=".$clienteid."&registrosporpag=".$cant_reg."&paginaactual=".$currentPage;

      \Log::info('service procesoLogistico '.env('URL_USCIIWS53')); 

      $client = new \GuzzleHttp\Client();
      $result_tmp = $client->get($url);                
      $response = $result_tmp->getBody()->getContents();
      //$response = json_decode($response, true);  
      $response = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response), true );
      
      if (!isset($response['meta'])) {

        $info_response_empty = true;

        return view('comercializadora.procesoLogistico.procesoLogistico', ['data' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>" , 'info_response_empty' => $info_response_empty, 'cant_reg' => $cant_reg]); 
      }

      $validation = GeneralController::validateJsonResponse($response);
      if (!$validation) {
          // return error
      }

      $response = $response['data']['attributes'];

      //return $response;
      //fkm paginator
      //$arr_data = $this->paginate($response, 10);

      return view('comercializadora.procesoLogistico.procesoLogistico', ['data' => $response, 'info_response_empty' => $info_response_empty,'cliente_id' => $clienteid, 'cant_reg' => $cant_reg]);

    }

    public function procesoLogisticoBuscador(Request $request){

      $info_response_empty = false;

      if (empty($request->get('cliente_id')) || empty($request->get('numero'))) {

        $info_response_empty = true;

        return view('comercializadora.procesoLogistico.procesoLogistico', ['data' => "<br><p class='text-center'>No se recibió el NIT del cliente o el Numero de entrega o booking para consultar.</p>", 'info_response_empty' => $info_response_empty]); 
      }

      $params = array(
                'token' => env('TOKEN_SAP_COMERCIALIZADORA'),
                'clienteid' => $request->get('cliente_id'),
                'idbooking' => ($request->get('type') == 'booking') ? $request->get('numero') : null,
                'identrega' => ($request->get('type') == 'entrega') ? $request->get('numero') : null
              );

      $response = $this->generalWebServiceSap(env('URL_USCIIWS53'),"POST",null,$params);
      
      if (!isset($response['meta'])) {

        $info_response_empty = true;

        return view('comercializadora.procesoLogistico.procesoLogistico', ['data' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>" , 'info_response_empty' => $info_response_empty]); 
      }

      $validation = GeneralController::validateJsonResponse($response);
      if (!$validation) {
          // return error
      }

      $response = $response['data']['attributes'];

      //fkm paginator
      //$arr_data = $this->paginate($response, 10);

      return view('comercializadora.procesoLogistico.procesoLogistico', ['data' => $response, 'info_response_empty' => $info_response_empty,'cliente_id' => $request->get('cliente_id')]);
    }

    public function procesoLogisticoDetalle($type,$numero,$cliente_id){

      $info_response_empty = false;

      $IdBooking = null;
      $IdEntrega = null;

      if($type == 'entrega'){
        $IdEntrega = $numero;
      }elseif($type == 'booking'){
        $IdBooking = $numero;
      }

      $params = array(
                  'token' => env('TOKEN_SAP_COMERCIALIZADORA'),
                  'clienteid' => $cliente_id,
                  'booking' => $IdBooking,
                  'IdEntrega' => $IdEntrega
                );
      $response = $this->generalWebServiceSap(env('URL_USCIIWS54'),"POST",null,$params);

      if (!isset($response['meta'])) {

        $info_response_empty = true;

        return view('comercializadora.procesoLogistico.procesoLogistico', ['data' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>" , 'info_response_empty' => $info_response_empty]); 
      }

      $validation = GeneralController::validateJsonResponse($response);
      if (!$validation) {
          // return error
      }
      
      return view('comercializadora.procesoLogistico.procesoLogisticoDetalle', ['data' => $response, 'info_response_empty' => $info_response_empty]);
    }

    public function containersList($id_booking){
        $params = array(
                  'token' => env('TOKEN_SAP'),
                  'clienteid' => Auth::user()->identificacion,
                  'id_booking' => $id_booking
                );

        $response = $this->generalWebServiceSap(env('URL_USCIIWS66'),"POST",null,$params);

        if (!isset($response['meta'])) {
            return view('comercializadora.procesoLogistico.detalleEntrega', ['table1' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>"]); 
        }

        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            // return error
        }

        $response['data']['type'] = 'DetalleEntrega';//Se hace esto para que sea procesado como el detalle de una entrega en generalcontroller

        $html_proceso = GeneralController::processResponse($response, $condition=1);
        
        return view('comercializadora.procesoLogistico.detalleEntrega', ['table1' =>$html_proceso[0]]);
    } 
    

    public function contenedoresQuedados($cant_reg){

      $info_response_empty = false;

      $cliente_id = Auth::user()->identificacion;

      if (isset($_GET['page'])) {
        $currentPage = $_GET['page'];
      }else{
        $currentPage = 1;
      }

      /*$params = array(
                  'token' => env('TOKEN_SAP_COMERCIALIZADORA'),
                  'clienteid' => $cliente_id,
                  'idbooking' => null,
                  'status' => 'QUED',
                  'paginaactual' => $PaginaActual,
                  'registrosporpag' => $cant_reg
                );
      $response = $this->generalWebServiceSap(env('URL_USCIIWS55yUSCIIWS56'),"POST",null,$params);*/

      $url = env('URL_USCIIWS55yUSCIIWS56')."token=".env('TOKEN_SAP')."&clienteid=".$cliente_id."&status=QUED&registrosporpag=".$cant_reg."&paginaactual=".$currentPage;

      \Log::info('service contenedoresQuedados '.env('URL_USCIIWS55yUSCIIWS56'));

      $client = new \GuzzleHttp\Client();
      $result_tmp = $client->get($url);                
      $response = $result_tmp->getBody()->getContents();
      //$response = json_decode($response, true);  
      $response = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response), true );

      if (!isset($response['meta'])) {

        $info_response_empty = true;

        return view('comercializadora.estadoContenedores.contenedoresQuedados', ['data' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>" , 'info_response_empty' => $info_response_empty, 'cant_reg' => $cant_reg]); 
      }

      $validation = GeneralController::validateJsonResponse($response);
      if (!$validation) {
          // return error
      }

      $arr_data = $response['data']['attributes'][0]['data'];

      //Paginator fkm
      //$arr_data = $this->paginate($arr_data, 10);

      return view('comercializadora.estadoContenedores.contenedoresQuedados', ['data' => $arr_data, 'info_response_empty' => $info_response_empty, 'cant_reg' => $cant_reg]);
    }

    public function contenedoresQuedadosBuscador(Request $request){

      $PaginaActual = 1;
      $NumeroRegistrosPorPagina = 50;
      $info_response_empty = false;

      if (empty($request->get('cliente_id'))) {

        $info_response_empty = true;

        return view('comercializadora.estadoContenedores.contenedoresQuedados', ['data' => "<br><p class='text-center'>No se recibió el NIT del cliente o el Numero de entrega o booking para consultar.</p>", 'info_response_empty' => $info_response_empty]); 
      }

      $params = array(
                  'token' => env('TOKEN_SAP_COMERCIALIZADORA'),
                  'clienteid' => $request->get('cliente_id'),
                  'idbooking' => (!empty($request->get('numero'))) ? $request->get('numero') : null,
                  'status' => 'QUED'
                );

      $response = $this->generalWebServiceSap(env('URL_USCIIWS55yUSCIIWS56'),"POST",null,$params);

      if (!isset($response['meta'])) {

        $info_response_empty = true;

        return view('comercializadora.estadoContenedores.contenedoresQuedados', ['data' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>" , 'info_response_empty' => $info_response_empty]); 
      }

      $validation = GeneralController::validateJsonResponse($response);
      if (!$validation) {
          // return error
      }

      $arr_data = $response['data']['attributes'][0]['data'];

      //Paginator fkm
      //$arr_data = $this->paginate($arr_data, 10);

      return view('comercializadora.estadoContenedores.contenedoresQuedados', ['data' => $arr_data, 'info_response_empty' => $info_response_empty]);
    }

    public function contenedoresReprogramados($cant_reg){
      
      $info_response_empty = false;

      $cliente_id = Auth::user()->identificacion;

      if (isset($_GET['page'])) {
        $currentPage = $_GET['page'];
      }else{
        $currentPage = 1;
      }

      /*$params = array(
                  'token' => env('TOKEN_SAP_COMERCIALIZADORA'),
                  'clienteid' => $cliente_id,
                  'booking' => null,
                  'status' => 'RPRG',
                  'paginaactual' => $PaginaActual,
                  'registrosporpag' => $NumeroRegistrosPorPagina
                );
      $response = $this->generalWebServiceSap(env('URL_USCIIWS55yUSCIIWS56'),"POST",null,$params);*/

      $url = env('URL_USCIIWS55yUSCIIWS56')."token=".env('TOKEN_SAP_COMERCIALIZADORA')."&clienteid=".$cliente_id."&status=RPRG&registrosporpag=".$cant_reg."&paginaactual=".$currentPage;

      \Log::info('service contenedoresReprogramados '.env('URL_USCIIWS55yUSCIIWS56'));

      $client = new \GuzzleHttp\Client();
      $result_tmp = $client->get($url);                
      $response = $result_tmp->getBody()->getContents(); 
      $response = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response), true );

      if (!isset($response['meta'])) {

        $info_response_empty = true;

        return view('comercializadora.estadoContenedores.contenedoresReprogramados', ['data' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>" , 'info_response_empty' => $info_response_empty, 'cant_reg' => $cant_reg]); 
      }

      $validation = GeneralController::validateJsonResponse($response);
      if (!$validation) {
          // return error
      }

      $arr_data = $response['data']['attributes'][0]['data'];

      //fkm paginate
      //$arr_data = $this->paginate($arr_data, 10);

      return view('comercializadora.estadoContenedores.contenedoresReprogramados', ['data' => $arr_data, 'info_response_empty' => $info_response_empty, 'cant_reg' => $cant_reg]);
    }

    public function contenedoresReprogramadosBuscador(Request $request){
      
      $PaginaActual = 1;
      $NumeroRegistrosPorPagina = 50;
      $info_response_empty = false;

      if (empty($request->get('cliente_id'))) {

        $info_response_empty = true;

        return view('comercializadora.estadoContenedores.contenedoresQuedados', ['data' => "<br><p class='text-center'>No se recibió el NIT del cliente o el Numero de entrega o booking para consultar.</p>", 'info_response_empty' => $info_response_empty]); 
      }

      $params = array(
                  'token' => env('TOKEN_SAP_COMERCIALIZADORA'),
                  'clienteid' => $request->get('cliente_id'),
                  'idbooking' => (!empty($request->get('numero'))) ? $request->get('numero') : null,
                  'status' => 'RPRG',
                  'paginaactual' => $PaginaActual,
                  'registrosporpag' => $NumeroRegistrosPorPagina
                );

      $response = $this->generalWebServiceSap(env('URL_USCIIWS55yUSCIIWS56'),"POST",null,$params);

      if (!isset($response['meta'])) {

        $info_response_empty = true;

        return view('comercializadora.estadoContenedores.contenedoresReprogramados', ['data' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>" , 'info_response_empty' => $info_response_empty]); 
      }

      $validation = GeneralController::validateJsonResponse($response);
      if (!$validation) {
          // return error
      }

      $arr_data = $response['data']['attributes'][0]['data'];

      //fkm paginate
      //$arr_data = $this->paginate($arr_data, 10);

      return view('comercializadora.estadoContenedores.contenedoresReprogramados', ['data' => $arr_data, 'info_response_empty' => $info_response_empty]);
    }

}
