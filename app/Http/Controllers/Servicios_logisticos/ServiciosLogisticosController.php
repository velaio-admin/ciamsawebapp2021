<?php

namespace App\Http\Controllers\Servicios_logisticos;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use Illuminate\Support\Facades\Auth;

class ServiciosLogisticosController extends Controller
{
	public function getTypeDocuments(){
	    
	    $tipo_documentos = $this->generalWebServiceSap(env('URL_WS_DOCUMENTS_TYPE'),"GET",null,null);

	    $new_documents = [];

	    foreach ($tipo_documentos as $key => $value) {
	    	$new_documents[] = (object)$value;
	    }

	    return $new_documents;
	}

	public function uploadDocumentsView(){

		$tipo_documentos = $this->getTypeDocuments();

	    return view('serviciosLogisticos.documentosCarga.cargarDocumentos')->with('errors',null)->with('success',null)->with('tipo_documentos',$tipo_documentos);

	}

    public function uploadDocuments(Request $request){
    	
    	$request->validate([
            'select_documentos'     => 'required|string',
            'id_bl'    => 'required',
            'documento' => 'required|file'
        ]);
		
    	/*Tranport */
        if ($request->has('documento')) {

			$file = $request->file('documento');

			//obtenemos el nombre del archivo
			$name_img = sha1(Carbon::now()).".".$file->getClientOriginalExtension();

			//indicamos que queremos guardar un nuevo archivo en el disco local
			
			if(\Storage::disk('tmp')->put($name_img,  \File::get($file))){

				if ($request->get('select_documentos') == '04') {

					$fecha_tmp = explode("-", $request->get('f_limit'));

					$fecha = $fecha_tmp[2]."/".$fecha_tmp[1]."/".$fecha_tmp[0];

				}

			    $params = array(
	                        'token' => env('TOKEN_SAP'), //varchar
				            'id_bl' => $request->get('id_bl'), //varchar
				            'id_documento' => $request->get('select_documentos'), //varchar
				            'peso' => ($request->get('select_documentos') == '03') ? $request->get('peso') : null, //float 1 digito
				            'documento' => base64_encode(file_get_contents(public_path().'/tmp/'.$name_img)), //archivo base 64
				            'id_agencia_aduanas' => Auth::user()->identificacion, //varchar
				            'fecha_limit_devol_cont' => ($request->get('select_documentos') == '04') ? $fecha : null,
                        );
        
        		$response = $this->generalWebServiceSap(env('URL_WS_UPLOAD_DOCUMENTS'),"POST",null,$params);
                
			}else{
				$response[0] = array('code' => "" , 'message' => "Ocurrio un error al guardar el documento.");
			}

			$tipo_documentos = $this->getTypeDocuments();

			if ($response[0]['code'] != "1") {

				return view('serviciosLogisticos\documentosCarga\cargarDocumentos')->with('errors',$response[0]['message'])->with('success',null)->with('tipo_documentos',$tipo_documentos);

			}else{

				return view('serviciosLogisticos\documentosCarga\cargarDocumentos')->with('success','Carga de documento satisfactoria.')->with('errors',null)->with('tipo_documentos',$tipo_documentos);
			}

			
        }
    }

    public function getDetalleBl($id_bl,$cliente_id){

        // Agrego el / si el bl esta registrado con / entonces reemplazando la palabra clave.
        $id_bl = str_replace("-CIAMSA-","/",$id_bl);

        $params = array(
                        'token' => env('TOKEN_SAP'),
                        'id_bl' => $id_bl,
                        'clienteid' => $cliente_id
                        );
        
        $response = $this->generalWebServiceSap(env('URL_USCIIWS21'),"POST",null,$params);

        //return $response;
        
        if (!isset($response['meta'])) {

            return view('container.bldetalle', ['table1' =>"<br><p class='text-center'>".$response[0]['message'].".</p>",'table2' =>"<br><p class='text-center'>".$response[0]['message']."</p>", 'contenedor' => $id_bl]); 
       
        }

        $arr_documents = $response['data']['documentos']['data'];
        //dd($arr_documents);

        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            // return error
        }
        
        $html_proceso = GeneralController::processResponse($response, $condition=1);
        return view('container.bldetalle', ['table1' =>$html_proceso[0],'table2' =>$html_proceso[1], 'contenedor' => $id_bl, 'arr_documents' => $arr_documents]);   
    }

    public function getDetalleContenedor($id_contenedor,$cliente_id){

        $params = array(
                        'token' => env('TOKEN_SAP'),
                        'id_cont' => $id_contenedor,
                        'clienteid' => $cliente_id
                        );

        $response = $this->generalWebServiceSap(env('URL_USCIIWS22'),"POST",null,$params);
        
        if (!isset($response['meta'])) {

            return view('container.containerdetalle', ['table1' =>"<br><p class='text-center'>".$response[0]['message'].".</p>",'table2' =>"<br><p class='text-center'>".$response[0]['message']."</p>", 'contenedor' => $id_contenedor]); 
       
        }

        //print_r($response);die;
        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            // return error
        }
        
        $html_proceso = GeneralController::processResponse($response, $condition=1);
        return view('container.containerdetalle', ['table1' =>$html_proceso[0],'table2' =>$html_proceso[1], 'contenedor' => $id_contenedor]); 
    }

    public function rastreoList($estado,$cliente_id,$pagina,$cant_pag)
    { 

        $radioButtonsArray = [
            'TRANSITO' => 'transito',
            'PROCESO' => 'proceso',
            'PROC_FINAL' => 'proceso-finalizado'
        ];

        $params = array(
                        'token' => env('TOKEN_SAP'),
                        'estado' => $estado,
                        'clienteid' => $cliente_id,
                        'paginaactual' => (!empty($pagina))? $pagina : 1 ,
                        'numeroregistrosporpagina' => (!empty($cant_pag))? $cant_pag : 50
                        );
        
        //return env('URL_USCIIWS20');

        $response_tmp = $this->generalWebServiceSap(env('URL_USCIIWS20'),"POST",null,$params);   

        //dd($response_tmp);  

        if (!isset($response_tmp['meta'])) {
            return view('serviciosLogisticos.rastreoCarga.rastreoList', ['checked' => $radioButtonsArray[$estado],'table1' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>"]); 
        }

        $response = array(
            'meta' => $response_tmp['meta'],
            'data' => $response_tmp['data'][0],
        );

        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            return 'error';
        }

        //Validation Fecha Eta FK
        $condition = 1;
        $arr_consult = $response['data']['attributes'][0]['data'];

        if ($estado == 'PROCESO' || $estado == 'PROC_FINAL') {
            
            $condition = 2;

            foreach ($arr_consult as $key => $value) {
                $array_res[] = array(
                    0 => $value[0],
                    1 => $value[1],
                    2 => $value[2],
                    3 => $value[5],
                    4 => $value[6],
                    5 => $value[7]
                );
            }

            unset($response['data']['attributes'][0][3]);
            unset($response['data']['attributes'][0][4]);
            $response['data']['attributes'][0]['data'] = $array_res;

            //dd($response['data']['attributes']);

        }else{

            foreach ($arr_consult as $key => $value) {
                $array_res[] = array(
                    0 => $value[0],
                    1 => $value[1],
                    2 => $value[2],
                    3 => $value[3],
                    4 => $value[4],
                    5 => $value[7]
                );
            }
            
            unset($response['data']['attributes'][0][5]);
            unset($response['data']['attributes'][0][6]);
            $response['data']['attributes'][0]['data'] = $array_res;

        }
        //End Validation

        //dd($response);
        $html_proceso = GeneralController::processResponse($response, $condition);
        
        //dd($html_proceso[0]);
        return view('serviciosLogisticos.rastreoCarga.rastreoList', ['checked' => $radioButtonsArray[$estado],'table1' =>$html_proceso[0]]); 
    }

    public function rastreoListBuscador(Request $request)
    { 
        $radioButtonsArray = [
            'TRANSITO' => null,
            'PROCESO' => null,
            'PROC_FINAL' => null
        ];

        $estado = $request->get('estado');

        $params = array(
                        'token' => env('TOKEN_SAP'),
                        'estado' => $request->get('estado'),
                        'clienteid' => $request->get('cliente_id'),
                        'bls' => ($request->get('type') == 'bls' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'dos' => ($request->get('type') == 'dos' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'fechainicial' => (!empty($request->get('fechainicial')))?  GeneralController::cleanDates($request->get('fechainicial')): null,
                        'fechafinal' => (!empty($request->get('fechafinal')))? GeneralController::cleanDates($request->get('fechafinal')): null,
                        'paginaactual' => (!empty($pagina))? $pagina : 1 ,
                        'numeroregistrosporpagina' => (!empty($cant_pag))? $cant_pag : 50
                        );

        $response_tmp = $this->generalWebServiceSap(env('URL_USCIIWS20'),"POST",null,$params);

        if (!isset($response_tmp['meta'])) {
            return view('serviciosLogisticos.rastreoCarga.rastreoList', ['checked' => $radioButtonsArray[$estado],'table1' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>"]); 
        }

        $response = array(
            'meta' => $response_tmp['meta'],
            'data' => $response_tmp['data'][0],
        );
        
        //print_r($response);die;
        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            return 'error';
        }

        //print_r($response);die;

        $html_proceso = GeneralController::processResponse($response, $condition=1);
        return view('serviciosLogisticos.rastreoCarga.rastreoList', ['checked' => $radioButtonsArray[$estado],'table1' =>$html_proceso[0]]); 
    }

    public function despachoMercancia($cant_reg)
    {

        $clienteid = Auth::user()->identificacion;

        if (isset($_GET['page'])) {
          $currentPage = $_GET['page'];
        }else{
          $currentPage = 1;
        }

        /*$params = array(
                        'token' => env('TOKEN_SAP'),
                        'bls' => null,
                        'clienteid' => Auth::user()->identificacion,
                        'fechainicial' => Carbon::now()->subMonths(3)->format('d/m/Y'),
                        'fechafinal' => Carbon::now()->format('d/m/Y'),
                        'paginaactual' => $currentPage,
                        'registroporpag' => $cant_reg
                        );
        $response_tmp = $this->generalWebServiceSap(env('URL_USCIIWS24'),"POST",null,$params);*/

        $url = env('URL_USCIIWS24')."token=".env('TOKEN_SAP')."&clienteid=".$clienteid."&registrosporpag=".$cant_reg."&paginaactual=".$currentPage;

        \Log::info('service despacho '.env('URL_USCIIWS24'));

        $client = new \GuzzleHttp\Client();
        $result_tmp = $client->get($url);                
        $response = $result_tmp->getBody()->getContents();
        $response_tmp = json_decode($response, true); 

        if (!isset($response_tmp['meta'])) {

            return view('serviciosLogisticos.despachoMercancia.despachoMercancia', ['label_fecha' => 'Resultado para los últimos 90 días.','table1' =>[], 'cantidad_pag' => null, 'cant_reg' => $cant_reg, 'pagination' => null]); 

        }
        
        //dd($response_tmp['data']['attributes']);
        //Titulos
        $tab1_tmp = json_decode(config('list_services.headers.service_despacho_mercancia_table_1'),true);
        $tab2_tmp = json_decode(config('list_services.headers.service_despacho_mercancia_table_2'),true);

        $tab1 = [];
        $tab2 = [];

        foreach ($tab1_tmp['columns'] as $key => $value) {
            $tab1[$value['variable']] = $key;
        }

        foreach ($tab2_tmp['columns'] as $key => $value) {
            $tab2[$value['variable']] = $key;
        }
        //Titulos

        foreach ($response_tmp['data']['attributes'] as $key_1 => $value_1) {

            if ($value_1[0] == 'IdBl') {

                $response_tmp['data']['attributes'][$key_1]['data'][0][5] = number_format((float)$value_1['data'][0][5],0,',','.');
                $response_tmp['data']['attributes'][$key_1]['data'][0][6] = number_format((float)$value_1['data'][0][6],0,',','.');

                foreach ($value_1 as $key_titles => $value_titles) {
                    if ($key_titles != 'data') {
                        if (array_key_exists($value_titles, $tab1)) {
                            $response_tmp['data']['attributes'][$key_1][$key_titles] = $tab1_tmp['columns'][$tab1[$value_titles]]['label'];
                        }
                    }
                }

            }

            if ($value_1[0] == 'FechaRetiro') {

                unset($response_tmp['data']['attributes'][$key_1][8]);

                foreach ($value_1['data'] as $key_2 => $value_2) {
                    unset($response_tmp['data']['attributes'][$key_1]['data'][$key_2][8]);

                    $response_tmp['data']['attributes'][$key_1]['data'][$key_2][3] = number_format((float)$value_1['data'][$key_2][3],0,',','.');
                    $response_tmp['data']['attributes'][$key_1]['data'][$key_2][4] = number_format((float)$value_1['data'][$key_2][4],0,',','.');
                    $response_tmp['data']['attributes'][$key_1]['data'][$key_2][5] = number_format((float)$value_1['data'][$key_2][5],0,',','.');
                    $response_tmp['data']['attributes'][$key_1]['data'][$key_2][6] = number_format((float)$value_1['data'][$key_2][6],0,',','.');
                    $response_tmp['data']['attributes'][$key_1]['data'][$key_2][7] = number_format((float)$value_1['data'][$key_2][7],0,',','.');
                }

                foreach ($value_1 as $key_titles => $value_titles) {
                    unset($response_tmp['data']['attributes'][$key_1]['data'][$key_2][8]);
                    if ($key_titles != 'data') {
                        if (array_key_exists($value_titles, $tab2)) {
                            $response_tmp['data']['attributes'][$key_1][$key_titles] = $tab2_tmp['columns'][$tab2[$value_titles]]['label'];
                        }
                    }
                }

            }
        }


        $arr_data = $response_tmp['data']['attributes'];

        //$arr_data = $this->paginate($arr_data, 10);
        //return $arr_data;

        //$html_proceso = GeneralController::processResponse($response_tmp, $condition=1);
        //return view('serviciosLogisticos.despachoMercancia.despachoMercancia', ['label_fecha' => 'Resultado para los últimos 90 días.','table1' =>$html_proceso[0]]);

        $cantidad_pag = $response_tmp['meta'];
        $pagination = true;
        
        return view('serviciosLogisticos.despachoMercancia.despachoMercancia', ['label_fecha' => 'Resultado para los últimos 90 días.','table1' =>$arr_data, 'cantidad_pag' => $cantidad_pag, 'cant_reg' => $cant_reg, 'pagination' => $pagination]); 
    }

    public function despachoMercanciaBuscador(Request $request)
    {
        $params = array(
                        'token' => env('TOKEN_SAP'),
                        'bls' => ($request->get('type') == 'bls' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'dos' => ($request->get('type') == 'dos' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'clienteid' => Auth::user()->identificacion,
                        'fechainicial' => (!empty($request->get('fechainicial')))? GeneralController::cleanDates($request->get('fechainicial')): null,
                        'fechafinal' => (!empty($request->get('fechafinal')))? GeneralController::cleanDates($request->get('fechafinal')): null,
                        'paginaactual' => null ,
                        'numeroregistrosporpagina' => null
                        );

        $response_tmp = $this->generalWebServiceSap(env('URL_USCIIWS24'),"POST",null,$params);

        //dd($response_tmp);

        if (!isset($response_tmp['meta'])) {
            //return view('serviciosLogisticos.despachoMercancia.despachoMercancia', ['label_fecha' => null,'table1' =>"<br><p class='text-center'>No se encontro informacion para su busqueda.</p>"]); 
            return view('serviciosLogisticos.despachoMercancia.despachoMercancia_old', ['label_fecha' => null,'table1' =>"<br><p class='text-center'>No se encontro informacion para su busqueda.</p>"]); 
        }

        //print_r($response);die;
        $validation = GeneralController::validateJsonResponse($response_tmp);
        if (!$validation) {
            // return error
        }

        //print_r($response);die;

        foreach ($response_tmp['data']['attributes'] as $key_1 => $value_1) {

            if ($value_1[0] == 'IdBl') {

                $response_tmp['data']['attributes'][$key_1]['data'][0][5] = number_format((float)$value_1['data'][0][5],0,',','.');
                $response_tmp['data']['attributes'][$key_1]['data'][0][6] = number_format((float)$value_1['data'][0][6],0,',','.');

            }

            if ($value_1[0] == 'FechaRetiro') {

                unset($response_tmp['data']['attributes'][$key_1][8]);

                foreach ($value_1['data'] as $key_2 => $value_2) {
                    unset($response_tmp['data']['attributes'][$key_1]['data'][$key_2][8]);

                    $response_tmp['data']['attributes'][$key_1]['data'][$key_2][3] = number_format((float)$value_1['data'][$key_2][3],0,',','.');
                    $response_tmp['data']['attributes'][$key_1]['data'][$key_2][4] = number_format((float)$value_1['data'][$key_2][4],0,',','.');
                    $response_tmp['data']['attributes'][$key_1]['data'][$key_2][5] = number_format((float)$value_1['data'][$key_2][5],0,',','.');
                    $response_tmp['data']['attributes'][$key_1]['data'][$key_2][6] = number_format((float)$value_1['data'][$key_2][6],0,',','.');
                    $response_tmp['data']['attributes'][$key_1]['data'][$key_2][7] = number_format((float)$value_1['data'][$key_2][7],0,',','.');
                }

            }
        }



        $html_proceso = GeneralController::processResponse($response_tmp, $condition=1);

        //dd($html_proceso);

        return view('serviciosLogisticos.despachoMercancia.despachoMercancia_old', ['label_fecha' => null,'table1' =>$html_proceso[0]]); 
    }

    public function fechaLimiteDevolucionContenedor($cant_reg)
    {

        $clienteid = Auth::user()->identificacion;

        if (isset($_GET['page'])) {
          $currentPage = $_GET['page'];
        }else{
          $currentPage = 1;
        }

        $url = env('URL_USCIIWS26')."token=".env('TOKEN_SAP')."&clienteid=".$clienteid."&registrosporpag=".$cant_reg."&paginaactual=".$currentPage;

        \Log::info('service devolución '.env('URL_USCIIWS26'));

        $client = new \GuzzleHttp\Client();
        $result_tmp = $client->get($url);                
        $response = $result_tmp->getBody()->getContents();
        $response = json_decode($response, true);

        /*
        $params = array(
                        'token' => env('TOKEN_SAP'),
                        //'bls' => null,
                        //'fechainicial' => Carbon::now()->subMonths(3)->format('d/m/Y'),
                        //'fechafinal' => Carbon::now()->format('d/m/Y'),
                        'clienteid' => Auth::user()->identificacion,
                        'paginaactual' => $currentPage,
                        'registroporpag' => $cant_reg
                        );
        $response = $this->generalWebServiceSap(env('URL_USCIIWS26'),"POST",null,$params);
        */
        
        //return $response;

        if (!isset($response['meta'])) {
             return view('serviciosLogisticos.fechaLimiteDevolucionContenedor.fechaLimiteDevolucionContenedor', ['label_fecha' => 'Resultado para los últimos 90 días.','table1' =>[], 'cant_reg' => $cant_reg]); 
        }

        $arr_data = $response['data']['attributes'][0]['data'];

        //$html_proceso = GeneralController::processResponse($response, $condition=1);

        //return view('serviciosLogisticos.fechaLimiteDevolucionContenedor.fechaLimiteDevolucionContenedor', ['label_fecha' => 'Resultado para los últimos 90 días.','table1' =>$html_proceso[0]]); 
        return view('serviciosLogisticos.fechaLimiteDevolucionContenedor.fechaLimiteDevolucionContenedor', ['label_fecha' => 'Resultado para los últimos 90 días.','table1' =>$arr_data, 'cant_reg' => $cant_reg]); 

    }

    public function fechaLimiteDevolucionContenedorBuscador(Request $request)
    {

        $params = array(
                        'token' => env('TOKEN_SAP'),
                        'bls' => ($request->get('type') == 'bls' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'dos' => ($request->get('type') == 'dos' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'fechainicial' => (!empty($request->get('fechainicial')))? GeneralController::cleanDates($request->get('fechainicial')): null,
                        'fechafinal' => (!empty($request->get('fechafinal')))? GeneralController::cleanDates($request->get('fechafinal')): null,
                        'clienteid' => Auth::user()->identificacion,
                        'paginaactual' => (!empty($pagina))? $pagina : 1 ,
                        'numeroregistrosporpagina' => (!empty($cant_pag))? $cant_pag : 50
                        );

        $response = $this->generalWebServiceSap(env('URL_USCIIWS26'),"POST",null,$params);

        if (!isset($response['meta'])) {
             return view('serviciosLogisticos.fechaLimiteDevolucionContenedor.fechaLimiteDevolucionContenedor', ['label_fecha' => null,'table1' =>[]]); 
        }

        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            // return error
        }

        //$html_proceso = GeneralController::processResponse($response, $condition=1);

        //print_r($response);die;
        $arr_data = $response['data']['attributes'][0]['data'];

        //fkm paginate
        //$arr_data = $this->paginate($arr_data, 10);

        return view('serviciosLogisticos.fechaLimiteDevolucionContenedor.fechaLimiteDevolucionContenedor', ['label_fecha' => null,'table1' =>$arr_data]); 
    }

    public function existenciaMercancia($cant_reg)
    {

        $clienteid = Auth::user()->identificacion;

        if (isset($_GET['page'])) {
          $currentPage = $_GET['page'];
        }else{
          $currentPage = 1;
        }

        $url = env('URL_USCIIWS23')."token=".env('TOKEN_SAP')."&clienteid=".$clienteid."&registrosporpag=".$cant_reg."&paginaactual=".$currentPage;

        \Log::info('service mercancia '.env('URL_USCIIWS23'));
        
        $client = new \GuzzleHttp\Client();
        $result_tmp = $client->get($url);                
        $response = $result_tmp->getBody()->getContents();
        $response = json_decode($response, true);        
        
        if (!isset($response['meta'])) {
             return view('serviciosLogisticos.existenciaMercancia.existenciaMercancia', ['label_fecha' => 'Resultado para los últimos 90 días.','table1' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>", 'cant_reg' => $cant_reg]); 
        }  

        //print_r($response);die;
        
        $data = $response['data']['attributes'][0]['data'];
        
        foreach ($data as $key => $value) {
            //$data[$key][12] = str_replace("/", " ", $value[12]);
            //$data[$key][8] = number_format((float)$value[8],0,',','.');
            $data[$key][9] = number_format((float)$value[9],0,',','.');
            $data[$key][10] = number_format((float)$value[10],0,',','.');
            $data[$key][11] = number_format((float)$value[11],0,',','.');
            $data[$key][12] = number_format((float)$value[12],0,',','.');
        }

        $headers = json_decode(config("list_services.headers.existencia_mercancia"));
        $html_proceso = array('headers' => $headers->columns,'data' => $data);

        //$html_proceso = GeneralController::processResponse($response, $condition=2);
        return view('serviciosLogisticos.existenciaMercancia.existenciaMercancia', ['label_fecha' => 'Resultado para los últimos 90 días.','table1' => $html_proceso, 'cant_reg' => $cant_reg]); 

    }
    
    public function existenciaMercanciaBuscador(Request $request)
    {

        $params = array(
                        'token' => env('TOKEN_SAP'),
                        'fechainicial' => (!empty($request->get('fechainicial')))? GeneralController::cleanDates($request->get('fechainicial')): null,
                        'fechafinal' => (!empty($request->get('fechafinal')))? GeneralController::cleanDates($request->get('fechafinal')): null,
                        'bls' => ($request->get('type') == 'bls' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'dos' => ($request->get('type') == 'dos' && !empty($request->get('numero')))? $request->get('numero'): null,
                        'clienteid' => Auth::user()->identificacion,
                        'paginaactual' => null,
                        'registroporpag' => null
                        );

        $response = $this->generalWebServiceSap(env('URL_USCIIWS23'),"POST",null,$params);

        if (!isset($response['meta'])) { 
             return view('serviciosLogisticos.existenciaMercancia.existenciaMercancia', ['label_fecha' => null,'table1' => "<br><p class='text-center'>No se encontro informacion para su busqueda.</p>"]); 
        }

        //print_r($response);die;
        $validation = GeneralController::validateJsonResponse($response);
        if (!$validation) {
            // return error
        }

        //print_r($response);die;
        $data = $response['data']['attributes'][0]['data'];

        foreach ($data as $key => $value) {
            /*$data[$key][12] = str_replace("/", " ", $value[12]);
            $data[$key][8] = number_format((float)$value[8],0,',','.');
            $data[$key][9] = number_format((float)$value[9],0,',','.');
            $data[$key][10] = number_format((float)$value[10],0,',','.');
            $data[$key][11] = number_format((float)$value[11],0,',','.');*/
            $data[$key][9] = number_format((float)$value[9],0,',','.');
            $data[$key][10] = number_format((float)$value[10],0,',','.');
            $data[$key][11] = number_format((float)$value[11],0,',','.');
            $data[$key][12] = number_format((float)$value[12],0,',','.');
        }

        $headers = json_decode(config("list_services.headers.existencia_mercancia"));
        $html_proceso = array('headers' => $headers->columns,'data' => $data);
        //dd($html_proceso);
        //$html_proceso = GeneralController::processResponse($response);
        return view('serviciosLogisticos.existenciaMercancia.existenciaMercancia', ['label_fecha' => null,'table1' => $html_proceso]); 
    }

    public function reservarTurnoVehiculo($cliente_id,$pagina,$cant_pag)
    {   

        return view('serviciosLogisticos.reservarTurno.reservarTurno', ['cliente_id' => $cliente_id]); 

    }

    public function test()
    {

        //Here i get Data From the Api
        $data = file_get_contents(env('APP_URL').'/services/serviceRastreoList');
      
        return view('test',[
             "messages" => $data,
        ]); 
    }

    public function gestionEstados(){

        //dd('fk');
        $est = 0;
        $msjBool = false;

        return view('serviciosLogisticos.gestionarEstados.gestionarEstados', [
            'msjBool' => $msjBool,
            'estados' => $est
        ]);

    }

    public function estadoCarga(){

        //dd('fk');
        $est = 0;
        $msjBool = false;

        return view('serviciosLogisticos.estadoCarga.estadoCarga', [
            'msjBool' => $msjBool,
            'estados' => $est
        ]);

    }

    public function anuncioCarga(){

        //dd('fk');
        $est = 0;
        $msjBool = false;

        return view('serviciosLogisticos.anuncioCarga.anuncioCarga', [
            'msjBool' => $msjBool,
            'estados' => $est
        ]);

    }

}
