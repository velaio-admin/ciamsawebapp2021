<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use Mail;
use App\MailToken;
use Carbon\Carbon;

class RecoverController extends Controller
{
    
    public function recoverPassword(){

        return view('recover');
        
    }
    public function getPasswordEmail(Request $request){

    	$this->validate($request, [
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

    	$email = $request['email'];

    	$info_user = DB::table('users')->where('email', $email)->first();

    	if (!empty($info_user)) {

    		$today = Carbon::now()->toDateTimeString();
    		$token_value = bcrypt($today);
            $token_value_res = str_replace("/", "", $token_value);
    		//$token_value = Hash::make($today);

    		$tokenCreation = MailToken::create([
                'user_id' => $info_user->id,
                'token' => $token_value_res,
                'state' => 0,
            ]);

            if ($tokenCreation) {

            	$data = array(
	                "id_user"=> $info_user->id, 
	                "name"=> $info_user->name, 
	                "identificacion"=> $info_user->identificacion, 
	                "token_email" => $token_value_res
	            );        
	    
	            Mail::send('mails.recover', $data, function($message) use ($email){
	                $message->to($email, 'Ciamsa')
	                ->subject('Restablecer Contraseña');
	                $message->from(env('MAIL_USERNAME'),' CIAMSA DIGITAL');         
	            });

	            if (Mail::failures()) {
	                return back()->with('error', 'Proceso Fallido!! Lo sentimos no se pudo envíar correo electrónico.');
	            }else{
	              return back()->with('success', 'Proceso Exitoso!! Por favor revise su correo electrónico para continuar con el proceso de recuperación de contraseña.');   
	            }

            }else{
            	return back()->with('error', 'Proceso Fallido!! Lo sentimos no se pudo almacenar información de recuperación.');
            }    		
    		
    	}else{
    		return back()->with('error', 'Proceso Fallido!! Lo sentimos el correo no ha sido encontrado en nuestra base de datos.'); 
    	}

        
    }
    public function recoveringAccess($token, $id){

    	$token_found = MailToken::query()
          ->where('user_id', '=', $id)
          ->where('token', '=', $token)
          ->where('state', '=', 0)
          ->select('*')
          ->first();

        if (empty($token_found)) {

            $msg = 'La contraseña ya ha sido generada o ya ha caducado este acceso. Lo invitamos a volver a ejecutar el proceso.';            
            return view('notToken', ['msg' => $msg]);	

        }else{
        	$id_mail_tkn = $token_found->id;
        	return view('recoverAdd', ['id' => $id, 'id_mail_tkn' => $id_mail_tkn]);
        }   
        
    }
    public function savePasswordEmail(Request $request){

    	$this->validate($request, [
            'password' => 'required|string|min:6',
            'password_conf' => 'required|string|min:6',
        ]);

        $id_mail_tkn	= $request['id_mail_tkn'];

        $id_user		= $request['id_user'];
        $password       = $request['password'];
        $password_conf 	= $request['password_conf'];

        if ($password !== $password_conf) {

            return back()->with('error', 'Error!! Las contraseñas no coinciden.'); 

        }else{

        	$user_info = User::find($id_user);
            $user_info->password = Hash::make($password);

            if ($user_info->save()) {

            	$op_action = MailToken::where('user_id', $id_user)
		          			->update(['state' => 1]);

      			if ($op_action) {

      				$msg = 'Proceso Exitoso!! Su nueva contraseña ha sido creada exitosamente por favor intente iniciar sesión.';            
                    return view('notToken', ['msg' => $msg]);   

      			}else{
      				return back()->with('success', 'La contraseña ha sido cambiada exitosamente. Pero no se pudo actualizar el estado del Token.');
      			}

            }else{

               return back()->with('error', 'Hubo un error en el proceso de cambio de contraseña.');

            } 

		}

    }

}
