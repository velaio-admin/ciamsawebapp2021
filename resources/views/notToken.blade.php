@extends('layouts.app_login')

@section('content')
<link href="{{ asset('css/login.css') }}" rel="stylesheet">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="color: white;">
                    {{ __('Recuperar Contraseña') }}
                </div>

                <div class="card-body">

                    <br>

                        <h2>
                            {{$msg}}
                        </h2>

                    <br>

                    <div class="text-center">
                        <a href="/" class="btn btn-primary">Ir a iniciar sesión</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
