@extends('layouts.app_login')

@section('content')
<link href="{{ asset('css/login.css') }}" rel="stylesheet">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="color: white;">
                    {{ __('Recuperar Contraseña') }}
                </div>

                <div class="card-body">

                    <br>

                    @if (Session::has('error'))
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ Session::get('error') }}</p>
                        </div>
                    @endif

                    @if (Session::has('success'))
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ Session::get('success') }}</p>
                        </div>
                    @endif

                    <p>
                        Complete los campos requeridos para crear su nueva contraseña de acceso a <strong>Ciamsa Digital.</strong>
                    </p>

                    <hr>

                    <form method="POST" action="{{ route('savePasswordEmail') }}">
                        @csrf

                        <input type="hidden" name="id_user" value="{{$id}}">
                        <input type="hidden" name="id_mail_tkn" value="{{$id_mail_tkn}}">

                        <div class="form-group row">

                            <label for="password" class="col-md-4 col-form-label text-md-right">
                                {{ __('Contraseña') }}
                            </label>

                            <div class="col-md-6">
                                <input style="border: 1px solid lightgray !important; color: black !important;" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ $password ?? old('password') }}" required autofocus>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group row">

                            <label for="password_conf" class="col-md-4 col-form-label text-md-right">
                                {{ __('Confirmar') }}
                            </label>

                            <div class="col-md-6">
                                <input style="border: 1px solid lightgray !important; color: black !important;" type="password" class="form-control{{ $errors->has('password_conf') ? ' is-invalid' : '' }}" name="password_conf" value="{{ $password_conf ?? old('password_conf') }}" required autofocus>

                                @if ($errors->has('password_conf'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_conf') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Craer Nueva Contraseña') }}
                                </button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
