@extends('layouts.app')

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154479968-1"></script>

@can('Servicios_logisticos')

	@section('titlepage')
	<div class="row justify-content-between header-rastreoList">
	  <div class="col-md-6">
	    <a href="/home">
	    	<h4 style="color: rgb(0, 107, 177);"><u>Volver a Indicadores</u></h4>
	    </a>
	  </div>
	  <div onclick="tableToExcel('tblData', 'name', 'cumplimiento_historico_detalle.xls');" style="cursor: pointer;">
		    <span style="color: rgb(0, 107, 177); margin-right: 5px;">
		      Exportar excel
		    </span> 
		    <img src="http://digital.ciamsa.com/img/excel-icon.png" alt="icon-excel" height="30px" title="Exportar datos a Excel">
	   </div>
	</div>
	@endsection

	@section('content')

	<style type="text/css">
	  	.dat-form{
			padding: 7px;
	    	margin: 3px 7px;
	    	width: 100%;
		}
		input[type="text"] {
		    margin: 3px 7px;
		    padding-left: 10px;
		    width: 100%;
		}
		.search-data{
			background-color: lightgray;
		    border-radius: 5px;
		    padding: 7px;
		}
		.art-graf{
			margin-bottom: 20px;
		}
		.vdp-datepicker__calendar {
	    	position: relative !important;
	    }
	    .cont-data-loading {
		    text-align: center;
		}
		@media (max-width: 680px) {
			.wrapper{
			  padding: initial !important;
			}
		}
		.table-cump th, .table-cump td{
			font-size: 11px !important;
		}
	</style>
		
	<!--Moment Librery-->
	<script src="{{asset('storage/assets/js/moment.js')}}"></script>

	<div class="container">

	    <section id="indicators" class="row" style="margin: 5px 0;">

	    	<input type="hidden" id="id_user" value="{{ Auth::user()->identificacion }}">

			<article class="col-md-12 art-graf table-cump">

	    		<h3 class="text-center">
	    			<a href="#" style="color: rgb(0, 107, 177);">
	    				Detalles generales
	    			</a>
	    		</h3>

	    		<br>

	    		@if(sizeof($table1) > 0)

		    		<div class="table-responsive" style="height: 400px;">
		    			<table id="tblData" class="table table-striped">
							 <thead>
							    <tr>
							      	@foreach ($table1 as $t)
										<th><?php echo $t[0]; ?></th>
										<th><?php echo $t[1]; ?></th>
										<th><?php echo $t[2]; ?></th>
										<th><?php echo $t[3]; ?></th>
										<th><?php echo $t[4]; ?></th>
										<th><?php echo $t[5]; ?></th>
										<th><?php echo $t[6]; ?></th>
										<th><?php echo $t[7]; ?></th>
										<th><?php echo $t[8]; ?></th>
										<th><?php echo $t[9]; ?></th>
										<th><?php echo $t[10]; ?></th>
										<th><?php echo $t[11]; ?></th>
										<th><?php echo $t[12]; ?></th>
										<th><?php echo $t[13]; ?></th>
										<th><?php echo $t[14]; ?></th>
										<th><?php echo $t[15]; ?></th>
										<th><?php echo $t[16]; ?></th>
										<th><?php echo $t[17]; ?></th>
										<th><?php echo $t[18]; ?></th>
										<th><?php echo $t[19]; ?></th>
										<th><?php echo $t[20]; ?></th>
										<th><?php echo $t[21]; ?></th>
										<th><?php echo $t[22]; ?></th>
										<th><?php echo $t[23]; ?></th>
									@endforeach
							    </tr>
							  </thead>
							  <tbody>
							    @foreach ($t['data'] as $val)
					              <tr>
					                <td><?php echo $val[0]; ?></td>
					                <td><?php echo $val[1]; ?></td>
					                <td><?php echo $val[2]; ?></td>
					                <td><?php echo $val[3]; ?></td>
					                <td><?php echo $val[4]; ?></td>
					                <td><?php echo $val[5]; ?></td>
					                <td><?php echo $val[6]; ?></td>
					                <td><?php echo $val[7]; ?></td>
					                <td><?php echo $val[8]; ?></td>
					                <td><?php echo $val[9]; ?></td>
					                <td><?php echo $val[10]; ?></td>
					                <td><?php echo $val[11]; ?></td>
					                <td><?php echo $val[12]; ?></td>
					                <td><?php echo $val[13]; ?></td>
					                <td><?php echo $val[14]; ?></td>
					                <td><?php echo $val[15]; ?></td>
					                <td><?php echo $val[16]; ?></td>
					                <td><?php echo $val[17]; ?></td>
					                <td><?php echo $val[18]; ?></td>
					                <td><?php echo $val[19]; ?></td>
					                <td><?php echo $val[20]; ?></td>
					                <td><?php echo $val[21]; ?></td>
					                <td><?php echo $val[22]; ?></td>
					                <td><?php echo $val[23]; ?></td>
					              </tr>
					            @endforeach
							  </tbody>
						</table>
		    		</div>

		    	@else

			        <br>

			        <p class='text-center'>No se encontró información para su búsqueda.</p>

			    @endif
					    		
    		</article>
		

	    </section>

	</div>

		<script type="text/javascript">

		    function tableToExcel(table, name, filename){
		      let uri = 'data:application/vnd.ms-excel;base64,', 
		            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><title></title><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>', 
		            base64 = function(s) { return window.btoa(decodeURIComponent(encodeURIComponent(s))) },         
		            format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; })}
		            
		            if (!table.nodeType) table = document.getElementById(table)
		            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}

		            var link = document.createElement('a');
		            link.download = filename;
		            link.href = uri + base64(format(template, ctx));
		            link.click();
		    }

 		</script>

		<script src="{{asset('storage/assets/js/vue.js')}}"></script>
		<script src="{{asset('storage/assets/js/vuejs-datepicker.min.js')}}"></script>

		<!-- Axios To Run Api Services -->
	  	<script src="{{asset('storage/assets/js/axios.js')}}"></script>
		<script src="https://unpkg.com/vue-select@latest"></script>
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/highcharts-vue@1.3.5/dist/highcharts-vue.min.js"></script>

		<script src="https://code.highcharts.com/modules/series-label.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
		<script src="https://code.highcharts.com/modules/export-data.js"></script>

	@endsection

@endcan
@can('Agricola')
	
	@section('titlepage')
	<div class="row justify-content-between header-rastreoList">
	  <div class="col-md-12">
	    <h4>Bienvenido(a): {{ Auth::user()->name }}</h4>
	  </div>
	</div>
	@endsection

@endcan
@can('Comercializadora')
	
	@section('titlepage')
	<div class="row justify-content-between header-rastreoList">
	  <div class="col-md-12">
	    <h4>Bienvenido(a): {{ Auth::user()->name }}</h4>
	  </div>
	</div>
	@endsection

@endcan
