@extends('layouts.app')

@section('titlepage')
<div class="row justify-content-between header-rastreoList">
	<div class="col-md-4">
		<h4>Facturas</h4>
	</div>
	<div class="col-md-6 open-observ">
		<!--<a href="/agricola/solicitar-pedido"><span>
			Realizar un pedido
		</span></a>-->
	</div>
	<div class="col-md-2 open-search">
		<span>
			Busqueda avanzada
		</span>
	</div>
</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Agricola'))

<div id="agricola-facturas" class="page-content-wrapper">
	<div class="container-fluid">
		<div>
			<div class="alert alert-primary alert-dismissible fade show" role="alert" style="background-color: #64b338; font-size: 16px;">
			  <strong style="border-bottom: 1px solid;">
			  	*Su factura podrá visualizarla y descargarla, 24 horas después del despacho.
			  </strong>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>	
		</div>
		<div class="table-responsive m-t-35">
			<div class="header">
				<h3>Historico de facturas</h3>
			</div>
			{!!  $table1 !!}
		</div>
	</div>
</div>
@include('agricola.facturas.buscador')

@else

<div class="row">
	<div class="col-md-12">
		<div class="description">
			<h4>No tiene permisos para acceder a esta sección</h4>
		</div>
	</div>
</div>

@endif

@endsection
