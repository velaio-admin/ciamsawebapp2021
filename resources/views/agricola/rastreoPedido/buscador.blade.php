<div id="buscador-cargas">
	<div class="cerrar-formulario">
		Cerrar <span>X</span>
	</div>
	<form action="/agricola/rastreo-de-pedido/buscador" method="POST">
	@csrf
		<div class="form-filds">
			<div class="form-field-bl-or-do">
				<h5>Buscar por </h5>
				<div class="form-field form-type-radio form-field-numero">
					<input type="text" name="numero" id="numero" class="form-control" value="" placeholder="Ingrese el número" />
				</div>
			</div>
			<div class="form-field-fecha">
				<h5>Fecha desde </h5>
				<div class="form-field form-type-text input-group date form-field-fecha-inicio">
					<input type="date" name="fechainicial" id="fechainicial" value="" placeholder="dd//mm/aaaa" min="1000-01-01" max="{{date('Y-m-d')}}" class="form-control" />
					<!-- <div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div> -->
				</div>
				<h5>Hasta </h5>
				<div class="form-field form-type-text input-group date form-field-fecha-fin">
					<input type="date" name="fechafinal" id="fechafinal" value="" placeholder="dd//mm/aaaa" min="1000-01-01" max="{{date('Y-m-d')}}" class="form-control" />
					<!-- <div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div> -->
				</div>
			</div>
		</div>
		<div class="form-action">
			<input type="submit" value="Aplicar" name="submit" id="submitform">
		</div>
	</form>
</div>