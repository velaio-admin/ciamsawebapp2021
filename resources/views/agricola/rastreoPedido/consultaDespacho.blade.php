@extends('layouts.app')

@section('titlepage')
<div class="row justify-content-between header-rastreoList header-estadosContenedores">
	<div class="col-md-10 list">
		<h4>Rastreo de carga</h4>
	</div>
	<div class="col-md-2 open-search">
		<span>
			Busqueda avanzada
		</span>
	</div>
</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Agricola'))

<div id="agricola-Pedido" class="page-content-wrapper">
	<div class="container-fluid">
		{!!$table1!!}
	</div>
</div>
@include('agricola.rastreoPedido.buscador')

@else

<div class="row">
	<div class="col-md-12">
		<div class="description">
			<h4>No tiene permisos para acceder a esta sección</h4>
		</div>
	</div>
</div>

@endif

@endsection