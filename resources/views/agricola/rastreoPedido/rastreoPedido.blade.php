@extends('layouts.app')

@section('titlepage')
<div class="row justify-content-between header-rastreoList">
	<div class="col-md-4">
		<h4><a class="back" href="javascript:history.back()"></a> Rastreo del pedido</h4>
	</div>
	<div class="col-md-2 open-observ">
		<!--<span>
			Realizar un pedido
		</span> -->
	</div>
</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Agricola'))

<div id="agricola-rastreoPedido" class="page-content-wrapper">
	<div class="container-fluid">
		<div class="table-responsive m-t-35">
			<table class="htmlTable table"> 
				<thead>   
					<tr>
						<th>Factura</th>
						<th>Pedido</th>
						<th>Fecha solicitud</th>
						<th>Cantidad</th>
						<th>Entregado</th>
						<th>Saldo</th>
						<th>Estado</th>
						<th>Producto</th>
						<th>Unidad de medida</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						@foreach($data['data']['attributes'][0]['data'][0] as $value)
						<td>{{$value}}</td>
						@endforeach
					</tr>					
				</tbody>
			</table>
		</div>
		<div class="table-responsive m-t-35">
			<div class="header">
				<h3>Actividad reciente del despacho</h3>
			</div>
			<table class="htmlTable table"> 
				<thead>   
					<tr>
						<th>Actividad</th>
						<th>Fecha de ultimo movimiento</th>
						<th>Estado del despacho</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data['data']['relationships']['tracking']['data'] as $value)
						<tr>
							@foreach($value as $value_)
								<td>{{$value_}}</td>
							@endforeach	
						</tr>
					@endforeach				
				</tbody>
			</table>
		</div>
	</div>
</div>
@else

<div class="row">
	<div class="col-md-12">
		<div class="description">
			<h4>No tiene permisos para acceder a esta sección</h4>
		</div>
	</div>
</div>

@endif

@endsection
