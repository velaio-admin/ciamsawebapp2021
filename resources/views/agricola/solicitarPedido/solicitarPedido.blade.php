@extends('layouts.app')

@section('titlepage')
<div class="row justify-content-between header-rastreoList">
	<div class="col-md-4">
		<h4>Solicitar pedido</h4>
	</div>
</div>
@endsection
<!-- Page Content -->
@section('content')
<div id="message"></div>

@if(Auth::user()->can('Agricola'))

	@if($msjBool)

		<div class="alert alert-danger" role="alert">
			{{$msj}}
		</div>

	@else

		<div id="agricola-solicitarPedido" class="page-content-wrapper">
			<div class="container-fluid">
				<div id="form-pedidos">
					<form id="solicitarPedido">
						<div class="header">
							<div class="v-row">					
								<div class="col">
									<label for="placeDelivery">Lugar de entrega</label>
									<select name="placeDelivery" id="placeDelivery" disabled="">
										<option value="0" selected="">Buga</option>
										@foreach($ciudades['data']['attributes'][0]['data'][0] as $key => $value)
											<option value="{{$key}}">{{$value}}</option>
										@endforeach
									</select>

								</div>
								<div class="col">
									<label for="wayPay">Forma de pago</label>
									<select name="wayPay" id="wayPay">
										@foreach($formaDePago['data']['attributes'][0]['data'][0] as $key => $value)
											<option value="{{$key}}">{{$value}}</option>
										@endforeach
									</select>
								</div>
								<div class="col">
									<label for="typeProduct">Tipo</label>
									<select name="typeProduct" id="typeProduct">
										<option value="ZPME" selected="selected">Estandar</option>
										<option value="ZPVN">Sencillo</option>
									</select>
								</div>
							</div>
						</div>
						<div class="body">
							<div class="content">
								<div id="input1" class="v-row v-row-first">
									<div class="col left">
										<div class="form-field">
											<label for="product">Detalle ( tipo de producto )</label>
											<select name="product[]" class="product">
												<option value="none" data-type="none" data-und="" data-price-ton="0" data-price-saco="0" selected>Seleccione un producto</option>
											</select>
										</div>
										<div class="form-field">
											<label for="quantity">Cantidad</label>
											<input type="number" min="1" name="quantity" class="quantity" value="1">
										</div>
										<div class="form-field">
											<label for="presentation">Presentación</label>
											<div class="presentation">
												<span class="value"></span>
											</div>
										</div>
										<div class="remove disable">Eliminar</div>
									</div>
									<div class="col right">
										<div class="form-field">
											<label>Precio de lista</label>
											<div class="price-und"><span>$</span><span class="value">0</span></div>
										</div>
										<div class="form-field">
											<label>Precio total</label>
											<div class="price-total"><span>$</span><span class="value">0</span></div>
										</div>
									</div>
								</div>
							</div>
							<div class="row-footer">
								<div class="add">
									<span></span>Agregar otro producto
								</div>
								<div class="totals">
									<div class="subtotal">
										<label>Sub total</label>
										<span>$<span class="value">0</span></span>
									</div>
									<div class="total">
										<label>Total</label>
										<span>$<span class="value">0</span></span>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="id_info_u" id="id_info_u" value="{{\Auth::user()->identificacion}}">
						<div class="calltoaction">
							<input type="submit" value="Solicitar pedido" name="form-submit" id="form-submit">
						</div>
					</form>
				</div>
			</div>
		</div>
		@include('agricola.facturas.buscador')

	@endif

@else

	<div class="row">
		<div class="col-md-12">
			<div class="description">
				<h4>No tiene permisos para acceder a esta sección</h4>
			</div>
		</div>
	</div>

@endif

@endsection

<script type="text/javascript">
     
    window.onbeforeunload = preguntarAntesDeSalir;
     
    function preguntarAntesDeSalir()
    {
    	var user = $('#id_info_u').val();

    	var subtotal = $('.body .row-footer .subtotal .value').text();
	    var total = $('.body .row-footer .total .value').html();
	    var ciudad = "BUGA"; //Quemado
	    var Condicionpago = $("#wayPay option:selected").val();
	    var user = $("#id_info_u").val();

	    var data =   {"subtotal":subtotal,"descuentos":0,"total":total,"Ciudad":ciudad,"condicionpago":Condicionpago,"id_info_u":user,"productos":[]};
	    alert('Lo hizo');
	    console.log('Si');
	    $(".body .v-row-first select.product").each(function(i){
	      var product = $(this).val();
	      if(product !== "none"){
	      var cod_material = $('option:selected', this).attr('value');
	      var tipo = $('option:selected', this).attr('data-type');
	      var parent = $(this).parent().parent().parent();
	      var cantidad = parent.find('.quantity').val(); 
	      var cod_material_real = cod_material.split("-");
	      console.log(cod_material_real);
	        data.productos.push({
	          "cod_material":cod_material_real[0],
	          "cantidad":cantidad,
	          "tipo":tipo        
	        });
	      } 
	    });

	    $.ajax({
	      type: "POST",
	      url: '/api/v1/products/notificar/pedido',
	      data: {'id_info_u': user, "Productos" : data},
	      cache: false,
	      success: function(msg) {
	        //alert(msg);
	        //console.log('Informacion enviada correctamente');
	      },
	      error: function(msg) {       
	        //alert(msg);
	        //console.log('Informacion enviada correctamente');
	      }
	    });

    }
</script>
