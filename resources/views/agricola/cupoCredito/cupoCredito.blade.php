@extends('layouts.app')

@section('titlepage')
<div class="row justify-content-between header-rastreoList">
	<div class="col-md-4">
		<h4>Cupo de crédito</h4>
	</div>
	<div class="col-md-6 open-observ">
		<!--<a href="/agricola/solicitar-pedido"><span>
			Realizar un pedido
		</span></a>-->
	</div>
	<div class="col-md-2 open-search">
		<span>
			Busqueda avanzada
		</span>
	</div>
</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Cupo_credito'))

<div id="agricola-cupoCredito" class="page-content-wrapper">
	<div class="container-fluid">
		<div>
			<div class="alert alert-primary alert-dismissible fade show" role="alert" style="background-color: #64b338; font-size: 16px;">
			  <strong style="border-bottom: 1px solid;">
			  	*Recuerde que su consignación se reflejará en 48 horas hábiles.
			  </strong>
			  <br>
			  <strong style="border-bottom: 1px solid;">
			  	*Agradecemos reportar sus pagos al correo CARTERA-CIAMSA@CIAMSA.COM con el detalle de las facturas canceladas.
			  </strong>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>	
		</div>
		<div class="columns">
			<div class="column column-left">
				<div class="average">
					<h3>Crédito disponible</h3>
					<div class="bg-white section_border circliful_section">
						<div class="tab-content text-center">
							<div  class="tab-pane active" id="fa-icons">
								<div class="porcentaje" style="display: none;">{{$cupo_credito['per_credito_disponible']}}</div>
								<div id="myStat"></div>
							</div>
						</div>
					</div>
					<div class="text-footer">
						<!--<span class="color-green">Crédito utilizado</span> <span>Crédito disponible</span>-->
					</div>
				</div>
				<div class="approved credit">
					<span>Cupo crédito aprobado</span>
					<p>${{$cupo_credito['credito_aprobado']}}</p>
					<span class="porcen">{{$cupo_credito['per_credito_aprobado']}} % del valor</span>
				</div>
				<div class="used credit">
					<span>Cupo de crédito utilizado</span>
					<p>$ {{$cupo_credito['credito_utilizado']}}</p>
					<span class="porcen">{{$cupo_credito['per_credito_utilizado']}} % del valor</span>
				</div>
				<div class="available credit">
					<span>Cupo de crédito disponible</span>
					<p>$ {{$cupo_credito['credito_disponible']}}</p>
					<span class="porcen">{{$cupo_credito['per_credito_disponible']}} % del valor</span>
				</div>
			</div>
			<div class="column column-right">
				{!!  $table1 !!}
			</div>
		</div>
	</div>
</div>
@include('agricola.cupoCredito.buscador')

@else

<div class="row">
	<div class="col-md-12">
		<div class="description">
			<h4>No tiene permisos para acceder a esta sección</h4>
		</div>
	</div>
</div>

@endif

@endsection
