<!DOCTYPE html>
<html>
<style>
body {font-family: Arial, Helvetica, sans-serif;}
 
form {
  border: 3px solid #f1f1f1;
  font-family: Arial;
}
 
.container {
  padding: 20px;
  background-color: #f1f1f1;
}
 
input[type=text], input[type=submit] {
  width: 100%;
  padding: 12px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}
 
input[type=checkbox] {
  margin-top: 16px;
}
 
input[type=submit] {
  background-color: #4CAF50;
  color: white;
  border: none;
}
 
input[type=submit]:hover {
  opacity: 0.8;
}
</style>
<body>
 
<h1>Restablecer Contraseña Ciamsa Digital</h1>

  <hr>

  <div class="container">

    <h2>
      Estimado usuario {{ $name }} identificado con el número NIT {{ $identificacion }} ha solicitado restablecer su contraseña de acceso desde nuestro sistema de Ciamsa Digital.
    </h2>  

    <p>
      Lo invitamos a crear su nueva contraseña haciendo click sobre este 
      <a href="{{env('APP_URL')}}/recovering_access/{{ $token_email }}/{{ $id_user }}/execute" target="_blank">Enlace.</a>
    </p>

    <hr>

    <p>
      <i>
        En caso de inconvenientes por favor comunicarse con la línea de atención (572) 664 7911 Dir. Av. 3A Norte # 56-N32
      </i>
    </p>

  </div>
 
</body>

</html>