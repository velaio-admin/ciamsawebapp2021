@extends('layouts.app')

<style type="text/css">
  
  /* Absolute Center Spinner */
  .loading {
    position: fixed;
    z-index: 999;
    height: 2em;
    width: 2em;
    overflow: show;
    margin: auto;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
  }

  /* Transparent Overlay */
  .loading:before {
    content: '';
    display: block;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
      background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

    background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
  }

  /* :not(:required) hides these rules from IE9 and below */
  .loading:not(:required) {
    /* hide "loading..." text */
    font: 0/0 a;
    color: transparent;
    text-shadow: none;
    background-color: transparent;
    border: 0;
  }

  .loading:not(:required):after {
    content: '';
    display: block;
    font-size: 10px;
    width: 1em;
    height: 1em;
    margin-top: -0.5em;
    -webkit-animation: spinner 1500ms infinite linear;
    -moz-animation: spinner 1500ms infinite linear;
    -ms-animation: spinner 1500ms infinite linear;
    -o-animation: spinner 1500ms infinite linear;
    animation: spinner 1500ms infinite linear;
    border-radius: 0.5em;
    -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
  }

  /* Animation */

  @-webkit-keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @-moz-keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @-o-keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  /* End Loading */

</style>

@section('titlepage')
<div class="row justify-content-between header-rastreoList">
	<div class="col-md-4">
		<h4><a class="back" href="javascript:history.back()"></a> Detalle de la entrega <?=$id_entrega; ?></h4>
	</div>
	<div class="col-md-2">
	</div>
</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Comercializadora'))

<!--<div id="comercializadora-detalleEntrega" class="page-content-wrapper">
	<div class="container-fluid">
		
	</div>
</div>-->

<div id="comercializadora-detalleEntrega" class="page-content-wrapper">

	<div :class="var_load"></div>

	<div class="container-fluid">

		<div class="table-responsive m-t-35">

	      @if(sizeof($table1) > 0)
	        <table id="tblData" class="htmlTable table">

	          @foreach ($table1 as $t)

	            <tr>
	              <th style="background: #C9C7C6;">
	              	Contenedor
	              </th>
	              <th style="background: #C9C7C6;">
	              	Sellos
	              </th>
	              <th style="background: #C9C7C6;">
	              	Tipo de contenedor
	              </th>
	              <th style="background: #C9C7C6;">
	              	Marca
	              </th>
	              <th style="background: #C9C7C6;">
	              	ID Booking
	              </th>
	              <th style="background: #C9C7C6;">
	              	Material (es)
	              </th>
	              <th style="background: #C9C7C6;">
	              	Cantidad (es) Embalada
	              </th>
	              <th style="background: #C9C7C6;">
	              	Empaque(s)
	              </th>
	              <th style="background: #C9C7C6;">
	              	Estado del contenedor
	              </th>
	              <th style="background: #C9C7C6;">
	              	Opciones
	              </th>
	            </tr>
	            @foreach ($t['data'] as $key => $val)
	              <tr>
	                <td>
	                	<?php echo $val[0]; ?>
	                	<input type="hidden" value="<?=$val[0]; ?>" id="cont_value<?=$key; ?>">
                    <input type="hidden" value="<?=$val[9]; ?>" id="fecha_value<?=$key; ?>">
	                </td>
	                <td><?php echo $val[1]; ?></td>
	                <td><?php echo $val[2]; ?></td>
	                <td><?php echo $val[3]; ?></td>
	                <td><?php echo $val[4]; ?></td>
	                <td><?php echo $val[5]; ?></td>
	                <td><?php echo $val[6]; ?></td>
	                <td><?php echo $val[7]; ?></td>
	                <td><?php echo $val[8]; ?></td>
	                <td>
	                	<a href="#" v-on:click="dataConsult($event, <?=$key ?>)" style="color: rgb(0, 107, 177) !important;">
	                		Registro fotográfico
	                	</a>
	                </td>
	              </tr>
	            @endforeach

	          @endforeach

	     	</table>
	      @else

	       <br>

	       <p class='text-center'>No se encontro informacion para su busqueda.</p>

      	  @endif
       
     	</div>
		
	</div>
</div>

@else

<div class="row">
	<div class="col-md-12">
		<div class="description">
			<h4>No tiene permisos para acceder a esta sección</h4>
		</div>
	</div>
</div>

@endif

<!-- Vuejs -->
<script src="{{asset('storage/assets/js/vue.js')}}"></script>
<!-- Axios To Run Api Services -->
<script src="{{asset('storage/assets/js/axios.js')}}"></script>

<script type="text/javascript">
	var app = new Vue({
    el: '#comercializadora-detalleEntrega',
    data: {
      var_load: '', 
      base_url: '' 
    },
    mounted () {
    },
    methods: {
      dataConsult: function (event, data_key) {

        event.preventDefault();

        var cont_value = document.getElementById("cont_value"+data_key).value;  
        var fecha_value = document.getElementById("fecha_value"+data_key).value;

        try {
          this.var_load = 'loading'; 

          axios.post('/api/v1/comercializadora/servicio/obtenerpdf', {
              data_detail: cont_value,
              data_date: fecha_value
          })
          .then(response => {
            console.log('resp data request', response);
            if (response.data.success === true) {

              this.base_url = response.data.data;
              var url_res_pdf = this.base_url .replace(/\"/g, "");
              console.log('this url ',url_res_pdf);
              window.open(url_res_pdf, '_blank');
              this.var_load = ''; 

            }else{
              this.var_load = ''; 
              alert(response.data.message);
            }            
          })
        }catch(error) {
          this.var_load = '';  
          alert(error+ '- Fallo en consultar servicio.');
        }

      }

    }
    
  })
</script>

@endsection
