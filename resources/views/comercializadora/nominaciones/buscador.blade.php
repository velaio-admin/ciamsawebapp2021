<div id="buscador-cargas">
	<div class="cerrar-formulario">
		Cerrar <span>X</span>
	</div>
	<form action="/comercializadora/nominaciones/buscador" method="POST">
	@csrf
		<div class="form-filds">
			<div class="form-field-bl-or-do">
				<h5>Buscar por </h5>
				<div class="form-field form-type-radio form-field-bl">
					<input type="radio" name="type" id="radicado" value="radicado" checked="checked"/>
					<label for="radicado"><span></span>Radicado</label>
				</div>
				<div class="form-field form-type-radio form-field-do">
					<input type="radio" name="type" id="booking" value="booking" />
					<label for="booking"><span></span>Booking</label>
				</div>
				<div class="form-field form-type-radio form-field-do">
					<input type="radio" name="type" id="entrega" value="entrega" />
					<label for="entrega"><span></span>Entrega</label>
				</div>
				<div class="form-field form-type-radio form-field-numero">
					<input type="text" name="numero" id="numero" class="form-control" value="" placeholder="Ingrese el número" />
				</div>
			</div>
			<div class="form-field-fecha">
				<h5>Fecha desde </h5>
				<div class="form-field form-type-text input-group date form-field-fecha-inicio">
					<input type="date" name="fechainicial" id="fechainicial" value="" placeholder="dd//mm/aaaa" min="1000-01-01" max="{{date('Y-m-d')}}" class="form-control" />
					<!-- <div class="input-group-append" data-target="#fechainicial" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div> -->
				</div>
				<h5>Hasta </h5>
				<div class="form-field form-type-text input-group date form-field-fecha-fin">
					<input type="date" name="fechafinal" id="fechafinal" value="" placeholder="dd//mm/aaaa" min="1000-01-01" max="{{date('Y-m-d')}}" class="form-control" />
					<!-- <div class="input-group-append" data-target="#fechafinal" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div> -->
				</div>
			</div>
		</div>
		<input type="hidden" name="cliente_id" id="cliente_id" value="{{\Auth::user()->identificacion}}">
		<div class="form-action">
			<input type="submit" value="Aplicar" name="submit" id="submitform">
		</div>
	</form>
</div>