@extends('layouts.app')

@section('titlepage')
  <div class="row justify-content-between header-rastreoList">
    <div class="col-md-4">
      <h4><a class="back" href="javascript:history.back()"></a> Detalle de la nominación</h4>
    </div>
    <div class="col-md-2">
    </div>
  </div>
@endsection
<!-- Page Content -->
@section('content')
  <style type="text/css">
    /* Absolute Center Spinner */
    .loading {
      position: fixed;
      z-index: 999;
      height: 2em;
      width: 2em;
      overflow: show;
      margin: auto;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
    }

    /* Transparent Overlay */
    .loading:before {
      content: '';
      display: block;
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background: radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));

      background: -webkit-radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));
    }

    /* :not(:required) hides these rules from IE9 and below */
    .loading:not(:required) {
      /* hide "loading..." text */
      font: 0/0 a;
      color: transparent;
      text-shadow: none;
      background-color: transparent;
      border: 0;
    }

    .loading:not(:required):after {
      content: '';
      display: block;
      font-size: 10px;
      width: 1em;
      height: 1em;
      margin-top: -0.5em;
      -webkit-animation: spinner 1500ms infinite linear;
      -moz-animation: spinner 1500ms infinite linear;
      -ms-animation: spinner 1500ms infinite linear;
      -o-animation: spinner 1500ms infinite linear;
      animation: spinner 1500ms infinite linear;
      border-radius: 0.5em;
      -webkit-box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
      box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
    }

    /* Animation */

    @-webkit-keyframes spinner {
      0% {
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }

      100% {
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    @-moz-keyframes spinner {
      0% {
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }

      100% {
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    @-o-keyframes spinner {
      0% {
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }

      100% {
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    @keyframes spinner {
      0% {
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }

      100% {
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    /* End Loading */

  </style>

  @if (Auth::user()->can('Comercializadora'))
    <div id="comercializadora-detalleNominacion" class="page-content-wrapper">
      <div :class="var_load"></div>
      <input type="hidden" id="cliente_id" value="<?= $cliente_id ?>">
      <div class="modal fade comercializadora-modal" id="myModal" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
							<img id="logo-short" src="{{ asset('storage/assets/img/v-c-logo-short.png') }}" alt="logo">
							<div class="title">
								<h4 id="modalTitle" class="modal-title">Entrega @{{ entrega }}</h4>
								<h5 id="modalSubtitle" class="modal-subtitle">Subtitulo</h5>
							</div>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
							{{-- EXPORT TO EXCEL --}}
							<div class="modal-actions py-2 px-3">
								<div class="excel-btn" onclick="tableToExcel('nominacion-table', 'Ciamsa Export', 'detalle_nominacion.xls');" style="cursor: pointer;">
									<span style="color: rgb(0, 107, 177); margin-right: 5px;">
										Exportar excel
									</span> 
									<img src="http://digital.ciamsa.com/img/excel-icon.png" alt="icon-excel" height="23px" title="Exportar datos a Excel">
								 </div>
							</div>
							 {{-- END EXPORT TO EXCEL --}}
							<div v-if="res_consult" class="table-responsive">
								<table id="nominacion-table" class="htmlTable table">
									<thead>
										<tr>
											<th style="border: 1px solid">Contenedor</th>
											<th style="border: 1px solid">Lote Ciamsa</th>
											<th style="border: 1px solid">Cantidad</th>
											<th style="border: 1px solid">Sticker</th>
											<th style="border: 1px solid">Remision</th>
											<th style="border: 1px solid">Placa</th>
											<th style="border: 1px solid">Lote Ingenio</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="(arr, key) in data_body">
											<td v-if="arr[0] == arr[0]" style="border: 1px solid">@{{ arr[0] }}</td>
											<td v-else>@{{ arr[0] }}</td>
											<td style="border: 1px solid">@{{ arr[1] }}</td>
											<td style="border: 1px solid">@{{ arr[2] }}</td>
											<td style="border: 1px solid">@{{ arr[3] }}</td>
											<td style="border: 1px solid">@{{ arr[4] }}</td>
											<td style="border: 1px solid">@{{ arr[5] }}</td>
											<td style="border: 1px solid">@{{ arr[6] }}</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div v-else>
								<p class="text-center">No hay información disponible para esta entrega.</p>
							</div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="table-responsive Bg-gray marging-bottom-20">
          <table class="htmlTable table">
            <thead>
              <tr>
                <th>Radicado</th>
                <th>Booking</th>
                <th>Motonave</th>
                <th>Viaje</th>
                <th>Puerto de origen</th>
                <th>Puerto destino</th>
                <th>Fecha Cierre</th>
                <th>ETA</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                @foreach ($table['data']['attributes'][0]['data'] as $row1)
                  @for ($i = 0; $i <= 7; $i++)
                    <td>{{ $row1[$i] }}</td>
                  @endfor
                @endforeach
              </tr>
            </tbody>
            <thead>
              <tr>
                <th>Linea Maritima</th>
                <th>Toneladas nominadas</th>
                <th>Capacidad por contenedor</th>
                <th>Toneladas cumplidas</th>
                <th>Cantidad contenedores embalados</th>
                <th>Tipo de venta</th>
                <th>Contrato</th>
                <th>Estado</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                @foreach ($table['data']['attributes'][0]['data'] as $row2)
                  @for ($i = 8; $i <= 15; $i++)
                    <td>{{ $row2[$i] }}</td>
                  @endfor
                @endforeach
              </tr>
            </tbody>
          </table>
        </div>
        <div class="table-responsive m-t-20">
          <table class="htmlTable table">
            <thead>
              <tr>
                <th>Entrega</th>
                <th>Producto</th>
                <th>Cantidad nominada</th>
                <th>Cantidad embalada</th>
                <th>Cantidad en inventario</th>
                <th>Saldo por embalar</th>
                <th>Estado</th>
                <th>Unidad de medida</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($table['data']['attributes'][1]['data'] as $value)
                <?php //dd($table['data']['attributes'][1]['data']);?>
                <tr>
                  @for ($i = 0; $i <= 7; $i++)
                    @if ($i == 0)
										<td>
											<a
												href="/comercializadora/nominaciones/detalle-de-entrega/{{ Auth::user()->identificacion }}/{{ $value[$i] }}">
												{{ $value[$i] }}
											</a>
                    </td>
                  	@else
                    	<td>{{ $value[$i] }}</td>
										@endif
                  @endfor
                  <td>
                    <?php if ($value[0] !== ''): ?>
										<script>
											productName = "{{ $value[1] }}";
											console.log("productName: " + productName);
										</script>
                    <a 
											v-on:click="dataConsult($event, {{ $value[0] }});getProductName(productName)" 
											data-toggle="modal" 
											data-target="#myModal"
                      style="cursor: pointer;"
										>
                      Lotes Embalados
                    </a>
                    <?php endif ?>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  @else
    <div class="row">
      <div class="col-md-12">
        <div class="description">
          <h4>No tiene permisos para acceder a esta sección</h4>
        </div>
      </div>
    </div>
  @endif
  <!-- Vuejs -->
  <script src="{{ asset('storage/assets/js/vue.js') }}"></script>
  <!-- Axios To Run Api Services -->
  <script src="{{ asset('storage/assets/js/axios.js') }}"></script>
  <script type="text/javascript">
    var app = new Vue({
      el: '#comercializadora-detalleNominacion',
      data: {
        var_load: '',
        data_body: [],
        cliente_id: '',
        res_consult: true,
        entrega: '',
				nombre: '',
      },
      mounted() {
        this.cliente_id = document.getElementById("cliente_id").value;
      },
      methods: {
        dataConsult: function(event, id_entrega) {
          event.preventDefault();
					// console.log('Params: ', arguments);
          this.entrega = id_entrega;
          try {
            this.var_load = 'loading';
            axios.post('/api/v1/comercializadora/servicio/infolotes', {
                clienteid: this.cliente_id,
                id_entrega: this.entrega
              })
              .then(response => {
                console.log('resp data request', response);
                if (response.data.success === true) {
                  this.data_body = response.data.data;
                  this.var_load = '';
                  if (this.data_body.length > 0) {
                    this.res_consult = true;
                  } else {
                    this.res_consult = false;
                  }
                } else {
                  this.var_load = '';
                  alert(response.data.message);
                }
              })
          } catch (error) {
            this.var_load = '';
            alert(error + '- Fallo en consultar servicio.');
          }
        },
				getProductName: (name = "no hay nada") => {
					console.log("getProductName function");
					console.log("Producto: " + name);
					this.nombre = name;
					document.querySelector('#modalSubtitle').innerHTML = this.nombre;
				}
      }
    })
  </script>
	<script type="text/javascript">
		function tableToExcel(table, name, filename){
			let uri = 'data:application/vnd.ms-excel;base64,'; 
			let numEntrega = document.getElementById('modalTitle').innerText;
			let producto = document.getElementById('modalSubtitle').innerText;
			template = `
			<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">
			<title></title>
			<head>
				<!--[if gte mso 9]>
					<xml>
						<x:ExcelWorkbook><x:ExcelWorksheets>
						<x:ExcelWorksheet><x:Name>{worksheet}</x:Name>
						<x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions>
						</x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook>
					</xml>
				<![endif]-->
				<meta http-equiv="content-type" content="text/plain; charset=UTF-8"/>
			</head>
			<body>
				<table>
					<thead>
						<tr>
							<th colspan="7" style="background: #006aaf;color: #fff;font-size:20px">${numEntrega}</th>
						</tr>
						<tr>
							<th colspan="7" style="background: #006aaf;color: #fff;font-size:16px">${producto}</th>
						</tr>
					</thead>
					{table}
				</table>
			</body>
			</html>`, 
			base64 = function(s) { return window.btoa(decodeURIComponent(encodeURIComponent(s))) },         
			format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; })}
			
			if (!table.nodeType) table = document.getElementById(table)
			var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
			var link = document.createElement('a');
			link.download = filename;
			link.href = uri + base64(format(template, ctx));
			link.click();
		}
	</script>
@endsection
