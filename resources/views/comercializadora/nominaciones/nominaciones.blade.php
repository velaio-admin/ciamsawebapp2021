@extends('layouts.app')

<style type="text/css">
	/* Paginator Style FKM */
  .paginated .pagination li.next a {
    background-color: #006bb1;
    color: #fff;
    border: 1px solid;
  }

  .table-responsive .paginated .pagination {
    border: 1px solid #e6e6e6;
  }

  .paginated{
    align-items: center;
    display: flex;
    justify-content: space-between;
    margin-top: 30px;
  }

  .paginated .pagination li a, .table-responsive .paginated .pagination li span{
    display: block;
    font-size: 14px;
    line-height: 1;
    padding: 14px 14px;
  }

  .disabled {
    pointer-events: none;
    cursor: default;
  }
  /* End Paginator Style FKM */
</style>

@section('titlepage')
<div class="row justify-content-between header-rastreoList">
	<div class="col-md-4">
		<h4>Nominaciones</h4>
	</div>
	<div class="col-md-2 open-search">
		<span>
			Busqueda avanzada
		</span>
	</div>
</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Comercializadora'))

	<?php  

	    if (isset($_GET['page'])) {
	      $curPage = $_GET['page'];
	    }else{
	      $curPage = 1;
	    }

	    $nextPage = $curPage + 1;
	    $previusPage = $curPage > 1 ? $curPage - 1 : 1;

	    $disablePrev = $curPage == 1 ? 'disabled' : '';

	    if (is_array($table1)) {
	      $results = sizeof($table1);
	    }else{
	      $results = 0;
	    }

	    $disableNext = $results == 0 ? 'disabled' : '';

  	?>

<div id="comercializadora-nominaciones" class="page-content-wrapper">
	<div class="container-fluid">

	@if (count($table1) > 0)
		
	<div class="table-responsive m-t-35">
		<h3>Resumen de nominaciones 
			<span> </span>
		</h3>
		<table id="tblData" class="htmlTable table fkm">
			<!--Table fk --> 
			<thead>
				<tr>
					<th style="background:#C9C7C6"> Radicado </th>
					<th style="background:#C9C7C6"> Booking </th>
					<th style="background:#C9C7C6"> Cliente </th>
					<th style="background:#C9C7C6"> Motonave </th>
					<th style="background:#C9C7C6"> Fecha Cierre </th>
					<th style="background:#C9C7C6"> ETA </th>
					<th style="background:#C9C7C6"> Contrato </th>
					<th style="background:#C9C7C6"> Tipo de venta </th>
				</tr>
			</thead>
			<tbody>
				@foreach($table1 as $tab)
					<tr class="fk">                
						<td>{{$tab[0]}}</td>
						<td>
							<a href="/comercializadora/nominaciones/detalle/nominacion/{{$tab[1]}}/{{Auth::user()->identificacion}}" style="color: #006bb1 !important; text-decoration: underline !important;">{{$tab[1]}}</a>							
						</td>
						<td>{{$tab[2]}}</td>
						<td>{{$tab[3]}}</td>
						<td>{{$tab[4]}}</td>                
						<td>{{$tab[5]}}</td>                
						<td>{{$tab[6]}}</td>                
						<td>{{$tab[7]}}</td>                
					</tr>
            	@endforeach	
			</tbody>
		</table>

	</div>

	@else

		<br>

		<p class='text-center'>No se encontro informacion para su busqueda.</p>

	@endif	

	<?php if (isset($cant_reg) ) { ?>
            
      <div class="paginated">
         <div class="result">
            <!-- Mostrando 1  a 10  de 10 resultados -->
         </div>
         <ul class="pagination fk123">
            <li class="next <?=$disablePrev; ?>">
                <a id="link_url_prev" href="/comercializadora/nominaciones/<?=$cant_reg; ?>?page=<?=$previusPage; ?>" rel="prev" class="<?=$disablePrev; ?>">
                  Anterior
                </a>
            </li>
            <li class="next <?=$disableNext; ?>">
                <a id="link_url_next" href="/comercializadora/nominaciones/<?=$cant_reg; ?>?page=<?=$nextPage; ?>" class="<?=$disableNext; ?>">
                  Siguiente
                </a>
            </li>
         </ul>
      </div>

    <?php } ?>

	</div>
</div>
@include('comercializadora.nominaciones.buscador')

@else

<div class="row">
	<div class="col-md-12">
		<div class="description">
			<h4>No tiene permisos para acceder a esta sección</h4>
		</div>
	</div>
</div>

@endif

@endsection
