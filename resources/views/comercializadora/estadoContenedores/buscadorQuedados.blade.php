<div id="buscador-cargas">
	<div class="cerrar-formulario">
		Cerrar <span>X</span>
	</div>
	<form action="/comercializadora/estados-de-contenedores/contenedores-quedados/buscador" method="POST">
		@csrf
		<div class="form-filds">
			<div class="form-field-bl-or-do">
				<h5>Número Booking</h5>
				<div class="form-field form-type-radio form-field-numero">
					<input type="text" name="numero" id="numero" class="form-control" value="" placeholder="Ingrese el número" />
				</div>
			</div>
		</div>
		<input type="hidden" name="cliente_id" id="cliente_id" value="{{\Auth::user()->identificacion}}">
		<div class="form-action">
			<input type="submit" value="Aplicar" name="submit" id="submitform">
		</div>
	</form>
</div>