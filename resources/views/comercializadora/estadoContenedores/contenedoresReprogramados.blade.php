@extends('layouts.app')

<style type="text/css">
	/* Paginator Style FKM */
  .paginated .pagination li.next a {
    background-color: #006bb1;
    color: #fff;
    border: 1px solid;
  }

  .table-responsive .paginated .pagination {
    border: 1px solid #e6e6e6;
  }

  .paginated{
    align-items: center;
    display: flex;
    justify-content: space-between;
    margin-top: 30px;
  }

  .paginated .pagination li a, .table-responsive .paginated .pagination li span{
    display: block;
    font-size: 14px;
    line-height: 1;
    padding: 14px 14px;
  }

  .disabled {
    pointer-events: none;
    cursor: default;
  }
  /* End Paginator Style FKM */
</style>

@section('titlepage')
<div class="row justify-content-between header-rastreoList header-estadosContenedores">
	<div class="col-md-10 list">
		<h4>Estados de los contenedores</h4>
		<ul>
			<li ><a href="/comercializadora/estados-de-contenedores/contenedores-quedados/50">Contenedores quedados</a></li>
			<li class="active"><a href="/comercializadora/estados-de-contenedores/contenedores-reprogramados/50">Contenedores reprogramados</a></li>
		</ul>
	</div>
	<div class="col-md-2 open-search">
		<span>
			Busqueda avanzada
		</span>
	</div>
</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Comercializadora'))

	<?php  

	    if (isset($_GET['page'])) {
	      $curPage = $_GET['page'];
	    }else{
	      $curPage = 1;
	    }

	    $nextPage = $curPage + 1;
	    $previusPage = $curPage > 1 ? $curPage - 1 : 1;

	    $disablePrev = $curPage == 1 ? 'disabled' : '';

	    if (is_array($data)) {
	      $results = sizeof($data);
	    }else{
	      $results = 0;
	    }

	    $disableNext = $results == 0 ? 'disabled' : '';

  	?>

	@if($info_response_empty)

		<div class="container-fluid">
			{!!$data!!}
		</div>
		
	@else

		<div id="comercializadora-procesoLogistico" class="page-content-wrapper">
			<div class="container-fluid">
				@foreach($data as $value)
					<div class="table-responsive marging-bottom-20 closed">
						<span class="dropdown"></span>
						<div class="header first">
							<h3>Booking: {{$value['booking']}}</h3>
						</div>
						<table class="htmlTable table"> 
							<thead>   
								<tr>
									<th>Entrega</th>
									<th>ID del contenedor</th>
									<th>Cliente</th>
									<th>Ingenio</th>
									<th>Motonave</th>
									<th>Viaje</th>
									<th>Linea maritima</th>
									<th>Destino</th>
									<th>Causa</th>
									<th>Descripción del material</th>
									<th>Cantidad de sacos</th>
									<th>ETA</th>
								</tr>
							</thead>
							<tbody>
								@foreach($value['data'] as $value2)
								<tr>
									@foreach($value2 as $value3)
										<td>{{$value3}}</td>
									@endforeach	
								</tr>
								@endforeach			
							</tbody>
						</table>
					</div>
				@endforeach

				<?php if (isset($cant_reg) ) { ?>
            
			      <div class="paginated">
			         <div class="result">
			            <!-- Mostrando 1  a 10  de 10 resultados -->
			         </div>
			         <ul class="pagination fk123">
			            <li class="next <?=$disablePrev; ?>">
			                <a id="link_url_prev" href="/comercializadora/estados-de-contenedores/contenedores-reprogramados/<?=$cant_reg; ?>?page=<?=$previusPage; ?>" rel="prev" class="<?=$disablePrev; ?>">
			                  Anterior
			                </a>
			            </li>
			            <li class="next <?=$disableNext; ?>">
			                <a id="link_url_next" href="/comercializadora/estados-de-contenedores/contenedores-reprogramados/<?=$cant_reg; ?>?page=<?=$nextPage; ?>" class="<?=$disableNext; ?>">
			                  Siguiente
			                </a>
			            </li>
			         </ul>
			      </div>

			    <?php } ?>
				
			</div>
		</div>
		@include('comercializadora.estadoContenedores.buscadorReprogramados')

	@endif	
@else

<div class="row">
	<div class="col-md-12">
		<div class="description">
			<h4>No tiene permisos para acceder a esta sección</h4>
		</div>
	</div>
</div>

@endif

@endsection
