@extends('layouts.app')

@section('titlepage')
<div class="row justify-content-between header-rastreoList">
	<div class="col-md-4">
		<h4><a class="back" href="javascript:history.back()"></a> Listado de contenedores</h4>
	</div>
	<div class="col-md-2">
	</div>
</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Comercializadora'))

<div id="comercializadora-detalleEntrega" class="page-content-wrapper">
	<div class="container-fluid">
		{!!$table1!!}
	</div>
</div>

@else

<div class="row">
	<div class="col-md-12">
		<div class="description">
			<h4>No tiene permisos para acceder a esta sección</h4>
		</div>
	</div>
</div>

@endif

@endsection
