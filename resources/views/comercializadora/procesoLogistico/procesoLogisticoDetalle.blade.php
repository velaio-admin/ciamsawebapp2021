@extends('layouts.app')

@section('titlepage')
<div class="row justify-content-between header-rastreoList header-procesoLogistico">
	<div class="col-md-4">
		<h4><a class="back" href="javascript:history.back()"></a> Proceso logistico (<label>Booking:</label><span>{{$data['data']['attributes'][0]['booking']}}</span> <label>Entrega:</label><span>{{$data['data']['attributes'][0]['entrega']}}</span>)</h4>
	</div>
</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Comercializadora'))

	@if($info_response_empty)

		<div class="container-fluid">
			{!!$data!!}
		</div>
		
	@else
		<div id="comercializadora-procesoLogistico" class="page-content-wrapper">
			<div class="container-fluid">
				
				<div class="table-responsive marging-bottom-20">
					<div class="header first">
						<h3>Contenedores</h3>
					</div>
					<table class="htmlTable table"> 
						<thead>   
							<tr>
								<th>Cantidad nominados</th>
								<th>Cantidad de embalados</th>
								<th>Pedientes por embalar</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								@foreach($data['data']['attributes'][0]['contenedores'] as $key => $value)
									@if($key != 'cantidadDeContenedoresIngresados')
										<td>{{$value}}</td>
									@endif
								@endforeach
							</tr>					
						</tbody>
					</table>
				</div>
				<div class="table-responsive marging-bottom-20">
					<div class="header first">
						<h3>Carga</h3>
					</div>
					<table class="htmlTable table"> 
						<thead>   
							<tr>
								<th>Cantidad de sacos nominados</th>
								<th>Cantidad de sacos embalados</th>
								<th>Cantidad de sacos pendiente por embalar</th>
								<th>Total existencia</th>
								<th>Faltante de carga</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								@foreach($data['data']['attributes'][0]['carga'] as $value)
									<td>{{$value}}</td>
								@endforeach
							</tr>					
						</tbody>
					</table>
				</div>
			</div>
		</div>
	@endif
@else

<div class="row">
	<div class="col-md-12">
		<div class="description">
			<h4>No tiene permisos para acceder a esta sección</h4>
		</div>
	</div>
</div>

@endif

@endsection
