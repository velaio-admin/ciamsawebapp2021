<div id="buscador-cargas">
	<div class="cerrar-formulario">
		Cerrar <span>X</span>
	</div>
	<form action="/comercializadora/proceso-logistico/buscador" method="POST">
	@csrf
		<div class="form-filds">
			<div class="form-field-bl-or-do">
				<h5>Buscar por </h5>
				<div class="form-field form-type-radio form-field-bl">
					<input type="radio" name="type" id="booking" value="booking" checked="checked"/>
					<label for="booking"><span></span>Booking</label>
				</div>
				<div class="form-field form-type-radio form-field-do">
					<input type="radio" name="type" id="entrega" value="entrega" />
					<label for="entrega"><span></span>Entrega</label>
				</div>
				<div class="form-field form-type-radio form-field-numero">
					<input type="text" name="numero" id="numero" class="form-control" value="" placeholder="Ingrese el número" />
				</div>
			</div>
		</div>
		<input type="hidden" name="cliente_id" id="cliente_id" value="{{\Auth::user()->identificacion}}">
		<div class="form-action">
			<input type="submit" value="Aplicar" name="submit" id="submitform">
		</div>
	</form>
</div>