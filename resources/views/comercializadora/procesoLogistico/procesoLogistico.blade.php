@extends('layouts.app')

<style type="text/css">
   /* Paginator Style FKM */
  .paginated .pagination li.next a {
    background-color: #006bb1;
    color: #fff;
    border: 1px solid;
  }

  .table-responsive .paginated .pagination {
    border: 1px solid #e6e6e6;
  }

  .paginated{
    align-items: center;
    display: flex;
    justify-content: space-between;
    margin-top: 30px;
  }

  .paginated .pagination li a, .table-responsive .paginated .pagination li span{
    display: block;
    font-size: 14px;
    line-height: 1;
    padding: 14px 14px;
  }

  .disabled {
    pointer-events: none;
    cursor: default;
  }
  /* End Paginator Style FKM */
</style>

@section('titlepage')
<div class="row justify-content-between header-rastreoList">
	<div class="col-md-4">
		<h4>Proceso logistico</h4>
	</div>
	<div class="col-md-2 open-search">
		<span>
			Busqueda avanzada
		</span>
	</div>
</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Comercializadora'))

	<?php  

	    if (isset($_GET['page'])) {
	      $curPage = $_GET['page'];
	    }else{
	      $curPage = 1;
	    }

	    $nextPage = $curPage + 1;
	    $previusPage = $curPage > 1 ? $curPage - 1 : 1;

	    $disablePrev = $curPage == 1 ? 'disabled' : '';

	    if (is_array($data)) {
	      $results = sizeof($data);
	    }else{
	      $results = 0;
	    }

	    $disableNext = $results == 0 ? 'disabled' : '';

  	?>
	
	@if($info_response_empty)

		<div class="container-fluid">
			{!!$data!!}
		</div>
		
	@else

		<div id="comercializadora-procesoLogistico" class="page-content-wrapper">
			<div class="container-fluid">
				@foreach($data as $value)
				
					<div class="table-responsive marging-bottom-20 closed">
						<span class="dropdown"></span>
						<div class="header first">
							<!-- <h3>Booking: <a href="/comercializadora/proceso-logistico/booking/{{$value['booking'][0]}}/{{$cliente_id}}">{{$value['booking'][0]}}</a></h3> -->
							<div class="row">
								<h3>Booking: {{$value['booking'][0]}}</h3><h3>&nbsp;&nbsp;<a href="/comercializadora/listado/contenedores/{{$value['booking'][0]}}">Listado de contenedores</a></h3>
							</div>
						</div>
						<table class="htmlTable table"> 
							<thead>   
								<tr>
									<th>Motonave</th>
									<th>Viaje</th>
									<th>Linea maritima</th>
									<th>ETA</th>
									<th>Fecha Cierre</th>
									<th>Total de contenedores nominados</th>
									<th>Tipo de contenedores</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									@foreach($value['booking']['data'] as $booking)							
										<td>{{ $booking }}</td>
									@endforeach
								</tr>					
							</tbody>
						</table>
						@foreach($value['entrega'] as $entrega)
						<div class="header border-top">
							<h3>Entrega: <a href="/comercializadora/proceso-logistico/entrega/{{$entrega[0]}}/{{$cliente_id}}">{{$entrega[0]}}</a></h3>
						</div>
						<table class="htmlTable table"> 
							<thead>   
								<tr>
									<th>Nombre del cliente</th>
									<th>Nombre del ingenio</th>
									<th>Descripción del material</th>
									<th>Cantidad nominada sacos</th>
									<th>Unidad de medida</th>
									<th>Cantidad de unidades nominadas por contenedor</th>
									<th>Supersión externa</th>
									<th>Instrucciones especiales</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									@foreach($entrega['data'] as $sub_entrega)							
										<td>{{ $sub_entrega }}</td>
									@endforeach
								</tr>					
							</tbody>
						</table>
						@endforeach
					</div>
				@endforeach

			</div>
		</div>
		@include('comercializadora.procesoLogistico.buscador')

	@endif

	<?php if (isset($cant_reg) ) { ?>
            
      <div class="paginated">
         <div class="result">
            <!-- Mostrando 1  a 10  de 10 resultados -->
         </div>
         <ul class="pagination fk123">
            <li class="next <?=$disablePrev; ?>">
                <a id="link_url_prev" href="/comercializadora/proceso-logistico/<?=$cant_reg; ?>?page=<?=$previusPage; ?>" rel="prev" class="<?=$disablePrev; ?>">
                  Anterior
                </a>
            </li>
            <li class="next <?=$disableNext; ?>">
                <a id="link_url_next" href="/comercializadora/proceso-logistico/<?=$cant_reg; ?>?page=<?=$nextPage; ?>" class="<?=$disableNext; ?>">
                  Siguiente
                </a>
            </li>
         </ul>
      </div>

    <?php } ?>

@else

<div class="row">
	<div class="col-md-12">
		<div class="description">
			<h4>No tiene permisos para acceder a esta sección</h4>
		</div>
	</div>
</div>

@endif

@endsection
