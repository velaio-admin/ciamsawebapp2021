@extends('layouts.app')

@section('titlepage')
<div class="row justify-content-between header-rastreoContainers">

  <div class="col-md-10">
   <h4><a class="back col-md-12" href="javascript:history.back()"></a>Detalle del Contenedor {{$contenedor}}</h4>
 </div>
 <div class="col-md-2 open-observ">
  <span>
    Agregar observación
  </span>
</div>
</div>
@endsection
<!-- Page Content -->
@section('content')
<div id="observaciones">
  <form action="#">
    <form action="#">
      <div class="form-fields">
        <div class="form-field form-type-textarea input-group form-field-observ">
          <textarea name="observacion" id="observacion" class="form-control" placeholder="Escribe tus observaciones aquí"></textarea>
        </div>
      </div>
      <div class="form-action">
        <input type="button" value="Cancelar" name="submit" class="cancelform">
        <input type="button" value="Agregar" name="submit" class="submitform" onclick="send('{{$contenedor}}');">
      </div>
    </form>
  </div>
  <div id="rastreoContainers" class="page-content-wrapper">
   <div class="container-fluid">
     {!! $table1 !!}
     {!! $table2 !!}
   </div>
 </div>
 @endsection

 <script type="text/javascript">
   
  function send(id_){

    var obs = document.getElementById("observacion").value;

    if(obs){

      $.ajax({
          url: '{{env('APP_URL')}}/api/v1/bl/cont/observaciones',
          dataType: 'text',
          type: 'post',
          contentType: 'application/x-www-form-urlencoded',
          data: {
            observacion: obs,
            id: id_,
            tipo: 'cont',
            id_cliente: '{{ Auth::user()->identificacion }}'
          },
          success: function( data, textStatus, jQxhr ){
              var obj = JSON.parse(data);
              alert(obj.message);
          },
          error: function( jqXhr, textStatus, errorThrown ){
              alert(errorThrown);
              console.log(errorThrown);
          }
      });

      e.preventDefault;

    } else {
      alert('El campo de observciones no puede estar vacío');
    }

  }

 </script>