@extends('layouts.app')

<style type="text/css">
  
  /* Absolute Center Spinner */
  .loading {
    position: fixed;
    z-index: 999;
    height: 2em;
    width: 2em;
    overflow: show;
    margin: auto;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
  }

  /* Transparent Overlay */
  .loading:before {
    content: '';
    display: block;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
      background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

    background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
  }

  /* :not(:required) hides these rules from IE9 and below */
  .loading:not(:required) {
    /* hide "loading..." text */
    font: 0/0 a;
    color: transparent;
    text-shadow: none;
    background-color: transparent;
    border: 0;
  }

  .loading:not(:required):after {
    content: '';
    display: block;
    font-size: 10px;
    width: 1em;
    height: 1em;
    margin-top: -0.5em;
    -webkit-animation: spinner 1500ms infinite linear;
    -moz-animation: spinner 1500ms infinite linear;
    -ms-animation: spinner 1500ms infinite linear;
    -o-animation: spinner 1500ms infinite linear;
    animation: spinner 1500ms infinite linear;
    border-radius: 0.5em;
    -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
  }

  /* Animation */

  @-webkit-keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @-moz-keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @-o-keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @keyframes spinner {
    0% {
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  /* End Loading */

</style>

@section('titlepage')
<div class="row justify-content-between header-rastreoContainers">
  <div class="col-md-4">
   <h4><a class="back col-md-12" href="javascript:history.back()"></a>Detalle del BL {{$contenedor}}</h4>
 </div>
 <div class="col-md-2 open-observ">
  <span>
    Agregar observación
  </span>
</div>
</div>
@endsection
<!-- Page Content -->
@section('content')
<div id="observaciones">

  <form action="#">
    <form action="#">
      <div class="form-fields">
        <div class="form-field form-type-textarea input-group form-field-observ">
          <textarea name="observacion" id="observacion" class="form-control" placeholder="Escribe tus observaciones aquí"></textarea>
        </div>
      </div>
      <div class="form-action">
        <input type="button" value="Cancelar" name="submit" class="cancelform">
        <input type="button" value="Agregar" name="submit" class="submitform" onclick="send('{{$contenedor}}');">
      </div>
    </form>
  </div>

  <div id="rastreoContainers" class="page-content-wrapper">

    <div :class="var_load"></div>

   <div class="container-fluid">
     {!! $table1 !!}
     {!! $table2 !!}

     <!-- Informción de documentos cargados por BL -->
     <div class="table-responsive m-t-35">

      <h3>Documentos</h3>

      @if(sizeof($arr_documents) > 0)
        <table id="tblData" class="htmlTable table">

            <tr>
              <th style="background:#C9C7C6">IdDocumento</th>
              <th style="background:#C9C7C6">NombreDocumento</th>
              <th style="background:#C9C7C6">FechaCargueDocumento</th>
              <th style="background:#C9C7C6">HoraCargueDocumento</th>
              <th style="background:#C9C7C6">Estado</th>
              <th style="background:#C9C7C6">Factura</th>
              <th style="background:#C9C7C6">Opciones</th>
            </tr>

            @foreach ($arr_documents as $t)
              <tr>
                <td><?php echo $t[0]; ?></td>
                <td><?php echo $t[1]; ?></td>
                <td><?php echo $t[2]; ?></td>
                <td><?php echo $t[3]; ?></td>
                <td><?php echo $t[4]; ?></td>
                <td><?php echo $t[5]; ?></td>
                <td>
                  <a style="color: #006bb1 !important; text-decoration: underline !important;" href="#" v-on:click="dataConsult($event, {{$t[0]}})">
                    Descarga Documentos Soportes
                  </a>
                </td>
              </tr>
            @endforeach

        </table>
      @else

        <br>

        <p class='text-center'>No se encontró información de documentos.</p>

      @endif
       
     </div>

   </div>
 </div>

  <!-- Vuejs -->
  <script src="{{asset('storage/assets/js/vue.js')}}"></script>
   <!-- Axios To Run Api Services -->
  <script src="{{asset('storage/assets/js/axios.js')}}"></script>

  <script src="{{asset('storage/assets/js/bl_detalle_vue.js?v=1.5')}}"></script>

 @endsection

 <script type="text/javascript">
   
  function send(id_){

    var obs = document.getElementById("observacion").value;

    if(obs){

      $.ajax({
          url: '{{env('APP_URL')}}/api/v1/bl/cont/observaciones',
          dataType: 'text',
          type: 'post',
          contentType: 'application/x-www-form-urlencoded',
          data: {
            observacion: obs,
            id: id_,
            tipo: 'bl',
            id_cliente: '{{ Auth::user()->identificacion }}'
          },
          success: function( data, textStatus, jQxhr ){
              var obj = JSON.parse(data);
              alert(obj.message);
          },
          error: function( jqXhr, textStatus, errorThrown ){
              alert(errorThrown);
              console.log(errorThrown);
          }
      });

      e.preventDefault;

    } else {
      alert('El campo de observciones no puede estar vacío');
    }

  }

 </script>