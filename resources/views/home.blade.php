@extends('layouts.app')

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154479968-1"></script>

@can('Servicios_logisticos')

	@section('titlepage')
	<div class="row justify-content-between header-rastreoList">
	  <div class="col-md-6">
	    <h4>Indicadores</h4>
	  </div>
	  <div onclick="tableToExcel('tblData', 'name', 'cumplimiento_historico_ciamsa.xls');" style="cursor: pointer;">
		    <span style="color: rgb(0, 107, 177); margin-right: 5px;">
		      Exportar excel
		    </span> 
		    <img src="http://digital.ciamsa.com/img/excel-icon.png" alt="icon-excel" height="30px" title="Exportar datos a Excel">
	   </div>
	</div>
	@endsection

	@section('content')

	<style type="text/css">
		.loader {
		  border: 16px solid #f3f3f3;
		  border-radius: 50%;
		  border-top: 16px solid #3498db;
		  display: inline-block;
		  width: 120px;
		  height: 120px;
		  -webkit-animation: spin 2s linear infinite; /* Safari */
		  animation: spin 2s linear infinite;
		}
		/* Safari */
		@-webkit-keyframes spin {
		  0% { -webkit-transform: rotate(0deg); }
		  100% { -webkit-transform: rotate(360deg); }
		}

		@keyframes spin {
		  0% { transform: rotate(0deg); }
		  100% { transform: rotate(360deg); }
		}
	  	.dat-form{
			padding: 7px;
	    	margin: 3px 7px;
	    	width: 100%;
		}
		input[type="text"] {
		    margin: 3px 7px;
		    padding-left: 10px;
		    width: 100%;
		}
		.search-data{
			background-color: lightgray;
		    border-radius: 5px;
		    padding: 7px;
		}
		.art-graf{
			margin-bottom: 20px;
		}
		.vdp-datepicker__calendar {
	    	position: relative !important;
	    }
	    .cont-data-loading {
		    text-align: center;
		}
		@media (max-width: 680px) {
			.wrapper{
			  padding: initial !important;
			}
		}
		.table-cump th, .table-cump td{
			font-size: 11px !important;
		}
	</style>
		
	<!--Moment Librery-->
	<script src="{{asset('storage/assets/js/moment.js')}}"></script>

	<div id="indicators" class="container">

	    <section class="row" style="margin: 5px 0;">

	    	<input type="hidden" id="id_user" value="{{ Auth::user()->identificacion }}">

	    	<article class="col-md-12">

	    		<div v-show="loader_1" class="cont-data-loading">
					<div class="loader"></div>
				</div>
	    		
	    	</article>

			<article v-show="graph_element_1" class="col-md-5 art-graf table-cump">

	    		<h3 class="text-center">
	    			<!--<a href="#" v-on:click="open_detail($event)" style="color: rgb(0, 107, 177);">-->
	    			<a href="#" v-on:click="open_detail_new($event)" data-toggle="modal" style="color: rgb(0, 107, 177);" data-target="#myModal">
	    				Detalle
	    			</a>
	    		</h3>

	    		<br>

	    		<div class="table-responsive" style="height: 350px;">
	    			<table id="tblData" class="table table-striped">
						 <thead>
						    <tr>
						      <th scope="col">Mes</th>
						      <th scope="col">N° Cont. Ran</th>
						      <th scope="col">Total Cont.</th>
						      <th scope="col">% Cumpl.</th>
						      <th scope="col">Meta</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr v-for="se1 in series_data1_table" >
						      <th scope="row">@{{se1.month}}</th>
						      <td>@{{se1.cont_rango}}</td>
						      <td>@{{se1.cont_total}}</td>
						      <td>@{{se1.fulfillment}}</td>
						      <td>@{{se1.goal}}</td>
						    </tr>
						  </tbody>
					</table>
	    		</div>
					    		
    		</article>

	    	<article class="col-md-7 art-graf">
				<div>
					<br>
					<!--<div v-show="loader_1" class="cont-data-loading">
						<div class="loader"></div>
					</div>-->
					<highcharts v-show="graph_element_1" :options="chartOptionsTest"></highcharts>
					<!--<highcharts v-show="graph_element_1" :options="chartOptions"></highcharts>-->
				</div>
			</article>	    	


			<article class="col-md-12 art-graf">

				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-secondary" role="alert">
						  <strong>Global:</strong> Fecha Descargue Motonave (Menos) Fecha Traslado A Deposito.
						  <br>
						  <strong>Deposito:</strong> Fecha Recibo Documento Traslado (Menos) Fecha Traslado a Deposito.
						</div>
					</div>
				</div>		
				<div class="row search-data">

					<div class="col-md-3 col-sm-12">
					    <select v-model="form_consult_1" class="dat-form">
					      	<option value="">Tipo de consulta</option>
					      	<option value="1">Global</option>
					      	<option value="2">Depósito</option>
					    </select>
					</div>

					<div class="col-md-3 col-sm-12">
				      	<select v-model="data_range_1" class="dat-form">
				      		<option value="">Rango de tiempo</option>
				      		<option value="1">Entre 0-3 dias</option>
				      		<option value="2">Entre 4-5 dias</option>
				      		<option value="3">Entre 6 dias</option>
				      		<option value="4">Entre 7-8 dias</option>
				      		<option value="5">Mas de 8 dias</option>
				      	</select>					
					</div>				

					<div class="col-md-2 col-sm-12">
					    <vuejs-datepicker 
					    	v-model="fec_ini_1" placeholder="Fecha inicial">
					    </vuejs-datepicker>
					</div>

					<div class="col-md-2 col-sm-12">
					    <vuejs-datepicker 
					    	v-model="fec_fin_1" placeholder="Fecha final">
					    </vuejs-datepicker>
					</div>

					<div class="col-md-2 col-sm-12">

				  		<div style="text-align: right;">
							<button type="button" v-on:click="consultData($event, 1)" class="btn btn-default" style="background-color: #006BB1; color: white;">
				            	Buscar <i class="fa fa-search"></i>
				          	</button>
						</div>	
				  		
				  	</div>				    

				</div>	
				
			</article>

			<article class="col-md-12 art-graf">

				<div>

					<br>
					<div v-show="loader_3" class="cont-data-loading">
						<div class="loader"></div>
					</div>
					<highcharts v-show="graph_element_3" :options="chartOptions3"></highcharts>

				</div>	
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-secondary" role="alert">
						  <strong>Global:</strong> Fecha Descargue Motonave (Menos) Fecha Traslado A Deposito.
						  <br>
						  <strong>Deposito:</strong> Fecha Recibo Documento Traslado (Menos) Fecha Traslado a Deposito.
						</div>
					</div>
				</div>
				<div class="row search-data">

					<div class="col-md-3">
					    <select v-model="form_consult_3" class="dat-form" v-on:change="validate_options">
					      	<option value="">Tipo de consulta</option>
					      	<option value="1">Global</option>
					      	<option value="2">Depósito</option>
					    </select>
					</div>

					<div class="col-md-3">
				      	<select v-if="filter_cump1" v-model="data_range_3" class="dat-form">
				      		<option value="">Rango de tiempo</option>
				      		<option value="1">Entre 0-3 dias</option>
				      		<option value="2">Entre 4-5 dias</option>
				      		<option value="3">Entre 6 dias</option>
				      		<option value="4">Entre 7-8 dias</option>
				      		<option value="5">Mas de 8 dias</option>
				      	</select>
				      	<select v-if="filter_cump2" v-model="data_range_3" class="dat-form">
				      		<option value="">Rango de tiempo</option>
				      		<option value="1">Entre 0-2 dias</option>
				      		<option value="2">3 dias</option>
				      		<option value="3">4 dias</option>
				      		<option value="4">Más de 4 días</option>
				      	</select>					
					</div>				

					<div class="col-md-2">
					    <vuejs-datepicker 
					    	v-model="fec_ini_3" placeholder="Fecha inicial">
					    </vuejs-datepicker>
					</div>

					<div class="col-md-2">
					    <vuejs-datepicker 
					    	v-model="fec_fin_3" placeholder="Fecha final">
					    </vuejs-datepicker>
					</div>

					<div class="col-md-2">

				  		<div style="text-align: right;">
							<button type="button" v-on:click="consultData($event, 3)" class="btn btn-default" style="background-color: #006BB1; color: white;">
				            	Buscar <i class="fa fa-search"></i>
				          </button>
						</div>	
				  		
				  	</div>				    

				</div>				

			</article>

	    </section>

		<div class="modal fade" id="myModal" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		        </div>
		        <div class="modal-body">

		          <h3 class="text-center" style="color: rgb(0, 107, 177);">
		          	Detalles generales 
		          </h3>

		          <div v-if="fec_ini_1 !== '' && fec_fin_1 !== ''" onclick="tableToExcel('tblDataDetail', 'name', 'cumplimiento_historico_detalle.xls');" style="cursor: pointer;">
					    <span style="color: rgb(0, 107, 177); margin-right: 5px;">
					      Exportar excel
					    </span> 
					    <img src="http://digital.ciamsa.com/img/excel-icon.png" alt="icon-excel" height="30px" title="Exportar datos a Excel">
				   </div>

				   <br>

		          <div style="margin: 0 auto;">

		          	<div v-if="result_details">

		          		<div class="table-responsive" style="height: 400px;">
			    			<table id="tblDataDetail" class="table table-striped">
								 <thead>
								    <tr>
								    	<th></th>
								      	<th>@{{res_details[0]}}</th>
								      	<th>@{{res_details[8]}}</th>
								      	<th>@{{res_details[1]}}</th>
								      	<th>@{{res_details[2]}}</th>
								      	<th>@{{res_details[3]}}</th>
								      	<th>@{{res_details[4]}}</th>
								      	<th>@{{res_details[5]}}</th>
								      	<th>@{{res_details[6]}}</th>
								      	<th>@{{res_details[7]}}</th>
								      	<th>@{{res_details[9]}}</th>
								      	<th>@{{res_details[10]}}</th>
								      	<th>@{{res_details[11]}}</th>
								      	<th>@{{res_details[12]}}</th>
								      	<th>@{{res_details[13]}}</th>
								      	<th>@{{res_details[14]}}</th>
								      	<th>@{{res_details[15]}}</th>
								      	<th>@{{res_details[16]}}</th>
								      	<th>@{{res_details[17]}}</th>
								      	<th>@{{res_details[18]}}</th>
								      	<th>@{{res_details[19]}}</th>
								      	<th>@{{res_details[20]}}</th>
								      	<th>@{{res_details[21]}}</th>
								      	<th>@{{res_details[22]}}</th>
								      	<th>@{{res_details[23]}}</th>
								    </tr>
								  </thead>
								  <tbody>
								    <tr v-for="(det, index) in res_details['data']">
								    	<td><strong>@{{index+1}}</strong></td>
								    	<td>@{{det[0]}}</td>
								    	<td>@{{det[8]}}</td>
								    	<td>@{{det[1]}}</td>
								    	<td>@{{det[2]}}</td>
								    	<td>@{{det[3]}}</td>
								    	<td>@{{det[4]}}</td>
								    	<td>@{{det[5]}}</td>
								    	<td>@{{det[6]}}</td>
								    	<td>@{{det[7]}}</td>
								    	<td>@{{det[9]}}</td>
								    	<td>@{{det[10]}}</td>
								    	<td>@{{det[11]}}</td>
								    	<td>@{{det[12]}}</td>
								    	<td>@{{det[13]}}</td>
								    	<td>@{{det[14]}}</td>
								    	<td>@{{det[15]}}</td>
								    	<td>@{{det[16]}}</td>
								    	<td>@{{det[17]}}</td>
								    	<td>@{{det[18]}}</td>
								    	<td>@{{det[19]}}</td>
								    	<td>@{{det[20]}}</td>
								    	<td>@{{det[21]}}</td>
								    	<td>@{{det[22]}}</td>
								    	<td>@{{det[23]}}</td>
								    </tr>
								  </tbody>
							</table>
			    		</div>
		          		
		          	</div>
		            
		            <div v-else>

		            	<h3 class="text-center">
		            		No se encontró información para ese rango de fechas.
		            	</h3>
			            	
		            </div>	           	

		          </div>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		        </div>
		      </div>
		      
		    </div>
		</div>

	</div>

		<script type="text/javascript">

		    function tableToExcel(table, name, filename){
		      let uri = 'data:application/vnd.ms-excel;base64,', 
		            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><title></title><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>', 
		            base64 = function(s) { return window.btoa(decodeURIComponent(encodeURIComponent(s))) },         
		            format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; })}
		            
		            if (!table.nodeType) table = document.getElementById(table)
		            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}

		            var link = document.createElement('a');
		            link.download = filename;
		            link.href = uri + base64(format(template, ctx));
		            link.click();
		    }

 		</script>

		<script src="{{asset('storage/assets/js/vue.js')}}"></script>
		<script src="{{asset('storage/assets/js/vuejs-datepicker.min.js')}}"></script>

		<!-- Axios To Run Api Services -->
	  	<script src="{{asset('storage/assets/js/axios.js')}}"></script>
		<script src="https://unpkg.com/vue-select@latest"></script>
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/highcharts-vue@1.3.5/dist/highcharts-vue.min.js"></script>

		<script src="https://code.highcharts.com/modules/series-label.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
		<script src="https://code.highcharts.com/modules/export-data.js"></script>

		<script src="{{asset('storage/assets/js/indicators.js?v=3.5')}}"></script>

	@endsection

@endcan
@can('Agricola')
	
	@section('titlepage')
	<div class="row justify-content-between header-rastreoList">
	  <div class="col-md-12">
	    <h4>Bienvenido(a): {{ Auth::user()->name }}</h4>
	  </div>
	</div>
	@endsection

@endcan
@can('Comercializadora')
	
	@section('titlepage')
	<div class="row justify-content-between header-rastreoList">
	  <div class="col-md-12">
	    <h4>Bienvenido(a): {{ Auth::user()->name }}</h4>
	  </div>
	</div>
	@endsection

@endcan
