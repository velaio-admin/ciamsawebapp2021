@foreach($arreglo as $key => $row)
@if(isset($salt) && $salt == "DespM*" && $key%2==0)
    <div class="form-control" style="background-color: #f9f9f9;">
@endif
<div class="table-responsive m-t-35">
    @if(isset($row['title'])) 
    <h3>{{$row['title'][0]}}
        @if(isset($row['titlevalue'])) 
        <span> {{$row['titlevalue'][0]}}</span>
        @endif
    </h3>
    @endif
    <table id="tblData" class="htmlTable table fkm">
    <!--Table fk --> 
        <thead>   
            <tr>
                @foreach($row as $delta => $value)

                    @if(!is_array($value)) 
                        <th style="background:#C9C7C6"> {{ $value }} </th>
                    @endif

                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($row['data'] as $delta => $value)
            <tr class="fk">
                @foreach($value as $key => $file)

                    @if(isset($salt) && strpos($file, $salt) !== false)

                        <?php
                            $file = str_replace($salt,"",$file);
                            //dd($salt);
                            if($salt == "Bl*"){
                                $url_tmp_1 = str_replace("%var%", $file, config('list_links_tables.tables.links.Bls'));
                                $url_tmp_2 = str_replace("%user%", Auth::user()->identificacion, $url_tmp_1);
                                $url = $url_tmp_2; //importante 1
                            }

                            if($salt == "Bls*"){

                                $url_tmp_1 = str_replace("%var%", $file, config('list_links_tables.tables.links.Bl'));

                                $file_new = str_replace("/","-CIAMSA-",$file);
                                $url_tmp_new = str_replace("%var%", $file_new, config('list_links_tables.tables.links.Bl'));

                                $url_tmp_2 = str_replace("%user%", Auth::user()->identificacion, $url_tmp_1);

                                //URL LINK FK
                                $url = str_replace("%user%", Auth::user()->identificacion, $url_tmp_new);

                                //$url = $url_tmp_2; //importante 1
                                //echo $url;

                                //$url = str_replace("/","-CIAMSA-",$url);

                            }

                            if($salt == "NomL*"){
                                $url_tmp_1 = str_replace("%var%", $file, config('list_links_tables.tables.links.NomL'));
                                $url_tmp_2 = str_replace("%user%", Auth::user()->identificacion, $url_tmp_1);
                                $url = $url_tmp_2; //importante 1
                            }

                            if($salt == "ConsD*"){
                                $url_tmp_1 = str_replace("%var%", $file, config('list_links_tables.tables.links.ConsD'));
                                $url_tmp_2 = str_replace("%user%", Auth::user()->identificacion, $url_tmp_1);
                                $url = $url_tmp_2; //importante 1
                                
                            }

                            if($salt == "EntG*"){
                                $url_tmp_1 = str_replace("%var%", $file, config('list_links_tables.tables.links.EntG'));
                                $url_tmp_2 = str_replace("%user%", Auth::user()->identificacion, $url_tmp_1);
                                $url = $url_tmp_2; //importante 1
                            }

                            if($salt == "FactD*"){
                                if (!empty($file)) {
                                    $url_tmp_1 = str_replace("%var%", $file, config('list_links_tables.tables.links.FactD'));
                                }else{
                                    $url_tmp_1 = "#";
                                }
                                
                                $url = $url_tmp_1; //importante 1
                            }
                        ?> 

                        <td> <a href="{{$url}}" style="color: #006bb1 !important; text-decoration: underline !important;" @if($salt == "FactD*") target="_blank" class="download" @endif>@if($salt != "FactD*"){{ $file }}@endif</a></td>

                    @else
                        @if(isset($salt) && $salt == "FactD*")
                            @if($key == 3 && $file == 'Vencida')
                                <td style = "color:  #ff6b61;;"> {{ $file }} <i class="fas fa-exclamation-circle"></i></td>
                            @else
                                <td> {{ $file }}</td>
                            @endif
                        @else
                            <!-- data td table fk -->
                            <td> {{ $file }}</td>
                        @endif
                    @endif
                
                @endforeach
            </tr>

            @endforeach
        </tbody>
    </table>
    @if($pagination)
        @if(!empty($cantidad_pag) && sizeof($cantidad_pag) > 0)
        <div class="paginated">
            <div class="result">
                <!-- Mostrando 1  a 10  de 10 resultados -->
            </div>
            <ul class="pagination fk123">
                <?php //echo 'current '.((int)$cantidad_pag['currentPage']); ?>
                <li @if((int)$cantidad_pag['currentPage'] == 1) class="next disabled" @else class="next" @endif>
                    <a id="link_url_prev" href="" rel="prev">Anterior</a>
                </li>
                <?php 
                    $cantPages = 1; 
                    $initPage = ((int)$cantidad_pag["currentPage"]*1 > (int)env("TOTAL_PAGES_PAGINATOR")) ? (int)$cantidad_pag["currentPage"] : 1;
                    //echo 'init '.($initPage)."<br>";
                    //echo 'totalPages '.((int)$cantidad_pag['totalPages']);
                ?>
                @for($i=$initPage; $i <= (int)$cantidad_pag['totalPages'] ; $i++)
                    @if($cantPages <= (int)env('TOTAL_PAGES_PAGINATOR'))
                        <li @if((int)$cantidad_pag['currentPage'] == $i) style="background-color: #006bb1;" @endif>
                            <a id="link_url_{{$i}}" href="" @if((int)$cantidad_pag['currentPage'] == $i) style="color: white;" @endif>{{$i}}</a>
                        </li>
                    @endif
                    <?php $cantPages += 1; ?>
                @endfor
                <li class="next">
                    <a id="link_url_next" href="">Siguiente</a>
                </li>
            </ul>
        </div>
        @endif
    @endif
</div>
@if(isset($salt) && $salt == "DespM*" && $key%2==0)
    <br>
    </div>
    <br>
@endif
@endforeach

<script type="text/javascript">
    
    var cantPag = parseInt('{{$cantidad_pag["totalPages"]*1}}');
    var pathname = location.href;
    var cantPages = 1;
    var currentPage = parseInt('{{$cantidad_pag["currentPage"]*1}}');
    var envTotalPages = parseInt('{{env("TOTAL_PAGES_PAGINATOR")*1}}');
    var initPage = currentPage > envTotalPages ? currentPage : 1;

    console.log(cantPag);
    console.log(currentPage);
    console.log(envTotalPages);

    for (var i = initPage; i <= cantPag; i++) {

        if (cantPages <= envTotalPages) {

            if (i >= cantPag) {
                var next = i;
            }else{
                var next = currentPage+1;
            }

            if (i <= 1) {
                var prev = 1;
            }else{
                var prev = currentPage-1;
            }

            if (i <= 1) {
                var pag = 1;
            }else{
                var pag = i;
            }

            var url = pathname.replace("/"+currentPage+"/","/"+pag+"/");
            var url_prev = pathname.replace("/"+currentPage+"/","/"+prev+"/");
            var url_next = pathname.replace("/"+currentPage+"/","/"+next+"/");

            console.log('url next ',next);

            console.log('url cantPag ',cantPag);
            console.log('url pathname ',pathname);
            console.log('url url_next ',url_next);

            document.getElementById("link_url_"+i).href = url;
            document.getElementById("link_url_prev").href = url_prev;
            document.getElementById("link_url_next").href = url_next;

            cantPages = cantPages+1;

        }
    }

</script>