<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        @include('includes.docs.api.v1.head')
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-3" id="sidebar">
                    <div class="column-content">
                        <div class="search-header">
                            <img src="/assets/docs/api.v1/img/f2m2_logo.svg" class="logo" alt="Logo" />
                            <input id="search" type="text" placeholder="Search">
                        </div>
                        <ul id="navigation">

                            <li><a href="#introduction">Introduction</a></li>

                            

                            <li>
                                <a href="#ServiciosLogisticos">ServiciosLogisticos</a>
                                <ul>
									<li><a href="#ServiciosLogisticos_observations">observations</a></li>

									<li><a href="#ServiciosLogisticos_getBlContenedor">getBlContenedor</a></li>

									<li><a href="#ServiciosLogisticos_getClientsData">getClientsData</a></li>

									<li><a href="#ServiciosLogisticos_getDetailCont">getDetailCont</a></li>

									<li><a href="#ServiciosLogisticos_getBlContStates">getBlContStates</a></li>

									<li><a href="#ServiciosLogisticos_updateBlCont">updateBlCont</a></li>

									<li><a href="#ServiciosLogisticos_getBlAvalibleTran">getBlAvalibleTran</a></li>

									<li><a href="#ServiciosLogisticos_getBlAvaliblePro">getBlAvaliblePro</a></li>

									<li><a href="#ServiciosLogisticos_getConfig">getConfig</a></li>

									<li><a href="#ServiciosLogisticos_saveUpdateConfig">saveUpdateConfig</a></li>

									<li><a href="#ServiciosLogisticos_deleteConfig">deleteConfig</a></li>

									<li><a href="#ServiciosLogisticos_semaforos">semaforos</a></li>

									<li><a href="#ServiciosLogisticos_getDetalleBl">getDetalleBl</a></li>

									<li><a href="#ServiciosLogisticos_getDetalleContenedor">getDetalleContenedor</a></li>

									<li><a href="#ServiciosLogisticos_rastreoList">rastreoList</a></li>

									<li><a href="#ServiciosLogisticos_despachoMercancia">despachoMercancia</a></li>

									<li><a href="#ServiciosLogisticos_fechaLimiteDevolucionContenedor">fechaLimiteDevolucionContenedor</a></li>

									<li><a href="#ServiciosLogisticos_existenciaMercancia">existenciaMercancia</a></li>
</ul>
                            </li>


                            <li>
                                <a href="#Agricola">Agricola</a>
                                <ul>
									<li><a href="#Agricola_tipoProductos">tipoProductos</a></li>

									<li><a href="#Agricola_generarPedido">generarPedido</a></li>

									<li><a href="#Agricola_notificarPedido">notificarPedido</a></li>
</ul>
                            </li>


                            <li>
                                <a href="#Auth">Auth</a>
                                <ul>
									<li><a href="#Auth_login">login</a></li>

									<li><a href="#Auth_signup">signup</a></li>

									<li><a href="#Auth_logout">logout</a></li>

									<li><a href="#Auth_user">user</a></li>
</ul>
                            </li>


                            <li>
                                <a href="#Agricola">Agricola</a>
                                <ul>
									<li><a href="#Agricola_getTypeInvoiceStates">getTypeInvoiceStates</a></li>

									<li><a href="#Agricola_cupoCredito">cupoCredito</a></li>

									<li><a href="#Agricola_descargarFactura">descargarFactura</a></li>

									<li><a href="#Agricola_historialFacturas">historialFacturas</a></li>

									<li><a href="#Agricola_consultaPedido">consultaPedido</a></li>

									<li><a href="#Agricola_detallePedido">detallePedido</a></li>

									<li><a href="#Agricola_productos">productos</a></li>

									<li><a href="#Agricola_formasDePago">formasDePago</a></li>

									<li><a href="#Agricola_ciudades">ciudades</a></li>

									<li><a href="#Agricola_radicarPedido">radicarPedido</a></li>

									<li><a href="#Agricola_notificarPedido">notificarPedido</a></li>
</ul>
                            </li>


                            <li>
                                <a href="#Comercializadora">Comercializadora</a>
                                <ul>
									<li><a href="#Comercializadora_nominaciones">nominaciones</a></li>

									<li><a href="#Comercializadora_detalleNominacion">detalleNominacion</a></li>

									<li><a href="#Comercializadora_detalleEntrega">detalleEntrega</a></li>

									<li><a href="#Comercializadora_procesoLogistico">procesoLogistico</a></li>

									<li><a href="#Comercializadora_procesoLogisticoDetalle">procesoLogisticoDetalle</a></li>

									<li><a href="#Comercializadora_contenedoresQuedados">contenedoresQuedados</a></li>

									<li><a href="#Comercializadora_contenedoresReprogramados">contenedoresReprogramados</a></li>
</ul>
                            </li>


                        </ul>
                    </div>
                </div>
                <div class="col-9" id="main-content">

                    <div class="column-content">

                        @include('includes.docs.api.v1.introduction')

                        <hr />

                                                

                                                <a href="#" class="waypoint" name="ServiciosLogisticos"></a>
                        <h2>ServiciosLogisticos</h2>
                        <p></p>

                        
                        <a href="#" class="waypoint" name="ServiciosLogisticos_observations"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>observations</h3></li>
                            <li>api/v1/bl/cont/observaciones</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite ingresar las observaciones a un BL o un Contenedor.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/bl/cont/observaciones" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">id_cliente</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Id del cliente. Es el numero de cedula o nit del consultante.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="id_cliente">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">observacion</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Observaciónes. Son las observaciones que se van a incluir en el BL o Contenedor</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="observacion">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">tipo</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Tipo. Es el tipo de objeto al cual le adicionaremos la observacion, sea un BL o Contenedor, las opciones son bl y cont.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="tipo">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">id</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Id. Es el id del BL o el Contenedor.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="id">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_getBlContenedor"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>getBlContenedor</h3></li>
                            <li>api/v1/Servicios_logisticos/getBlContenedor</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc"></p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/Servicios_logisticos/getBlContenedor" type="GET">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_getClientsData"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>getClientsData</h3></li>
                            <li>api/v1/Servicios_logisticos/getClientsData</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc"></p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/Servicios_logisticos/getClientsData" type="GET">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_getDetailCont"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>getDetailCont</h3></li>
                            <li>api/v1/Servicios_logisticos/getDetailCont</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc"></p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/Servicios_logisticos/getDetailCont" type="POST">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_getBlContStates"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>getBlContStates</h3></li>
                            <li>api/v1/Servicios_logisticos/getBlContStates</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc"></p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/Servicios_logisticos/getBlContStates" type="POST">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_updateBlCont"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>updateBlCont</h3></li>
                            <li>api/v1/Servicios_logisticos/updateBlCont</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc"></p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/Servicios_logisticos/updateBlCont" type="POST">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_getBlAvalibleTran"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>getBlAvalibleTran</h3></li>
                            <li>api/v1/Servicios_logisticos/getBlAvalibleTran</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc"></p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/Servicios_logisticos/getBlAvalibleTran" type="POST">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_getBlAvaliblePro"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>getBlAvaliblePro</h3></li>
                            <li>api/v1/Servicios_logisticos/getBlAvaliblePro</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc"></p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/Servicios_logisticos/getBlAvaliblePro" type="POST">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_getConfig"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>getConfig</h3></li>
                            <li>api/v1/Servicios_logisticos/get/config/{id_user}</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la informacion de configuraciones actuales en formato JSON.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/Servicios_logisticos/get/config/{id_user}" type="GET">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">id_user</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">Nombre: Id del cliente. Es id de base de datos del usuario.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="id_user">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_saveUpdateConfig"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>saveUpdateConfig</h3></li>
                            <li>api/v1/Servicios_logisticos/save/update/config</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite guardar y/o actualizar la informacion de las configuraciones de cada columna. Esta operacion se hace columna por columna.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/Servicios_logisticos/save/update/config" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">id_user</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">Nombre: Id del cliente. Es id de base de datos del usuario.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="id_user">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">configuration</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Configuracion. Json en formato string con la informacion actualizada o para insertar de configuracion</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="configuration">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_deleteConfig"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>deleteConfig</h3></li>
                            <li>api/v1/Servicios_logisticos/delete/update/config/{key}/{id_user}</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite eliminar una configuracion de una columna.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/Servicios_logisticos/delete/update/config/{key}/{id_user}" type="GET">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">id_user</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">Nombre: Id del cliente. Es id de base de datos del usuario.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="id_user">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">key</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">Nombre: Llave. Llave o key indicadora del array donde se encuentra la columna.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="key">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_semaforos"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>semaforos</h3></li>
                            <li>api/v1/Servicios_logisticos/generate/list/trafficLight</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite generar la informacion especifica de Ciamsa para sus clientes de forma semaforizada.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/Servicios_logisticos/generate/list/trafficLight" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">id_user</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">Nombre: Id del cliente. Es id de base de datos del usuario.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="id_user">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">fecha_inicial</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Fecha inicial. Fecha inicial desde donde se quiere obtener la informacion</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fecha_inicial">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">fecha_final</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Fecha final. Fecha final desde donde se quiere obtener la informacion</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fecha_final">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_getDetalleBl"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>getDetalleBl</h3></li>
                            <li>api/v1/serviciosLogisticos/getDetalleBl</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información de detalle del BL.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/serviciosLogisticos/getDetalleBl" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">id_bl</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Id del BL. Es el numero de BL.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="id_bl">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_getDetalleContenedor"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>getDetalleContenedor</h3></li>
                            <li>api/v1/serviciosLogisticos/getDetalleContenedor</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información de detalle del Contenedor.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/serviciosLogisticos/getDetalleContenedor" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">id_contenedor</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Id del Contenedor. Es el numero de Contenedor.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="id_contenedor">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_rastreoList"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>rastreoList</h3></li>
                            <li>api/v1/serviciosLogisticos/rastreoList</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información del rastreo de bls segun la identificación del usuario y tambien realizar una busqueda especifica por medio de los parametros opcionales.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/serviciosLogisticos/rastreoList" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">estado</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Estado. Es el filtro de estado con el que se buscará la información, los posibles estados son TRANSITO (transito), PROCESO (proceso) y PROC_FINAL (proceso finalizado).</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="estado">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">numero</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="numero">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">type</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son bls(son los BL) y dos(son los DO).</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="type">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">fechainicial</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Fecha inicial. Es la fecha de inicio para consultar la información en un rango determinado de tiempo.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fechainicial">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">fechafinal</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Fecha final. Es la fecha de finalización para consultar la información en un rango determinado de tiempo.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fechafinal">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">pagina</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="pagina">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">cant_pag</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="cant_pag">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_despachoMercancia"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>despachoMercancia</h3></li>
                            <li>api/v1/serviciosLogisticos/despachoMercancia</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información de despachos de mercancia de BLs o DOs segun la identificación del usuario y tambien realizar una busqueda especifica por medio de los parametros opcionales, este servicio por defecto trae la información de los ultimos 90 dias.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/serviciosLogisticos/despachoMercancia" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">numero</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="numero">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">type</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son bls(son los BL) y dos(son los DO).</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="type">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">fechainicial</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Fecha inicial. Es la fecha de inicio para consultar la información en un rango determinado de tiempo.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fechainicial">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">fechafinal</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Fecha final. Es la fecha de finalización para consultar la información en un rango determinado de tiempo.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fechafinal">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">pagina</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="pagina">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">cant_pag</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="cant_pag">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_fechaLimiteDevolucionContenedor"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>fechaLimiteDevolucionContenedor</h3></li>
                            <li>api/v1/serviciosLogisticos/fechaLimiteDevolucionContenedor</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información de las fechas limites de devolucion de contenedores de BLs o DOs segun la identificación del usuario y tambien realizar una busqueda especifica por medio de los parametros opcionales, este servicio por defecto trae la información de los ultimos 90 dias.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/serviciosLogisticos/fechaLimiteDevolucionContenedor" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">numero</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="numero">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">type</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son bls(son los BL) y dos(son los DO).</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="type">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">fechainicial</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Fecha inicial. Es la fecha de inicio para consultar la información en un rango determinado de tiempo.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fechainicial">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">fechafinal</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Fecha final. Es la fecha de finalización para consultar la información en un rango determinado de tiempo.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fechafinal">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">pagina</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="pagina">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">cant_pag</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="cant_pag">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="ServiciosLogisticos_existenciaMercancia"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>existenciaMercancia</h3></li>
                            <li>api/v1/serviciosLogisticos/existenciaMercancia</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información de existencia de mercancia de BLs o DOs segun la identificación del usuario y tambien realizar una busqueda especifica por medio de los parametros opcionales, este servicio por defecto trae la información de los ultimos 90 dias.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/serviciosLogisticos/existenciaMercancia" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">numero</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="numero">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">fechainicial</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Fecha inicial. Es la fecha de inicio para consultar la información en un rango determinado de tiempo.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fechainicial">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">fechafinal</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Fecha final. Es la fecha de finalización para consultar la información en un rango determinado de tiempo.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fechafinal">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">pagina</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="pagina">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">cant_pag</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="cant_pag">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>
                        

                                                <a href="#" class="waypoint" name="Agricola"></a>
                        <h2>Agricola</h2>
                        <p></p>

                        
                        <a href="#" class="waypoint" name="Agricola_tipoProductos"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>tipoProductos</h3></li>
                            <li>api/v1/products/list/{type}</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc"></p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/products/list/{type}" type="GET">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Agricola_generarPedido"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>generarPedido</h3></li>
                            <li>api/v1/products/radicar/pedido</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc"></p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/products/radicar/pedido" type="POST">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Agricola_notificarPedido"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>notificarPedido</h3></li>
                            <li>api/v1/products/notificar/pedido</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc"></p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/products/notificar/pedido" type="POST">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>
                        

                                                <a href="#" class="waypoint" name="Auth"></a>
                        <h2>Auth</h2>
                        <p></p>

                        
                        <a href="#" class="waypoint" name="Auth_login"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>login</h3></li>
                            <li>api/v1/auth/login</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite hacer loguin en la aplicacion de ciamsa.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/auth/login" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">email</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Email. Es el correo electronico del usuario previamente registrado.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="email">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">password</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Contraseña. Es el numero del BL a consultar.</div>
                                <div class="parameter-value">
                                    <input type="password" class="parameter-value-text" name="password">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">remember_me</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Recuerdame. Es un campo booleano que alarga la fecha de expiracion del token, los valores posibles son 1(true) o 0(false.)</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="remember_me">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Auth_signup"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>signup</h3></li>
                            <li>api/v1/auth/signup</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc"></p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/auth/signup" type="POST">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Auth_logout"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>logout</h3></li>
                            <li>api/v1/logout</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite hacer logout en la aplicacion de ciamsa, solo se le envia el token en el header, con esta informacion automaticamente ejecuta el logout del usuario.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/logout" type="GET">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Auth_user"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>user</h3></li>
                            <li>api/v1/user</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite traer la informacion del usuario en la aplicacion de ciamsa, solo se le envia el token en el header, con esta informacion automaticamente ejecuta este servicio del usuario.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/user" type="GET">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>
                        

                                                <a href="#" class="waypoint" name="Agricola"></a>
                        <h2>Agricola</h2>
                        <p></p>

                        
                        <a href="#" class="waypoint" name="Agricola_getTypeInvoiceStates"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>getTypeInvoiceStates</h3></li>
                            <li>api/v1/agricola/states/facturas</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite consultar los posibles estados de una factura.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/agricola/states/facturas" type="GET">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Agricola_cupoCredito"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>cupoCredito</h3></li>
                            <li>api/v1/agricola/cupo-credito</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio conocer el cupo de credito de un cliente en particular.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/agricola/cupo-credito" type="GET">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Agricola_descargarFactura"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>descargarFactura</h3></li>
                            <li>api/v1/agricola/descargar-factura/{numero_factura}</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la descarga de una factura.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/agricola/descargar-factura/{numero_factura}" type="GET">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">numero_factura</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">Nombre: Numero de factura.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="numero_factura">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Agricola_historialFacturas"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>historialFacturas</h3></li>
                            <li>api/v1/agricola/historial/facturas</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la informacion del historial de facturas.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/agricola/historial/facturas" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">numero</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="numero">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">fechainicial</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Fecha inicial. Es la fecha de inicio para consultar la información en un rango determinado de tiempo.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fechainicial">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">fechafinal</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Fecha final. Es la fecha de finalización para consultar la información en un rango determinado de tiempo.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fechafinal">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">pagina</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="pagina">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">cant_pag</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="cant_pag">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Agricola_consultaPedido"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>consultaPedido</h3></li>
                            <li>api/v1/agricola/consulta/pedido</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información de detalle del BL.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/agricola/consulta/pedido" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">pagina</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="pagina">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">cant_pag</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="cant_pag">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Agricola_detallePedido"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>detallePedido</h3></li>
                            <li>api/v1/agricola/consulta/detalle-pedido/{id_pedido}</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información de detalle del BL.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/agricola/consulta/detalle-pedido/{id_pedido}" type="GET">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">id_pedido</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Id Pedido. Es el numero del pedido a consultar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="id_pedido">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Agricola_productos"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>productos</h3></li>
                            <li>api/v1/agricola/consulta/productos</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información de todos los productos disponibles para la solicitud de pedido.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/agricola/consulta/productos" type="GET">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Agricola_formasDePago"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>formasDePago</h3></li>
                            <li>api/v1/agricola/consulta/formasDePago</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información de todos las formas de pago disponibles.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/agricola/consulta/formasDePago" type="POST">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Agricola_ciudades"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>GET</h2></li>
                            <li><h3>ciudades</h3></li>
                            <li>api/v1/agricola/consulta/ciudades</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información de todas las ciudades disponibles.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/agricola/consulta/ciudades" type="GET">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="GET"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Agricola_radicarPedido"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>radicarPedido</h3></li>
                            <li>api/v1/agricola/radicar/pedido</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite radicar una solicitud de pedido.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/agricola/radicar/pedido" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">productos</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Array de productos como STRING. Es el array de productos como STRING que contienen los codigos de material, cantidades y tipo de pedido que se va a solicitar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="productos">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Agricola_notificarPedido"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>notificarPedido</h3></li>
                            <li>api/v1/agricola/notificar/pedido</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite notificar una solicitud de pedido no efectuada.</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/agricola/notificar/pedido" type="POST">
                          <div class="endpoint-paramenters">
                            
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>
                        

                                                <a href="#" class="waypoint" name="Comercializadora"></a>
                        <h2>Comercializadora</h2>
                        <p></p>

                        
                        <a href="#" class="waypoint" name="Comercializadora_nominaciones"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>nominaciones</h3></li>
                            <li>api/v1/comercializadora/nominaciones</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información de las nominaciones .</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/comercializadora/nominaciones" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">fechainicial</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Fecha inicial. Es la fecha de inicio para consultar la información en un rango determinado de tiempo.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fechainicial">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">fechafinal</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Fecha final. Es la fecha de finalización para consultar la información en un rango determinado de tiempo.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="fechafinal">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">type</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son booking(son los booking) y radicado(son los radicado).</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="type">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">numero</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="numero">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">pagina</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="pagina">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">cant_pag</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="cant_pag">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Comercializadora_detalleNominacion"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>detalleNominacion</h3></li>
                            <li>api/v1/comercializadora/nominacion/detalle</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información del detalle de la nominacion .</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/comercializadora/nominacion/detalle" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">type</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son booking(son los booking) y radicado(son los radicado).</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="type">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">numero</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Numero de la nominacion. Es el numero de la nominacion a consultar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="numero">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Comercializadora_detalleEntrega"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>detalleEntrega</h3></li>
                            <li>api/v1/comercializadora/entrega/detalle</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información del detalle de la entrega .</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/comercializadora/entrega/detalle" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">numero</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Numero de la nominacion. Es el numero de la nominacion a consultar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="numero">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Comercializadora_procesoLogistico"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>procesoLogistico</h3></li>
                            <li>api/v1/comercializadora/proceso/logistico</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información del detalle de la nominacion .</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/comercializadora/proceso/logistico" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">type</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son booking(son los booking) y entrega(son los entrega).</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="type">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">numero</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Numero de la nominacion. Es el numero de la nominacion a consultar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="numero">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Comercializadora_procesoLogisticoDetalle"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>procesoLogisticoDetalle</h3></li>
                            <li>api/v1/comercializadora/proceso/logistico/detalle</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información del detalle del proceso logistico .</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/comercializadora/proceso/logistico/detalle" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">type</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Typo de consulta. Este parametro sirve para indicar que tipo de numero se va a consultar en el sistema, los valores posibles son booking(son los booking) y entrega(son los entrega).</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="type">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">numero</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">Nombre: Numero de la nominacion. Es el numero de la nominacion a consultar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="numero">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Comercializadora_contenedoresQuedados"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>contenedoresQuedados</h3></li>
                            <li>api/v1/comercializadora/contenedores/quedados</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información de los contenedores quedados .</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/comercializadora/contenedores/quedados" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">numero</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="numero">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">pagina</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="pagina">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">cant_pag</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="cant_pag">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>

                        <a href="#" class="waypoint" name="Comercializadora_contenedoresReprogramados"></a>
                        <div class="endpoint-header">
                            <ul>
                            <li><h2>POST</h2></li>
                            <li><h3>contenedoresReprogramados</h3></li>
                            <li>api/v1/comercializadora/contenedores/reprogramados</li>
                          </ul>
                        </div>

                        <div>
                          <p class="endpoint-short-desc">Este servicio permite obtener la información de los contenedores quedados .</p>
                        </div>
                       <!--  <div class="parameter-header">
                             <p class="endpoint-long-desc"></p>
                        </div> -->
                        <form class="api-explorer-form" uri="api/v1/comercializadora/contenedores/reprogramados" type="POST">
                          <div class="endpoint-paramenters">
                            <h4>Parameters</h4>
                            <ul>
                              <li class="parameter-header">
                                <div class="parameter-name">PARAMETER</div>
                                <div class="parameter-type">TYPE</div>
                                <div class="parameter-desc">DESCRIPTION</div>
                                <div class="parameter-value">VALUE</div>
                              </li>
                                                           <li>
                                <div class="parameter-name">numero</div>
                                <div class="parameter-type">string</div>
                                <div class="parameter-desc">(Opcional) Nombre: Numero de BL. Es el numero del BL a consultar.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="numero">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">pagina</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Pagina. Es el numero de la pagina actual de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="pagina">
                                </div>
                              </li>
                             <li>
                                <div class="parameter-name">cant_pag</div>
                                <div class="parameter-type">integer</div>
                                <div class="parameter-desc">(Opcional) Nombre: Cantidad de paginas. Es el numero de la cantidad de paginas de la información consultada.</div>
                                <div class="parameter-value">
                                    <input type="text" class="parameter-value-text" name="cant_pag">
                                </div>
                              </li>

                            </ul>
                          </div>
                           <div class="generate-response" >
                              <!-- <input type="hidden" name="_method" value="POST"> -->
                              <input type="submit" class="generate-response-btn" value="Generate Example Response">
                          </div>
                        </form>
                        <hr>


                    </div>
                </div>
            </div>
        </div>


    </body>
</html>
