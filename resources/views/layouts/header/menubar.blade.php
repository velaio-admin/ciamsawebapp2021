<div id="top" class="">
  <!-- .navbar -->
  <nav class="navbar navbar-static-top">
    <div class="container-fluid m-0">
      <a class="navbar-brand mr-0" href="/home">
        <img src="{{ asset('storage/assets/img/v-c-logo.png') }}" class="admin_img" alt="logo">
      </a>
      <div class="menu mr-sm-auto">
        <span class="toggle-left" id="menu-toggle">
          <i class="fa fa-bars text-white"></i>
        </span>
      </div>
      <div class="topnav dropdown-menu-right ml-auto">
        <div class="btn-group">

        </div>
        <!-- <div class="btn-group">
          <div class="notifications messages no-bg">
            <a class="btn btn-default btn-sm" data-toggle="dropdown"> <i class="fa fa-bell text-white"></i>
              <span class="badge badge-danger">9</span>
            </a>
            <div class="dropdown-menu drop_box_align" role="menu">
              <div class="popover-header">You have 9 Notifications</div>
              <div id="notifications">
                <div class="data">
                  <div class="row">
                    <div class="col-2">
                      <img src="{{ asset('storage/assets/img/mailbox_imgs/1.jpg') }}" class="message-img avatar rounded-circle" alt="avatar1"></div>
                      <div class="col-10 message-data">
                        <i class="fa fa-clock-o"></i>
                        <strong>Remo</strong> sent you an image
                        <br>
                        <small class="primary_txt">just now.</small>
                        <br>
                      </div>
                    </div>
                  </div>
                  <div class="data">
                    <div class="row">
                      <div class="col-2">
                        <img src="{{ asset('storage/assets/img/mailbox_imgs/2.jpg') }}" class="message-img avatar rounded-circle" alt="avatar1"></div>
                        <div class="col-10 message-data">
                          <i class="fa fa-clock-o"></i>
                          <strong>clay</strong> business propasals
                          <br>
                          <small class="primary_txt">20min Back.</small>
                          <br>
                        </div>
                      </div>
                    </div>
                    <div class="data">
                      <div class="row">
                        <div class="col-2">
                          <img src="{{ asset('storage/assets/img/mailbox_imgs/3.jpg') }}" class="message-img avatar rounded-circle" alt="avatar1"></div>
                          <div class="col-10 message-data">
                            <i class="fa fa-clock-o"></i>
                            <strong>John</strong> meeting at Ritz
                            <br>
                            <small class="primary_txt">2hrs Back.</small>
                            <br>
                          </div>
                        </div>
                      </div>
                      <div class="data">
                        <div class="row">
                          <div class="col-2">
                            <img src="{{ asset('storage/assets/img/mailbox_imgs/6.jpg') }}" class="message-img avatar rounded-circle" alt="avatar1"></div>
                            <div class="col-10 message-data">
                              <i class="fa fa-clock-o"></i>
                              <strong>Luicy</strong> Request Invitation
                              <br>
                              <small class="primary_txt">Yesterday.</small>
                              <br>
                            </div>
                          </div>
                        </div>
                        <div class="data">
                          <div class="row">
                            <div class="col-2">
                              <img src="{{ asset('storage/assets/img/mailbox_imgs/1.jpg') }}" class="message-img avatar rounded-circle" alt="avatar1"></div>
                              <div class="col-10 message-data">
                                <i class="fa fa-clock-o"></i>
                                <strong>Remo</strong> sent you an image
                                <br>
                                <small class="primary_txt">just now.</small>
                                <br>
                              </div>
                            </div>
                          </div>
                          <div class="data">
                            <div class="row">
                              <div class="col-2">
                                <img src="{{ asset('storage/assets/img/mailbox_imgs/2.jpg') }}" class="message-img avatar rounded-circle" alt="avatar1"></div>
                                <div class="col-10 message-data">
                                  <i class="fa fa-clock-o"></i>
                                  <strong>clay</strong> business propasals
                                  <br>
                                  <small class="primary_txt">20min Back.</small>
                                  <br>
                                </div>
                              </div>
                            </div>
                            <div class="data">
                              <div class="row">
                                <div class="col-2">
                                  <img src="{{ asset('storage/assets/img/mailbox_imgs/3.jpg') }}" class="message-img avatar rounded-circle" alt="avatar1"></div>
                                  <div class="col-10 message-data">
                                    <i class="fa fa-clock-o"></i>
                                    <strong>John</strong> meeting at Ritz
                                    <br>
                                    <small class="primary_txt">2hrs Back.</small>
                                    <br>
                                  </div>
                                </div>
                              </div>
                              <div class="data">
                                <div class="row">
                                  <div class="col-2">
                                    <img src="{{ asset('storage/assets/img/mailbox_imgs/6.jpg') }}" class="message-img avatar rounded-circle" alt="avatar1"></div>
                                    <div class="col-10 message-data">
                                      <i class="fa fa-clock-o"></i>
                                      <strong>Luicy</strong> Request Invitation
                                      <br>
                                      <small class="primary_txt">Yesterday.</small>
                                      <br>
                                    </div>
                                  </div>
                                </div>
                                <div class="data">
                                  <div class="row">
                                    <div class="col-2">
                                      <img src="{{ asset('storage/assets/img/mailbox_imgs/1.jpg') }}" class="message-img avatar rounded-circle" alt="avatar1"></div>
                                      <div class="col-10 message-data">
                                        <i class="fa fa-clock-o"></i>
                                        <strong>Remo</strong> sent you an image
                                        <br>
                                        <small class="primary_txt">just now.</small>
                                        <br>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="popover-footer">
                                  <a href="#">View All</a>
                                </div>
                              </div>
                            </div>
                          </div> -->

        <div class="btn-group">
          <div class="user-settings no-bg">
            <button type="button" class="btn btn-default no-bg micheal_btn" data-toggle="dropdown">
              <img src="{{ asset(Auth::user()->avatar) }}" class="admin_img2 rounded-circle avatar-img" alt="avatar">
              <strong>{{ Auth::user()->name }}</strong>
              <span class="fa fa-sort-down white_bg"></span>
            </button>
            <div class="dropdown-menu admire_admin">
              <div class="popover-header">{{ Auth::user()->name }}</div>
              @hasanyrole('Admin|Admin_ciamsa_1|Admin_ciamsa_2|Admin_ciamsa_fertilizantes|Usuario_ciamsa_2|Usuario_ciamsa_1|Usuario_ciamsa_fertilizantes')
              <a class="dropdown-item" href="/edit_user">
                <i class="fa fa-cogs"></i> Perfil de usuario
              </a>
              @endhasanyrole
              @hasanyrole('Admin|Admin_ciamsa_1|Admin_ciamsa_2|Admin_ciamsa_fertilizantes')
              <a class="dropdown-item" href="/register">
                <i class="fa fa-address-book"></i> Registro
              </a>
              @endhasanyrole
              @hasanyrole('Admin|Admin_ciamsa_1|Admin_ciamsa_2|Admin_ciamsa_fertilizantes')
              <a class="dropdown-item" href="/role">
                <i class="fa fa-lock" aria-hidden="true"></i> Roles y permisos
              </a>
              @endhasanyrole
              <a class="dropdown-item" href="/merge_pdf_view">
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Unir PDFs
              </a>
              <!-- 
                <a class="dropdown-item" href="#">
                  <i class="fa fa-user"></i> User Status
                </a>
                <a class="dropdown-item" href="mail_inbox">
                  <i class="fa fa-envelope"></i> Inbox
                </a>
                <a class="dropdown-item" href="lockscreen">
                  <i class="fa fa-lock"></i> Lock Screen
                </a> 
              -->
              <a class="dropdown-item" href="/change_password/{{ Auth::user()->id }}/execute">
                <i class="fa fa-edit"></i> Cambiar clave
              </a>

              <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i> Log Out
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </nav>
  <!-- /.navbar -->
  <!-- /.head -->
</div>
