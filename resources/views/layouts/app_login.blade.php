<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="UTF-8">
  <title>
    @section('title')
    Ciamsa | Ciamsa
    @show
  </title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="{{asset('storage/assets/img/logo1.ico')}}"/>
  <!-- global styles-->
  <link type="text/css" rel="stylesheet" href="#" id="skin_change"/>
  <!-- end of global styles-->
  <link rel="stylesheet" href="{{asset('storage/assets/css/components.css')}}" />
  <link rel="stylesheet" href="{{asset('storage/assets/css/custom.css')}}" />
  <link rel="stylesheet" href="{{asset('storage/assets/css/styles.css')}}" />
  <!-- end of global styles-->
  <link rel="stylesheet" href="{{asset('storage/assets/vendors/chartist/css/chartist.min.css')}}" />
  <link rel="stylesheet" href="{{asset('storage/assets/vendors/circliful/css/jquery.circliful.css')}}">
  <link rel="stylesheet" href="{{asset('storage/assets/css/pages/index.css')}}">
  <link rel="stylesheet" href="{{asset('storage/assets/css/login.css')}}">
  <link rel="stylesheet" href="#" id="skin_change" />

</head>
<body>
  <div id="app">
    <!-- /#left -->
    <div id="content" class="bg-container">
      
      <div class="outer">
        <div class="inner bg-container" style="background: #fff0;">
          @yield('content')
        </div>
      </div>

    </div>
  </div>

</div>
<script src="{{asset('storage/assets/js/components.js')}}"></script>
<script src="{{asset('storage/assets/js/custom.js')}}"></script>
<script src="{{asset('storage/assets/js/scripts.js')}}"></script>
<!--end of global scripts-->
<!--  plugin scripts -->
<script src="{{asset('storage/assets/vendors/countUp.js/js/countUp.min.js')}}"></script>
<script src="{{asset('storage/assets/vendors/flip/js/jquery.flip.min.js')}}"></script>
<script src="{{asset('storage/assets/js/pluginjs/jquery.sparkline.js')}}"></script>
<script src="{{asset('storage/assets/vendors/chartist/js/chartist.min.js')}}"></script>
<script src="{{asset('storage/assets/js/pluginjs/chartist-tooltip.js')}}"></script>
<script src="{{asset('storage/assets/vendors/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('storage/assets/vendors/circliful/js/jquery.circliful.min.js')}}"></script>
<script src="{{asset('storage/assets/vendors/flotchart/js/jquery.flot.js')}}" ></script>
<script src="{{asset('storage/assets/vendors/flotchart/js/jquery.flot.resize.js')}}"></script>
<!--end of plugin scripts-->

<script src="{{asset('storage/assets/js/pages/index.js')}}"></script>
</body>
</html>
