<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="UTF-8">
  <title>
    @section('title')
    Ciamsa | Ciamsa
    @show
  </title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="{{asset('storage/assets/img/logo1.ico')}}"/>
  <!-- global styles-->
  <!--<link type="text/css" rel="stylesheet" href="#" id="skin_change"/>-->
  <!-- end of global styles-->
  <link rel="stylesheet" href="{{asset('storage/assets/css/components.css')}}" />
  <link rel="stylesheet" href="{{asset('storage/assets/css/custom.css')}}" />
  <link rel="stylesheet" href="{{asset('storage/assets/css/styles.css')}}" />
  <!-- end of global styles-->
  <link rel="stylesheet" href="{{asset('storage/assets/vendors/chartist/css/chartist.min.css')}}" />
  <link rel="stylesheet" href="{{asset('storage/assets/vendors/circliful/css/jquery.circliful.css')}}">
  <link rel="stylesheet" href="{{asset('storage/assets/css/pages/index.css')}}">

  <link rel="stylesheet" href="{{asset('storage/assets/css/color_picker_vue.css')}}">

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154479968-1"></script>
  <script>
    
    var user_id = '{{ Auth::user()->name }} - {{ Auth::user()->identificacion }}';
    user_id = user_id.toUpperCase();

    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-154479968-1');
    gtag('config', 'GA_MEASUREMENT_ID', {
      'user_id': user_id
    });
    
  </script>

</head>
<body>
  <div id="app">
    <!--<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
      <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
          {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">

          </ul>
          <ul class="navbar-nav ml-auto">
            @guest
            <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            <li class="nav-item">
              @if (Route::has('register'))
              <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
              @endif
            </li>
            @else
            <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
              </a>

              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </div>
          </li>
          @endguest
        </ul>
      </div>
    </div>
  </nav>-->
  @include('layouts.header.menubar')

  <div class="wrapper">
    <div id="left">
      <div class="menu_scroll">
        <!-- <div class="media user-media">
          <div class="user-media-toggleHover">
            <span class="fa fa-user"></span>
          </div>
          <div class="user-wrapper">
            <a class="user-link" href="/home">
              <img class="media-object img-thumbnail user-img rounded-circle admin_img3" alt="User Picture" src="{{asset(Auth::user()->avatar)}}">
              <p class="text-white user-info">Bienvenido(a) {{ Auth::user()->name }}</p>
            </a>
          </div>
        </div> -->
        <!-- #menu -->
        <ul id="menu">

          @can('Servicios_logisticos')
          <li class="dropdown_menu active" data-color="blue">
            <a href="javascript:;">
              <i class="fa fa-anchor"></i>
              <span class="link-title menu_hide">&nbsp; Servicios logístico</span>
              <span class="fa arrow menu_hide"></span>
            </a>
            <ul>
              @can('Documentos_de_carga')
              <li>
                <a href="/cargarDocumentos" data-href="/cargarDocumentos">
                  <i class="fa fa-angle-right"></i> &nbsp; Documentos de carga
                </a>
              </li>
              @endcan
              @can('Rastreo_de_carga')
              <li>
                <a href="/rastreo-de-carga/TRANSITO/{{ Auth::user()->identificacion }}/1/50" data-href="/rastreo-de-carga/TRANSITO">
                  <i class="fa fa-angle-right"></i>
                  <span class="link-title">&nbsp;Rastreo de carga</span>
                </a>
              </li>
              @endcan
              @can('Existencia_de_mercancia')
              <li>
                <a href="/existencia-de-mercancia/50" data-href="/existencia-de-mercancia/50">
                  <i class="fa fa-angle-right"></i> &nbsp; Existencia de mercancia
                </a>
              </li>
              @endcan
              @can('Despacho_de_mercancia')
              <li>
                <a href="/despacho-de-mercancia/50" data-href="/despacho-de-mercancia/50">
                  <i class="fa fa-angle-right"></i> &nbsp; Despacho de mercancia
                </a>
              </li>
              @endcan
              @can('Fecha_limite_devolucion')
              <li>
                <a href="/devolucion-de-contenedor/50" data-href="/devolucion-de-contenedor/50">
                  <i class="fa fa-angle-right"></i> &nbsp; Fecha limite de devolución de contenedores
                </a>
              </li>
              @endcan
              @can('Actualizar_estados')  
              <li>
                <a href="/Servicios_logisticos/estados" data-href="/Servicios_logisticos/estados">
                  <i class="fa fa-angle-right"></i>
                  <span class="link-title">&nbsp; Actualización de estados</span>
                </a>
              </li>
              @endcan
              @can('Estado_carga')   
              <li>
                <a href="/Servicios_logisticos/estadoCarga" data-href="/Servicios_logisticos/estadoCarga">
                  <i class="fa fa-angle-right"></i>
                  <span class="link-title">&nbsp; Estado de la carga</span>
                </a>
              </li>
              @endcan
              @can('Anuncio_carga')  
              <li>
                <a href="/Servicios_logisticos/anuncioCarga" data-href="/Servicios_logisticos/anuncioCarga">
                  <i class="fa fa-angle-right"></i>
                  <span class="link-title">&nbsp; Anuncio de carga</span>
                </a>
              </li>
              @endcan 
              @can('Reserva_turno_CIAMSA2')
              <li>
                <a href="/reservar-turno/{{ Auth::user()->identificacion }}/1/50" data-href="/reservar-turno/TRANSITO">
                  <i class="fa fa-angle-right"></i>
                  <span class="link-title">&nbsp;Reservar turno de vehículo</span>
                </a>
              </li>
              @endcan                       
            </ul>
          </li>
          @endcan
          @can('Agricola')
          <li class="dropdown_menu" data-color="green">
            <a href="javascript:;">
              <i class="fa fa-th-large"></i>
              <span class="link-title menu_hide">&nbsp; Agricola</span>
              <span class="fa arrow menu_hide"></span>
            </a>
            <ul>
              @can('Cupo_credito')
              <li>
                <a href="/cupo-credito/all/{{ Auth::user()->identificacion }}/1/50" data-href="cupo-credito/all/{{ Auth::user()->identificacion }}/1/50">
                  <i class="fa fa-angle-right"></i> &nbsp; Consultar cupo de Crédito
                </a>
              </li>
              @endcan
              @can('Rastreo_de_pedido') 
              <li>
                <a href="/agricola/rastreo-de-pedido" data-href="/agricola/rastreo-de-pedido">
                  <i class="fa fa-angle-right"></i> &nbsp; Rastreo del pedido
                </a>
              </li>
              @endcan
              @can('Facturas')
              <li>
                <a href="/facturas/{{ Auth::user()->identificacion }}/1/50" data-href="facturas/{{ Auth::user()->identificacion }}/1/50">
                  <i class="fa fa-angle-right"></i>
                  <span class="link-title">&nbsp; Facturas</span>
                </a>
              </li>
              @endcan
              @can('Solicitar_pedidos')
              <li>
                <a href="/agricola/solicitar-pedido" data-href="/agricola/solicitar-pedido">
                  <i class="fa fa-angle-right"></i>
                  <span class="link-title">&nbsp; Solicitar pedidos</span>
                </a>
              </li>
              @endcan
              @can('Solicitar_pedidos')
              <li>
                <a href="/agricola/estados" data-href="/agricola/estados">
                  <i class="fa fa-angle-right"></i>
                  <span class="link-title">&nbsp; Estados</span>
                </a>
              </li>
              @endcan
              @can('Reserva_turno_agricola') 
              <li>
                <a href="/agricola/reservar-turno/{{ Auth::user()->identificacion }}/1/50" data-href="/agricola/reservar-turno">
                  <i class="fa fa-angle-right"></i> &nbsp; Reservar turno de vehículo
                </a>
              </li>
              @endcan
            </ul>
          </li>
          @endcan
          @can('Comercializadora')
          <li class="dropdown_menu" data-color="blue">
            <a href="javascript:;">
              <i class="fa fa-pencil"></i>
              <span class="link-title menu_hide">&nbsp; Comercializadora</span>
              <span class="fa arrow menu_hide"></span>
            </a>
            <ul>
              @can('Nominaciones')
              <li>
                <a href="/comercializadora/nominaciones/50" data-href="/comercializadora/nominaciones/50">
                  <i class="fa fa-angle-right"></i> &nbsp; Nominaciones
                </a>
              </li>
              @endcan
              @can('Proceso_logistico')
              <li>
                <a href="/comercializadora/proceso-logistico/50" data-href="/comercializadora/proceso-logistico/50">
                  <i class="fa fa-angle-right"></i> &nbsp; Proceso logistico
                </a>
              </li>
              @endcan
              @can('Estado_de_contenedores')
              <li>
                <a href="/comercializadora/estados-de-contenedores/contenedores-quedados/50" data-href="/comercializadora/estados-de-contenedores/contenedores-quedados/50">
                  <i class="fa fa-angle-right"></i> &nbsp; Estado de contenedores
                </a>
              </li>
              @endcan
            </ul>
          </li>
          @endcan
          @can('Agencia_de_aduanas')
          <li class="dropdown_menu" data-color="blue">
            <a href="javascript:;">
              <i class="fa fa-pencil"></i>
              <span class="link-title menu_hide">&nbsp; Agencia de aduanas</span>
              <span class="fa arrow menu_hide"></span>
            </a>
            <ul>
              <li>
                <a href="form_elements.html" data-href="form_elements.html">
                  <i class="fa fa-angle-right"></i> &nbsp; Form Elements
                </a>
              </li>
              <li>
                <a href="form_layouts.html" data-href="form_layouts.html">
                  <i class="fa fa-angle-right"></i> &nbsp; Form Layouts
                </a>
              </li>
              <li>
                <a href="form_validations.html" data-href="form_validations.html">
                  <i class="fa fa-angle-right"></i> &nbsp; Form Validations
                </a>
              </li>
              <li>
                <a href="form_editors.html" data-href="form_editors.html">
                  <i class="fa fa-angle-right"></i> &nbsp; Form Editors
                </a>
              </li>
              <li>
                <a href="radio_checkbox.html" data-href="radio_checkbox.html">
                  <i class="fa fa-angle-right"></i> &nbsp; Radio and Checkbox
                </a>
              </li>
              <li>
                <a href="form_wizards.html" data-href="form_wizards.html">
                  <i class="fa fa-angle-right"></i> &nbsp; Wizards
                </a>
              </li>
              <li>
                <a href="datetime_picker.html" data-href="datetime_picker.html">
                  <i class="fa fa-angle-right"></i> &nbsp; Date Time Picker
                </a>
              </li>
              <li>
                <a href="ratings.html" data-href="ratings.html">
                  <i class="fa fa-angle-right"></i> &nbsp; Ratings
                </a>
              </li>
            </ul>
          </li>
          @endcan  
        </ul>
        <!-- /#menu -->
      </div>
    </div>
    <!-- /#left -->
    <div id="content" class="bg-container">
      <header class="head">
        <div class="main-bar m-t-15 m-b-15">             
          @yield('titlepage','Dashboard')          
        </div>
      </header>
      <div class="outer">
        <div class="inner bg-container">
          @yield('content')
        </div>
      </div>
      
    </div>
  </div>

</div>

<footer style="background-color: #006AAF; color: white; padding: 7px;">

  @include('layouts.footer.footer')

</footer>

<script src="{{asset('storage/assets/js/sweetalert.min.js')}}"></script>

<script src="{{asset('storage/assets/js/components.js')}}"></script>

<script src="{{asset('storage/assets/js/custom.js?v=1.0')}}"></script>
<script src="{{asset('storage/assets/js/scripts.js')}}"></script>
<script src="https://kit.fontawesome.com/0a1dba32e1.js"></script>
<!--end of global scripts-->
<!--  plugin scripts -->
<script src="{{asset('storage/assets/vendors/countUp.js/js/countUp.min.js')}}"></script>
<script src="{{asset('storage/assets/vendors/flip/js/jquery.flip.min.js')}}"></script>
<script src="{{asset('storage/assets/js/pluginjs/jquery.sparkline.js')}}"></script>
<script src="{{asset('storage/assets/vendors/chartist/js/chartist.min.js')}}"></script>
<script src="{{asset('storage/assets/js/pluginjs/chartist-tooltip.js')}}"></script>
<script src="{{asset('storage/assets/vendors/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('storage/assets/vendors/circliful/js/jquery.circliful.min.js')}}"></script>
<script src="{{asset('storage/assets/vendors/flotchart/js/jquery.flot.js')}}" ></script>
<script src="{{asset('storage/assets/vendors/flotchart/js/jquery.flot.resize.js')}}"></script>
<!--end of plugin scripts-->

<script src="{{asset('storage/assets/js/pages/index.js')}}"></script>
</body>
</html>