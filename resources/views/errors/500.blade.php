@extends('errors::illustrated-layout')

@section('code', '500')
@section('title', __('Error'))

@section('image')
<div style="background-image: url({{ asset('/svg/500.jpg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
</div>
@endsection

@if(isset($msj))
	@section('message', __($msj))
@else
	@section('message', __('Lo sentimos, en este momento el sistema no está disponible, por favor intente más tarde o contacte a su administrador de sistema.'))
@endif
