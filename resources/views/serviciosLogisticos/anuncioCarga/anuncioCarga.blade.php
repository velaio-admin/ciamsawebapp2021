@extends('layouts.app')

@section('titlepage')
<div class="row justify-content-between header-rastreoList">
  <div class="col-md-4">
    <h4>Anuncio de carga</h4>
  </div>
</div>
@endsection
<!-- Page Content -->
@section('content')
<div id="message"></div>

@if(Auth::user()->can('Anuncio_carga'))

  @if($msjBool)

    <div class="alert alert-danger" role="alert">
      {{$msj}}
    </div>

  @else

    <style>
      
      /* Absolute Center Spinner */
      .loading {
        position: fixed;
        z-index: 999;
        height: 2em;
        width: 2em;
        overflow: show;
        margin: auto;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
      }

      /* Transparent Overlay */
      .loading:before {
        content: '';
        display: block;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
          background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

        background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
      }

      /* :not(:required) hides these rules from IE9 and below */
      .loading:not(:required) {
        /* hide "loading..." text */
        font: 0/0 a;
        color: transparent;
        text-shadow: none;
        background-color: transparent;
        border: 0;
      }

      .loading:not(:required):after {
        content: '';
        display: block;
        font-size: 10px;
        width: 1em;
        height: 1em;
        margin-top: -0.5em;
        -webkit-animation: spinner 1500ms infinite linear;
        -moz-animation: spinner 1500ms infinite linear;
        -ms-animation: spinner 1500ms infinite linear;
        -o-animation: spinner 1500ms infinite linear;
        animation: spinner 1500ms infinite linear;
        border-radius: 0.5em;
        -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
      box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
      }

  /* Animation */

      @-webkit-keyframes spinner {
        0% {
          -webkit-transform: rotate(0deg);
          -moz-transform: rotate(0deg);
          -ms-transform: rotate(0deg);
          -o-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          -moz-transform: rotate(360deg);
          -ms-transform: rotate(360deg);
          -o-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      @-moz-keyframes spinner {
        0% {
          -webkit-transform: rotate(0deg);
          -moz-transform: rotate(0deg);
          -ms-transform: rotate(0deg);
          -o-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          -moz-transform: rotate(360deg);
          -ms-transform: rotate(360deg);
          -o-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      @-o-keyframes spinner {
        0% {
          -webkit-transform: rotate(0deg);
          -moz-transform: rotate(0deg);
          -ms-transform: rotate(0deg);
          -o-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          -moz-transform: rotate(360deg);
          -ms-transform: rotate(360deg);
          -o-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      @keyframes spinner {
        0% {
          -webkit-transform: rotate(0deg);
          -moz-transform: rotate(0deg);
          -ms-transform: rotate(0deg);
          -o-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          -moz-transform: rotate(360deg);
          -ms-transform: rotate(360deg);
          -o-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }

      .theme--light.v-btn:not(.v-btn--flat):not(.v-btn--text):not(.v-btn--outlined) {
        background-color: #006BB1 !important;
        color: white !important;
      }
      .v-messages.theme--light.error--text {
          color: red !important;
      }

      @media (max-width: 680px) {
        .wrapper{
        padding: initial !important;
        }
      }

    </style>

    <!--Moment Librery-->
    <script src="{{asset('storage/assets/js/moment.js')}}"></script>

    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@3.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('storage/assets/css/vuetify.min.css')}}">

    <div id="carga_app" class="page-content-wrapper" style="margin: 0 auto; width: 75%;">

      <div :class="var_load"></div>

      <input type="hidden" id="id_user" value="{{ Auth::user()->identificacion }}">

      <div class="container-fluid">

        <div> 

          <v-form v-model="valid">
            <v-container>

              <v-row>
                <v-col
                  cols="12"
                  md="6"
                >
                  <v-text-field
                    v-model="num_bl"
                    :rules="numBlRules"
                    :counter="5"
                    label="Número. BL"
                    required
                  ></v-text-field>
                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >     
                  <label style="color: rgba(0,0,0,.54);">Seleccione motonave</label>             
                  <v-select 
                    v-model="motonave_form" 
                    placeholder="Motonave" 
                    :options="motonave_list" 
                    label="value">
                  </v-select>
                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >
                  <v-text-field
                    v-model="nit_cliente"
                    :rules="nitRules"
                    :counter="8"
                    label="Nit. Cliente"
                    required
                    type="number"
                  ></v-text-field>
                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >
                  <v-text-field
                    v-model="viaje"
                    :rules="viajeRules"
                    label="Viaje"
                    required
                  ></v-text-field>
                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >
                  <!--<v-text-field
                    v-model="lastname"
                    :rules="nameRules"
                    :counter="10"
                    label="Nombre Cliente"
                    required
                  ></v-text-field>-->
                  <label style="color: rgba(0,0,0,.54);">Seleccione cliente</label>
                  <v-select 
                    v-model="cliente_form" 
                    placeholder="Nombre Cliente" 
                    :options="cliente_list" 
                    label="value">
                  </v-select>

                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >
                  <v-text-field
                    v-model="et_mn"
                    :rules="et_mnRules"
                    label="ETA MN"
                    required
                  ></v-text-field>
                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >
                  <v-text-field
                    v-model="importador"
                    :rules="importadorRules"
                    label="Importador"
                    required
                  ></v-text-field>
                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >
                  <label style="color: rgba(0,0,0,.54);">Seleccione fecha</label>
                  <vuejs-datepicker 
                    v-model="fec_des_mn" placeholder="Fecha Descargue MN">
                  </vuejs-datepicker>

                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >
                  <label style="color: rgba(0,0,0,.54);">Seleccione carga</label>
                  <v-select 
                    v-model="tip_car_form" 
                    placeholder="Tipo de carga" 
                    :options="tip_car_list" 
                    label="value">
                  </v-select>

                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >
                  <label style="color: rgba(0,0,0,.54);">Seleccione muelle</label>
                  <v-select 
                    v-model="muelle_form" 
                    placeholder="Muelle de descargue" 
                    :options="muelle_list" 
                    label="value">
                  </v-select>

                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >
                  <v-text-field
                    v-model="contenedor"
                    :rules="contenedorRules"
                    label="Contenedor"
                    required
                  ></v-text-field>
                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >
                  <label style="color: rgba(0,0,0,.54);">Seleccione producto</label>
                  <v-select 
                    v-model="pro_muis_form" 
                    placeholder="Producto Muisca" 
                    :options="pro_muis_list" 
                    label="value">
                  </v-select>

                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >
                  <label style="color: rgba(0,0,0,.54);">Seleccione linea naviera</label>
                  <v-select 
                    v-model="lin_nav_form" 
                    placeholder="Linea Naviera" 
                    :options="lin_nav_list" 
                    label="value">
                  </v-select>

                </v-col>
                <v-col
                  cols="12"
                  md="6"
                > 
                  <label style="color: rgba(0,0,0,.54);">Seleccione fecha</label>
                  <vuejs-datepicker 
                    v-model="fec_ing_muis" placeholder="Fecha ingreso Muisca">
                  </vuejs-datepicker>
                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >
                  <v-text-field
                    v-model="num_importacion"
                    :rules="num_importacionRules"
                    label="Número importación"
                    required
                  ></v-text-field>
                </v-col>
                <v-col
                  cols="12"
                  md="6"
                >
                  <v-text-field
                    v-model="cant_contenedores"
                    :rules="cant_contenedoresRules"
                    label="Cantidad Contenedores"
                    required
                  ></v-text-field>
                </v-col>
                <v-col
                  cols="12"
                  md="12"
                >
                  <!--<input type="file" id="file_doc" name="file">-->
                  <v-file-input accept=".pdf" multiple id="file_doc" label="Adjuntar Documento" v-model="img_data"></v-file-input>
                </v-col>
              </v-row>

              <v-row style="text-align: center;">

                <v-col
                  cols="12"
                  md="12"
                >
                  <v-btn
                    :disabled="!valid"
                    class="mr-4"
                    v-on:click="dataConsult($event)" 
                  >
                    Guardar Información
                  </v-btn>
                </v-col>
                
              </v-row>              

            </v-container>
          </v-form>

        </div>

      </div>

      <br/>

    </div>

    <script src="{{asset('storage/assets/js/vue.js')}}"></script>
    <script src="{{asset('storage/assets/js/axios.js')}}"></script>

    <!-- Date Picker -->
    <script src="https://unpkg.com/vuejs-datepicker"></script>

    <!-- Vuefity Vuejs's Styles  -->
    <script src="{{asset('storage/assets/js/vuetify.js')}}"></script>

    <!-- Vue Select -->
    <script src="https://unpkg.com/vue-select@latest"></script>
    <link rel="stylesheet" href="https://unpkg.com/vue-select@latest/dist/vue-select.css">

    <script src="{{asset('storage/assets/js/anun_carga_vue.js')}}"></script>

  @endif

@else

  <div class="row">
    <div class="col-md-12">
      <div class="description">
        <h4>No tiene permisos para acceder a esta sección</h4>
      </div>
    </div>
  </div>

@endif

@endsection