@extends('layouts.app')

@section('titlepage')
Documentos de carga
@endsection
@section('content')

@if(Auth::user()->can('Documentos_de_carga'))

@if (isset($errors))
<div class="alert alert-danger">
  <ul>
    <li>{{ $errors }}</li>
  </ul>
</div>
@endif

@if (isset($success))
<div class="alert alert-success">
  <ul>
    <li>{{ $success }}</li>
  </ul>
</div>
@endif

<style>
  @media (max-width: 680px) {
    .wrapper{
      padding: initial !important;
    }
  }
</style>

<!-- Page Content -->
<div id="cargarDocumentos" class="page-content-wrapper">
  <div class="container-fluid">


    <div class="row">
      <div class="col-md-12">
        <form id="envio_documentos" name="envio_documentos" action="/subirDocumentos" method="post" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-md-12">
              <div class="description">
                <h4>Documentación</h4>
                <div class="text">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam nobis facere minima porro eveniet. Quod voluptate placeat architecto dolore placeat <strong>Declaración, certificado de INVIMA, certificado ICA</strong>
                  </p>
                </div>
              </div>
              <div class="row form-fields">
                <div class="col-md-4">
                  <label for="select_documentos">Tipo de documento</label>
                  <select class="form-control" id="select_documentos" name="select_documentos" required="">
                    @foreach ($tipo_documentos as $tipo_documento)
                    <option value="{{$tipo_documento->ID_TIPO_DOCUMENTO}}">{{ str_replace('_',' ',ucfirst(strtolower($tipo_documento->NOMBRE)))}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-4">
                  <label for="id_bl">ID del BL</label>
                  <input class="form-control" type="text" placeholder="Ingrese el código" name="id_bl" id="id_bl" required="">
                </div>
                <div class="col-md-4">
                  <label for="peso" id="label_peso" style="display: none;">Peso (Kg)</label><br>
                  <input class="form-control" type="number" placeholder="Ingrese el peso (Kg)" name="peso" id="peso" style="display: none;" min="0">
                </div>
                <div class="col-md-4">
                  <label for="f_limit" id="label_f_limit" style="display: none;">Fecha Limite (dd/mm/aaaa)</label><br>
                  <input class="form-control" type="date" placeholder="Ingrese la fecha limite (dd/mm/aaaa)" name="f_limit" id="f_limit" style="display: none;" min="{{date('Y-m-d')}}" max="2100/01/01">
                </div>
              </div>
            </div>
          </div>
          <div class="description">
            <h4>Carga de documentos</h4>
          </div>
          <div class="row cargadocumento">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-3 text-left">
                  <span>Tipo de documento</span>
                  <label id="info_tipo_documento">{{ str_replace('_',' ',ucfirst(strtolower($tipo_documentos[0]->NOMBRE)))}}</label>
                </div>
                <div class="col-md-9 text-right">
                  <div class="btn-group btn-group-md form-fields-load" role="group">
                    <input type="file" name="documento" id="documento" accept="image/*,.pdf" style="display: none;" onchange="labelDoc();" required="">
                    <label id="nombre_documento">No se ha cargado ningún archivo aún</label>
                    <button class="btn btn-secondary upload-file" type="button" onclick="selectFile();">
                      + Agregar Documento
                    </button>
                    <input type="hidden" name="maxSize" id="maxSize" value="false">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 btn-right">
              <button class="btn btn-secondary" type="button" onclick="submitForm();">
                Enviar documetación
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- /#page-content-wrapper -->

@else

<div class="row">
  <div class="col-md-12">
    <div class="description">
      <h4>No tiene permisos para acceder a esta sección</h4>
    </div>
  </div>
</div>

@endif
@endsection


