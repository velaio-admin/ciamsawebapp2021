@extends('layouts.app')

@section('titlepage')
<div class="row justify-content-between header-rastreoList">
	<div class="col-md-4">
		<h4>Actualización del estado de la carga</h4>
	</div>
</div>
@endsection
<!-- Page Content -->
@section('content')
<div id="message"></div>

@if(Auth::user()->can('Actualizar_estados'))

	@if($msjBool)

		<div class="alert alert-danger" role="alert">
			{{$msj}}
		</div>

	@else

		<style>
			.loader {
			  border: 16px solid #f3f3f3;
			  border-radius: 50%;
			  border-top: 16px solid #3498db;
			  display: inline-block;
			  width: 120px;
			  height: 120px;
			  -webkit-animation: spin 2s linear infinite; /* Safari */
			  animation: spin 2s linear infinite;
			}
			.cont-data-loading{
				text-align: center;
    			width: 100%;
			}
			.awesomplete{
				width: 100%;
			}
			.awesomplete > ul > li{
				padding: initial !important;
			}
			.radio {
			    display: inline-block;
			    padding: 5px;
			}
			.vs__search{
				padding: 4px 0 !important;
			}
			.div-containers{
    			background-color: lightgrey;
    			margin-bottom: 13px;
				padding: 10px;
			}
			/* Safari */
			@-webkit-keyframes spin {
			  0% { -webkit-transform: rotate(0deg); }
			  100% { -webkit-transform: rotate(360deg); }
			}

			@keyframes spin {
			  0% { transform: rotate(0deg); }
			  100% { transform: rotate(360deg); }
			}

			@media (max-width: 680px) {
			  .wrapper{
				padding: initial !important;
			  }
			}

		</style>

		<!--<link rel="stylesheet" href="https://cdn.rawgit.com/LeaVerou/awesomplete/gh-pages/awesomplete.css">
		<script src="https://cdn.rawgit.com/LeaVerou/awesomplete/gh-pages/awesomplete.min.js"></script>-->

		<div id="estado_app" class="page-content-wrapper" style="margin: 0 auto; width: 75%;">

			<div class="container-fluid">

				<div class="row">	

					<div class="col-md-12">

						<div class="form-group">
						    <label for="id_client">
						    	Clientes con BLs en tránsito o en proceso
						    </label>
							<v-select v-model="id_client" 
			  						  :options="clients" 
			  						  label="name">
			  				</v-select>
						</div>
					</div> 	

					<div class="col-md-12" style="text-align: center;">
						<button class="btn btn-secondary" type="button" v-on:click="search_bl"  style="background-color: #006BB1 !important;">
						    Buscar <i class="fa fa-search"></i>
						</button>
					</div>

					<div v-show="result_request" style="width: 100%;">
						<div class="col-md-12">
							<div class="form-group">
							    <label for="bl_item">Seleccione BL</label>
								<!--<v-select 
										  v-model="bl_item" 
				  						  :options="array_globals" 
				  						  label="array_globals">
				  				</v-select>-->
				  				<v-select 
										  v-model="bl_item" 
				  						  :options="states_det" 
				  						  label="BL">
				  				</v-select>
							</div>
						</div>

						<div v-show="show_state_bl">

							<div class="col-md-12">
								<div class="form-group">
								    <label for="id_state">Estado Del BL</label>
									<v-select v-model="id_state" 
					  						  :options="states" 
					  						  label="NOMBRE_ESTADO">
					  				</v-select>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
								    <label for="observacion">Observación</label>
									<textarea v-model="observacion" class="form-control" rows="3"></textarea>
								</div>
							</div>

							<div style="text-align: center; margin-top: 10px;">

								<button type="button" class="btn btn-default" style="background-color: #006BB1; color: white;" v-on:click="update_bl_est">
								  Actualizar BL<i class="fa fa-check"></i>
								</button>
								
							</div>

						</div>

					</div>

				</div>

			</div>

			<br/>

			<div v-show="loader" class="cont-data-loading">
				<div class="loader"></div>
			</div>

			<div v-show="result_request">

				<hr>

				<div class="container-fluid">
					
					<div class="row">

						<div class="col-md-12" style="text-align: center;">

							<div class="radio">
						      <label>
						      	<input type="radio" v-on:change="changeOption" name="state_info" value="1" v-model="consult" checked>
						      	<strong>Actualizar Individual</strong>
						      </label>
						    </div>
						    <div class="radio">
						      <label>
						      	<input type="radio" v-on:change="changeOption" name="state_info" value="2" v-model="consult">
						      	<strong>Global</strong>
						      </label>
						    </div>

						</div>

					</div>

					<div class="row">

						<div class="col-md-12">

						    <div v-if="individual">

						    	<div class="form-group">
								    <label for="cont_form">Contenedor</label>
						    		<multiselect v-model="cont_form" :options="CONTENEDOR_ARR" :multiple="true" :close-on-select="false" :clear-on-select="false" :preserve-search="true" placeholder="Seleccione Contenedor" label="CONTENEDOR" track-by="CONTENEDOR" :preselect-first="true">
  									</multiselect>
  								</div> 

						    	<!--<div class="form-group">
								    <label for="cont_form">Contenedor</label>
									<v-select v-model="cont_form" 
					  						  :options="CONTENEDOR_ARR" 
					  						  label="CONTENEDOR">
					  				</v-select>
								</div>-->

								<div v-for="(list, key) in list_edit_cont" class="div-containers">
									<div class="form-group">
									    <label for="id_state_cont">
									    	Estado Contenedor - <strong>
									    						@{{ list['CONTENEDOR'] }} 
									    					  </strong>
									    </label>									    
									    <!--<v-select v-model="id_state_cont" 
						  						  :options="states_cont" 
						  						  label="NOMBRE_ESTADO">
						  				</v-select>-->

						  				<select :id="key + 'val_cont'" class="form-control">
						  					<option v-for="cont in states_cont"
						  					:selected="cont['ID_ESTADO'] == list['ID_ESTADO'] ? 'selected' : ''" 
						  					:value="cont['ID_ESTADO']">

						  						@{{ cont['NOMBRE_ESTADO'] }}

						  					</option>
						  				</select>

									</div>

									<div class="form-group">
									    <label for="observation">Observaciones</label>
									    <textarea :id="key + 'val_obser'" class="form-control" rows="3"></textarea>
									</div>

									<div style="text-align: center;">
										<button type="button" class="btn btn-default" style="background-color: #006BB1; color: white;" v-on:click="update_bl_cont_ind(key, list['CONTENEDOR'])">
										  <i class="fa fa-check"></i>
										</button>	
									</div>											
								</div>
						    	
						    </div>

							
						</div>
						<div class="col-md-12">

						    <div v-if="global">

						    	<div class="form-group">
								    <label for="id_state_cont">Estado Contenedor</label>
								    <v-select v-model="id_state_cont" 
					  						  :options="states_cont" 
					  						  label="NOMBRE_ESTADO">
					  				</v-select>
								</div>

								<div class="form-group">
								    <label for="observation">Observaciones</label>
								    <textarea class="form-control" v-model="observacion_global" rows="3" id="observation"></textarea>
								</div>

								<div style="text-align: center; margin-top: 10px;">

									<button type="button" class="btn btn-default" style="background-color: #006BB1; color: white;" v-on:click="update_bl_cont">
									  Actualizar Globalmente <i class="fa fa-check"></i>
									</button>
									
								</div>
						    	
						    </div>
							
						</div>
						
					</div>

				</div>
				
			</div>

		</div>

		<script src="{{asset('storage/assets/js/vue.js')}}"></script>
		<script src="{{asset('storage/assets/js/axios.js')}}"></script>

		<script src="https://unpkg.com/vue-select@3.0.0"></script>
		<link rel="stylesheet" href="https://unpkg.com/vue-select@3.0.0/dist/vue-select.css">

		<script src="https://unpkg.com/vue-multiselect@2.0.6"></script>
		<link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.0.6/dist/vue-multiselect.min.css">

		<script src="{{asset('storage/assets/js/estados_vue.js')}}"></script>

	@endif

@else

	<div class="row">
		<div class="col-md-12">
			<div class="description">
				<h4>No tiene permisos para acceder a esta sección</h4>
			</div>
		</div>
	</div>

@endif

@endsection