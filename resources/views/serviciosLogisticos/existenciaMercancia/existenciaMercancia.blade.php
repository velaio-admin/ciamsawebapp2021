@extends('layouts.app')

<style type="text/css">

  .table thead th {
    text-align: center !important;
    vertical-align: top !important;
    border-bottom: 2px solid #e9ecef !important;
  }
  .tableFixHead { 
    /*height: 800px;
    overflow-y: auto; */
  }
  .tableFixHead thead th { 
    position: sticky; top: 0; 
  }

  /* Just common table stuff. Really. */
  table  { 
    border-collapse: collapse; width: 100%; 
  }
  th, td { 
    padding: 8px 16px; 
  }

  /* Paginator Style FKM */
  .paginated .pagination li.next a {
    background-color: #006bb1;
    color: #fff;
    border: 1px solid;
  }

  .table-responsive .paginated .pagination {
    border: 1px solid #e6e6e6;
  }

  .paginated{
    align-items: center;
    display: flex;
    justify-content: space-between;
    margin-top: 30px;
  }

  .paginated .pagination li a, .table-responsive .paginated .pagination li span{
    display: block;
    font-size: 14px;
    line-height: 1;
    padding: 14px 14px;
  }

  .disabled {
    pointer-events: none;
    cursor: default;
  }
  /* End Paginator Style FKM */

  @media (max-width: 780px) {
    .wrapper{
      padding: initial !important;
    }
    .tableFixHead { 
      /*height: 400px; */
    }
  }

</style>

@section('titlepage')
<div class="row justify-content-between header-rastreoList">

  <div class="col-md-4">
    <h4>Existencia de mercancia</h4>{!! $label_fecha !!}
  </div>
  <div class="col-md-6 open-search">
    <span>
      Busqueda avanzada
    </span>
  </div>
  <div onclick="tableToExcel('tblData', 'name', 'mercancia_ciamsa.xls');" style="cursor: pointer;">
    <span style="color: rgb(0, 107, 177); margin-right: 5px;">
      Exportar excel
    </span> 
    <img src="http://digital.ciamsa.com/img/excel-icon.png" alt="icon-excel" height="30px" title="Exportar datos a Excel">
  </div>

</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Existencia_de_mercancia'))

  <?php  

    if (isset($_GET['page'])) {
      $curPage = $_GET['page'];
    }else{
      $curPage = 1;
    }

    $nextPage = $curPage + 1;
    $previusPage = $curPage > 1 ? $curPage - 1 : 1;

    $disablePrev = $curPage == 1 ? 'disabled' : '';

    if (is_array($table1)) {
      $results = sizeof($table1['data']);
    }else{
      $results = 0;
    }

    $disableNext = $results == 0 ? 'disabled' : '';

  ?>

  <div id="existenciaMercancia" class="page-content-wrapper">
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-12" style="overflow-x: auto">
          @if(isset($table1['headers']) && isset($table1['data']))
          <div class="tableFixHead">
          <table id="tblData" class="table table-default table-xs">
            <thead>
              <tr>
                @foreach($table1['headers'] as $head)
                <th style="background:#6eb135;">
                  {{$head->label}}
                </th> 
                @endforeach
              </tr>
            </thead>
            <tbody>
              @foreach($table1['data'] as $data)
              <tr>
                @foreach($data as $key => $dat)
                <!--<td class="text-center" @if($key == 12) style="font-size:0.7rem;" @endif>-->
                <td class="text-center">
                  {{$dat}}
                </td>
                @endforeach
              </tr>
              @endforeach
            </tbody>
          </table>
          </div>

          @else

            {!! $table1 !!}

          @endif

          <?php if (isset($cant_reg) ) { ?>
            
            <div class="paginated">
               <div class="result">
                  <!-- Mostrando 1  a 10  de 10 resultados -->
               </div>
               <ul class="pagination fk123">
                  <li class="next <?=$disablePrev; ?>">
                      <a id="link_url_prev" href="/existencia-de-mercancia/<?=$cant_reg; ?>?page=<?=$previusPage; ?>" rel="prev" class="<?=$disablePrev; ?>">
                        Anterior
                      </a>
                  </li>
                  <li class="next <?=$disableNext; ?>">
                      <a id="link_url_next" href="/existencia-de-mercancia/<?=$cant_reg; ?>?page=<?=$nextPage; ?>" class="<?=$disableNext; ?>">
                        Siguiente
                      </a>
                  </li>
               </ul>
            </div>

          <?php } ?>

        </div>
      </div>
   </div>
 </div>
 @include('serviciosLogisticos.existenciaMercancia.buscador')

 <script type="text/javascript">

    function tableToExcel(table, name, filename){
      let uri = 'data:application/vnd.ms-excel;base64,', 
            
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" ' +
                                        'xmlns="http://www.w3.org/TR/REC-html40"><head>' +
                                        '<!--[if gte mso 9]>' +

                                        '<xsl:template name="styles">' +
                                        '<style>' +

                                        'td {' +
                                        'padding-top:1px;' +
                                        'padding-right:1px;' +
                                        'padding-left:1px;' +
                                        'mso-ignore:padding;' +
                                        'color:windowtext;' +
                                        'text-align:general;' +
                                        'vertical-align:bottom;' +
                                        'text-align:left;' +
                                        'white-space:wrap;' +
                                        '}' +

                                        '.color-th {' +
                                        'mso-style-parent:style0;' +
                                        'background:#C9C7C6;;' +
                                        '}' +

                                        '.header {' +
                                        'mso-style-parent:style0;' +
                                        'font-weight:700;' +
                                        '}' +

                                        '.number {mso-number-format:"0";}' +
                                        '</style>' +

                                        '<xml>' +
                                        '<x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>' +
                                        '<x:Name>{worksheet}</x:Name>' +
                                        '<x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions>' +
                                        '</x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook>' +
                                        '</xml>' +
                                        '<![endif]-->' +
                                        '</head>' +
                                        '<body>' +
                                        '<table>{table}</table>' +
                                        '</body>' +
                                        '</html>',

            
            base64 = function(s = "äöüÄÖÜçéèñ") { 
              return window.btoa(unescape(encodeURIComponent(s))) 
            }, 

            format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; })}
            
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}

            var link = document.createElement('a');
            link.download = filename;
            link.href = uri + base64(format(template, ctx));
            link.click();
    }

 </script>

 @else

<div class="row">
  <div class="col-md-12">
    <div class="description">
      <h4>No tiene permisos para acceder a esta sección</h4>
    </div>
  </div>
</div>

@endif

<script type="text/javascript">
  /*document.onscroll = function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
      $("thead").css({
        "position": "fixed",
        "top": "70px"
      });
      $("th").css({"padding":"10px 55px", "margin":"auto", "background-color": "rgba(111, 177, 54,1)"});
    } else {
      $("thead").css({
        "position": "relative",
        "top": "0px"
      });
    }
  };*/
</script>

 @endsection