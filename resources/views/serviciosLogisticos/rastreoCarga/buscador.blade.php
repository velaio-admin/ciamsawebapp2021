<!-- <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" /> -->

<div id="buscador-cargas">
	<div class="cerrar-formulario">
		Cerrar <span>X</span>
	</div>
	<form action="/rastreo-de-carga/buscador" method="POST">

		@csrf

		<div class="form-filds">
			<div class="form-field-status">
				<h5>Estado del BL </h5>
				<div class="form-field form-type-radio form-field-transito">
					<input type="radio" name="estado" id="s-transito" value="TRANSITO" checked="checked" />
					<label for="s-transito"><span></span>En transito</label>
				</div>
				<div class="form-field form-type-radio form-field-proceso">
					<input type="radio" name="estado" id="s-proceso" value="PROCESO" />
					<label for="s-proceso"><span></span>En proceso</label>
				</div>
				<div class="form-field form-type-radio form-field-proceso-finalizado">
					<input type="radio" name="estado" id="s-proceso-finalizado" value="PROC_FINAL" />
					<label for="s-proceso-finalizado"><span></span>Proceso finalizado</label>
				</div>
			</div>
			<div class="form-field-bl-or-do">
				<h5>Buscar por </h5>
				<div class="form-field form-type-radio form-field-bl">
					<input type="radio" name="type" id="bl" value="bls" checked="checked"/>
					<label for="bl"><span></span>BL</label>
				</div>
				<div class="form-field form-type-radio form-field-do">
					<input type="radio" name="type" id="do" value="dos" />
					<label for="do"><span></span>DO</label>
				</div>
				<div class="form-field form-type-radio form-field-numero">
					<input type="text" name="numero" id="numero" class="form-control" value="" placeholder="Ingrese el número" />
				</div>
			</div>
			<div class="form-field-fecha">
				<h5>Fecha desde </h5>
				<div class="form-field form-type-text input-group date form-field-fecha-inicio">
					<input type="date" name="fechainicial" id="fechainicial" value="" placeholder="dd//mm/aaaa" min="1000-01-01" max="{{date('Y-m-d')}}" class="form-control" />
					<!-- <div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div> -->
				</div>
				<h5>Hasta </h5>
				<div class="form-field form-type-text input-group date form-field-fecha-fin">
					<input type="date" name="fechafinal" id="fechafinal" value="" placeholder="dd//mm/aaaa" min="1000-01-01" max="{{date('Y-m-d')}}" class="form-control" />
					<!-- <div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div> -->
				</div>
			</div>
			<input type="hidden" name="cliente_id" id="cliente_id" value="{{Auth::user()->identificacion}}">
		</div>
		<div class="form-action">
			<input type="submit" value="Aplicar" name="submit" id="submitform">
		</div>
	</form>
</div>

<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>

<script type="text/javascript">

	window.onload = function() {
      $('#fechainicial').datepicker({
        uiLibrary: 'bootstrap4'
    });

    $('#fechafinal').datepicker({
        uiLibrary: 'bootstrap4'
    });
    };
	
</script> -->