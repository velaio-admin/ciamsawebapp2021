@extends('layouts.app')

@section('titlepage')
<div class="row justify-content-between header-rastreoList">
  <div class="col-md-4">
    <h4>Rastreo de carga</h4>
  </div>
  <div class="col-md-6 open-search">
    <span>
      Busqueda avanzada
    </span>
  </div>
  <div onclick="tableToExcel('tblData', 'name', 'rastreo_ciamsa.xls');" style="cursor: pointer;">
    <span style="color: rgb(0, 107, 177); margin-right: 5px;">
      Exportar excel
    </span> 
    <img src="http://digital.ciamsa.com/img/excel-icon.png" alt="icon-excel" height="30px" title="Exportar datos a Excel">
  </div>

</div>
@endsection
@section('content')

@if(Auth::user()->can('Rastreo_de_carga'))

@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

@if (isset($success))
<div class="alert alert-success">
  <ul>
    <li>{{ $success }}</li>
  </ul>
</div>
@endif

<style>
  /* The container-checkbox */
  .container-checkbox {
    display: inline-block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }
  /* Hide the browser's default checkbox */
  .container-checkbox input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }
  /* Create a custom checkbox */
  .checkmark {
    box-sizing: initial;
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    border: 1px solid black;
    border-radius: 5px;
  }
  /* On mouse-over, add a grey background color */
  .container-checkbox:hover input ~ .checkmark {
    background-color: #ccc;
  }
  /* When the checkbox is checked, add a blue background */
  .container-checkbox input:checked ~ .checkmark {
    /*background-color: #2196F3;*/
  }
  /* Create the checkmark/indicator (hidden when not checked) */
  .checkmark:after {
    content: "";
    position: absolute;
    display: none;
  }
  /* Show the checkmark when checked */
  .container-checkbox input:checked ~ .checkmark:after {
    display: block;
  }
  /* Style the checkmark/indicator */
  .container-checkbox .checkmark:after {
    left: 9px;
    top: 5px;
    width: 8px;
    height: 13px;
    border: solid #6eb135;
    border-width: 0px 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }
  .icon-move{
    display: inline-block;
    vertical-align: middle;
  }
  .document-item {
    margin-bottom: 7px;
  }
  .conf-sem{
    display: inline-block;
    padding-right: 20px;
  }
  .add-sem{
    color: #006BB1;
    font-size: 15px;
  }
  .add-sem span{
    border-bottom: 1px solid;
  }
  @media (max-width: 680px) {
    .wrapper{
      padding: initial !important;
    }
  }
</style>

<!-- Page Content -->
<div id="rastreoList" class="page-content-wrapper">
  <div class="form-filter">
    <form action="" >
      <div class="row">
        <div class="col-md-4">
          <div class="form-field form-type-radio form-field-transito">
            <input type="radio" name="filter" id="transito" value="transito" @if($checked == 'transito') checked="checked" @endif  onclick="redireccionar('{{env('APP_URL')}}','TRANSITO','{{ Auth::user()->identificacion }}');" />
            <label for="transito"><span></span><i class="transito"></i>En transito</label>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-field form-type-radio form-field-proceso">
            <input type="radio" name="filter" id="proceso" value="proceso" @if($checked == 'proceso') checked="checked" @endif onclick="redireccionar('{{env('APP_URL')}}','PROCESO','{{ Auth::user()->identificacion }}');" />
            <label for="proceso"><span></span><i class="proceso"></i>En proceso</label>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-field form-type-radio form-field-proceso-finalizado">
            <input type="radio" name="filter" id="proceso-finalizado" value="proceso_finalizado" @if($checked == 'proceso-finalizado') checked="checked" @endif onclick="redireccionar('{{env('APP_URL')}}','PROC_FINAL','{{ Auth::user()->identificacion }}');" />
            <label for="proceso-finalizado"><span></span><i class="proceso-finalizado"></i>Proceso finalizado</label>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div class="container-fluid">
    {!!  $table1 !!}
  </div>

</div>

@include('serviciosLogisticos.rastreoCarga.buscador')

<script type="text/javascript">
    
    function redireccionar(url,estado,id_cliente){
        window.location.href=url+'/rastreo-de-carga/'+estado+'/'+id_cliente+'/1/50';
    }

    function tableToExcel(table, name, filename){
      let uri = 'data:application/vnd.ms-excel;base64,', 

            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" ' +
                                        'xmlns="http://www.w3.org/TR/REC-html40"><head>' +
                                        '<!--[if gte mso 9]>' +

                                        '<xsl:template name="styles">' +
                                        '<style>' +

                                        'td {' +
                                        'padding-top:1px;' +
                                        'padding-right:1px;' +
                                        'padding-left:1px;' +
                                        'mso-ignore:padding;' +
                                        'border:1px;' +
                                        'color:windowtext;' +
                                        'text-align:general;' +
                                        'vertical-align:bottom;' +
                                        'text-align:left;' +
                                        'white-space:wrap;' +
                                        '}' +

                                        '.header {' +
                                        'mso-style-parent:style0;' +
                                        'font-weight:700;' +
                                        '}' +

                                        '.number {mso-number-format:"0";}' +
                                        '</style>' +

                                        '<xml>' +
                                        '<x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>' +
                                        '<x:Name>{worksheet}</x:Name>' +
                                        '<x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions>' +
                                        '</x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook>' +
                                        '</xml>' +
                                        '<![endif]-->' +
                                        '</head>' +
                                        '<body>' +
                                        '<table>{table}</table>' +
                                        '</body>' +
                                        '</html>',


            base64 = function(s = "äöüÄÖÜçéèñ") { 
              return window.btoa(unescape(encodeURIComponent(s))) 
            }, 

            format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; })}
            
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}

            var link = document.createElement('a');
            link.download = filename;
            link.href = uri + base64(format(template, ctx));
            link.click();
    }

</script>

@else

<div class="row">
  <div class="col-md-12">
    <div class="description">
      <h4>No tiene permisos para acceder a esta sección</h4>
    </div>
  </div>
</div>

@endif

@endsection

