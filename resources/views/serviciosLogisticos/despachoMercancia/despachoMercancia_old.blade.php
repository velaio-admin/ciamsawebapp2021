@extends('layouts.app')

@section('titlepage')
<div class="row justify-content-between header-rastreoList">

  <div class="col-md-4">
   <h4>Despacho de mercancia</h4>{!! $label_fecha !!}
  </div>
  <div class="col-md-6 open-search">
    <span>
      Busqueda avanzada
    </span>
  </div>
  <div onclick="tableToExcel('tblData', 'name', 'despacho_mercancia_ciamsa.xls');" style="cursor: pointer;">
    <span style="color: rgb(0, 107, 177); margin-right: 5px;">
      Exportar excel
    </span> 
    <img src="http://digital.ciamsa.com/img/excel-icon.png" alt="icon-excel" height="30px" title="Exportar datos a Excel">
  </div>

</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Despacho_de_mercancia'))

  <div id="despachoMercancia" class="page-content-wrapper">
   <div class="container-fluid">

      {!! $table1 !!}

   </div>
 </div>
 @include('serviciosLogisticos.despachoMercancia.buscador')


 <script type="text/javascript">

    function tableToExcel(table, name, filename){

      let uri = 'data:application/vnd.ms-excel;base64,', 

          /*template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><title></title><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>', */

          template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" ' +
                                        'xmlns="http://www.w3.org/TR/REC-html40"><head>' +
                                        '<!--[if gte mso 9]>' +

                                        '<xsl:template name="styles">' +
                                        '<style>' +

                                        'td {' +
                                        'padding-top:1px;' +
                                        'padding-right:1px;' +
                                        'padding-left:1px;' +
                                        'mso-ignore:padding;' +
                                        'color:windowtext;' +
                                        'vertical-align:bottom;' +
                                        'text-align:left;' +
                                        'white-space:wrap;' +
                                        '}' +

                                        '.header {' +
                                        'mso-style-parent:style0;' +
                                        'font-weight:700;' +
                                        '}' +

                                        '.number {mso-number-format:"0";}' +
                                        '</style>' +

                                        '<xml>' +
                                        '<x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>' +
                                        '<x:Name>{worksheet}</x:Name>' +
                                        '<x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions>' +
                                        '</x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook>' +
                                        '</xml>' +
                                        '<![endif]-->' +
                                        '</head>' +
                                        '<body>' +
                                        '<table>{table}</table>' +
                                        '</body>' +
                                        '</html>',

            base64 = function(s = "äöüÄÖÜçéèñ") { 
              return window.btoa(unescape(encodeURIComponent(s))) 
            },

            format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; })}
            
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}

            var link = document.createElement('a');
            link.download = filename;
            link.href = uri + base64(format(template, ctx));
            link.click();
    }

 </script>

  @else

	<div class="row">
	  <div class="col-md-12">
	    <div class="description">
	      <h4>No tiene permisos para acceder a esta sección</h4>
	    </div>
	  </div>
	</div>

	@endif

 @endsection