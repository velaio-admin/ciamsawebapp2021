@extends('layouts.app')

<style type="text/css">

  .link_toggle{
    display: inline-block;
    text-align: right;
    width: 48%;
  }
  .span_toggle{
    display: inline-block;
    width: 50%;
  }

  /* Paginator Style FKM */
  .paginated .pagination li.next a {
    background-color: #006bb1;
    color: #fff;
    border: 1px solid;
  }

  .table-responsive .paginated .pagination {
    border: 1px solid #e6e6e6;
  }

  .paginated{
    align-items: center;
    display: flex;
    justify-content: space-between;
    margin-top: 30px;
  }

  .paginated .pagination li a, .table-responsive .paginated .pagination li span{
    display: block;
    font-size: 14px;
    line-height: 1;
    padding: 14px 14px;
  }

  .disabled {
    pointer-events: none;
    cursor: default;
  }
  /* End Paginator Style FKM */
  
</style>

@section('titlepage')
<div class="row justify-content-between header-rastreoList">

  <div class="col-md-4">
   <h4>Despacho de mercancia</h4>{!! $label_fecha !!}
  </div>
  <div class="col-md-6 open-search">
    <span>
      Busqueda avanzada
    </span>
  </div>
  <div onclick="tableToExcel('tblData', 'name', 'despacho_mercancia_ciamsa.xls');" style="cursor: pointer;">
    <span style="color: rgb(0, 107, 177); margin-right: 5px;">
      Exportar excel
    </span> 
    <img src="http://digital.ciamsa.com/img/excel-icon.png" alt="icon-excel" height="30px" title="Exportar datos a Excel">
  </div>

</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Despacho_de_mercancia'))

  <?php  

    if (isset($_GET['page'])) {
      $curPage = $_GET['page'];
    }else{
      $curPage = 1;
    }

    $nextPage = $curPage + 1;
    $previusPage = $curPage > 1 ? $curPage - 1 : 1;

    $disablePrev = $curPage == 1 ? 'disabled' : '';

    if (is_array($table1)) {
      $results = sizeof($table1);
    }else{
      $results = 0;
    }

    $disableNext = $results == 0 ? 'disabled' : '';

  ?>

  <div id="despachoMercancia" class="page-content-wrapper">
   <div class="container-fluid">

   @if (count($table1) > 0)

     <div class="table-responsive m-t-35">

      <?php //dd($table1); ?>

      @if(sizeof($table1) > 0)
        <table id="tblData" class="htmlTable table">

          @foreach ($table1 as $index => $t)

          <?php //echo $t[0]
          //dd($index) ?>

            <?php if ($t[0] == 'IdBl') { ?>

              <tr>

                <th colspan="9" class="color-th" style="color: black; background: #C9C7C6;">
                  Despachos
                </th>
                
              </tr>

              <tr>
                <th><?php echo $t[0]; ?></th>
                <th><?php echo $t[1]; ?></th>
                <th><?php echo $t[2]; ?></th>
                <th><?php echo $t[3]; ?></th>
                <th><?php echo $t[4]; ?></th>
                <th><?php echo $t[5]; ?></th>
                <th><?php echo $t[6]; ?></th>
                <th><?php echo $t[7]; ?></th>
                <th><?php echo $t[8]; ?></th>
              </tr>

              @foreach ($t['data'] as $val)
                <tr>
                  <td><?php echo $val[0]; ?></td>
                  <td><?php echo $val[1]; ?></td>
                  <td><?php echo $val[2]; ?></td>
                  <td><?php echo $val[3]; ?></td>
                  <td><?php echo $val[4]; ?></td>
                  <td><?php echo $val[5]; ?></td>
                  <td><?php echo $val[6]; ?></td>
                  <td><?php echo $val[7]; ?></td>
                  <td><?php echo $val[8]; ?></td>
                </tr>
              @endforeach

            <?php } ?>

            <?php if ($t[0] == 'FechaRetiro') { ?>

                <th class="tit-icon-toggle" colspan="9">
                  <div style="color: black; background: lightblue; padding: 18px 0; margin-bottom: 10px;">
                    <span class="span_toggle">
                      Detalle del despacho de la mercancia
                    </span><a href="#" v-on:click="actionBtn($event, <?=$index ?>)" class="link_toggle">
                      Ver detalle de despachos <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                  </div>
                </th>

                <tr style="display: none;" class="content_section_<?=$index ?>">
                  <th colspan="2"><?php echo $t[0]; ?></th>
                  <th><?php echo $t[1]; ?></th>
                  <th><?php echo $t[2]; ?></th>
                  <th><?php echo $t[3]; ?></th>
                  <th><?php echo $t[4]; ?></th>
                  <th><?php echo $t[5]; ?></th>
                  <th><?php echo $t[6]; ?></th>
                  <th><?php echo $t[7]; ?></th>
                </tr>

                @foreach ($t['data'] as $val)
                  <tr style="display: none;" class="content_section_<?=$index ?>">
                    <td colspan="2"><?php echo $val[0]; ?></td>
                    <td><?php echo $val[1]; ?></td>
                    <td><?php echo $val[2]; ?></td>
                    <td><?php echo $val[3]; ?></td>
                    <td><?php echo $val[4]; ?></td>
                    <td><?php echo $val[5]; ?></td>
                    <td><?php echo $val[6]; ?></td>
                    <td><?php echo $val[7]; ?></td>
                  </tr>
                @endforeach

          <?php } ?>

          @endforeach

        </table>
      @else

        <br>

        <p class='text-center'>No se encontro informacion para su busqueda.</p>

      @endif
       
     </div>

    @else

      <br><p class='text-center'>No se encontro informacion para su busqueda.</p>

    @endif

    <?php if (isset($cant_reg) ) { ?>
            
      <div class="paginated">
         <div class="result">
            <!-- Mostrando 1  a 10  de 10 resultados -->
         </div>
         <ul class="pagination fk123">
            <li class="next <?=$disablePrev; ?>">
                <a id="link_url_prev" href="/despacho-de-mercancia/<?=$cant_reg; ?>?page=<?=$previusPage; ?>" rel="prev" class="<?=$disablePrev; ?>">
                  Anterior
                </a>
            </li>
            <li class="next <?=$disableNext; ?>">
                <a id="link_url_next" href="/despacho-de-mercancia/<?=$cant_reg; ?>?page=<?=$nextPage; ?>" class="<?=$disableNext; ?>">
                  Siguiente
                </a>
            </li>
         </ul>
      </div>

    <?php } ?>

   </div>
 </div>
 @include('serviciosLogisticos.despachoMercancia.buscador')

 <script type="text/javascript">
    
    var cantPag = parseInt('{{$cantidad_pag["totalPages"]*1}}');
    var cantPages = 1;
    var currentPage = parseInt('{{$cantidad_pag["currentPage"]*1}}');
    var envTotalPages = parseInt('{{env("TOTAL_PAGES_PAGINATOR")*1}}');
    var initPage = currentPage > envTotalPages ? currentPage : 1;

    var pathname = location.href+'/'+currentPage+'/'+envTotalPages;

    /*console.log(cantPag);
    console.log(currentPage);
    console.log(envTotalPages);*/

    for (var i = initPage; i <= cantPag; i++) {

        if (cantPages <= envTotalPages) {

            if (i >= cantPag) {
                var next = i;
            }else{
                var next = currentPage+1;
            }

            if (i <= 1) {
                var prev = 1;
            }else{
                var prev = currentPage-1;
            }

            if (i <= 1) {
                var pag = 1;
            }else{
                var pag = i;
            }

            var url = pathname.replace("/"+currentPage+"/","/"+pag+"/");
            var url_prev = pathname.replace("/"+currentPage+"/","/"+prev+"/");
            var url_next = pathname.replace("/"+currentPage+"/","/"+next+"/");

            /*console.log('url next ',next);
            console.log('url cantPag ',cantPag);
            console.log('url pathname ',pathname);
            console.log('url url_next ',url_next);*/

            document.getElementById("link_url_"+i).href = url;
            document.getElementById("link_url_prev").href = url_prev;
            document.getElementById("link_url_next").href = url_next;

            cantPages = cantPages+1;

        }
    }

</script>

 <script type="text/javascript">

    function tableToExcel(table, name, filename){

      let uri = 'data:application/vnd.ms-excel;base64,', 

          /*template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><title></title><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>', */

          template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" ' +
                                        'xmlns="http://www.w3.org/TR/REC-html40"><head>' +
                                        '<!--[if gte mso 9]>' +

                                        '<xsl:template name="styles">' +
                                        '<style>' +

                                        'td {' +
                                        'padding-top:1px;' +
                                        'padding-right:1px;' +
                                        'padding-left:1px;' +
                                        'mso-ignore:padding;' +
                                        'color:windowtext;' +
                                        'vertical-align:bottom;' +
                                        'text-align:left;' +
                                        'white-space:wrap;' +
                                        '}' +

                                        '.header {' +
                                        'mso-style-parent:style0;' +
                                        'font-weight:700;' +
                                        '}' +

                                        '.number {mso-number-format:"0";}' +
                                        '</style>' +

                                        '<xml>' +
                                        '<x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>' +
                                        '<x:Name>{worksheet}</x:Name>' +
                                        '<x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions>' +
                                        '</x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook>' +
                                        '</xml>' +
                                        '<![endif]-->' +
                                        '</head>' +
                                        '<body>' +
                                        '<table>{table}</table>' +
                                        '</body>' +
                                        '</html>',

            base64 = function(s = "äöüÄÖÜçéèñ") { 
              return window.btoa(unescape(encodeURIComponent(s))) 
            },

            format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; })}
            
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}

            var link = document.createElement('a');
            link.download = filename;
            link.href = uri + base64(format(template, ctx));
            link.click();
    }

 </script>

  @else

	<div class="row">
	  <div class="col-md-12">
	    <div class="description">
	      <h4>No tiene permisos para acceder a esta sección</h4>
	    </div>
	  </div>
	</div>

	@endif

  <!-- Vuejs -->
<script src="{{asset('storage/assets/js/vue.js')}}"></script>

<script type="text/javascript">
  var app = new Vue({
    el: '#despachoMercancia',
    data: {

    },
    mounted () {
    },
    methods: {
      actionBtn: function (event, section) {

        event.preventDefault();

        var elements = document.getElementsByClassName('content_section_'+section);
        for(var i=0; i<elements.length; i++) { 
          //elements[i].style.display='none';
          if (elements[i].style.display === "none") {
            elements[i].style.display = "table-row";
          } else {
            elements[i].style.display = "none";
          }
        }

      }

    }
    
  })
</script>

 @endsection