@extends('layouts.app')

<link rel="stylesheet" href="{{asset('storage/assets/css/traffic_light.css')}}" />
<!--Moment Librery-->
<script src="{{asset('storage/assets/js/moment.js')}}"></script>

<link rel="stylesheet" type="text/css" href="https://unpkg.com/vue-popperjs@2.2.0/dist/vue-popper.css">

<!-- Paginator Style -->
<style>
  .pagination>li {
    display: inline;
  }

  .pagination>li>a, .pagination>li>span {
      position: relative;
      float: left;
      padding: 6px 12px;
      margin-left: -1px;
      line-height: 1.42857143;
      color: #337ab7;
      text-decoration: none;
      background-color: #fff;
      border: 1px solid #ddd;
  }
</style>

<div id="semaforo">

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <h3 class="text-center">Manejo de semáforos</h3>
          <br>
          <div style="width: 600px; margin: 0 auto;">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/Xk9JYZ7BJlA?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
  
  @section('titlepage')
  <div class="row justify-content-between header-rastreoList">
    <div class="col-md-4 col-sm-6 col-xs-12">
      <h4 style=" font-size: 1.28rem; display: inline-block;">Estado de la carga</h4>
      <div class="col-inf">
        <!--<a href="https://youtu.be/Xk9JYZ7BJlA" target="_blank" style="color: #006bb1">-->
        <a data-toggle="modal" data-target="#myModal" style="color: #006bb1; cursor: pointer;">
          <img src="{{asset('img/question_bar.png')}}" 
             alt="icon-excel" 
             height="30px"           
             title="Vídeo de ayuda">
        </a> 
      </div>
    </div>
    <div class="col-md-8 col-sm-6 col-xs-12" style="text-align: right;">
          <div class="col-inf">
            <div class="nav-head-drag">
              <span style="color: #006bb1; cursor: pointer;"
                    :style="[section_1 ? {'border-bottom': '1px solid #006BB1'} : {}]"
                    v-on:click="go_conf_sem">
                Configurar columnas
              </span>
              <img src="{{asset('img/columns-ciamsa.png')}}" 
                   alt="icon-move" 
                   height="30px"
                   title="Configurar columnas"
                   :style="[section_1 ? {'border-bottom': '2px solid #006BB1'} : {}]"
                   v-on:click="go_conf_sem">
              <span style="color: #006bb1; cursor: pointer; margin-right: 5px;"
                    :style="[section_2 ? {'border-bottom': '1px solid #006BB1'} : {}]"
                    v-on:click="go_sect_2">
                Configurar semáforos
              </span>
              <img src="{{asset('img/config-ciamsa.png')}}" 
                   alt="icon-task" 
                   height="30px"           
                   title="Configurar semáforos"
                   :style="[section_2 ? {'border-bottom': '2px solid #006BB1'} : {}]"
                   v-on:click="go_sect_2">       
            </div>
          </div>
          <div class="col-inf">
            <div class="open-search">
              <span>
                Buscar
              </span>
            </div> 
          </div>
          <div class="col-inf" v-show="exp_excel_btn" style="padding-left: 35px;">
            <span style="color: #006bb1; cursor: pointer; margin-right: 5px;" v-on:click="tableToExcel('tblData', 'name', 'registros_ciamsa.xls')">
              Exportar excel
            </span>
            <img src="{{asset('img/excel-icon.png')}}" 
                   alt="icon-excel" 
                   height="30px"           
                   title="Exportar datos a Excel"
                   v-on:click="tableToExcel('tblData', 'name', 'registros_ciamsa.xls')"> 
          </div>
      </div>     
      
    </div>
  </div>
  @endsection
  <!-- Page Content -->
  @section('content')
  <div id="message"></div>

  @if(Auth::user()->can('Estado_carga'))

    @if($msjBool)

      <div class="alert alert-danger" role="alert">
        {{$msj}}
      </div>

    @else

  <!-- Page Content -->
  <div class="page-content-wrapper">

    <div :class="var_load"></div>

    <input type="hidden" id="id_user" value="{{ Auth::user()->id }}">
    <input type="hidden" id="identification" value="{{ Auth::user()->identificacion }}">

        <section v-show="section_1" style="padding-left: 10px;">

          <h3 style="font-size: 1.28rem;">Configurar columnas a mostrar</h3>

          <br>

          <draggable 
            class="dragArea" 
            :options="{group:'columns'}"
            @change="afterAdd"
            :list="items">
            <div
              class="document-item"
              v-for="(item, index) in items" 
              :key="index">
              <div class="icon-move">
                <img src="{{asset('img/move_data_row.png')}}" alt="icon-move" height="25px" style="padding-right: 3px;">
              </div>
              <label class="container-checkbox">@{{item.name}}
                <input @change="check_event" type="checkbox" v-model="item.state" :value="item.variable">
                <span class="checkmark"></span>
              </label>
            </div>
          </draggable>

          <div style="text-align: center; margin-top: 10px;">

            <button type="button" v-on:click="save_drag_drop" class="btn btn-default" style="background-color: #006BB1; color: white;">
              Guardar
            </button>
            <button type="button" v-on:click="restore_drag_drop" class="btn btn-default">
              Restablecer columnas
            </button>
            <button type="button" v-on:click="cancel_save_drag_drop" class="btn btn-default">
              Cancelar
            </button>
            
          </div>    
        
      </section>

      <section v-show="section_2" style="padding-left: 10px;">

        <div class="row" v-show="add_sem === false">
          <div class="col">
            <h3 class="conf-sem">Configurar semáforos</h3>
            <a href="#" class="add-sem" v-on:click="add_sem = !add_sem, action_config=false">
              <i class="fa fa-plus-circle"></i>
              <span>Adicionar semáforo</span>
            </a>
          </div>
        </div>

        <article v-show="add_sem" style="width: 99%;">

          <div class="row">
            <div class="col"> 
              <h4>Adicionar semáforo</h4>
              <p>Columna para el semáforo</p>          
            </div>
          </div>

          <div v-if="action_config">

            <div class="row">
              
              <div class="col-md-12">
                <div class="form-group">
                  <input type="text" class="form-control" disabled :value="name_field_edit">
                </div>
              </div>

            </div>

            <div v-for="(data, index) in sem_conf_data_to_edit" class="row">
              
              <div class="col-md-4">
                <div class="form-group">
                  <label v-show="index == 0">Regla</label>
                  <select class="form-control" :id="'cond_edit'+index">
                    <option v-if="options_data_validate" value=">" :selected="data.operator == '>' ? 'selected' : ''">
                      Mayor que
                    </option>
                    <option v-if="options_data_validate" value=">=" :selected="data.operator == '>=' ? 'selected' : ''">
                      Mayor o igual que
                    </option>
                    <option v-if="options_data_validate" value="<" :selected="data.operator == '<' ? 'selected' : ''">
                      Menor que
                    </option>
                    <option v-if="options_data_validate" value="<=" :selected="data.operator == '<=' ? 'selected' : ''">
                      Menor o igual que
                    </option>
                    <option value="==" :selected="data.operator == '==' ? 'selected' : ''">
                      Igual
                    </option>
                  </select>
                </div>
                
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label v-show="index == 0">Número de días</label>

                  <input v-if="options_data_validate" 
                        :id="'dias_edit'+index" 
                        type="text"
                        :value="data.value" 
                        class="form-control" 
                        min="1">
                    <select v-if="selects_especials" class="form-control" :id="'dias_edit'+index">
                      <option v-for="arr in array_dinamic_sel_especials" :value="arr.name" :selected="data.value == arr.name ? 'selected' : ''">
                        @{{arr.name}} 
                      </option>
                    </select>  
                </div>
              </div>

              <div class="col-md-3">

                <div class="form-group">
                  <label v-show="index == 0">Colores</label>

                  <select class="form-control" :id="'color_get_edit'+index" v-model="data.color">
                    <option v-for="color in colors" :value="color.hex" :selected="color.hex == data.color ? 'selected' : ''">
                      @{{color.name}}
                    </option>
                  </select>

                </div>

              </div>

              <div class="col-md-1" style="text-align: right;">
                
                  <div class="form-group color-sem-request sem-icon-edit">
                    <span style="display: inline-block; text-align: right;" 
                        :style="[data.color ? {'background-color': data.color} : {}]">                    
                    </span>
                  </div>

              </div>

            </div>

          </div>

          <div v-else>
            
            <div class="row">

              <div class="col-md-12">              
                <div class="form-group">
                  <label>Columna</label>
                  <select class="form-control" v-model="item_var" v-on:change="validate_options">
                    <option 
                      v-for="(item, index) in sem_conf_data" 
                      :id="'item_var'+index" 
                      :data-rs="item.variable" 
                      :data-validate="item.string_validate" 
                      :data-definition="item.tooltip_msj" 
                      :value="index"
                    >
                      @{{item.name}} <!--(@{{item.tooltip_msj}})-->
                    </option>
                  </select>
                </div>
              </div>

            </div>

            <div class="row">
              <div v-if="definition != ''" class="col-md-12">
                <strong>Definición:</strong> @{{definition}}
                <hr>
              </div>   
            </div>

            <div class="row">  

              <div class="col-md-4">
                <div class="form-group">
                  <label>Regla</label>
                  <select class="form-control" v-model="condition">
                    <option v-if="options_data_validate" value=">">Mayor que</option>
                    <option v-if="options_data_validate" value=">=">Mayor o igual que</option>
                    <option v-if="options_data_validate" value="<">Menor que</option>
                    <option v-if="options_data_validate" value="<=">Menor o igual que</option>
                    <option value="==">Igual</option>
                  </select>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>@{{help_text}}</label>
                  <input v-if="options_data_validate" 
                         type="number" 
                         class="form-control"
                         v-model="number_semaforo" 
                         min="1"
                         v-on:change="inputSemRule">
                  <select v-if="selects_especials" class="form-control" v-model="number_semaforo">
                    <option v-for="data in array_dinamic_sel_especials" :value="data.name">
                      @{{data.name}}
                    </option>
                  </select>       
                </div>
              </div>

              <div class="col-md-4">
                <label>Colores</label>
                <div class="wrapper-dropdown-pick-color">
                  <span @click="toggleDropdown(1)" v-html="selector"></span>
                  <ul class="dropdown-data-pick" v-show="active_col_pick">
                    <li v-for="color in colors" @click="setColor(color.hex, color.name, 1)">
                      <span :style="{background: color.hex}"></span> @{{color.name}}
                    </li>
                  </ul>
                </div>

              </div>

            </div>

            <div class="row"> 

              <div class="col-md-4">
                <div class="form-group">
                  <select class="form-control" v-model="condition_2">
                    <option v-if="options_data_validate" value=">">Mayor que</option>
                    <option v-if="options_data_validate" value=">=">Mayor o igual que</option>
                    <option v-if="options_data_validate" value="<">Menor que</option>
                    <option v-if="options_data_validate" value="<=">Menor o igual que</option>
                    <option value="==">Igual</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <input v-if="options_data_validate" 
                         type="number" 
                         class="form-control" 
                         v-model="number_semaforo_2" 
                         min="1"
                         v-on:change="inputSemRule">
                  <select v-if="selects_especials" class="form-control" v-model="number_semaforo_2">
                    <option v-for="data in array_dinamic_sel_especials" :value="data.name">
                      @{{data.name}}
                    </option>
                  </select> 
                </div>
              </div>
              <div class="col-md-4">
                <div class="wrapper-dropdown-pick-color">
                  <span @click="toggleDropdown(2)" v-html="selector2"></span>
                  <ul class="dropdown-data-pick" v-show="active_col_pick2">
                    <li v-for="color in colors" @click="setColor(color.hex, color.name, 2)"><span :style="{background: color.hex}"></span> @{{color.name}}</li>
                  </ul>
                </div>

              </div>

            </div>

            <div class="row">

              <div class="col-md-4">
                <div class="form-group">
                  <select class="form-control" v-model="condition_3">
                    <option v-if="options_data_validate" value=">">Mayor que</option>
                    <option v-if="options_data_validate" value=">=">Mayor o igual que</option>
                    <option v-if="options_data_validate" value="<">Menor que</option>
                    <option v-if="options_data_validate" value="<=">Menor o igual que</option>
                    <option value="==">Igual</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <input v-if="options_data_validate" 
                         type="number" 
                         class="form-control"
                         v-model="number_semaforo_3" 
                         min="1"
                         v-on:change="inputSemRule">
                  <select v-if="selects_especials" class="form-control" v-model="number_semaforo_3">
                    <option v-for="data in array_dinamic_sel_especials" :value="data.name">
                      @{{data.name}}
                    </option>
                  </select> 
                </div>
              </div>
              <div class="col-md-4">
                <div class="wrapper-dropdown-pick-color">
                  <span @click="toggleDropdown(3)" v-html="selector3"></span>
                  <ul class="dropdown-data-pick" v-show="active_col_pick3">
                    <li v-for="color in colors" @click="setColor(color.hex, color.name, 3)"><span :style="{background: color.hex}"></span> @{{color.name}}</li>
                  </ul>
                </div>
              </div>

            </div>

          </div>          

          <div class="row">
            <div style="text-align: center; margin-top: 12px; width: 100%;">

              <button type="button" class="btn btn-default" v-on:click="save_sem_btn" style="background-color: #006BB1; color: white;">
                Guardar
              </button>
              <button type="button" v-on:click="add_sem = !add_sem" class="btn btn-default">
                Cancelar
              </button>
              
            </div> 
          </div>

        </article>

        <br>

        <article v-show="!add_sem">       

          <div class="row">

            <div class="col-md-6" style="margin-bottom: 20px;" v-for="(rule, name, index) in sem_conf_data_result">

              <div class="height_conf_line">                
              
                <div class="row col-sem-res" style="margin: 0; padding: 0;">
                  <div class="col-md-8 col-sm-6 col-xs-8 data3 pad-targets-sem">
                    <strong>@{{rule.name}}</strong>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-4" style="margin: 3px 0px; text-align: right;">
                    <img src="https://image.flaticon.com/icons/svg/1159/1159633.svg" 
                         height="25px" 
                         alt="edit"
                         title="Editar"
                         v-on:click="edit_especific_rule($event, rule.name, name, rule.variable, rule.config_color, rule.string_validate)">
                    <img src="https://image.flaticon.com/icons/svg/1214/1214428.svg"
                         height="25px"
                         alt="delete"
                         title="Eliminar"
                         v-on:click="del_especific_rule($event, name)">
                  </div>
                </div>

                <div class="row" v-for="(item, index) in rule.config_color">
                  <div class="col-md-9" style="padding: 10px 30px;">
                    <div v-if="item.operator !== ''">
                      <strong>Regla</strong>
                      <p>
                        Si @{{rule.variable}} es <strong v-if="item.operator === '>'">MAYOR</strong><strong v-if="item.operator === '>='">MAYOR O IGUAL</strong><strong v-if="item.operator === '=='">IGUAL</strong><strong v-if="item.operator === '<'">MENOR</strong><strong v-if="item.operator === '<='">MENOR O IGUAL</strong> a <strong>@{{item.value}}</strong> <span v-if="rule.string_validate === false">días</span> use el color <strong v-if="item.color === '#008000'">Verde</strong> <strong v-if="item.color === '#FFFF00'">Amarillo</strong> <strong v-if="item.color === '#FF0000'">Rojo</strong>
                      </p>
                    </div>
                    <div v-else><br/></div>
                  </div>
                  <div class="col-md-3">
                    <div class="color-sem-request text-center">
                      <strong>Color</strong>
                      <span 
                        :style="[item.color ? {'background-color': item.color} : {}]">                    
                      </span>
                      <br>
                    </div>
                  </div>
                </div>

              </div>
                
            </div>            

          </div>

          <div class="row">
            
            <div style="text-align: center; margin-top: 12px; width: 100%;">

              <button type="button" class="btn btn-default" v-on:click="save_conf_finish($event)" style="background-color: #006BB1; color: white;">
                Aceptar
              </button>
              
            </div> 

          </div>

        </article>
        
      </section>

      <section v-show="section_table" style="padding-left: 10px;">        

        <div class="row">
          <!--<div class="col-md-12" style="overflow-y: auto; height: 400px;">-->

            <div class="col-md-12" style="overflow-y: auto;">

            <div class="row">

              <div class="col-md-4">
                <div class="alert alert-success" role="alert" style="padding: 3px 2px; display: inline-table; font-size: 12.5px; font-weight: bold;">
                  Los valores de tiempos están expresados en “Días”.
                </div>
              </div>
              <div class="col-md-8" style="text-align: right; line-height: 2;">
                <div v-if="state_search">
                  Resultados del mes actual - Registros encontrados: <strong>@{{cant_res_table}}</strong>
                </div>
                <div v-else>
                  Resultados para la fecha <strong>@{{fechainicial}}</strong> hasta la fecha <strong>@{{fechafinal}}</strong> - Registros encontrados: <strong>@{{cant_res_table}}</strong>
                </div>
              </div>

            </div>                

            <table id="tblData" class="table table-default table-md">
              <thead>
                <tr class="scaffolding" style="background-color: lightgrey; overflow-x: auto !important;">
                  <th scope="col" v-for="(item, index) in result_table">

                    <div v-if="item.tooltip_msj !== ''" class="bord-tit-line">
                      <popper trigger="clickToToggle" :options="{placement: 'top'}">
                        <div class="popper">
                          <span class="test-dat">@{{ item.tooltip_msj }}</span>
                        </div>
                        <a href="#" slot="reference" class="top">
                          @{{ item.name }} <strong style="color: #006BB1; border-bottom: 1px solid;">?</strong>
                        </a>  
                      </popper> 
                    </div>
                    <div v-else class="bord-tit-line">
                      @{{ item.name }}
                    </div>                    

                    <div id='object1'></div>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr class="scaffolding" v-for="(item, index) in result_table_vals" v-show="(pag - 1) * NUM_RESULTS <= index  && pag * NUM_RESULTS > index">
                  <!--<td v-for="(value, index) in item" :style="{'background-color': returnColor(value)}">-->
                  <td v-for="(value, index) in item" :style="{'background-color': value.color}">
                    <div v-if="value.config.config_color.length > 0">
                      <div style="font-weight: bold;">
                        <div>

                          <div v-if="value.color === '#FF0000'" class="img-icon-sem">
                            <img src="{{asset('img/semaforo-rojo.png')}}" class="img-traffic" height="25px;">
                          </div>
                          <div v-if="value.color === '#FFFF00'" class="img-icon-sem">
                            <img src="{{asset('img/semaforo-amarillo.png')}}" class="img-traffic" height="25px;">
                          </div>
                          <div v-if="value.color === '#008000'" class="img-icon-sem">
                            <img src="{{asset('img/semaforo-verde.png')}}" class="img-traffic" height="25px;">
                          </div>

                          <div v-if="value.color === '#FFFF00'" class="txt-icon-sem" style="color: black;">
                            @{{returnName(value)}} 
                          </div>
                          <div v-else-if="value.color === '#FF0000'" class="txt-icon-sem" style="color: white;">
                            @{{returnName(value)}} 
                          </div>
                          <div v-else-if="value.color === '#008000'" class="txt-icon-sem" style="color: white;">
                            @{{returnName(value)}} 
                          </div>
                          <div v-else class="txt-icon-sem" style="color: black; width: 100% !important;">
                            @{{returnName(value)}}
                          </div>  

                        </div>                     
                      </div>
                    </div>
                    <div v-else>
                      <div class="txt-icon-sem" style="width: 100% !important;">@{{returnName(value)}}</div>  
                    </div> 
                  </td>
                </tr>
            </table>

          </div>        
        </div>

        <!-- Controles vuejs paginator -->   
        Página <strong>@{{pag}}</strong> de <strong>@{{Math.ceil(result_table_vals.length/NUM_RESULTS)}}</strong> - Resultados <strong>@{{result_table_vals.length}}</strong>      
        <nav aria-label="Page navigation" class="text-center">
            <ul class="pagination text-center">
                <li>
                    <a href="#" aria-label="Previous" v-show="pag != 1" @click.prevent="pag -= 1">
                        <span aria-hidden="true">Anterior</span>
                    </a>
                </li>
                <li>
                    <a href="#" aria-label="Next" v-show="pag * NUM_RESULTS / result_table_vals.length < 1" @click.prevent="pag += 1">
                        <span aria-hidden="true">Siguiente</span>
                    </a>
                </li>
            </ul>
        </nav>

        <br>

        <!--<button v-show="btn_export_data" type="button" class="btn btn-default" onclick="exportTableToExcel('tblData')" style="background-color: #006BB1; color: white;">
            Exportar datos a excel
        </button>-->
        <!--<button v-show="btn_export_data" type="button" class="btn btn-default" onclick="tableToExcel('tblData', 'name', 'myfile.xls')" style="background-color: #006BB1; color: white;">
            Exportar datos a excel
        </button>-->

      </section>
      
    </div>

  </div>

  @include('serviciosLogisticos.estadoCarga.buscador')

</div>

  <!-- CDNJS :: Vue (https://cdnjs.com/) -->
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.3.4/vue.min.js"></script>-->
  <script src="{{asset('storage/assets/js/vue.js')}}"></script>
  <!-- Vue Popper -->
  <script src="https://unpkg.com/popper.js@1.14.6/dist/umd/popper.js"></script>
  <script src="https://unpkg.com/vue-popperjs@2.2.0/dist/vue-popper.js"></script>
  <!-- Axios To Run Api Services -->
  <script src="{{asset('storage/assets/js/axios.js')}}"></script>
  <!-- CDNJS :: Sortable (https://cdnjs.com/) -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.6.0/Sortable.min.js"></script>
  <!-- CDNJS :: Vue.Draggable (https://cdnjs.com/) -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.14.1/vuedraggable.min.js"></script>
  <!-- Vuefity Vuejs's Styles  
  <script src="{{asset('storage/assets/js/vuetify.js')}}"></script>-->

  <script src="{{asset('storage/assets/js/semaforos.js?v=3.2')}}"></script>

    <script type="text/javascript">        
      function redireccionar(url,estado,id_cliente){
        window.location.href=url+'/rastreo-de-carga/'+estado+'/'+id_cliente+'/1/50';
      }
    </script>
  @endif

@else

  <div class="row">
    <div class="col-md-12">
      <div class="description">
        <h4>No tiene permisos para acceder a esta sección</h4>
      </div>
    </div>
  </div>

@endif

@endsection
