<div id="buscador-cargas">
	<div class="cerrar-formulario">
		Cerrar <span>X</span> 
	</div>
	<form>

		@csrf

		<div class="form-filds">
			<div class="form-field-fecha">
				<h5>Fecha desde </h5>
				<div class="form-field form-type-text input-group date form-field-fecha-inicio">
					<input type="date" name="fechainicial" v-model="fechainicial" id="fechainicial" value="" placeholder="dd//mm/aaaa" min="1000-01-01" max="{{date('Y-m-d')}}" class="form-control" />
				</div>
				<h5>Hasta </h5>
				<div class="form-field form-type-text input-group date form-field-fecha-fin">
					<input type="date" name="fechafinal" v-model="fechafinal" id="fechafinal" value="" placeholder="dd//mm/aaaa" min="1000-01-01" max="{{date('Y-m-d')}}" class="form-control" />
				</div>
			</div>
			<input type="hidden" name="cliente_id" id="cliente_id" value="{{Auth::user()->identificacion}}">
		</div>
		<div class="form-action">
			<input type="submit" v-on:click="search($event)" value="Aplicar" name="submit" id="submitform">
		</div>

	</form>
</div>