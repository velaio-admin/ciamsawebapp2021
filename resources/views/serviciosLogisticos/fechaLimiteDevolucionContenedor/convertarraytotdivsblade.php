@foreach($arreglo as $key => $row)
<?php 
$content = $row['data'][0];
?>
<div class="table-responsive m-t-35">
    <div class="thead">

    </div>
    <div class="tbody">
        <div class="v-c-rows">
            @foreach($row as $delta => $value)
                @if(!is_array($value))
                    <div class="v-c-row">
                        <label>{{ $value }}</label>
                        <span>{{ $content[$delta] }}</span>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>
@endforeach