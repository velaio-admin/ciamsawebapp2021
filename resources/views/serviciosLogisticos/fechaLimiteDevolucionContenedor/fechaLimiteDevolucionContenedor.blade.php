@extends('layouts.app')

<style type="text/css">

  /* Paginator Style FKM */
  .paginated .pagination li.next a {
    background-color: #006bb1;
    color: #fff;
    border: 1px solid;
  }

  .table-responsive .paginated .pagination {
    border: 1px solid #e6e6e6;
  }

  .paginated{
    align-items: center;
    display: flex;
    justify-content: space-between;
    margin-top: 30px;
  }

  .paginated .pagination li a, .table-responsive .paginated .pagination li span{
    display: block;
    font-size: 14px;
    line-height: 1;
    padding: 14px 14px;
  }

  .disabled {
    pointer-events: none;
    cursor: default;
  }
  /* End Paginator Style FKM */

</style>

@section('titlepage')
<div class="row justify-content-between header-rastreoList">

  <div class="col-md-4">
    <h4>Fecha límite de devolución de contenedores</h4>{!! $label_fecha !!}
  </div>
  <div class="col-md-6 open-search">
    <span>
      Busqueda avanzada
    </span>
  </div>
  <div onclick="tableToExcel('tblData', 'name', 'devolucion_contenedores.xls');" style="cursor: pointer;">
    <span style="color: rgb(0, 107, 177); margin-right: 5px;">
      Exportar excel
    </span> 
    <img src="http://digital.ciamsa.com/img/excel-icon.png" alt="icon-excel" height="30px" title="Exportar datos a Excel">
  </div>

</div>
@endsection
<!-- Page Content -->
@section('content')

@if(Auth::user()->can('Fecha_limite_devolucion'))

  <?php  

    if (isset($_GET['page'])) {
      $curPage = $_GET['page'];
    }else{
      $curPage = 1;
    }

    $nextPage = $curPage + 1;
    $previusPage = $curPage > 1 ? $curPage - 1 : 1;

    $disablePrev = $curPage == 1 ? 'disabled' : '';

    if (is_array($table1)) {
      $results = sizeof($table1);
    }else{
      $results = 0;
    }

    $disableNext = $results == 0 ? 'disabled' : '';

  ?>

 <div id="fechaLimiteDevolucionContenedor" class="page-content-wrapper">
   <div class="container-fluid">

   @if (count($table1) > 0)
      
      <div class="table-responsive m-t-35">
        <h3>Detalle
            <span> </span>
        </h3>
        <table id="tblData" class="htmlTable table fkm">
            <!--Table fk --> 
            <thead>
              <tr>
                  <th style="background:#C9C7C6"> BL </th>
                  <th style="background:#C9C7C6"> DO/Pedido </th>
                  <th style="background:#C9C7C6"> ID del contenedor </th>
                  <th style="background:#C9C7C6"> Fecha limite de devolucion </th>
                  <th style="background:#C9C7C6"> Fecha de devolucion </th>
              </tr>
            </thead>
            <tbody>
              
                @foreach($table1 as $tab)
                  <tr class="fk">                
                      <td>{{$tab[0]}}</td>
                      <td>{{$tab[1]}}</td>
                      <td>{{$tab[2]}}</td>
                      <td>{{$tab[3]}}</td>
                      <td>{{$tab[4]}}</td>                
                  </tr>
                @endforeach	
              
            </tbody>
        </table>
      </div>

    @else

      <br><p class='text-center'>No se encontro informacion para su busqueda.</p>

    @endif

    <?php if (isset($cant_reg) ) { ?>
            
      <div class="paginated">
         <div class="result">
            <!-- Mostrando 1  a 10  de 10 resultados -->
         </div>
         <ul class="pagination fk123">
            <li class="next <?=$disablePrev; ?>">
                <a id="link_url_prev" href="/devolucion-de-contenedor/<?=$cant_reg; ?>?page=<?=$previusPage; ?>" rel="prev" class="<?=$disablePrev; ?>">
                  Anterior
                </a>
            </li>
            <li class="next <?=$disableNext; ?>">
                <a id="link_url_next" href="/devolucion-de-contenedor/<?=$cant_reg; ?>?page=<?=$nextPage; ?>" class="<?=$disableNext; ?>">
                  Siguiente
                </a>
            </li>
         </ul>
      </div>

    <?php } ?>

   </div>
 </div>
 @include('serviciosLogisticos.fechaLimiteDevolucionContenedor.buscador')

 <script type="text/javascript">

    function tableToExcel(table, name, filename){
      let uri = 'data:application/vnd.ms-excel;base64,',

            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" ' +
                                        'xmlns="http://www.w3.org/TR/REC-html40"><head>' +
                                        '<!--[if gte mso 9]>' +

                                        '<xsl:template name="styles">' +
                                        '<style>' +

                                        'td {' +
                                        'padding-top:1px;' +
                                        'padding-right:1px;' +
                                        'padding-left:1px;' +
                                        'mso-ignore:padding;' +
                                        'color:windowtext;' +
                                        'text-align:general;' +
                                        'vertical-align:bottom;' +
                                        'text-align:left;' +
                                        'white-space:wrap;' +
                                        '}' +

                                        '.color-th {' +
                                        'mso-style-parent:style0;' +
                                        'background:#C9C7C6;;' +
                                        '}' +

                                        '.header {' +
                                        'mso-style-parent:style0;' +
                                        'font-weight:700;' +
                                        '}' +

                                        '.number {mso-number-format:"0";}' +
                                        '</style>' +

                                        '<xml>' +
                                        '<x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>' +
                                        '<x:Name>{worksheet}</x:Name>' +
                                        '<x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions>' +
                                        '</x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook>' +
                                        '</xml>' +
                                        '<![endif]-->' +
                                        '</head>' +
                                        '<body>' +
                                        '<table>{table}</table>' +
                                        '</body>' +
                                        '</html>',

            
            base64 = function(s = "äöüÄÖÜçéèñ") { 
              return window.btoa(unescape(encodeURIComponent(s))) 
            },    

            format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; })}
            
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}

            var link = document.createElement('a');
            link.download = filename;
            link.href = uri + base64(format(template, ctx));
            link.click();
    }

</script>

 @else

	<div class="row">
	  <div class="col-md-12">
	    <div class="description">
	      <h4>No tiene permisos para acceder a esta sección</h4>
	    </div>
	  </div>
	</div>

	@endif
 @endsection