@extends('layouts.app')

<style type="text/css">
  .label-resp-turn{
    color: rgb(0, 107, 177);
  }
  .reserva-form input{
    color: red;
    padding: 1;
    width: auto;
  }
  .container-fluid{
    width: 90% !important;
  }

  /* Absolute Center Spinner */
      .loading {
        position: fixed;
        z-index: 999;
        height: 2em;
        width: 2em;
        overflow: show;
        margin: auto;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
      }

      /* Transparent Overlay */
      .loading:before {
        content: '';
        display: block;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
          background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

        background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
      }

      /* :not(:required) hides these rules from IE9 and below */
      .loading:not(:required) {
        /* hide "loading..." text */
        font: 0/0 a;
        color: transparent;
        text-shadow: none;
        background-color: transparent;
        border: 0;
      }

      .loading:not(:required):after {
        content: '';
        display: block;
        font-size: 10px;
        width: 1em;
        height: 1em;
        margin-top: -0.5em;
        -webkit-animation: spinner 1500ms infinite linear;
        -moz-animation: spinner 1500ms infinite linear;
        -ms-animation: spinner 1500ms infinite linear;
        -o-animation: spinner 1500ms infinite linear;
        animation: spinner 1500ms infinite linear;
        border-radius: 0.5em;
        -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
      box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
      }

  /* Animation */

      @-webkit-keyframes spinner {
        0% {
          -webkit-transform: rotate(0deg);
          -moz-transform: rotate(0deg);
          -ms-transform: rotate(0deg);
          -o-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          -moz-transform: rotate(360deg);
          -ms-transform: rotate(360deg);
          -o-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      @-moz-keyframes spinner {
        0% {
          -webkit-transform: rotate(0deg);
          -moz-transform: rotate(0deg);
          -ms-transform: rotate(0deg);
          -o-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          -moz-transform: rotate(360deg);
          -ms-transform: rotate(360deg);
          -o-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      @-o-keyframes spinner {
        0% {
          -webkit-transform: rotate(0deg);
          -moz-transform: rotate(0deg);
          -ms-transform: rotate(0deg);
          -o-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          -moz-transform: rotate(360deg);
          -ms-transform: rotate(360deg);
          -o-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      @keyframes spinner {
        0% {
          -webkit-transform: rotate(0deg);
          -moz-transform: rotate(0deg);
          -ms-transform: rotate(0deg);
          -o-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          -moz-transform: rotate(360deg);
          -ms-transform: rotate(360deg);
          -o-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }

      .theme--light.v-btn:not(.v-btn--flat):not(.v-btn--text):not(.v-btn--outlined) {
        background-color: #006BB1 !important;
        color: white !important;
      }
      .v-messages.theme--light.error--text {
          color: red !important;
      }

      @media (max-width: 680px) {
        .wrapper{
        padding: initial !important;
        }
      }

</style>

<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>

@section('titlepage')
<div class="row justify-content-between header-rastreoList">
  <div class="col-md-4">
    <h4>Formulario para el registro de reserva de turno</h4>
  </div>

</div>
@endsection
@section('content')

@if(Auth::user()->can('Reserva_turno_CIAMSA2'))

@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

@if (isset($success))
<div class="alert alert-success">
  <ul>
    <li>{{ $success }}</li>
  </ul>
</div>
@endif

<!-- Page Content -->
<div id="reservation" class="page-content-wrapper">

  <div :class="var_load"></div>

  <br>

  <input type="hidden" value="{{$cliente_id}}" id="cliente_id">
  
  <div class="container-fluid reserva-form">
   
   <div class="row">

     <div class="col-md-12">
       <form>

        <div class="form-group row">
          <label for="idCli" class="col-sm-2 col-form-label">
            <strong>Cliente</strong>
          </label>
          <div class="col-sm-3">
            <input type="text" readonly="" class="form-control" value="{{ Auth::user()->identificacion }}">
          </div>
          <div class="col-sm-7">
            <span class="label-resp-turn">
              @{{nom_cliente}}
            </span>
          </div>
        </div>

        <div class="form-group row">
          <label for="idPlaca" class="col-sm-2 col-form-label">
            <strong>Placa Vehículo</strong>
          </label>
          <div class="col-sm-3">
            <input type="text" class="form-control" id="idPlaca" value="" placeholder="Placa Vehículo">
          </div>
          <div class="col-sm-7">
            
          </div>
        </div>

        <div class="form-group row">
          <label for="idCcond" class="col-sm-2 col-form-label">
            <strong>C.C Conductor</strong>
          </label>
          <div class="col-sm-3">
            <input type="number" v-on:keyup.enter="consult_conductor" class="form-control" id="idCcond" value="" placeholder="C.C Conductor">
          </div>
          <div class="col-sm-7">
            <span class="label-resp-turn">
              @{{nombre_conductor}}
            </span>
          </div>
        </div>

        <div class="form-group row">
          <label for="idDate" class="col-sm-2 col-form-label">
            <strong>Fecha Prevista</strong>
          </label>
          <div class="col-sm-3">
            <input type="date" class="form-control" id="idDate" value="" placeholder="dd//mm/aaaa">
          </div>
          <label for="idDate" class="col-sm-2 col-form-label">
            <strong>Hora Prevista</strong>
          </label>
          <div class="col-sm-5">
            <input type="time" class="form-control" id="idHora" value="" placeholder="Hora Prevista">
          </div>
        </div>

      </form>
     </div>

     <!--<div class="col-md-6">

        <div class="form-group row">
          <div class="col-sm-12">
            <span class="label-resp-turn">
              @{{nom_cliente}}
            </span>
          </div>
        </div> 

        <div class="form-group row" style="margin-top: 80px;">
          <div class="col-sm-12">
            <span class="label-resp-turn">
              @{{nombre_conductor}}
            </span>
          </div>
        </div> 


        <div class="form-group row" style="margin-top: 20px;">
          <label for="idHora" class="col-sm-3 col-form-label">
            <strong>Hora Prevista</strong>
          </label>
          <div class="col-sm-9">
            <input type="time" class="form-control" id="idHora" value="" placeholder="Hora Prevista">
          </div>
        </div>       

     </div>-->

      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">BL</th>
              <th scope="col">Código</th>
              <th scope="col">Descripción</th>
              <th scope="col">Cantidad</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="pro in productos" >
              <th scope="row" style="color: red;">
                <input type="checkbox" v-model="pro.state" :value="pro.bl">
                @{{pro.bl}}
              </th>
              <td>
                @{{pro.cod_producto}}
              </td>
              <td>
                @{{pro.desc_producto}}
              </td>
              <td>
                <input type="number" min="1" class="form-control" v-model="pro.cantidad">
              </td>
            </tr>
          </tbody>
        </table>
      </div>

   </div>

   <div v-if="btn_reserva" style="text-align: center; margin-top: 10px;">
      <button type="button" v-on:click="generar_reserva" class="btn btn-default" style="background-color: #006AAF; color: white;">Reservar turno
      </button>
    </div>    

  </div>

</div>

@include('serviciosLogisticos.rastreoCarga.buscador')

<script src="{{asset('storage/assets/js/vue.js')}}"></script>
<!-- Axios To Run Api Services -->
<script src="{{asset('storage/assets/js/axios.js')}}"></script>
<script src="{{asset('storage/assets/js/reservation_vue.js?v=2.2')}}"></script>


@else

<div class="row">
  <div class="col-md-12">
    <div class="description">
      <h4>No tiene permisos para acceder a esta sección</h4>
    </div>
  </div>
</div>

@endif

@endsection

