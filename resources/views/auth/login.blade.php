@extends('layouts.app_login')

@section('content')
<link href="{{ asset('css/login.css') }}" rel="stylesheet">
<div class="container">
    <div class="row">
        <div class="col-md-12"></div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="card-new">
                <div class="card-header">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center" style="color: #fff;"><img src="{{ asset('img/login_fonts/ciamsa.png') }}"> Digital</p>
                        <p class="text-center" style="color: #fff; font-size: 1.3rem;">Bienvenido, por favor inicie sesion.</p></div>
                    </div>
                </div>
                

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            
                            <label for="email" class="col-sm-4 col-form-label" style="color: #fff; font-size: 1.2rem; max-width: 100%; flex: 0 0 100%;">{{ __('Correo electrónico') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus style="border-bottom: solid 1px white !important;">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label" style="color: #fff; font-size: 1.2rem; max-width: 100%; flex: 0 0 100%;">{{ __('Contraseña') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required style="border-bottom: solid 1px white !important;">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                    <label class="form-check-label" for="remember" style="color: #fff; font-size: 10px; margin-left: 0rem;">
                                        {{ __('Mantener sesión abierta') }}
                                    </label>
                                    <input class="form-check-input form-control" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            </div>
                            <div class="col-md-6">
                                <!--@if (Route::has('password.recover'))
                                <a class="btn btn-link text-right" href="{{ route('password.recover') }}" style="color: #fff; font-size: 10px;">
                                    {{ __('¿Olvidó su contraseña?') }}
                                </a>
                                @endif-->
                                <a class="btn btn-link text-right" href="/recover_password" style="color: #fff; font-size: 10px;">
                                    {{ __('¿Olvidó su contraseña?') }}
                                </a>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-primary" style="font-size: 1.5rem;">
                                    {{ __('Iniciar Sesion') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-12"></div>
    </div>
</div>
@endsection
