@extends('layouts.app')

@section('content')

<style type="text/css">
    .head {
        background-color: #006bb1;
        color: #fff;
    }

    .card-header {
        background-color: #006bb1;
        color: #fff;
    }

    .btn-primary-new {
        color: #fff !important;
        background-color: #006bb1 !important;
        border-color: #006bb1 !important;
    }

    .btn-primary-new:hover {
        color: #fff !important;
        background-color: #006bb1bf !important;
        border-color: #006bb1bf !important;
    }

</style>

@if (isset($error))
<div class="alert alert-danger">
  <ul>
    <li>{{ $error }}</li>
  </ul>
</div>
@endif

@if (isset($success))
<div class="alert alert-success">
  <ul>
    <li>{{ $success }}</li>
  </ul>
</div>
@endif


<div class="container">

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <h3 class="text-center">Creación completa de usuarios CIAMSA</h3>
              <br>
              <div style="width: 600px; margin: 0 auto;"><iframe width="100%" height="315" src="https://www.youtube.com/embed/opQB8eCIegk?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
          
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('Registro') }} 
                    <a data-toggle="modal" data-target="#myModal" style="color: #006bb1; cursor: pointer;">
                      <img src="{{asset('img/question_bar.png')}}" 
                         alt="icon-excel" 
                         height="30px"           
                         title="Vídeo de ayuda">
                    </a> 
                </div>

                <div class="card-body"><br>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">

                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="identificacion" class="col-md-4 col-form-label text-md-right">{{ __('Identificacion') }}</label>

                            <div class="col-md-6">
                                <input id="identificacion" type="number" class="form-control{{ $errors->has('identificacion') ? ' is-invalid' : '' }}" name="identificacion" required>

                                @if ($errors->has('identificacion'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('identificacion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tipo_cliente" class="col-md-4 col-form-label text-md-right">{{ __('Tipo de cliente') }}</label>

                            <div class="col-md-6">

                                <select class="form-control{{ $errors->has('tipo_cliente') ? ' is-invalid' : '' }}" id="tipo_cliente" name="tipo_cliente" required>
                                    <option value="CLIENTE" selected="">Cliente</option>
                                    <option value="AGE_ADU" >Agencia de Aduanas</option>
                                    <option value="USER_CIAMSA" >Usuario de Ciamsa</option>
                                    <option value="ADMIN_CIAMSA" >Administrador de Ciamsa</option>
                                </select>

                                @if ($errors->has('tipo_cliente'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tipo_cliente') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary-new">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
