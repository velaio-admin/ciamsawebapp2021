@extends('layouts.app')
@section('content')
<style type="text/css">
    .head {
        background-color: #006bb1;
        color: #fff;
    }

    .card-header {
        background-color: #006bb1;
        color: #fff;
    }

    .btn-primary-new {
        color: #fff !important;
        background-color: #006bb1 !important;
        border-color: #006bb1 !important;
    }

    .btn-primary-new:hover {
        color: #fff !important;
        background-color: #006bb1bf !important;
        border-color: #006bb1bf !important;
    }

</style>
@hasanyrole('Admin|Admin_ciamsa_1|Admin_ciamsa_2|Admin_ciamsa_fertilizantes|Usuario_ciamsa_2|Usuario_ciamsa_1|Usuario_ciamsa_fertilizantes')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Editar Usuario') }}</div>
                <div class="card-body"><br>
                    <form method="POST" action="{{ route('editUser.post') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="@if(!empty($user->name)) {{$user->name}} @elseif(!empty(old('name'))) {{ old('name') }} @endif" required autofocus>
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="identificacion" class="col-md-4 col-form-label text-md-right">{{ __('Identificacion') }}</label>

                            <div class="col-md-6">
                                <input id="identificacion" type="number" class="form-control{{ $errors->has('identificacion') ? ' is-invalid' : '' }}" name="identificacion" value="@if(!empty($user->identificacion)){{$user->identificacion}}@elseif(!empty(old('identificacion'))){{old('identificacion')}}@endif" required @hasanyrole('Guest|Agencia_de_aduanas|Cliente_ciamsa_1|Cliente_ciamsa_2|Cliente_ciamsa_fertilizantes') readonly="" @endrole>
                                @if ($errors->has('identificacion'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('identificacion') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="@if(!empty($user->email)) {{$user->email}} @elseif(!empty(old('email'))) {{ old('email') }} @endif" required>

                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->

                        <!-- <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div> -->

                        <div class="form-group row" onclick="selectFile()">
                            <label for="avatar" class="col-md-4 col-form-label text-md-right">{{ __('Avatar') }}</label>
                            <div class="col-md-6" onclick="selectFile()">
                                <img alt="Bootstrap Image Preview" src="{{asset($user->avatar)}} " class="rounded-circle" style="width: 80px; height: 80px;" onclick="selectFile()">
                                <input id="avatar" name="avatar" type="file" class="form-control{{ $errors->has('avatar') ? ' is-invalid' : '' }}" avatar="avatar" value="" style="display: none">
                                @if ($errors->has('avatar'))
                                <span class="invalid-feedback" role="alert" onclick="selectFile()">
                                    <strong>{{ $errors->first('avatar') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary-new">
                                    {{ __('Editar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="row">
  <div class="col-md-12">
    <div class="description">
      <h4>No tiene permisos para acceder a esta sección</h4>
    </div>
  </div>
</div>
@endrole
<script type="text/javascript">
    function selectFile(){
        $('#avatar').trigger('click');
    }
</script>  
@endsection
