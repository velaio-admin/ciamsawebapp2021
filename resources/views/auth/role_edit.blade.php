@extends('layouts.app')

@section('content')

<style type="text/css">
    .head {
        background-color: #006bb1;
        color: #fff;
    }

    .card-header {
        background-color: #006bb1;
        color: #fff;
    }

    .btn-primary-new {
        color: #fff !important;
        background-color: #006bb1 !important;
        border-color: #006bb1 !important;
    }

    .btn-primary-new:hover {
        color: #fff !important;
        background-color: #006bb1bf !important;
        border-color: #006bb1bf !important;
    }

    /*Swict*/

    /* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

@hasanyrole('Admin|Admin_ciamsa_1|Admin_ciamsa_2|Admin_ciamsa_fertilizantes')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Asigación de roles y permisos de Usuario') }}</div>

                <div class="card-body"><br>

                  <form action="{{route('editar_informacion')}}" method="POST" id="form_info" name="form_info">
                  @csrf
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                Avatar
                                            </th>
                                            <th>
                                                Nombre
                                            </th>
                                            <th>
                                                Identificación
                                            </th>
                                            <th>
                                                Email
                                            </th>
                                            <th>
                                              
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">

                                        <tr>
                                            <td>
                                                <img alt="Bootstrap Image Preview" src="{{asset($user->avatar)}} " class="rounded-circle" style="width: 80px; height: 80px;" onclick="selectFile()">
                                            </td>
                                            <td>
                                              <input  class="form-control" type="text" name="name" id="name" value="{{$user->name}}">
                                            </td>
                                            <td>
                                            <input  class="form-control" type="text" name="identificacion" id="identificacion" value="{{$user->identificacion}}">
                                            </td>
                                            <td>
                                                {{$user->email}}
                                            </td>
                                            <td>
                                              <a style="color: #2196f3;" href="/change_password/{{$user->id}}/execute">
                                                Cambiar Contraseña <i class="fa fa-edit" aria-hidden="true"></i>
                                              </a>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" form="form_info" value="Submit" style="border-radius: 100px; background-color: #2196f3; color: #fff;">Guardar</button>
                            </div>
                        </div>
                      <br>
                    </div>
                  </form>
                    <hr>

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <p><b>Roles</b></p>
                                <form action="{{route('asignar_roles')}}" method="POST" id="form_roles" name="form_roles">
                                    @csrf
                                    <div class="row text-center">
                                        <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                                        @foreach($roles as $rol)
                                            <div class="col-md-6">
                                                {{$rol->name}}
                                            </div>
                                            <div class="col-md-6">
                                                <!-- Rounded switch -->
                                                <label class="switch">
                                                  <input type="checkbox" id="rol_{{$rol->id}}" name="rol_{{$rol->id}}" value="{{$rol->name}}" @if(in_array($rol->id, $rel_roles)) checked @endif>
                                                  <span class="slider round"></span>
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" form="form_roles" value="Submit" style="border-radius: 100px; background-color: #2196f3; color: #fff;">Guardar Roles</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                    <br>
                    <hr>

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <p><b>Permisos</b></p>
                                <form action="{{route('asignar_permisos')}}" method="POST" id="form_permisos" name="form_permisos">
                                    @csrf
                                    <div class="row text-center">
                                    <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                                        @foreach($permisos as $permiso)
                                            <div class="col-md-6">
                                                {{$permiso->name}}
                                            </div>
                                            <div class="col-md-6">
                                                <!-- Rounded switch -->
                                                <label class="switch">
                                                  <input type="checkbox" id="permiso_{{$permiso->id}}" name="permiso_{{$permiso->id}}" value="{{$permiso->name}}" @if(in_array($permiso->id, $rel_permisos)) checked @endif>
                                                  <span class="slider round"></span>
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" form="form_permisos" value="Submit" style="border-radius: 100px; background-color: #2196f3; color: #fff;">Guardar Permisos</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@else
<div class="row">
  <div class="col-md-12">
    <div class="description">
      <h4>No tiene permisos para acceder a esta sección</h4>
    </div>
  </div>
</div>
@endrole