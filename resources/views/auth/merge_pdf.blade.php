@extends('layouts.app')

@section('content')

<style type="text/css">
    .head {
        background-color: #006bb1;
        color: #fff;
    }

    .card-header {
        background-color: #006bb1;
        color: #fff;
    }

    .btn-primary-new {
        color: #fff !important;
        background-color: #006bb1 !important;
        border-color: #006bb1 !important;
    }

    .btn-primary-new:hover {
        color: #fff !important;
        background-color: #006bb1bf !important;
        border-color: #006bb1bf !important;
    }

</style>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Unir PDFs') }}</div>

                <div class="card-body"><br>
                    <form method="POST" action="{{ route('merge_pdf_ctrl') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-primary-new" onclick="$('#list_pdfs').trigger('click')">{{ __('Seleccione los PDF para unir') }}</button>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">

                                <input id="list_pdfs" name="list_pdfs[]" type="file" class="form-control{{ $errors->has('list_pdfs') ? ' is-invalid' : '' }}" value="" style="display: none" accept="application/pdf" multiple="true" required="" onchange="selectFile()">

                                @if ($errors->has('list_pdfs'))
                                    <span class="invalid-feedback" role="alert" onclick="$('#list_pdfs').trigger('click')">
                                        <strong>{{ $errors->first('list_pdfs') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6" id="info_pdfs">
                                
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary-new">
                                    {{ __('UNIR') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function load() {
        $('input[type="file"]').change(function(){
            var files = document.getElementById("list_pdfs").value;
            var div = document.getElementById("info_pdfs");

            /*files.forEach(function(element) {
              console.log(element);
            });*/

            div.innerHTML = "PDFs cargados";
        });
    }
    
    window.onload = load;
</script>  
@endsection
