@extends('layouts.app')

@section('content')

<style type="text/css">
    .head {
        background-color: #006bb1;
        color: #fff;
    }

    .card-header {
        background-color: #006bb1;
        color: #fff;
    }

    .btn-primary-new {
        color: #fff !important;
        background-color: #006bb1 !important;
        border-color: #006bb1 !important;
    }

    .btn-primary-new:hover {
        color: #fff !important;
        background-color: #006bb1bf !important;
        border-color: #006bb1bf !important;
    }

</style>

<div class="container">

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <h3 class="text-center">Creación completa de usuarios CIAMSA</h3>
              <br>
              <div style="width: 600px; margin: 0 auto;"><iframe width="100%" height="315" src="https://www.youtube.com/embed/opQB8eCIegk?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
          
        </div>
    </div>
    
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('Asigación de roles y permisos de Usuario') }}
                    <a data-toggle="modal" data-target="#myModal" style="color: #006bb1; cursor: pointer;">
                      <img src="{{asset('img/question_bar.png')}}" 
                         alt="icon-excel" 
                         height="30px"           
                         title="Vídeo de ayuda">
                    </a> 
                </div>

                <div class="card-body"><br>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                Avatar
                                            </th>
                                            <th>
                                                Nombre
                                            </th>
                                            <th>
                                                Identificación
                                            </th>
                                            <th>
                                                Email
                                            </th>
                                            <th>
                                                Estado
                                            </th>
                                            <th>
                                                Acciones
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td onclick="window.location='/role/edit/{{$user->id}}';" style="cursor: pointer;">
                                                    <img alt="Bootstrap Image Preview" src="{{asset($user->avatar)}} " class="rounded-circle" style="width: 80px; height: 80px;" onclick="selectFile()">
                                                </td>
                                                <td onclick="window.location='/role/edit/{{$user->id}}';" style="cursor: pointer;">
                                                    {{$user->name}}
                                                </td>
                                                <td onclick="window.location='/role/edit/{{$user->id}}';" style="cursor: pointer;">
                                                    {{$user->identificacion}}
                                                </td>
                                                <td onclick="window.location='/role/edit/{{$user->id}}';" style="cursor: pointer;">
                                                    {{$user->email}}
                                                </td>
                                                <td>
                                                    @if($user->state == 1)
                                                        Activo
                                                    @else
                                                        Inactivo
                                                    @endif
                                                </td>
                                                <td>

                                                    <i class="far fa-edit" onclick="window.location='/role/edit/{{$user->id}}';" style="cursor: pointer;" alt="Editar"></i>

                                                    @if($user->state == 1)
                                                        <i class="far fa-thumbs-down" onclick="window.location='/role/inactive/{{$user->id}}';" style="cursor: pointer;" alt="Inactivar"></i>
                                                    @else
                                                        <i class="far fa-thumbs-up" onclick="window.location='/role/active/{{$user->id}}';" style="cursor: pointer;" alt="Activar"></i>
                                                    @endif

                                                    <i class="far fa-trash-alt" onclick="if(confirm('¿Desea eliminar este registro?')) window.location='/role/delete/{{$user->id}}';" style="cursor: pointer;" alt="Eliminar"></i>

                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
