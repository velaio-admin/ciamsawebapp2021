<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Permission list
        /*Permission::create(['name' => 'products.index']);
        Permission::create(['name' => 'products.edit']);
        Permission::create(['name' => 'products.show']);
        Permission::create(['name' => 'products.create']);
        Permission::create(['name' => 'products.destroy']);*/

        Permission::create(['name' => 'Servicios_logisticos']);//Seccion 1
        Permission::create(['name' => 'Documentos_de_carga']);
        Permission::create(['name' => 'Rastreo_de_carga']);
        Permission::create(['name' => 'Existencia_de_mercancia']);
        Permission::create(['name' => 'Despacho_de_mercancia']);
        Permission::create(['name' => 'Fecha_limite_devolucion']);

        Permission::create(['name' => 'Agricola']);//Seccion 2
        Permission::create(['name' => 'Cupo_credito']);
        Permission::create(['name' => 'Rastreo_de_pedido']);
        Permission::create(['name' => 'Facturas']);
        Permission::create(['name' => 'Solicitar_pedidos']);

        Permission::create(['name' => 'Comercializadora']);//Seccion 3
        Permission::create(['name' => 'Nominaciones']);
        Permission::create(['name' => 'Proceso_logistico']);
        Permission::create(['name' => 'Estado_de_contenedores']);

        Permission::create(['name' => 'Agencia_de_aduanas']);//Seccion 4

        //Roles list
        Role::create(['name' => 'Admin']);
        Role::create(['name' => 'Usuario_ciamsa_2']);
        Role::create(['name' => 'Admin_ciamsa_2']);
        Role::create(['name' => 'Cliente_ciamsa_2']);
        Role::create(['name' => 'Usuario_ciamsa_1']);
        Role::create(['name' => 'Admin_ciamsa_1']);
        Role::create(['name' => 'Cliente_ciamsa_1']);
        Role::create(['name' => 'Usuario_ciamsa_fertilizantes']);
        Role::create(['name' => 'Admin_ciamsa_fertilizantes']);
        Role::create(['name' => 'Cliente_ciamsa_fertilizantes']);
        Role::create(['name' => 'Agencia_de_aduanas']);
        Role::create(['name' => 'Guest']);
        /*//Admin
        $admin = Role::create(['name' => 'Admin']);

        $admin->givePermissionTo([
            'products.index',
            'products.edit',
            'products.show',
            'products.create',
            'products.destroy'
        ]);
        //$admin->givePermissionTo('products.index');
        //$admin->givePermissionTo(Permission::all());
       
        //Guest
        $guest = Role::create(['name' => 'Guest']);

        $guest->givePermissionTo([
            'products.index',
            'products.show'
        ]);

        //User Admin
        $user = User::find(1); //Italo Morales
        $user->assignRole('Admin');*/
    }
}
